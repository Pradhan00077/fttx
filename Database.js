import React from 'react';
import * as SQLite from "expo-sqlite";

const database_name = "SterliteOfflineApp.db";
const db = SQLite.openDatabase(database_name);

const Database = () => {
    db.transaction(tx => {
            
        // items Table
        tx.executeSql("create table if not exists workdoneActivity (id integer primary key not null, userid int(255) not null, lattitude varchar(100) not null, longitude varchar(100) not null, date varchar(100) not null);");
        
    });
    return db;
}
export default Database;