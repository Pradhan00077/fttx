import React, { Component } from "react";
import { NavigationActions } from "react-navigation";
import PropTypes from "prop-types";
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  AsyncStorage,
  TouchableOpacity
} from "react-native";
import { DrawerActions } from "react-navigation";
import { Container } from "native-base";
import { LinearGradient } from "expo-linear-gradient";
import Menus from "./Menus";

const screenHeight = Math.round(Dimensions.get("window").height);

class DrawerScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      userEmail: "",
      userRole: "",
      userProject: "",
      RJO: false,
      TFB: false,
      MahaNet: false,
      MP4: false,
      FTTX:false,
      ATL:false,
      active: null
    };
  }

  navigateToScreen = route => () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  _handleUsersDetails = async (page_link, page_name) => {
    this.props.navigation.navigate(page_link, { headerName: page_name });
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  };

  componentDidMount() {
    this._bootstrapAsync();
  }

  _bootstrapAsync = async () => {
    const userName = await AsyncStorage.getItem("name");
    const userEmail_id = await AsyncStorage.getItem("useremail");
    const user_role = await AsyncStorage.getItem("user_role");
    const user_projectID = await AsyncStorage.getItem("user_project_id");

    this.setState({
      userName: userName,
      userEmail: userEmail_id,
      userRole: user_role,
      userProject: user_projectID
    });
  };

  userLogout = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate("Auth");
  };

  async openDrowerMenuMethod(project_name, index) {
    this.setState({ active: index });
    if (project_name === "RJO") {
      this.setState({
        RJO: !this.state.RJO,
        TFB: false,
        MahaNet: false,
        MP4: false,
        ATL: false,
        FTTX:false
      });
    } else if (project_name === "TFB") {
      this.setState({
        TFB: !this.state.TFB,
        MahaNet: false,
        MP4: false,
        RJO: false,
        ATL: false,
        FTTX:false
      });
    } else if (project_name === "MahaNet") {
      this.setState({
        MahaNet: !this.state.MahaNet,
        RJO: false,
        TFB: false,
        MP4: false,
        ATL: false,
        FTTX:false
      });
    } else if (project_name === "MP4") {
      this.setState({
        MP4: !this.state.MP4,
        RJO: false,
        TFB: false,
        MahaNet: false,
        ATL: false,
        FTTX:false
      });
    }else if (project_name === "FTTX") {
      this.setState({
        MP4: false,
        RJO: false,
        TFB: false,
        MahaNet: false,
        ATL: false,
        FTTX: !this.state.FTTX
      });
    }else if (project_name === "ATL") {
      this.setState({
        MP4: false,
        RJO: false,
        TFB: false,
        MahaNet: false,
        FTTX: false,
        ATL: !this.state.ATL
      });
    }
  }
  render() {
    const MenuItems = [
      {
        name: "R-Jio",
        state: "RJO",
        color: "#1659c2"
      },
      {
        name: "MahaNet",
        state: "MahaNet",
        color: "#1976d3"
      },
      {
        name: "T-Fiber",
        state: "TFB",
        color: "#1d89e4"
      },
      {
        name: "Airtel",
        state: "ATL",
        color: "#2196f3"
      },
      {
        name: "FTTX",
        state: "FTTX",
        color: "#42a5f6"
      }
    ];
    return (
      <Container style={{ elevation: 15 }}>
        <LinearGradient
          colors={["#224fc6", "#3b5998", "#ffffff"]}
          start={{ x: 0, y: 0 }}
          end={{ x: 0, y: 1 }}
          style={styles.body}
        >
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}
          >
            <View
              style={{ flex: 1, flexDirection: "row", paddingHorizontal: 24 }}
            >
              <View style={{ flex: 1 }}>
                <View
                  style={{
                    borderRadius: 100,
                    borderWidth: 2,
                    borderColor: "#8fa6e2",
                    alignSelf: "center",
                    padding: 10
                  }}
                >
                  <Image
                    source={require("../assets/images/profile-1.png")}
                    style={{ alignSelf: "center" }}
                  />
                </View>
              </View>
            </View>

            <View
              style={{ flex: 1, flexDirection: "row", paddingHorizontal: 24 }}
            >
              <View style={{ flex: 1, paddingVertical: 15 }}>
                <Text
                  style={{
                    fontSize: 22,
                    fontWeight: "600",
                    color: "#fff",
                    textAlign: "center"
                  }}
                >
                  {this.state.userName}
                </Text>
                <Text
                  style={{
                    fontSize: 16,
                    textAlign: "center",
                    color: "#8fa6e2"
                  }}
                >
                  {this.state.userEmail}
                </Text>
              </View>
            </View>

            <View
              style={{ flex: 1, flexDirection: "row", paddingHorizontal: 24 }}
            >
              <View style={{ flex: 1, paddingVertical: 20 }}>
                <Image
                  source={require("../assets/images/logo.png")}
                  resizeMode="contain"
                  style={{ opacity: 0.1, alignSelf: "center", width: "80%" }}
                />
              </View>
            </View>
            <View style={{ borderWidth: 0 }}>
              {this.state.userRole === "adm" ? (
                <View>
                  {MenuItems.map((items, index) => {
                    return (
                      <TouchableOpacity
                        key={index}
                        style={[
                          styles.menuItem,
                          { backgroundColor: items.color }
                        ]}
                        onPress={() =>
                          this.openDrowerMenuMethod(items.state, index)
                        }
                      >
                        <View style={{ flexDirection: "row" }}>
                          <View style={{ flex: 0.95 }}>
                            <Text style={styles.menusDesign}>{items.name}</Text>
                          </View>
                          <View style={{ flex: 0.05 }}>
                            {this.state.active === index &&
                            (this.state.RJO ||
                              this.state.TFB ||
                              this.state.MahaNet ||
                              this.state.MP4 ||
                              this.state.FTTX ||
                              this.state.ATL ) ? (
                              <Image
                                source={require("../assets/images/down-arrow.png")}
                                style={{ tintColor: "#fff" }}
                                style={{ width: 16, height: 16 }}
                              />
                            ) : (
                              <Image
                                source={require("../assets/images/right-arrow.png")}
                                style={{ tintColor: "#fff" }}
                                style={{ width: 16, height: 16 }}
                              />
                            )}
                          </View>
                        </View>
                        {this.state.RJO && items.state === "RJO" ? (
                          <View>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "RJO_AllUsersList"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/all-users.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  All Users
                                </Text>
                              </View>
                            </TouchableOpacity>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={() =>
                                    this._handleUsersDetails(
                                      "RJO_EngineerReports",
                                      "Engineer Reports"
                                    )
                                  }
                                  style={styles.menusDesign}
                                >
                                  Engineer Reports
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={() =>
                                    this._handleUsersDetails(
                                      "RJO_AMReports",
                                      "AM Reports"
                                    )
                                  }
                                  style={styles.menusDesign}
                                >
                                  AM Reports
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={() =>
                                    this._handleUsersDetails(
                                      "RJO_WIPReports",
                                      "WIP Reports"
                                    )
                                  }
                                  style={styles.menusDesign}
                                >
                                  WIP Reports
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "RJO_EngineerForm"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Engineer Form
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "RJO_AreaManagerForm"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Area Manager Form
                                </Text>
                              </View>
                            </View>
                          </View>
                        ) : this.state.MahaNet && items.state === "MahaNet" ? (
                          <View>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "MNT_AllUsersList"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/all-users.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  All Users
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "MNT_EngineerForm"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                 Work Done Activity
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={() =>
                                    this._handleUsersDetails(
                                      "MNT_WorkReports",
                                      "Work Reports"
                                    )
                                  }
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Work Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "MNT_IssuesReportForm",
                                ''
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Issues Reported
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={() =>
                                    this._handleUsersDetails(
                                      "MNT_WIPReports",
                                      "WIP Reports"
                                    )
                                  }
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                 WIP Reports
                                </Text>
                              </View>
                            </TouchableOpacity>

                           
                          </View>
                        ) : this.state.TFB && items.state === "TFB" ? (
                          <View>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "TFB_AllUsersList"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/all-users.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  All Users
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-reports.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "TFBWorkReports"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Engineer Reports
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>

                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "TFBWipReports"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  WIP Reports
                                </Text>
                              </View>
                            </View>

                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>

                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "TFB_WorkDoneForm"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Engineer Form
                                </Text>
                              </View>
                            </View>
                            
                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>

                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "TFB_SurveyForm"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Survey Form
                                </Text>
                              </View>
                            </View>
                            
                            <View style={[styles.submenu]}>
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>

                              <View style={{ flex: 0.9 }}>
                                <Text
                                  onPress={this.navigateToScreen(
                                    "TFB_SurveyReports"
                                  )}
                                  style={styles.menusDesign}
                                >
                                  Survey Reports
                                </Text>
                              </View>
                            </View>
                          </View>
                        ) : this.state.MP4 && items.state === "MP4" ? (
                          <View></View>
                        ) : this.state.ATL && items.state === "ATL" ? (
                          <View>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "ATL_AllUsersList"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/all-users.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  All Users
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "ATL_EngineerReports"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Engineer Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "ATL_WIPReports"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Issue Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "ATL_EngineerForm"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Engineer Form
                                </Text>
                              </View>
                            </TouchableOpacity>                            
                          </View>
                        ): this.state.FTTX && items.state === "FTTX" ? (
                          <View>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "FTTX_AllUsersList"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/all-users.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  All Users
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "SurveyForm"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                  Survey form
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "Execution_OLT_TO_FDCScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                OLT TO FDC or Chamber Form
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "Execution_FDC_TO_FATScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                 FDC or Chamber TO FAT Form
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "Execution_FAT_TO_OTBScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/engineer-form.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                FAT TO OTB (SDU) Form
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "SurveyReports"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                 Survey Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "OLT_TO_FDC_ReportsScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                OLT TO FDC Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "FDC_or_Chamber_TO_FAT_ReportsScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                FDC TO FAT Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                              style={[styles.submenu]}
                              onPress={this.navigateToScreen(
                                "FAT_TO_OTB_ReportsScreen"
                              )}
                            >
                              <View style={{ flex: 0.1 }}>
                                <Image
                                  source={require("../assets/images/wip-reports-menu.png")}
                                  style={{ tintColor: "#fff" }}
                                />
                              </View>
                              <View style={{ flex: 0.9 }}>
                                <Text style={styles.menusDesign}>
                                FAT TO OTB Reports
                                </Text>
                              </View>
                            </TouchableOpacity>
                            
                          </View>
                        ): (
                          <View></View>
                        )}
                      </TouchableOpacity>
                    );
                  })}
                </View>
              ) : (
                <Menus
                  user_roll={this.state.userRole}
                  user_project={this.state.userProject}
                  navigation={this.props.navigation}
                />
              )}
              <View
                style={[
                  styles.menuItem,
                  { backgroundColor: "#81bfee", flexDirection: "row" }
                ]}
              >
                <View style={{ flex: 0.1 }}>
                  <Image
                    source={require("../assets/images/logout.png")}
                    style={{ tintColor: "#fff" }}
                  />
                </View>

                <View style={{ flex: 0.9 }}>
                  <Text onPress={this.userLogout} style={styles.menusDesign}>
                    Logout
                  </Text>
                </View>
              </View>
            </View>
          </ScrollView>
        </LinearGradient>
      </Container>
    );
  }
}

DrawerScreen.propTypes = {
  navigation: PropTypes.object
};

const styles = StyleSheet.create({
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    paddingTop: screenHeight - (screenHeight - screenHeight / 14)
  },
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  heading: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  menuItem: {
    flex: 1,
    paddingHorizontal: 15,
    paddingVertical: 12.1
  },
  submenu: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 12.1
  },
  menusDesign: {
    color: "#ffffff",
    fontSize: 16
  }
});

export default DrawerScreen;
