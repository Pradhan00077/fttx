import React from 'react';
import { Platform, View, Image, Dimensions, AsyncStorage } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

// IMPORT SCREENS AS FOLLOWING
import DrawerScreen from '../DrawerScreen';
import AuthLoadingScreen from '../AuthLoadingScreen';

import LoginScreen from '../../screens/LoginScreen';
import ForgetPassword from '../../screens/ForgetPasswordScreen';
import ProfileScreen from '../../screens/ProfileScreen';
import AllUsersListScreen from '../../screens/AllUsersList';
import UserDetailsScreen from '../../screens/UserDetailsScreen';
import AddUsersScreen from '../../screens/AddUsersScreen';
import EditUsersScreen from '../../screens/EditUsersScreen';
import EditTeamUsersScreen from '../../screens/EditTeamUsersScreen';
import TeamUserDetailsScreen from '../../screens/TeamUserDetailsScreen';

import DashboardScreen from '../../screens/DashboardScreen';

import TFBWorkDoneFormScreen from '../../screens/T-Fiber/WorkDoneFormScreen';
import TFBWipReportsScreen from '../../screens/T-Fiber/WipReportsScreen';
import TFBWipReportsDetailsScreen from '../../screens/T-Fiber/WipReportsDetailsScreen';
import TFBWorkReportsScreen from '../../screens/T-Fiber/WorkReportsScreen';
import TFBWorkReportsDetailsScreen from '../../screens/T-Fiber/WorkReportDetailsScreen';
import TFBLocationMapScreen from '../../screens/T-Fiber/LocationMap';
import TFBWorkDoneEditFormScreen from '../../screens/T-Fiber/WorkDoneEditFormScreen';
import TFBAllUsersListScreen from '../../screens/T-Fiber/AllUsersList';
import TFBMyTeamScreen from '../../screens/T-Fiber/MyTeam';

import RjioAllUsersListScreen from '../../screens/Rjio/AllUsersList';
import RjioAMReportsScreen from '../../screens/Rjio/AMReports';
import RjioAreaManagerFormScreen from '../../screens/Rjio/AreaManagerForm';
import RjioEngineerFormScreen from '../../screens/Rjio/EngineerForm';
import RjioEngineerReportsScreen from '../../screens/Rjio/EngineerReports';
import RjioWorkReportsDetailsScreen from '../../screens/Rjio/EngineerReportDetailsScreen';
import RjioAmReportsDetailsScreen from '../../screens/Rjio/AmReportDetailsScreen';
import RjioWIPReportsDetailsScreen from '../../screens/Rjio/WIPReportDetailsScreen';
import RjioWIPReportsScreen from '../../screens/Rjio/WIPReports';
import RjioMyTeamScreen from '../../screens/Rjio/MyTeam';
import RjioWorkSummary from '../../screens/Rjio/WorkSummary';

import MNTEngineerFormScreen from '../../screens/Mahanet/EngineerForm';
import MNTAllUsersListScreen from '../../screens/Mahanet/AllUsersList';
import MNTWorkReportsScreen from '../../screens/Mahanet/WorkReports';
import MNTWorkReportsDetailScreen from '../../screens/Mahanet/WorkReportDetailsScreen';
import MNTWIPReportsDetailsScreen from '../../screens/Mahanet/WIPReportDetailsScreen';
import MNTWIPReportsScreen from '../../screens/Mahanet/WIPReports';
import MNTIssuesReportForm from '../../screens/Mahanet/IssuesReportForm';

import MPEngineerFormScreen from '../../screens/MP4/EngineerForm';
import MPAllUsersListScreen from '../../screens/MP4/AllUsersList';

const { width, height } = Dimensions.get('screen');
const config = Platform.select({
  web: { headerMode: 'screen' },
  default: {},
});

const DashboardStack = createStackNavigator(
  {
    Dashboard: DashboardScreen,

    RJO_AllUsersList: RjioAllUsersListScreen,
    RJO_AMReports: RjioAMReportsScreen,
    RJO_AreaManagerForm: RjioAreaManagerFormScreen,
    RJO_EngineerReports: RjioEngineerReportsScreen,
    RJO_EngineerForm: RjioEngineerFormScreen,
    RJO_WIPReports: RjioWIPReportsScreen,
    RJO_EngineerReportDetails:RjioWorkReportsDetailsScreen,
    RJO_AmReportDetails:RjioAmReportsDetailsScreen,
    RJO_WIPReportDetails:RjioWIPReportsDetailsScreen,
    RJO_MyTeam: RjioMyTeamScreen,
    RJO_WorkSummary: RjioWorkSummary,


    // MNT_EngineerForm: MNTEngineerFormScreen,
    MNT_AllUsersList: MNTAllUsersListScreen,
    // MNT_WorkReports:MNTWorkReportsScreen,
    // MNT_WorkReportsDetail:MNTWorkReportsDetailScreen,
    // MNT_WIPReports: MNTWIPReportsScreen,
    // MNT_WIPReportDetails:MNTWIPReportsDetailsScreen,
    MNT_IssuesReportForm:MNTIssuesReportForm,

    MP_EngineerForm: MPEngineerFormScreen,
    MP_AllUsersList: MPAllUsersListScreen,

    TFBWorkReports: TFBWorkReportsScreen,
    TFBWorkReportsDetails: TFBWorkReportsDetailsScreen,
    TFB_WorkDoneEditForm: TFBWorkDoneEditFormScreen,
    TFBWipReports: TFBWipReportsScreen,
    TFBWipReportsDetails: TFBWipReportsDetailsScreen,
    TFB_WorkDoneForm: TFBWorkDoneFormScreen,
    TFB_LocationMap: TFBLocationMapScreen,
    TFB_AllUsersList: TFBAllUsersListScreen,
    TFB_MyTeam: TFBMyTeamScreen,

    AllUsersList: AllUsersListScreen,
    UserDetails: UserDetailsScreen,
    AddUsers: AddUsersScreen,
    EditUsers: EditUsersScreen,

    TeamUserDetails: TeamUserDetailsScreen,
    EditTeamUsers: EditTeamUsersScreen,
  },
  config
);

DashboardStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  const openedScreen = navigation.state.routes;

  // CHECKING ALL ROUTERS TO USE CONDITION FOR TAB BAR VISIBLEITY ACCORDING TO SCREENS
  openedScreen.map(route => {
    // IF ROUTE EXIST THEN TAB BAR NOT VISIBLE
    if (route.routeName === "Login" ) {
      tabBarVisible = false;
    } else { // ELSE TAB BAR VISIBLE
      tabBarVisible = true;
    }
  });

  return {
    tabBarVisible,
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused }) => (
      <Image source = {require('../assets/images/home.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28 }: { tintColor: "gray", width: 28, height: 28 }} />
    ),
    tabBarOptions: {
      activeTintColor: '#13c16a',
      inactiveTintColor: '#a0a0a2',
      style: {
        borderTopWidth: 2,
        borderTopColor: "#13c16a",
        backgroundColor: '#ffffff',
        paddingTop: 5,
        height: 60
      },
    },
  }
};
DashboardStack.path = '';


const ActivityStack = createStackNavigator(
  {
    MNT_EngineerForm: MNTEngineerFormScreen,
  },
  config
);
ActivityStack.navigationOptions = {
  tabBarLabel: 'Activity',
  tabBarIcon: ({ focused }) => (
    <Image source = {require('../assets/images/activity.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28}: { tintColor: "gray", width: 28, height: 28 }} />
  ),
  tabBarOptions: {
    activeTintColor: '#13c16a',
    inactiveTintColor: '#a0a0a2',
    style: {
      borderTopWidth: 2,
      borderTopColor: "#13c16a",
      backgroundColor: '#ffffff',
      paddingTop: 5,
      height: 60
    },
  },
};
ActivityStack.path = '';


const WipReportsStack = createStackNavigator(
  {
    MNT_WIPReports: MNTWIPReportsScreen,
    MNT_WIPReportDetails:MNTWIPReportsDetailsScreen,
  },
  config
);
WipReportsStack.navigationOptions = {
  tabBarLabel: 'WIP Reports',
  tabBarIcon: ({ focused }) => (
    <Image source = {require('../assets/images/wip-reports.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28}: { tintColor: "gray", width: 28, height: 28 }} />
  ),
  tabBarOptions: {
    activeTintColor: '#13c16a',
    inactiveTintColor: '#a0a0a2',
    style: {
      borderTopWidth: 2,
      borderTopColor: "#13c16a",
      backgroundColor: '#ffffff',
      paddingTop: 5,
      height: 60
    },
  },
};
WipReportsStack.path = '';


const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
  },
  config
);
ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <Image source = {require('../assets/images/profileTab.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28}: { tintColor: "gray", width: 28, height: 28 }} />
  ),
  tabBarOptions: {
    activeTintColor: '#13c16a',
    inactiveTintColor: '#a0a0a2',
    style: {
      borderTopWidth: 2,
      borderTopColor: "#13c16a",
      backgroundColor: '#ffffff',
      paddingTop: 5,
      height: 60
    },
  },
};
ProfileStack.path = '';

const WorkReportsStack = createStackNavigator(
  {
    MNT_WorkReports:MNTWorkReportsScreen,
    MNT_WorkReportsDetail:MNTWorkReportsDetailScreen,
  },
  config
);
WorkReportsStack.navigationOptions = {
  tabBarLabel: 'Work Reports',
  tabBarIcon: ({ focused }) => (
    <Image source = {require('../assets/images/work-reports.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28}: { tintColor: "gray", width: 28, height: 28 }} />
  ),
  tabBarOptions: {
    activeTintColor: '#13c16a',
    inactiveTintColor: '#a0a0a2',
    style: {
      borderTopWidth: 2,
      borderTopColor: "#13c16a",
      backgroundColor: '#ffffff',
      paddingTop: 5,
      height: 60
    },
  },
};
WorkReportsStack.path = '';

const AuthStack = createStackNavigator({
  // Landing: {
  //   screen: Example,
  //   navigationOptions: {
  //     headerTitle: 'Landing',
  //   },
  // },
  LogIn: {
    screen: LoginScreen,
    navigationOptions: {
      headerTitle: 'Log In',
    },
  },
  ForgetPassword: {
    screen: ForgetPassword,
    navigationOptions: {
      headerTitle: 'Forget Password',
    },
  },

});

// Using to display Bottom Tab(Footer Tab) navigation
const tabNavigator = createBottomTabNavigator(
  {
    Dashboard: { screen: DashboardStack },
    Activity: { screen: ActivityStack },
    WorkReports: { screen: WorkReportsStack },
    WipReports: { screen: WipReportsStack },
    Profile: { screen: ProfileStack },
  }
);

// Using to display Drawer(Side bar Menus) navigation
const sideTabNavigator = createDrawerNavigator({
    Home: tabNavigator,
  },{
    initialRouteName: 'Home',
    contentComponent: DrawerScreen,
    drawerWidth: Math.min(height, width) * 0.8,
  }
);

// Several options get passed to the underlying router to modify navigation logic
const App = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: {
    screen: AuthStack,
  },
  App: {
    screen: sideTabNavigator,
  },
});


tabNavigator.path = '';

export default createAppContainer(App);
