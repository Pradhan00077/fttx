import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions } from 'react-native';
import { Container } from 'native-base';
import { LinearGradient } from 'expo-linear-gradient';
import Drawer from 'react-native-drawer'

const screenHeight = Math.round(Dimensions.get('window').height);

class NavigationDrawer extends Component {

    constructor(props) {
      super(props);
      this.state = {
          loginSuccess: "Nothing"
      };
    }

    _handleButtonPress = () => {
      this.setState({ loginSuccess: "Success Login!" });
      this.props.navigation.navigate('Login');
    }

    closeControlPanel = () => {
        this._drawer.close()
    };
    openControlPanel = () => {
        this._drawer.open()
    };

    render() {
  
        const drawerStyles = {
            drawer: { shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3},
            main: {paddingLeft: 3},
        }

        return (
            <Drawer
                type="overlay"
                content={<ControlPanel />}
                tapToClose={true}
                openDrawerOffset={0.2} // 20% gap on the right side of drawer
                panCloseMask={0.2}
                closedDrawerOffset={-3}
                styles={drawerStyles}
                tweenHandler={(ratio) => ({
                    main: { opacity:(2-ratio)/2 }
                })}
            >
                <ScrollView  contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>

                    <View style={styles.body}>

                        <TouchableOpacity activeOpacity={1} style={styles.sectionContainer} onPress={this._handleButtonPress} >
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={styles.sectionIcon}>
                                    <Image source = {require('../assets/images/home.png')} resizeMode='contain' 
                                style={{ alignSelf: "auto" }} />
                                </View>
                                <Text style={styles.sectionTitle}>
                                    adsfsad
                                </Text>
                            </View>
                        </TouchableOpacity>
                        
                    </View>

                </ScrollView>
            </Drawer>
        )
    }
  
}

const styles = StyleSheet.create({
    engine: {
      position: 'absolute',
      right: 0,
    },
    body: {
      backgroundColor: '#eeeeee',
      height: (screenHeight-70),
      paddingBottom: 20,
    },
    sectionContainer: {
      marginTop: 32,
      marginLeft: 26,
      marginRight: 26,
      paddingHorizontal: 18,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "row",
      elevation: 4,
      borderRadius: 2,
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitle: {
      flex: 0.6,
      fontSize: 26,
      fontWeight: '600',
      color: "#1949c7",
      alignSelf:"center"
    },
    
  
  });
  
export default NavigationDrawer;
