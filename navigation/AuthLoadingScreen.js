import React from 'react';
import { ActivityIndicator, AsyncStorage, StyleSheet, View, Text } from 'react-native';
import {BASE_URL} from '../basepath';

export default class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userId = await AsyncStorage.getItem('user_id');
    let response = await fetch(BASE_URL + "apis/usersCheckAuth"+"/"+userId);
    try {
      responseJson = await response.json();
      
      //Successful response from the API Call
      if(responseJson.logIn){
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate('App');
      }else{
        await AsyncStorage.clear();
        this.props.navigation.navigate("Auth");
      }
    }catch (error) {
      console.log("Response error - ",error);
    }
  };



  // Render any loading content that you like here
  render() {
    return (
        <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
            <ActivityIndicator size="large" color="#2157c4"/>
        </View>
    );
  }
}

const styles = StyleSheet.create({
    ActivityIndicatorContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    ActivityIndicatorHorizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around'
      // padding: 10,
    }
});