import React from 'react';
import { Platform, View, Image, Dimensions } from 'react-native';
import { createStackNavigator, createBottomTabNavigator, createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';

// IMPORT SCREENS AS FOLLOWING
import DrawerScreen from './DrawerScreen';
import AuthLoadingScreen from './AuthLoadingScreen';

import LoginScreen from '../screens/LoginScreen';
import ForgetPassword from '../screens/ForgetPasswordScreen';
import ProfileScreen from '../screens/ProfileScreen';
import AllUsersListScreen from '../screens/AllUsersList';
import UserDetailsScreen from '../screens/UserDetailsScreen';
import AddUsersScreen from '../screens/AddUsersScreen';
import EditUsersScreen from '../screens/EditUsersScreen';
import EditTeamUsersScreen from '../screens/EditTeamUsersScreen';
import TeamUserDetailsScreen from '../screens/TeamUserDetailsScreen';

import DashboardScreen from '../screens/DashboardScreen';

import TFBWorkDoneFormScreen from '../screens/T-Fiber/WorkDoneFormScreen';
import TFBWipReportsScreen from '../screens/T-Fiber/WipReportsScreen';
import TFBWipReportsDetailsScreen from '../screens/T-Fiber/WipReportsDetailsScreen';
import TFBWorkReportsScreen from '../screens/T-Fiber/WorkReportsScreen';
import TFBWorkReportsDetailsScreen from '../screens/T-Fiber/WorkReportDetailsScreen';
import TFBLocationMapScreen from '../screens/T-Fiber/LocationMap';
import TFBWorkDoneEditFormScreen from '../screens/T-Fiber/WorkDoneEditFormScreen';
import TFBAllUsersListScreen from '../screens/T-Fiber/AllUsersList';
import TFBMyTeamScreen from '../screens/T-Fiber/MyTeam';
import TFBSurveyFormScreen from '../screens/T-Fiber/SurveyFormScreen';
import TFBSurveyReportsScreen from '../screens/T-Fiber/SurveyReportsScreen';
import TFBSurveyReportsDetailsScreen from '../screens/T-Fiber/SurveyReportsDetailsScreen';
import TFBMasterDataListScreen from '../screens/T-Fiber/MasterDataListScreen';
import TFBMasterDataDetailsScreen from '../screens/T-Fiber/MasterDataDetailsScreen';
import TFBMasterDataFormScreen from '../screens/T-Fiber/MasterDataFormScreen';
import TFBMasterDataEditFormScreen from '../screens/T-Fiber/MasterDataEditFormScreen';

import RjioAllUsersListScreen from '../screens/Rjio/AllUsersList';
import RjioAMReportsScreen from '../screens/Rjio/AMReports';
import RjioAreaManagerFormScreen from '../screens/Rjio/AreaManagerForm';
import RjioEngineerFormScreen from '../screens/Rjio/EngineerForm';
import RjioEngineerReportsScreen from '../screens/Rjio/EngineerReports';
import RjioWorkReportsDetailsScreen from '../screens/Rjio/EngineerReportDetailsScreen';
import RjioAmReportsDetailsScreen from '../screens/Rjio/AmReportDetailsScreen';
import RjioWIPReportsDetailsScreen from '../screens/Rjio/WIPReportDetailsScreen';
import RjioWIPReportsScreen from '../screens/Rjio/WIPReports';
import RjioMyTeamScreen from '../screens/Rjio/MyTeam';
import RjioWorkSummary from '../screens/Rjio/WorkSummary';

import MNTEngineerFormScreen from '../screens/Mahanet/EngineerForm';
import MNTAllUsersListScreen from '../screens/Mahanet/AllUsersList';
import MNTWorkReportsScreen from '../screens/Mahanet/WorkReports';
import MNTWorkReportsDetailScreen from '../screens/Mahanet/WorkReportDetailsScreen';
import MNTWIPReportsDetailsScreen from '../screens/Mahanet/WIPReportDetailsScreen';
import MNTWIPReportsScreen from '../screens/Mahanet/WIPReports';
import MNTIssuesReportForm from '../screens/Mahanet/IssuesReportForm';

import MPEngineerFormScreen from '../screens/MP4/EngineerForm';
import MPAllUsersListScreen from '../screens/MP4/AllUsersList';

import ATLAllUsersList from '../screens/Airtel/AllUsersList';
import ATLEngineerForm from '../screens/Airtel/EngineerForm';
import ATLEngineerReportDetails from '../screens/Airtel/EngineerReportDetailsScreen';
import ATLEngineerReports from '../screens/Airtel/EngineerReports';
import ATLIssueReport from '../screens/Airtel/IssueReport';
import ATLocationTracking from '../screens/Airtel/LocationTracking';
import ATMyTeam from '../screens/Airtel/MyTeam';
import ATLWIPReportDetails from '../screens/Airtel/WIPReportDetailsScreen';
import ATLWIPReports from '../screens/Airtel/WIPReports';
import ATLMasterDataListScreen from '../screens/Airtel/MasterDataListScreen';
import ATLMasterDataDetailsScreen from '../screens/Airtel/MasterDataDetailsScreen';
import ATLMasterDataFormScreen from '../screens/Airtel/MasterDataFormScreen';
import ATLMasterDataEditFormScreen from '../screens/Airtel/MasterDataEditFormScreen';

import FTTXDashboardScreen from '../screens/FTTX/Dashboard';
import FTTXExecutionDashboardScreen from '../screens/FTTX/DashboardFttx';
import SurveyFormScreen from '../screens/FTTX/Forms/SurveyForm';
import ExecutionFormScreen from '../screens/FTTX/Forms/ExecutionForm';
import ExecutionFormitemScreen from '../screens/FTTX/Forms/ExecutionFormItem';

import Execution_OLT_TO_FDC from '../screens/FTTX/Forms/OLT_TO_FDCExecutionForm';
import Execution_FDC_TO_FAT from '../screens/FTTX/Forms/FDC_TO_FATExecutionForm';
import Execution_FAT_TO_OTB from '../screens/FTTX/Forms/FAT_TO_OTBExecutionForm';
import FAT_TO_OTB_ReportsScreen from '../screens/FTTX/Listing/FAT_TO_OTB_Reports';
import FAT_TO_OTB_ReportsDetailScreen from '../screens/FTTX/Listing/FAT_TO_OTBDetailsScreen';
import FDC_or_Chamber_TO_FAT_ReportsScreen from '../screens/FTTX/Listing/FDC_or_Chamber_TO_FAT_Reports';
import FDC_or_Chamber_TO_FAT_ReportsDetailScreen from '../screens/FTTX/Listing/FDC_or_Chamber_TO_FAT_ReportsDetailsScreen';
import OLT_TO_FDC_ReportsScreen from '../screens/FTTX/Listing/OLT_TO_FDC_Reports';
import OLT_TO_FDC_ReportsDetailScreen from '../screens/FTTX/Listing/OLT_TO_FDC_ReportsDetailsScreen';

import SurveyReportsScreen from '../screens/FTTX/Listing/SurveyReports';
import SurveyReportsDetailScreen from '../screens/FTTX/Listing/SurveyReportDetailsScreen';
import ExecutionReportsScreen from '../screens/FTTX/Listing/ExecutionReports'
import ExecutionReportsDetailScreen from '../screens/FTTX/Listing/ExecutionReportDetailsScreen';
import FTTXAllUsersListScreen from '../screens/FTTX/User/AllUsersList';
import FTTXMyTeamScreen from '../screens/FTTX/User/MyTeam';

const { width, height } = Dimensions.get('screen');

const DashboardStack = createStackNavigator(
  {
    Dashboard: DashboardScreen,
    
    RJO_AllUsersList: RjioAllUsersListScreen,
    RJO_AMReports: RjioAMReportsScreen,
    RJO_AreaManagerForm: RjioAreaManagerFormScreen,
    RJO_EngineerReports: RjioEngineerReportsScreen,
    RJO_EngineerForm: RjioEngineerFormScreen,
    RJO_WIPReports: RjioWIPReportsScreen,
    RJO_EngineerReportDetails:RjioWorkReportsDetailsScreen,
    RJO_AmReportDetails:RjioAmReportsDetailsScreen,
    RJO_WIPReportDetails:RjioWIPReportsDetailsScreen,
    RJO_MyTeam: RjioMyTeamScreen,
    RJO_WorkSummary: RjioWorkSummary,

    MNT_EngineerForm: MNTEngineerFormScreen,
    MNT_AllUsersList: MNTAllUsersListScreen,
    MNT_WorkReports:MNTWorkReportsScreen,
    MNT_WorkReportsDetail:MNTWorkReportsDetailScreen,
    MNT_WIPReports: MNTWIPReportsScreen,
    MNT_WIPReportDetails:MNTWIPReportsDetailsScreen,
    MNT_IssuesReportForm:MNTIssuesReportForm,

    MP_EngineerForm: MPEngineerFormScreen,
    MP_AllUsersList: MPAllUsersListScreen,

    TFB_WorkDoneForm: TFBWorkDoneFormScreen,
    TFBWipReports: TFBWipReportsScreen,
    TFBWipReportsDetails: TFBWipReportsDetailsScreen,
    TFBWorkReports: TFBWorkReportsScreen,
    TFBWorkReportsDetails: TFBWorkReportsDetailsScreen,
    TFB_WorkDoneEditForm: TFBWorkDoneEditFormScreen,
    TFB_LocationMap: TFBLocationMapScreen,
    TFB_AllUsersList: TFBAllUsersListScreen,
    TFB_MyTeam: TFBMyTeamScreen,
    TFB_SurveyForm: TFBSurveyFormScreen,
    TFB_SurveyReports: TFBSurveyReportsScreen,
    TFB_SurveyReportsDetails: TFBSurveyReportsDetailsScreen,
    TFB_MasterDataList: TFBMasterDataListScreen,
    TFB_MasterDataDetails: TFBMasterDataDetailsScreen,
    TFB_MasterDataForm: TFBMasterDataFormScreen,
    TFB_MasterDataEditForm: TFBMasterDataEditFormScreen,

    AllUsersList: AllUsersListScreen,
    UserDetails: UserDetailsScreen,
    AddUsers: AddUsersScreen,
    EditUsers: EditUsersScreen,

    TeamUserDetails: TeamUserDetailsScreen,
    EditTeamUsers: EditTeamUsersScreen,

    ATL_AllUsersList: ATLAllUsersList,
    ATL_EngineerForm: ATLEngineerForm,
    ATL_EngineerReportDetails: ATLEngineerReportDetails,
    ATL_EngineerReports: ATLEngineerReports,
    ATL_IssueReport: ATLIssueReport,
    ATL_LocationTracking: ATLocationTracking,
    ATL_MyTeam: ATMyTeam,
    ATL_WIPReportDetails: ATLWIPReportDetails,
    ATL_WIPReports: ATLWIPReports,
    ATL_MasterDataList: ATLMasterDataListScreen,
    ATL_MasterDataDetails: ATLMasterDataDetailsScreen,
    ATL_MasterDataForm: ATLMasterDataFormScreen,
    ATL_MasterDataEditForm: ATLMasterDataEditFormScreen,

    FTTXDashboardScreen:FTTXDashboardScreen,
    FTTXExecutionDashboardScreen:FTTXExecutionDashboardScreen,
    ExecutionForm:ExecutionFormScreen,
    ExecutionFormitem:ExecutionFormitemScreen,
    SurveyForm:SurveyFormScreen,
    SurveyReports:SurveyReportsScreen,
    SurveyReportsDetail:SurveyReportsDetailScreen,
    Execution_OLT_TO_FDCScreen:Execution_OLT_TO_FDC,
    Execution_FDC_TO_FATScreen:Execution_FDC_TO_FAT,
    Execution_FAT_TO_OTBScreen:Execution_FAT_TO_OTB,
    FAT_TO_OTB_ReportsScreen:FAT_TO_OTB_ReportsScreen,
    FAT_TO_OTB_ReportsDetailScreen:FAT_TO_OTB_ReportsDetailScreen,
    FDC_or_Chamber_TO_FAT_ReportsScreen:FDC_or_Chamber_TO_FAT_ReportsScreen,
    FDC_or_Chamber_TO_FAT_ReportsDetailScreen:FDC_or_Chamber_TO_FAT_ReportsDetailScreen,
    OLT_TO_FDC_ReportsScreen:OLT_TO_FDC_ReportsScreen,
    OLT_TO_FDC_ReportsDetailScreen:OLT_TO_FDC_ReportsDetailScreen,

    ExecutionReports:ExecutionReportsScreen,
    ExecutionReportsDetail:ExecutionReportsDetailScreen,
    FTTX_AllUsersList: FTTXAllUsersListScreen,
    FTTX_MyTeam: FTTXMyTeamScreen

  }
);

DashboardStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
  const openedScreen = navigation.state.routes;

  // CHECKING ALL ROUTERS TO USE CONDITION FOR TAB BAR VISIBLEITY ACCORDING TO SCREENS
  openedScreen.map(route => {
    // IF ROUTE EXIST THEN TAB BAR NOT VISIBLE
    if (route.routeName === "Login" ) {
      tabBarVisible = false;
    } else { // ELSE TAB BAR VISIBLE
      tabBarVisible = true;
    }
  });

  return {
    tabBarVisible,
    tabBarLabel: 'Home',
    tabBarIcon: ({ focused }) => (
      <Image source = {require('../assets/images/home.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28 }: { tintColor: "gray", width: 28, height: 28 }} />
    ),
    tabBarOptions: {
      activeTintColor: '#13c16a',
      inactiveTintColor: '#a0a0a2',
      style: {
        borderTopWidth: 2,
        borderTopColor: "#13c16a",
        backgroundColor: '#ffffff',
        paddingTop: 5,
        height: 60
      },
    },
  }
};
DashboardStack.path = '';

const ProfileStack = createStackNavigator(
  {
    Profile: ProfileScreen,
  }
);
ProfileStack.navigationOptions = {
  tabBarLabel: 'Profile',
  tabBarIcon: ({ focused }) => (
    <Image source = {require('../assets/images/profileTab.png')} resizeMode='contain'  style={(focused)? { alignSelf: "auto", tintColor: "#13c16a", width: 28, height: 28}: { tintColor: "gray", width: 28, height: 28 }} />
  ),
  tabBarOptions: {
    activeTintColor: '#13c16a',
    inactiveTintColor: '#a0a0a2',
    style: {
      borderTopWidth: 2,
      borderTopColor: "#13c16a",
      backgroundColor: '#ffffff',
      paddingTop: 5,
      height: 60
    },
  },
};
ProfileStack.path = '';


const AuthStack = createStackNavigator({
  // Landing: {
  //   screen: Example,
  //   navigationOptions: {
  //     headerTitle: 'Landing',
  //   },
  // },
  LogIn: {
    screen: LoginScreen,
    navigationOptions: {
      headerTitle: 'Log In',
    },
  },
  ForgetPassword: {
    screen: ForgetPassword,
    navigationOptions: {
      headerTitle: 'Forget Password',
    },
  },

});

// Using to display Bottom Tab(Footer Tab) navigation
const tabNavigator = createBottomTabNavigator({
    DashboardStack,
    ProfileStack
}
);

// Using to display Drawer(Side bar Menus) navigation
const sideTabNavigator = createDrawerNavigator({
    Home: tabNavigator,
  },{
    initialRouteName: 'Home',
    contentComponent: DrawerScreen,
    drawerWidth: Math.min(height, width) * 0.8,
  }
);

// Several options get passed to the underlying router to modify navigation logic
const App = createSwitchNavigator({
  AuthLoading: AuthLoadingScreen,
  Auth: {
    screen: AuthStack,
  },
  App: {
    screen: sideTabNavigator,
  },
});


tabNavigator.path = '';

export default createAppContainer(App);
