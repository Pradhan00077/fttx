import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";
import { DrawerActions } from "react-navigation";

class Menus extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_roll: props.user_roll,
      user_project: props.user_project,
    };
  }

  _handleUsersDetails=async(page_link, page_name)=>{
    this.props.navigation.navigate(page_link, {'headerName': page_name});
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
  }

  render() {
    const MenuItemsRJO = {
      asm: [
        { name: "My Team", link: "RJO_MyTeam", color: "#55aef7", imgsource: require("../assets/images/all-users.png") },
        { name: "Engineer Form", link: "RJO_EngineerForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Area Manager Form", link: "RJO_AreaManagerForm", color: "#2497f5", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Reports", link: "RJO_AMReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "WIP Reports", link: "RJO_WIPReports", color: "#0a7ddb", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Reports", link: "RJO_EngineerReports", color: "#096fc3", imgsource: require("../assets/images/engineer-reports.png") }
      ],
      eng: [
        { name: "Work Done Activity", link: "RJO_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "RJO_EngineerReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "My WIP Reports", link: "RJO_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "Engineer Reports", link: "RJO_EngineerReports", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "AM Reports", link: "RJO_AMReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "WIP Reports", link: "RJO_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Form", link: "RJO_EngineerForm", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Area Manager Form", link: "RJO_AreaManagerForm", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ]
    };

    const MenuItemsTFB = {
      asm: [
        { name: "My Team", link: "TFB_MyTeam", color: "#55aef7", imgsource: require("../assets/images/all-users.png") },
        { name: "Work Done Activity", link: "TFB_WorkDoneForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "TFBWorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "My WIP Reports", link: "TFBWipReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      eng: [
        { name: "Work Done Activity", link: "TFB_WorkDoneForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "TFBWorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "My WIP Reports", link: "TFBWipReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "Work Done Activity", link: "TFB_WorkDoneForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Work Reports", link: "TFBWorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "WIP Reports", link: "TFBWipReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Master Data Form", link: "TFB_MasterDataForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Master Data List", link: "TFB_MasterDataList", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
      ]
    };

    const MenuItemsMNT = {
      asm: [
        { name: "Work Done Activity", link: "MNT_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "MNT_WorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issues Reported", link: "MNT_IssuesReportForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "WIP Reports", link: "MNT_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      eng: [
        { name: "Work Done Activity", link: "MNT_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "MNT_WorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issues Reported", link: "MNT_IssuesReportForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "WIP Reports", link: "MNT_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "Work Done Activity", link: "MNT_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Work Reports", link: "MNT_WorkReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issues Reported", link: "MNT_IssuesReportForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "WIP Reports", link: "MNT_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ]
    };

    const MenuItemsATL = {
      asm: [
        { name: "Engineer Form", link: "ATL_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Team", link: "ATL_MyTeam", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issue Reports", link: "ATL_IssueReport", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Reports", link: "ATL_EngineerReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      eng: [
        { name: "Engineer Form", link: "ATL_EngineerForm", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Reports", link: "ATL_EngineerReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issue Reports", link: "ATL_IssueReport", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "All Users", link: "ATL_AllUsersList", color: "#0a7ddb", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Reports", link: "ATL_EngineerReports", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Issue Reports", link: "ATL_WIPReports", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Form", link: "ATL_EngineerForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Master Data Form", link: "ATL_MasterDataForm", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Master Data List", link: "ATL_MasterDataList", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") },
      ]
    };

    const MenuItemsFTTX = {
      asm: [
        { name: "My Team", link: "FTTX_MyTeam", color: "#0c8bf4", imgsource: require("../assets/images/all-users.png") },
        { name: "Survey Form", link: "SurveyForm", color: "#1a92f5", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC or Chamber Form", link: "Execution_OLT_TO_FDCScreen", color: "#2396f5", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: " FDC or Chamber TO FAT Form", link: "Execution_FDC_TO_FATScreen", color: "#329df6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB (SDU) Form", link: "Execution_FAT_TO_OTBScreen", color: "#41a4f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Survey Reports", link: "SurveyReports", color: "#4aa9f7", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC Reports", link: "OLT_TO_FDC_ReportsScreen", color: "#54adf7", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FDC TO FAT Reports", link: "FDC_or_Chamber_TO_FAT_ReportsScreen", color: "#5eb2f8", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB Reports", link: "FAT_TO_OTB_ReportsScreen", color: "#6db9f8", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      eng: [
        { name: "Survey Form", link: "SurveyForm", color: "#0c8bf4", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC or Chamber Form", link: "Execution_OLT_TO_FDCScreen", color: "#2396f5", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: " FDC or Chamber TO FAT Form", link: "Execution_FDC_TO_FATScreen", color: "#329df6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB (SDU) Form", link: "Execution_FAT_TO_OTBScreen", color: "#41a4f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Survey Reports", link: "SurveyReports", color: "#4aa9f7", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC Reports", link: "OLT_TO_FDC_ReportsScreen", color: "#54adf7", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FDC TO FAT Reports", link: "FDC_or_Chamber_TO_FAT_ReportsScreen", color: "#5eb2f8", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB Reports", link: "FAT_TO_OTB_ReportsScreen", color: "#6db9f8", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "Survey Form", link: "SurveyForm", color: "#1a92f5", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC or Chamber Form", link: "Execution_OLT_TO_FDCScreen", color: "#2396f5", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: " FDC or Chamber TO FAT Form", link: "Execution_FDC_TO_FATScreen", color: "#329df6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB (SDU) Form", link: "Execution_FAT_TO_OTBScreen", color: "#41a4f6", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Survey Reports", link: "SurveyReports", color: "#4aa9f7", imgsource: require("../assets/images/engineer-form.png") },
        { name: "OLT TO FDC Reports", link: "OLT_TO_FDC_ReportsScreen", color: "#54adf7", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FDC TO FAT Reports", link: "FDC_or_Chamber_TO_FAT_ReportsScreen", color: "#5eb2f8", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "FAT TO OTB Reports", link: "FAT_TO_OTB_ReportsScreen", color: "#6db9f8", imgsource: require("../assets/images/wip-reports-menu.png") }
      ]
    };

    const MenuItems = {
      asm: [
        { name: "My Team", link: "", color: "#55aef7", imgsource: require("../assets/images/all-users.png") },
        { name: "Engineer Form", link: "", color: "#3ca2f6", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Area Manager Form", link: "", color: "#2497f5", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Reports", link: "", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "WIP Reports", link: "", color: "#0a7ddb", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "Engineer Reports", link: "", color: "#096fc3", imgsource: require("../assets/images/engineer-reports.png") }
      ],
      eng: [
        { name: "Work Done Activity", link: "", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "My Work Reports", link: "", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "My WIP Reports", link: "", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ],
      sdm: [
        { name: "Work Done Activity", link: "", color: "#0a7ddb", imgsource: require("../assets/images/engineer-form.png") },
        { name: "Work Reports", link: "", color: "#0b8bf4", imgsource: require("../assets/images/wip-reports-menu.png") },
        { name: "WIP Reports", link: "", color: "#3ca2f6", imgsource: require("../assets/images/wip-reports-menu.png") }
      ]
    };

    return (
      <View>

        {
        
          // T-Fiber MODAL lINKING
          (this.props.user_project === "RJO")?(
            <View>
              {
                Object.keys(MenuItemsRJO).map((item, key) => {
                  if (item === this.props.user_roll) {
                    return MenuItemsRJO[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          ):(this.props.user_project === "TFB") ?(
            <View>
              {
                Object.keys(MenuItemsTFB).map((item, key) => {
                  if (item === this.props.user_roll) {
                    return MenuItemsTFB[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          ):(this.props.user_project === "FTTX") ?(
            <View>
              {
                Object.keys(MenuItemsFTTX).map((item, key) => {
                  if (item === this.props.user_roll) {
                    return MenuItemsFTTX[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          ):(this.props.user_project === "ATL") ?(
            <View>
              {
                Object.keys(MenuItemsATL).map((item, key) => {
                  if (item === this.props.user_roll) {
                    return MenuItemsATL[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          ):(this.props.user_project === "MNT")?(
            <View>
              {
                Object.keys(MenuItemsMNT).map((item, key) => {
                  if (item === this.props.user_roll) {
                    return MenuItemsMNT[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          ):(
            <View>
              {
                Object.keys(MenuItems).map((item, key) => {    
                  if (item === this.props.user_roll) {
                    return MenuItems[item].map((itemname, index) => {
                        return (
                          <TouchableOpacity
                            key={index}
                            style={[styles.menuItem, { backgroundColor: itemname.color }]}
                            onPress= {() => this._handleUsersDetails(itemname.link, itemname.name)}
                          >
                            <View style={{ flex: 0.1 }}>
                              <Image
                                source={itemname.imgsource}
                                style={{ tintColor: "#fff" }}
                              />
                            </View>
                            <View style={{ flex: 0.9 }}>
                              <Text style={styles.menusDesign}>{itemname.name}</Text>
                            </View>
                          </TouchableOpacity>
                        )
                      }
                    );
                  }
                })
              }
            </View>
          )
      }
      </View>
    );
  }


  static navigationOptions = ({navigation}) => ({
    header:null
  })
}

const styles = StyleSheet.create({
  menuItem: {
    flex: 1,
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingVertical: 12.1
  },

  menusDesign: {
    color: "#ffffff",
    fontSize: 16
  }
});

export default Menus;
