import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, TextInput } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base'; 
import { TextField } from 'react-native-material-textfield';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {BASE_URL} from '../basepath';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class EditTeamUsers extends Component {

    constructor(props) {
      super(props);
      this.paramsData = this.props.navigation.state.params;
      this.config = {
        getUserDetailsAPI: BASE_URL + "apis/usersDetails",
        updateUserDetailsAPI: BASE_URL + "apis/User/updateTeamUserDetails",
      }

      this.state = {
        isLoading: true,
        areaManagerDisplay: false,
        userFirstName: "",
        userLastName: "",
        userEmail: "",
        userUsername: "",
        userPassword: "",
        
        selectedData: {
          userPrjoectName: this.paramsData.ProjectID,
          userFirstName: "",
          userEmail: "",
          userLastName: "",
          userUsername: "",
          userPassword: "",
        },
        usersData: [],
        errorMsg: "",
        successMsg: "",
        emailError: "",
        usernameError: "",
        firstNameError: "",
      }
    }
    
    // CALLING METHOD AFTER RENDER ELEMENTS 
    componentDidMount(){
      this.fetchDataOnLoad();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }

    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }
    
    // CALLING API TO GET ALL DETAILS OF USER 
    fetchDataOnLoad = async() => {
      let responseJson = [];
      let response = await fetch(this.config.getUserDetailsAPI+"/"+this.paramsData.userID);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ 
          userFirstName: responseJson.data.first_name,
          userLastName: responseJson.data.last_name,
          userEmail: responseJson.data.email_id,
          userUsername: responseJson.data.username,        
          usersData: responseJson.data,
          isLoading: false
        });
        this.state.selectedData["userFirstName"] = responseJson.data.first_name;
        this.state.selectedData["userLastName"] = responseJson.data.last_name;
        this.state.selectedData["userEmail"] = responseJson.data.email_id;
        this.state.selectedData["userUsername"] = responseJson.data.username;

        this.state.selectedData["userId"] = responseJson.data.user_id;
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // USING METHOD TO GET & SET DATA FROM FIELDS IN STATE
    getData(data, fieldName){
      if(fieldName === "userFirstName"){
        this.setState({ userFirstName: data });
        this.state.selectedData["userFirstName"] = data;
      }else if(fieldName === "userLastName"){
        this.setState({ userLastName: data });
        this.state.selectedData["userLastName"] = data;
      }else if(fieldName === "userEmail"){
        this.setState({ userEmail: data });
        this.state.selectedData["userEmail"] = data;
      }else if(fieldName === "userUsername"){
        this.setState({ userUsername: data });
        this.state.selectedData["userUsername"] = data;
      }else{
        this.state.selectedData[fieldName] = data;
      }
    }

    // USING METHOD FOR VALIDATION OF ENTERED FORM DATA & SAVING DATA ON SERVER
    async sumbmitFormData(){
      this.setState({ firstNameError: "", emailError: "", isLoading: true });
      let responseJson = [];
      const { selectedData } = this.state;
      let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      
      if((selectedData.userFirstName).trim() === ""){
        this.setState({ firstNameError: "First name can not be empty.", emailError: "", isLoading: false });
        return false;
      }else if(reg.test(selectedData.userEmail) === false){
        this.setState({ emailError: "Email is not Correct.", firstNameError: '', isLoading: false });
        return false;
      }else{
        this.setState({ firstNameError: "", emailError: "", isLoading: false });
      }


      let response = await fetch(this.config.updateUserDetailsAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      })
      try {
        responseJson = await response.json();

       // Successful response from the API Call
        if(responseJson.hasOwnProperty('email')){
          this.setState({ emailError: responseJson.email,usernameError: "",errorMsg: "",successMsg: "", isLoading: false });
        }else if(responseJson.hasOwnProperty('username')){
          this.setState({ usernameError: responseJson.username,emailError: "",errorMsg: "",successMsg: "", isLoading: false });
        }else if(responseJson.hasOwnProperty('error')){
          this.setState({ errorMsg: responseJson.error,usernameError: "",emailError: "",successMsg: "", isLoading: false });
        }else{
          this.setState({ successMsg: responseJson.success,usernameError: "",emailError: "",errorMsg: "", firstNameError: "", isLoading: false });
        }
      } catch (error) {
        console.log("Response error - ",error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }

    }


    render() {
      return (
          <Container style={styles.Container}>

            {/* START SECTION FOR PROFILE TOP WITH IMAGE AND NAME OF USER */}
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >

                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={[ styles.paddingBottomMore , { paddingBottom: 10 }]} >
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 0.3 }}><Text style={{ fontSize: 16 }}>User ID: </Text></View>
                        <View style ={{ flex: 0.7 }}>
                          <Text style={{ fontSize: 16 }}>{this.paramsData.userID}</Text>
                        </View>                          
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex: 1 }}>
                        <TextField
                          label='First Name'
                          keyboardType='default'
                          autoCapitalize='none'
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          returnKeyType='next'
                          value={this.state.userFirstName}
                          onChangeText={(value) => this.getData(value, "userFirstName")}
                          error	= {this.state.firstNameError}
                        />
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex: 1 }}>
                        <TextField
                          label='Last Name'
                          keyboardType='default'
                          autoCapitalize='none'
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          returnKeyType='next'
                          value={this.state.userLastName}
                          onChangeText={(value) => this.getData(value, "userLastName")}
                          error	= ""
                        />
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex: 1 }}>
                        <TextField
                          label='Email Id'
                          keyboardType='email-address'
                          autoCapitalize='none'
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          returnKeyType='next'
                          value={this.state.userEmail}
                          onChangeText={(value) => this.getData(value, "userEmail")}
                          error	= {this.state.emailError}
                        />
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex: 1 }}>
                        <TextField
                          label='Username/Mobile Number'
                          keyboardType='phone-pad'
                          autoCapitalize='none'
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          returnKeyType='next'
                          value={this.state.userUsername}
                          onChangeText={(value) => this.getData(value, "userUsername")}
                          error	= {this.state.usernameError}
                        />
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex: 1 }}>
                        <TextField
                          label='Password'
                          autoCapitalize='none'
                          autoCorrect={false}
                          enablesReturnKeyAutomatically={true}
                          clearTextOnFocus={true}
                          secureTextEntry={true}
                          maxLength={35}
                          returnKeyType='done'
                          value={this.state.userPassword}
                          onChangeText={(value) => this.getData(value, "userPassword")}
                          error	= ""
                        />
                      </View>
                    </View>                    

                  </View>

                  <KeyboardSpacer topSpacing ={5} />
                  
                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 1 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center" }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }
                  </View>
                  
                  {
                    (this.state.isLoading)?
                    (
                      <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                        <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                      </TouchableOpacity>
                    ):(
                      <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress={()=> this.sumbmitFormData()}>
                        <Text style={styles.loadMorebtnTxt}>Submit</Text>
                      </TouchableOpacity>
                    )
                  }
                  
                </View>
              </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }
  
    // USING navigationOptions TO SET HEADER OF SCREEN
    static navigationOptions = ({navigation}) => {
      return {
        title: 'Edit User',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  }

}

// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4
    },
    paddingBottomMore: {
      paddingBottom: 0
    },
    
    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    }
  
  });
  
export default EditTeamUsers;
