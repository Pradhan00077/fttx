import React, { Component } from "react";
import { AsyncStorage, StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { Container } from 'native-base';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';

const screenHeight = Math.round(Dimensions.get('window').height);

class Dashboard extends Component {

    constructor(props) {
      super(props);
      this.state = {
        userId: "",
        userProjectId: "",
        userRole: "",
        errorMsg: ""
      };
    }

    // USING METHOD TO CHECK SCREENS PERMISSION ACCORNING TO USER ASSIGNED PROJECTS & ROLES
    _handleButtonPress = async(projectName) => {
      
      let self = this;
      const user_id = await AsyncStorage.getItem('user_id');
      const userProjectId = await AsyncStorage.getItem('user_project_id');
      const userRole = await AsyncStorage.getItem('user_role');

      if(userProjectId != projectName){ // YOU ARE ALREADY IN THIS PROJECT OR NOT.
        if(userRole != 'adm'){ // LOGGED IN USER CHECKING.
          this.setState({ errorMsg: "You have not permission to access this project." });
        }else{
          if(projectName === 'RJO'){
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('RJO_EngineerForm');
          }else if(projectName === 'MNT'){
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('MNT_EngineerForm');
          }else if(projectName === 'TFB'){
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('TFB_LocationMap');
          }else if(projectName === 'ATL'){
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('ATL_EngineerForm');
          }else if(projectName === 'FTTX'){
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('FTTXDashboardScreen');
          }else {
            this.setState({ errorMsg: "" });
            self.props.navigation.navigate('TFB_EngineerForm');
          }
        }
      }else{
        if(projectName === 'RJO'){
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('RJO_EngineerForm');
        }else if(projectName === 'MNT'){
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('MNT_EngineerForm');
        }else if(projectName === 'TFB'){
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('TFB_LocationMap');
        }else if(projectName === 'ATL'){
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('ATL_EngineerForm');
        }else if(projectName === 'FTTX'){
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('FTTXDashboardScreen');
        }else {
          this.setState({ errorMsg: "" });
          self.props.navigation.navigate('TFB_WorkDoneForm');
        }
      }
    }

    // CALLING METHOD AFTER RENDER ELEMENTS 
    componentDidMount() {
      // SET PARAMS TO CALL METHOD FROM HEADER OF SCREEN
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }

    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }
    
    render() {

      const dashboardBoxes = [
        {
          "Image_path": require("../assets/images/icon-1.png"),
          "Name": "R-Jio",
          "link": "RJO",
        },{
          "Image_path": require("../assets/images/icon-2.png"),
          "Name": "MahaNet",
          "link": "MNT",
        },{
          "Image_path": require("../assets/images/icon-3.png"),
          "Name": "T-Fiber",
          "link": "TFB",
        },{
          "Image_path": require("../assets/images/icon-6-1.png"),
          "Name": "Airtel",
          "link": "ATL",
        },{
          "Image_path": require("../assets/images/icon-5-1.png"),
          "Name": "FTTX",
          "link": "FTTX",
        }
      ];
    
      return (
        <Container>

          <ScrollView  contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>

            <View style={styles.body}>
              
              {
                (this.state.errorMsg !='')?(
                  <View>
                    <Text style={styles.errorDesign}>{this.state.errorMsg}</Text>
                  </View>
                ):(
                  <View><Text></Text></View>
                )
              }
              

              {
                dashboardBoxes.map((item, i) => {
                    return(
                      <TouchableOpacity key={i} activeOpacity={1} style={styles.sectionContainer} onPress={()=> this._handleButtonPress(item.link)} >
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={styles.sectionIcon}>
                            <Image source = {item.Image_path} resizeMode='contain' 
                            style={{ alignSelf: "auto" }} />
                          </View>
                          <Text style={styles.sectionTitle}>
                            {item.Name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                })
              }
                
            </View>
            
          </ScrollView>
        </Container>
      )
    }
  
    // USING navigationOptions TO SET HEADER OF SCREEN
    static navigationOptions = ({ navigation }) => ({
        title: 'Welcome',
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign:"center", 
          flex:0.8,
          fontSize:20,
          lineHeight:25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),

    })

}


// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
    container: {
      padding: 20,
      flex: 1,
    },
    body: {
      backgroundColor: '#eeeeee',
      height: (screenHeight-110),
      paddingBottom: 5,
    },
    sectionContainer: {
      // marginTop: 32,
      marginBottom: 20,
      marginLeft: 26,
      marginRight: 26,
      paddingHorizontal: 18,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "row",
      
      borderRadius: 2,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 3,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 4,
        },
      })
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitle: {
      flex: 0.6,
      fontSize: 26,
      fontWeight: '600',
      color: "#1949c7",
      alignSelf:"center"
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    errorDesign: {
      paddingVertical: 10,
      paddingHorizontal: 25,
      textAlign: 'center',
      color: "red",
      fontSize: 14,
      fontWeight: '600'
    }
  
  });
  
export default Dashboard;
