import React, { Component } from "react";
import { SafeAreaView, StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';

const screenHeight = Math.round(Dimensions.get('window').height);

class Activity extends Component {

    constructor(props) {
      super(props);
    }

    _handleButtonPress = () => {
      this.setState({ loginSuccess: "Success Login!" });
      this.props.navigation.navigate('Logout');
    }

    render() {
        return (
          <Container style={styles.Container}>

            {/* START SECTION FOR PROFILE MIDLLE WITH DETAILS OF USER */}
            <ScrollView  contentInsetAdjustmentBehavior="automatic">
              <View style={styles.body}>
                <View style={styles.sectionContainer}>

                  <View style={styles.itemsSection}>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}>
                      <Text style={styles.sectionTitle}>Activity</Text>
                    </View>
                  </View>

                </View>
              </View>
            </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }
  
  
    static navigationOptions = {
        title: 'Activity',
        headerLeft: null,
        headerStyle: {
          height:60,
          borderBottomWidth: 0,
          elevation: 0
        },
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign:"center", 
          flex:1,
          fontSize:22,
          lineHeight:25,
        },
        headerBackground: (
          <LinearGradient colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 0.9, y: 0}}
            style={{ flex: 1 }}
          />
        ),

    }

}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#eeeeee"
    },
    body: {
      paddingBottom: 20,
    },
    sectionContainer: {
      marginTop: (screenHeight-(screenHeight-(screenHeight/8.5))),
      marginLeft: 20,
      marginRight: 20,
      paddingHorizontal: 12,
      // paddingVertical: 12,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "column",
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 4,
        },
      }),
      borderRadius: 2,
    },
    profileSection: {
      backgroundColor:'#ffffff',
      position:'absolute',
      top: 60,
      left: 0,
      zIndex: 2,
      width:'90%',
      marginLeft:'5%',
      alignSelf:'center',
      borderRadius: 4,
      flex: 1,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 5,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 5,
        },
      }),
    },
    profileImage:{
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center',
      position:'absolute',
      top:-50,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 6,
        },
      }),
      borderWidth:1,
      borderColor: "#ffffff",    
    },
    profileImageStyle: {
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center'
    },
    flexColumn:{
      marginTop:(screenHeight/8),
      flex:1,
      marginBottom:20,
    },
    userProfileName: {
      fontSize: 24,
      lineHeight: 16,
      alignSelf: "center",
      paddingTop:8,
      paddingBottom:8,
    },    
    itemsSection: {
      flex: 1,
      flexDirection: "row",
      borderBottomColor: "#dadada",
      borderBottomWidth: 1,
      paddingVertical: 14
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitleIcon: {
      flex: 0.16,
      paddingVertical: 5
    },
    sectionTitle: {
      flex: 0.83,
      fontSize: 16,
      fontWeight: '600',
      color: "#898989",
      alignSelf:"center",
    },
    sectionUserDet: {
      alignSelf: "flex-end",
      fontWeight: "600",
      fontSize: 16,
      color: "#898989",
      paddingTop: 4
    },
  
  });
  
export default Activity;
