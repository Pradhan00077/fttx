import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, ActivityIndicator, Dimensions, AsyncStorage , Platform, TouchableOpacity} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';
import {BASE_URL} from '../basepath';
import Constants from 'expo-constants';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class Profile extends Component {

  constructor(props) {
    super(props);
    this.paramsData = this.props.navigation.state.params;
    this.config = {
      apiUrl: BASE_URL + "apis/usersDetails",
    }
    this.state = {
      isLoading: true,
      usersData: [],
      confirmModalVisible: false,
      successModalVisible: false,
    };
  }

  // CALLING METHOD AFTER RENDER ELEMENTS 
  componentDidMount(){
    // CALLING METHOD TO GET DATA ON LOAD OF SCREEN
    this.fetchDataOnLoad();
    this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
  }

  // USING METHOD TO OPEN DRAWER
  openControlPanel = () => {
    this.props.navigation.openDrawer();
  }

  // CALLING METHOD TO GET DATA ON LOAD OF SCREEN
  fetchDataOnLoad = async() => {
    let responseJson = [];
    const userid = await AsyncStorage.getItem('user_id');

    let response = await fetch(this.config.apiUrl+"/"+userid);
    try {
      responseJson = await response.json();
      //Successful response from the API Call          
        this.setState({ usersData: responseJson.data, isLoading: false });
    }catch (error) {
      console.log("Response error - ",error);
    }
  }

    render() {
        let userData = this.state.usersData;
        let userRole = '';
        let userProject = '';

        if(userData.user_role === 'adm'){ userRole = 'Admin'; }
        else if(userData.user_role === 'asm'){ userRole = 'Area manager'; }
        else{ userRole = 'Engineer'; }
        
        if(userData.project_id === 'RJO'){ userProject = 'R-Jio'; }
        else if(userData.project_id === 'MNT'){ userProject = 'MahaNet'; }
        else if(userData.project_id === 'TFB'){ userProject = 'T-Fiber'; }
        else if(userData.project_id === 'ATL'){ userProject = 'Airtel'; }
        else{ userProject = 'MP4'; }

    return(
        <Container style={styles.Container}>
          {/* START SECTION FOR PROFILE TOP WITH IMAGE AND NAME OF USER */}
          {(this.state.isLoading)?(
              <View></View>
            ):(
            <View style={{ height: 150, }}>
              <LinearGradient colors={['#2157c4', '#3b5998', '#1ab679']} start={{x: 0, y: 0}} end={{x: 0.9, y: 0}} style={{ flex: 1 }} >
              {

                (Platform.OS==='android')?
                (
                  <View style = {styles.profileSection}>
                        <View style = {styles.profileImage}>
                          <Image  style = {[styles.profileImageStyle]} source = {require("../assets/images/profile.png")} />
                        </View>
                        <View style={{flexDirection:'row',flex:1 }}>
                          <View style={[styles.flexColumn]}>
                            <Text style = {styles.userProfileName}>{userData.first_name}</Text>
                          </View>
                        </View>
                      </View>
                ):
                (
                    <View></View>
                )
              }
              </LinearGradient>
              {

                (Platform.OS==='ios')?
                (
                  <View style = {styles.profileSection}>
                        <View style = {styles.profileImage}>
                          <Image  style = {[styles.profileImageStyle]} source = {require("../assets/images/profile.png")} />
                        </View>
                        <View style={{flexDirection:'row',flex:1 }}>
                          <View style={[styles.flexColumn]}>
                            <Text style = {styles.userProfileName}>{userData.first_name}</Text>
                          </View>
                        </View>
                      </View>
                ):
                (
                    <View></View>
                )
              }
            </View>
            )}
            {/* END SECTION */}

            {/* START SECTION FOR PROFILE MIDLLE WITH DETAILS OF USER */}
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
            <ScrollView  contentInsetAdjustmentBehavior="automatic">
              <View style={styles.body}>
                <View style={styles.sectionContainer}>

                <View style={styles.itemsSection}>
                    <View style={{ flex: 0.4, flexDirection: "row", alignItems: "flex-start" }}>
                      <View style={styles.sectionTitleIcon}>
                        <Image source={require('../assets/images/name.png')} />
                      </View>                      
                      <Text style={styles.sectionTitle}>Full name:</Text>
                    </View>

                    <View style={{ flex: 0.6 }}>
                      <Text style={styles.sectionUserDet}>{userData.first_name} {userData.last_name}</Text>
                    </View>
                  </View>
                  
                  
                  <View style={styles.itemsSection}>
                    <View style={{ flex: 0.4, flexDirection: "row", alignItems: "flex-start" }}>
                      <View style={styles.sectionTitleIcon}>
                        <Image source={require('../assets/images/email.png')} />
                      </View>                      
                      <Text style={styles.sectionTitle}>Email:</Text>
                    </View>

                    <View style={{ flex: 0.6 }}>
                      <Text style={styles.sectionUserDet}>{userData.email_id}</Text>
                    </View>
                  </View>

                  
                  <View style={styles.itemsSection}>
                    <View style={{ flex: 0.4, flexDirection: "row", alignItems: "flex-start" }}>
                      <View style={styles.sectionTitleIcon}>
                        <Image source={require('../assets/images/mobile.png')} />
                      </View>                      
                      <Text style={styles.sectionTitle}>Mobile no:</Text>
                    </View>

                    <View style={{ flex: 0.6 }}>
                      <Text style={styles.sectionUserDet}>{userData.mobile}</Text>
                    </View>
                  </View>

                  {
                    (userData.senior_role !== "0")?(
                      <View style={styles.itemsSection}>
                        <View style={{ flex: 0.4, flexDirection: "row", alignItems: "flex-start" }}>
                          <View style={styles.sectionTitleIcon}>
                            <Image source={require('../assets/images/area-manager.png')} />
                          </View>
                          <Text style={styles.sectionTitle}>Area Manager:</Text>
                        </View>
    
                        <View style={{ flex: 0.6 }}>
                          <Text style={styles.sectionUserDet}>{userData.sfname} {userData.slname}</Text>
                        </View>
                      </View>
                    ):(
                      <View></View>
                    )
                  }
                  
                  <View style={[styles.itemsSection, { borderBottomWidth: 0 }]}>
                    <View style={{ flex: 0.4, flexDirection: "row", alignItems: "flex-start" }}>
                      <View style={styles.sectionTitleIcon}>
                        <Image source={require('../assets/images/version.png')} />
                      </View>                      
                      <Text style={styles.sectionTitle}>Version:</Text>
                    </View>

                    <View style={{ flex: 0.6 }}>
                      {/* <Text style={styles.sectionUserDet}>1.0.0(54545)</Text> */}
                      <Text style={styles.sectionUserDet}>{ Constants.manifest.version }</Text>
                    </View>
                  </View>

                </View>
              </View>
            </ScrollView>
            
            )}
            {/* END SECTION */}
        </Container>
      )
    }
    
    // USING navigationOptions TO SET HEADER OF SCREEN
    static navigationOptions = ({navigation}) => {
      return {
        title: 'Profile',
        headerLeft: null,
        headerStyle: {
          height:60,
          borderBottomWidth: 0,
          elevation: 0
        },
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign:"center", 
          flex:0.8,
          fontSize:22,
          lineHeight:25,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 0.9, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 0 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
      }
    }

}


// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#eeeeee"
    },
    body: {
      paddingBottom: 20,
    },
    ActivityIndicatorContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    ActivityIndicatorHorizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    sectionContainer: {
      marginTop: (screenHeight-(screenHeight-(screenHeight/8.5))),
      marginLeft: 20,
      marginRight: 20,
      paddingHorizontal: 12,
      // paddingVertical: 12,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "column",
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 4,
        },
      }),
      borderRadius: 2,
    },
    profileSection: {
      backgroundColor:'#ffffff',
      position:'absolute',
      top: 60,
      left: 0,
      zIndex: 999,
      width:'90%',
      marginLeft:'5%',
      alignSelf:'center',
      borderRadius: 4,
      flex: 1,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 5,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 5,
        },
      })
    },
    profileImage:{
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center',
      position:'absolute',
      top:-50,

      borderWidth:1,
      borderColor: "#ffffff",    
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 6,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 6,
        },
      })
    },
    profileImageStyle: {
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center'
    },
    flexColumn:{
      marginTop:(screenHeight/8),
      flex:1,
      marginBottom:20,
    },
    userProfileName: {
      fontSize: 24,
      lineHeight: 16,
      alignSelf: "center",
      paddingTop:8,
      paddingBottom:8,
    },    
    itemsSection: {
      flex: 1,
      flexDirection: "row",
      borderBottomColor: "#dadada",
      borderBottomWidth: 1,
      paddingVertical: 14
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitleIcon: {
      flex: 0.16,
      paddingVertical: 5
    },
    sectionTitle: {
      flex: 0.83,
      fontSize: 16,
      fontWeight: '600',
      color: "#898989",
      alignSelf:"center",
    },
    sectionUserDet: {
      alignSelf: "flex-end",
      fontWeight: "600",
      fontSize: 16,
      color: "#898989",
      paddingTop: 4,
      flexWrap: 'wrap'
    },
  
  });
  
export default Profile;
