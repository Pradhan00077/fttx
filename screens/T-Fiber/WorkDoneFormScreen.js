import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, TextInput, AsyncStorage, FlatList, setIntervalAsync , Platform} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container, Radio, CheckBox } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import * as Network from 'expo-network';
import Textarea from 'react-native-textarea';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import LocationTracking from './LocationTracking';
import * as Progress from 'react-native-progress';
import { TextField } from 'react-native-material-textfield';
import Database from '../../Database';
const db = new Database();
import {BASE_URL} from '../../basepath'
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
import { MaterialIcons } from '@expo/vector-icons';

const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

let firstlat=0;
let firstlon=0;
let  lastlat=0;
let  lastlon=0;

class EngineerForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        DistrictAPI: BASE_URL + "apis/userDistrictFilter",
        TalukaAPI: BASE_URL + "apis/userTalukaFilter",
        SpanAPI: BASE_URL + "apis/userSpanFilter",
        SpanLength: BASE_URL + "apis/userGpLengthSpanFilter",
        // GetRingNo: BASE_URL + "apis/userRingNoFilter",
        ActivitiesAPI: BASE_URL + "apis/userActivitiesFilter",
        IssuesAPI: BASE_URL + "apis/userIssuesFilter",
        CheckWipTransAPI: BASE_URL + "apis/userWipTransFilter",
        CheckTransAPI: BASE_URL + "apis/userTransactionFilter",
        submitFormAPI: BASE_URL + "apis/workDoneFormSubmit/0",
      }
      this.state = {
        isLoading: false,
        allDistrict: [],
        allTaluka: [],
        allSpan: [],
        ringNoVal: "",
        rignTypeVal: "",
        activities: [],
        internalIssues: [],
        customerIssues: [],
        userName: 0,
        userRole: 0,
        userLoggedInId: 0,
        milestoneStartStop: false,
        IssuesReportChange: true,
        activeMachineResource: false,
        vendorDetails: true,
        IssuesReportValue: "No",
        mandalResource: "No",
        spanLength: "",
        spanId: "",
        pOpType: "",
        primaryVendor: "",
        activity: "T&D",
        IssuesChecked: 'Internal',
        milestoneChecked: 'WIP',
        chainageFrom: "0",
        chainageTo: "1",
        isCheckedInternalIssues: [],
        isCheckedCustomerIssues: [],
        activityTDDrtBookNo: false,
        activityDisabled: false,
        activityTD: true,
        activityBlowing: false,
        activityChmbrInst: false,
        activityInfraEquipment: false,
        pOpNameValue: "",
        popTypeMandal: false,
        popTypeGP: false,
        popTypeZone: false,

        fiberReadingFrom: "",
        fiberReadingTo: "",
        fiberReadingIn: "",
        fiberReadingOut: "",
        lattitude: "0.000",
        longitude: "0.000",
        errorMsg: "",
        splitedGPName: [],
        slctDateVal: new Date(),
        submitBtnDisabled: true,
        countedMint: 0,
        milestoneToTD: "1",
        milestoneFromTD: "0",
        milestoneFromBlowing: "0",
        milestoneFromBlowingMTS: "0",
        milestoneToBlowing: "0",
        milestoneToBlowingMTS: "0",
        distance: 0,
        trackingstart:false,
        locationFrom: "",
        locationTo: "",
        approxAvgDepth: "",

        milestoneSpansLen: [],

        selectedData: {
          issues: [],
          district: "",
          taluka: "",
          span: "",
          mainVender: "",
          activity: "T&D",
          issuesReportTD: "No",
          chainageFrom: "0",
          chainageTo: "1",
          soilStrata: "Normal",
          milestoneStatus: "WIP",
          excavationType: "",
          protection: "",
          issuesType: "Internal",
          approx_Avg_Depth: "0",
          issues_TD_Hndl: "No",
          mandalResource: "No",
          protection: "",
          blowingType: "",
          fiberReadingFrom: "",
          fiberReadingTo: "",
          fiberType: "",
          splicing: "0",
          termination: "0",
          chainagePointBlow: "",
          fiberReadingIn: "",
          fiberReadingOut: "",
          lattitude: "",
          longitude: "",
          milestoneFrom: "0",
          milestoneTo: "0",
          milestoneFromM: "0",
          milestoneToM: "0",
          mRPlannedJCB: "", mRActualJCB: "",
          mRPlannedHDD: "", mRActualHDD: "",
          mRPlannedPoklane: "", mRActualPoklane: "",
          mRPlannedRockBreaker: "", mRActualRockBreaker: "",
          mRPlannedExcavator: "", mRActualExcavator: "",
          mRPlannedBackhoeLoader: "", mRActualBackhoeLoader: "",
          mRPlannedBlowing: "", mRActualBlowing: "",
          mRPlannedSlicer: "", mRActualSlicer: "",
          mRPlannedManpower: "", mRActualManpower: "",
          slctDate: todayDate

        }
      }
      this.milestoneProcessHandler = this.milestoneProcessHandler.bind(this);
    }


    componentDidMount(){
      this.fetchUserDataState();
      this.fetchActivities();
      this.fetchInternalIssues();
      this.fetchCustomerIssues();
      if(firstlat === 0 && firstlon === 0){
        this.getCurrentLatLong();
      }

      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    fetchUserDataState = async() => {
      const userid = await AsyncStorage.getItem('user_id');
      const usern = await AsyncStorage.getItem('username');
      const userr = await AsyncStorage.getItem('user_role');
      const ipAddress = await Network.getIpAddressAsync();
      // const macAddress = await Network.getMacAddressAsync();

      this.state.selectedData["ipAddress"] = ipAddress;
      // this.state.selectedData["macAddress"] = macAddress;

      this.state.selectedData["user_id"] = userid;
      this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
      this.fetchDistrictAPI();
    }

    // CALLING API TO GET ALL ACTIVITIES
    async fetchActivities(){
      let responseJson = [];
      let response = await fetch(this.config.ActivitiesAPI);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ activities: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL Internal ISSUES
    async fetchInternalIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/1");
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ internalIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }


    // CALLING API TO GET ALL Customer ISSUES
    async fetchCustomerIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/2");
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ customerIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL DISTRICT
    async fetchDistrictAPI(){
      let responseJson = [];
      let response = await fetch(this.config.DistrictAPI+"/"+this.state.userRole+"/"+this.state.userName);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allDistrict: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL TALUKAS
    fetchTalukaAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.TalukaAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allTaluka: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN
    fetchSpanAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.SpanAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allSpan: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN
    fetchSpanLengthAPI = async(value) => {
      let gpName = [];
      let responseJson = [];
      let gpNameVals = value.split("-");

      let response = await fetch(this.config.SpanLength+"/"+value);
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        let spId = responseJson.data[0].span_id;
        let prmVendor = responseJson.data[0].primary_vendor;
        let spLength = responseJson.data[0].span_length;
        let locFrom = responseJson.data[0].location_from;
        let locTo = responseJson.data[0].location_to;
        let rignType = responseJson.data[0].ring_type;
        let ringNo = responseJson.data[0].ring_no;
        
        let milestoneSpan = parseInt(spLength, 10)+5;
        let milestoneSpanVal = [];
        for (var i=0 ; i <= milestoneSpan; i++) {
          milestoneSpanVal.push({ 'value': i });
        }

        gpName.push(gpNameVals.map((item, i) => {
          return {
            "value": gpNameVals[i]
          }
        }))
        
        this.setState({ spanLength: spLength, rignTypeVal: rignType, ringNoVal: ringNo, locationFrom: locFrom, locationTo: locTo, spanId: spId, primaryVendor: prmVendor, milestoneSpansLen: milestoneSpanVal, splitedGPName: gpName[0], pOpNameValue: gpName[0][0].value, pOpType: locFrom });
        
        // USING METHOND TO DISPLAY POP DROPDOWNS        
        this.popDataChanges(locFrom);

        // ADDING DATA IN OBJECT TO GET ALL
        this.state.selectedData.span_id = spId;
        this.state.selectedData.primary_vendor = prmVendor;
        this.state.selectedData.span_length = spLength;
        this.state.selectedData.location_from = locFrom;
        this.state.selectedData.location_to = locTo;
        this.state.selectedData.ring_type = rignType;
        this.state.selectedData.ring_no = ringNo;
        this.state.selectedData.pop_name = gpName[0][0].value;

      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // GET CURRENT LAT LONG
    async getCurrentLatLong(){

      navigator.geolocation.getCurrentPosition(
        position => {
          const lattitude = JSON.stringify(position.coords.latitude);
          const longitude = JSON.stringify(position.coords.longitude);

          this.state.selectedData["lattitude"] = lattitude;
          this.state.selectedData["longitude"] = longitude;

          this.setState({ lattitude: lattitude, longitude: longitude });
        },
        error => console.log(" error - ", error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );

    }

    // USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE ISSUES
    async internalIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.internalIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedInternalIssues: checkedIssuesList, internalIssues: checkedIssues });
    }

    // CUSTOMER ISSUES CHECKBOXES
    async customerIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.customerIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedCustomerIssues: checkedIssuesList });
    }

    
    // GETING DATA FROM FIELDS AND USING VALIDATION
    popDataChanges(data, index=0){
      if(index === 1){
        checkVal = this.state.locationTo;
        this.setState({ pOpType: checkVal });

        if (checkVal.toLowerCase() === 'mandal') {
          this.setState({ popTypeMandal: true, popTypeGP: false });
        }else if (checkVal.toLowerCase() === 'zone') {
          this.setState({ popTypeMandal: false, popTypeGP: false, popTypeZone: true });
        }else if (checkVal.toLowerCase() === 'taluka') {
          this.setState({ popTypeMandal: false, popTypeGP: false, popTypeZone: true });
        }else{
          this.setState({ popTypeMandal: false, popTypeGP: true });
        }
      }else{
        checkVal = this.state.locationFrom;
        this.setState({ pOpType: checkVal });

        if (checkVal.toLowerCase() === 'mandal') {
          this.setState({ popTypeMandal: true, popTypeGP: false });
        }else if (checkVal.toLowerCase() === 'zone') {
          this.setState({ popTypeMandal: false, popTypeGP: false, popTypeZone: true });
        }else if (checkVal.toLowerCase() === 'taluka') {
          this.setState({ popTypeMandal: false, popTypeGP: false, popTypeZone: true });
        }else{
          this.setState({ popTypeMandal: false, popTypeGP: true });
        }
      }

      this.state.selectedData.pop_name = data;
      this.state.selectedData.pop_type = this.state.pOpType;
    }

    // GETING DATA FROM FIELDS AND USING VALIDATION
    getData(data, fieldName){

      if(fieldName === "District"){
        this.state.selectedData.district = data;
        this.fetchTalukaAPI(data);
      }else if(fieldName === "Taluka"){
        this.state.selectedData.taluka = data;
        this.fetchSpanAPI(data);
      }else if(fieldName === "Span"){
        this.state.selectedData.span = data;
        this.fetchSpanLengthAPI(data);
      }else if(fieldName === "activity"){

        if(data === "T&D"){
          this.setState({ activityTD: true, activityBlowing: false, activityChmbrInst: false, activityInfraEquipment: false });
          (this.state.selectedData.fieldType === "Brown field")?this.setState({activityTDDrtBookNo:true}):this.setState({ activityTDDrtBookNo:false});
        }else if(data === "Blowing"){
          this.setState({ activityBlowing: true, activityChmbrInst: false, activityTD: false, activityInfraEquipment: false, activityTDDrtBookNo: false });
        }else if(data === "Infra/Equipment Readiness"){
          this.setState({ activityInfraEquipment: true, activityChmbrInst: false, activityTD: false, activityBlowing: false, activityTDDrtBookNo: false });
        }else if(data === "Chamber installation"){
          this.setState({ activityChmbrInst: true, activityInfraEquipment: false, activityTD: false, activityBlowing: false, activityTDDrtBookNo: false });
        }
        this.state.selectedData.activity = data;

      }else if(fieldName === "IssuesReport"){
        if(data === "Yes"){
          this.state.selectedData['issues_TD_Hndl'] = "Yes";
          this.setState({ IssuesReportChange: false, IssuesReportValue: "Yes", activityDisabled: true });
        }else{
          this.state.selectedData['issues_TD_Hndl'] = "No";
          this.setState({ IssuesReportChange: true, IssuesReportValue: "No", activityDisabled: false });
        }
        this.state.selectedData.issuesReportTD = data;
      }else if(fieldName === "fieldType"){
        if(data === "Brown field" && this.state.selectedData.activity === "T&D"){
          this.setState({ activityTDDrtBookNo: true })
        }else{
          this.setState({ activityTDDrtBookNo: false })
        }
        this.state.selectedData.fieldType = data;
      }else if(fieldName === "ChainageStart"){
        this.setState({ chainageFrom: data })
        this.state.selectedData.chainageFrom = data;
      }else if(fieldName === "ChainageEnd"){
        this.setState({ chainageTo: data })
        this.state.selectedData.chainageTo = data;
      }else if(fieldName === "approx_Avg_Depth"){
        this.setState({ approxAvgDepth: data })
        this.state.selectedData.fieldName = data;
      }else if(fieldName === "fiberReadingFrom"){
        this.setState({ fiberReadingFrom: data })
        this.state.selectedData.fiberReadingFrom = data;
      }else if(fieldName === "fiberReadingTo"){
        this.setState({ fiberReadingTo: data })
        this.state.selectedData.fiberReadingTo = data;
      }else if(fieldName === "fiberReadingIn"){
        this.setState({ fiberReadingIn: data })
        this.state.selectedData.fiberReadingIn = data;
      }else if(fieldName === "fiberReadingOut"){
        this.setState({ fiberReadingOut: data })
        this.state.selectedData.fiberReadingOut = data;
      }else if(fieldName === "lattitude"){
        this.setState({ lattitude: data })
        this.state.selectedData.lattitude = data;
      }else if(fieldName === "longitude"){
        this.setState({ longitude: data })
        this.state.selectedData.longitude = data;
      }else if(fieldName === "chainagePointBlow"){
        this.setState({ chainagePointBlow: data })
        this.state.selectedData.chainagePointBlow = data;
      }else if(fieldName === "SoilStrata"){
        this.setState({ soilStrataVal: data })
        this.state.selectedData.soilStrata = data;
      }else if(fieldName === "slctDate"){
        this.setState({ slctDateVal: data })
        this.state.selectedData.slctDate = data;
      }else if(fieldName === "milestoneFromTD"){
        this.setState({ milestoneToTD: (data + 1).toString(), milestoneFromTD: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromHHMH"){
        this.setState({ milestoneToHHMH: (data + 1).toString(), milestoneFromHHMH: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromBlowing"){
        this.setState({ milestoneFromBlowing: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromBlowingMTS"){
        this.setState({ milestoneFromBlowingMTS: data.toString() })
        this.state.selectedData.milestoneFromM = data;
      }else if(fieldName === "milestoneToBlowing"){
        this.setState({ milestoneToBlowing: data })
        this.state.selectedData.milestoneTo = data;
      }else if(fieldName === "milestoneToBlowingMTS"){
        this.setState({ milestoneToBlowingMTS: data.toString() })
        this.state.selectedData.milestoneToM = data;
      }else{
        this.state.selectedData[fieldName] = data;
      }

    }

    milestoneProcessStart = async(value) => {
      db.transaction(
        tx => {
          tx.executeSql(`delete from workdoneActivity;`);
        }
      );

      new LocationTracking().startTrackingInterval(this.milestoneProcessHandler, this.startProgressInterval, this.startTrackingIntervals);
      this.setState({trackingstart: true , errorMsg:'', successMsg: ""});
      this.setState({ milestoneStartStop: value, countedMint: 0, submitBtnDisabled: false });
    };

    async milestoneProcessStop(value=null){
      new LocationTracking().clearIntervalMtd();
      this.setState({ milestoneStartStop: value, submitBtnDisabled: true });
    }

    milestoneProcessHandler = async() => {
      this.setState({ milestoneStartStop: false, submitBtnDisabled: true });
    }

    startProgressInterval = async() => {
      this.setState({ countedMint: this.state.countedMint + 0.05 });
    }

    startTrackingIntervals = async(lattitude, longitude) => {

      if (firstlat === 0 && firstlon === 0) {
        firstlat = this.state.lattitude;
        firstlon = this.state.longitude;
      } else {
        firstlat = lastlat;
        firstlon = lastlon;
      }

      lastlat = lattitude;
      lastlon = longitude;

      this.getDistance(firstlat, firstlon, lastlat, lastlon);
    }

  // CALCULATE DISTANCE FROM STARTING
    getDistance = (lat, lon, lat2, long2) => {
      let R = 6371;
      let dLat = (lat2 - lat) * Math.PI / 180;
      let dLon = (long2 - lon) * Math.PI / 180;
      let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(lat * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);
      let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      let d = R * c;
      if (parseFloat(d * 1000, 2) >= 2) {
        let distance = this.state.distance + Number(parseFloat(d * 1000, 2).toFixed(2));
        let dist = distance;
        this.setState({ distance: dist })

      }
    }

  // USING FOLLOWING COMPONENT TO VALIDATE FORM
    fnSubmitValidateForm = async() => {
      this.getCurrentLatLong();
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      this.setState({ isLoading: true });
      let milst_frm_validation = parseFloat(selectedData.milestoneFrom+"."+selectedData.milestoneFromM);
      let milst_to_validation = parseFloat(selectedData.milestoneTo+"."+selectedData.milestoneToM);

      let chainageFrom = parseInt(selectedData.chainageFrom);
      let chainageTo = parseInt(selectedData.chainageTo);

      if(selectedData.district === '' || selectedData.taluka === ''){
        this.setState({ errorMsg: "Please select Zone and Mandal.", successMsg: "", isLoading: false });
      }else if(selectedData.span === ''){
        this.setState({ errorMsg: "Please select Span.", successMsg: "", isLoading: false });
      }else if(selectedData.activity === 'Infra/Equipment Readiness' && selectedData.gPName === ''){
        this.setState({ errorMsg: "Please select PoP Name.", successMsg: "", isLoading: false });
      }else if(selectedData.activity === ''){
        this.setState({ errorMsg: "Please select Acivity.", successMsg: "", isLoading: false });
      }else if( (chainageTo < chainageFrom || chainageTo === chainageFrom ) && selectedData.activity === 'T&D'){
        this.setState({ errorMsg: "Chainage End value must be higher than Chaninage From value.", successMsg: "", isLoading: false });
      }else if (( chainageTo - chainageFrom ) > 1000) {
        this.setState({ errorMsg: "You are only allowed to enter up to 1000 meters Chainage.", successMsg: "", isLoading: false });
      }else if ( selectedData.excavationType === '' && selectedData.activity === 'T&D' && this.state.activityDisabled === false ) {
        this.setState({ errorMsg: "Please select Excavation Type.", successMsg: "", isLoading: false });
      }else if ( (selectedData.issues === '' || selectedData.issues.length === 0) && this.state.activityDisabled === true ) {
        this.setState({ errorMsg: "Please select at least one Issue.", successMsg: "", isLoading: false });
      }else if((selectedData.fiberReadingFrom === '' || selectedData.fiberReadingTo === '') && selectedData.activity === 'Blowing'){
        this.setState({ errorMsg: "Please enter Fiber reading From and Fiber reading To.", successMsg: "", isLoading: false });
      }else if(selectedData.fiberType === '' && selectedData.activity === 'Blowing'){
        this.setState({ errorMsg: "Please select Fiber Type.", successMsg: "", isLoading: false });
      }else if(chainageFrom === 0 && selectedData.activity === 'Chamber installation'){
        this.setState({ errorMsg: "Please change Chainage Point.", successMsg: "", isLoading: false });
      }else if((selectedData.fiberReadingIn === '' || selectedData.fiberReadingOut === '') && selectedData.activity === 'Chamber installation' && this.state.activityDisabled === false ){
        this.setState({ errorMsg: "Please select Fiber Reading In and Fiber Reading Out.", successMsg: "", isLoading: false });
      }else if(( (selectedData.lattitude === '' || selectedData.lattitude === 0.000) || (selectedData.longitude === '' || selectedData.longitude === 0.000)) && selectedData.activity === 'Chamber installation'){
        this.setState({ errorMsg: "Please select Latitude and Longitude.", successMsg: "", isLoading: false });
      }else if ( (milst_frm_validation > milst_to_validation || milst_frm_validation === milst_to_validation) && selectedData.activity === 'Blowing'  ) {
        this.setState({ errorMsg: "Milestone To value must be higher than Milestone From value.", successMsg: "", isLoading: false });
      } else if(((this.state.activityTD && !this.state.trackingstart) && this.state.activityTD )){
          this.setState({ errorMsg: "Please start tracking before submit the form.", successMsg: "", isLoading: false });
      } else {
        let apiCall = '';
        // CALLING APIS ACCORDING TO ISSUES REPORTES SELECTION
        if(this.state.activityDisabled === true){
          apiCall = this.config.CheckWipTransAPI+"/0";
        }else{
          apiCall = this.config.CheckTransAPI+"/0";
        }

       // LET'S CALL API
        let response = await fetch(apiCall, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: selectedData
          })
        });


        try {
          // GOT SUCCESSFULLY RESPONSE FROM API
          responseJson = await response.json();

          console.log("Resp- ", responseJson)
          if(responseJson.data[0].value === ''){
            this.setState({ errorMsg: "", successMsg: "" });
            // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
            this.submitFormData();
          }else{

            let respVal1 = responseJson.data[0].value;
            respVal = respVal1.split('-');

            // RESPONSE BASED ERROR MESSAGES
            if(respVal[0] === 'range'){

              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal[1]+' to '+respVal[2]+', please select different range of Chainage Start & End Point.', successMsg: "", isLoading: false });

            }else{

              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal1+', You have not permission more than 1000 with these entries. \n\nPlease enter with another entry!', successMsg: "", isLoading: false });

            }

          }
        } catch (error) {
          alert(error);
          this.setState({ errorMsg:"", successMsg: "", isLoading: false });
        }

      }
    }

    // FOLLOWING COMPONENT USING TO SUBMIT FORM
    submitFormData = async() => {

      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;
      console.log("selectedData - ", selectedData)

      let response = await fetch(this.config.submitFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        responseJson = await response.json();
        console.log("Response - ", responseJson)

        // CALLING FOLLOWING CLASS WITH METHOD TO SUBMIT TRACKED DATA IN DB
        new LocationTracking().storeTrackedData(responseJson.data[0].inserted_record_id);
        alert(responseJson.data[0].success);
        this.setState({ successMsg: responseJson.data[0].success, isLoading: false });
      } catch (error) {
        alert(error);
        console.log(error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }

    }


    render() {

      let ringType = [{
        "value": 'Zonal Ring',
      }, {
        "value": 'Mandal Ring',
      }, {
        "value": 'GP Ring',
      }];

      let issuesReport = [{
        "value": "No"
      },{
        "value": "Yes"
      }]
      
      let excavationType = [{
        "value": "Open Trunch (excavator)"
      },{
        "value": "Open Trunch (Mannual)"
      },{
        "value": "HDD with armor rod"
      },{
        "value": "HDD with Rock breaker"
      }]

      let soilStrata = [{
        "value": "Hard"
      },{
        "value": "Mix"
      },{
        "value": "Soft"
      },{
        "value": "Sheet-Rock"
      }];

      let protection = [{
        "value": "GI"
      },{
        "value": "DWC"
      },{
        "value": "RCC"
      }];

      let blowingType = [{
        "value": "Green field"
      },{
        "value": "Brown field"
      }]

      let fiberType = [{
        "value": "24F"
      },{
        "value": "48F"
      },{
        "value": "96F"
      }]

      let splicingTermination = [{
        "value": "1"
      },{
        "value": "2"
      },{
        "value": "3"
      },{
        "value": "4"
      },{
        "value": "5"
      }]

      let pOpName = this.state.splitedGPName;

      let chamberType = [{
        "value": "HH"
      },{
        "value": "MM"
      }]

      let machineResourceData = [];
      for(let i=1; i<11; i++){
        machineResourceData.push({
          "value": i,
        })
      }

      let machineResLbData = [];
      for(let i=1; i<51; i++){
        machineResLbData.push({
          "value": i,
        })
      }

      let popTypeVal = [{
        "value": "YES"
      },{
        "value": "NO"
      }]

      let popTypeVal1 = [{
        "value": "YES"
      },{
        "value": "NO"
      },{
        "value": "NA"
      }]

      const { milestoneChecked, selectedData, IssuesChecked, mandalResource } = this.state;
      return (
          <Container style={styles.Container}>

            {/* START SECTION */}
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >

                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Date</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <DatePicker
                          style={{width: 200 }}
                          // date={this.state.date}
                          mode="date"
                          date= {this.state.slctDateVal}
                          format="YYYY-MM-DD"
                          minDate={fourDaysBack}
                          maxDate={new Date()}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          iconSource = {require("../../assets/images/date.png")}
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              marginLeft: 36,
                              backgroundColor: "#f0f0f0",
                              borderWidth: 0,
                              color: "#888888",
                            }
                          }}
                          onDateChange={(value) => this.getData(value, "slctDate")}
                        />
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Zone</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allDistrict}
                            // dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "District")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Mandal</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allTaluka}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "Taluka")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Span</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allSpan}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "Span")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 0.50, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Span ID</Text>
                        </View>
                        <View style ={{ flex: 0.50, paddingBottom: 5, paddingLeft: 15 }}>
                          <Text style={{ fontSize: 16 }}>Span Length</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "49%" }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.spanId}
                            editable = {false}
                          />
                        </View>
                        <View style ={{ width: "50%", paddingLeft: 10 }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.spanLength}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 0.50, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>From</Text>
                        </View>

                        <View style ={{ flex: 0.50, paddingBottom: 5, paddingLeft: 15 }}>
                          <Text style={{ fontSize: 16 }}>To</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "50%" }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.locationFrom}
                            editable = {false}
                          />
                        </View>
                        <View style ={{ width: "50%", paddingLeft: 10 }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.locationTo}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 0.50, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Ring Type</Text>
                        </View>

                        <View style ={{ flex: 0.50, paddingBottom: 5, paddingLeft: 15 }}>
                          <Text style={{ fontSize: 16 }}>Ring No</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "50%" }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.rignTypeVal}
                            editable = {false}
                          />
                        </View>
                        <View style ={{ width: "50%", paddingLeft: 10 }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.ringNoVal}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <Text style={{ fontSize: 16 }}>Primary Vendor</Text>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.primaryVendor}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <Text style={{ fontSize: 16 }}>3rd Party Vendor</Text>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "third_vendor")}
                            // editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                         <Text style={{ fontSize: 16 }}>Is main Vendor details OK?</Text>
                          <CheckBox
                            color= {"#1ab679"}
                            checked={(this.state.vendorDetails)?true:false}
                            onPress={() => {
                              this.setState({ vendorDetails: !this.state.vendorDetails });
                              (this.state.vendorDetails)?this.state.selectedData.mainVender = 'Yes':this.state.selectedData.mainVender = 'No';
                            }}
                          />
                          <Text style={{ fontSize: 16, paddingLeft: 15 }}>Yes</Text>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <Text style={{ fontSize: 16 }}>Is Mandal Resource changed?</Text>
                      </View>                    
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        
                        <View style ={{ flex: 0.5, flexDirection: "row" }}>
                          <View style={{ flex: 0.22 }}>
                            <Radio
                              selectedColor= {"#1ab679"}
                              standardStyle={true}
                              style={{ paddingLeft: 6, paddingTop: 4 }}
                              selected={mandalResource === 'Yes' ? true : false}
                              onPress={() => {
                                this.setState({ activeMachineResource: true });
                                this.setState({ mandalResource: 'Yes' });
                                this.state.selectedData.mandalResource = 'Yes';
                              }}
                            />
                          </View>
                          <View style={{ flex: 0.50, paddingLeft: 5, paddingRight: 1 }}>
                            <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                              Yes
                            </Text>
                          </View>
                        </View>

                        <View style ={{ flex: 0.5, flexDirection: "row" }}>
                          <View style={{ flex: 0.22, alignSelf: "flex-start" }}>
                            <Radio
                              selectedColor= {"#1ab679"}
                              standardStyle={true}
                              style={{ paddingLeft: 6, paddingTop: 4, alignSelf: "flex-start" }}
                              selected={mandalResource === 'No' ? true : false}
                              onPress={() => {
                                this.setState({ mandalResource: 'No' });
                                this.setState({ activeMachineResource: false });
                                this.state.selectedData.mandalResource = 'No';                              
                              }}
                            />
                          </View>
                          <View style={{ flex: 0.55, paddingLeft: 5, paddingRight: 1, alignSelf: "flex-start" }}>
                            <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >No</Text>
                          </View>
                        </View>

                      </View>                      
                    </View>

                    {
                      (this.state.activeMachineResource)?(
                        <View>
                          {/* PLANNED SECTION IN MACHINE RESOURCE */}
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16, fontWeight: "600" }}>Planned</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>JCB</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedJCB")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>HDD</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedHDD")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Poklane</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedPoklane")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedRockBreaker")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Compressor</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedExcavator")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Duct Rodder</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedBackhoeLoader")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Blowing</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedBlowing")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Splicing Team</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedSlicer")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Labour</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResLbData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedManpower")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          {/* ACTUAL SECTION IN MACHINE RESOURCE */}

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16, fontWeight: "600" }}>Actual</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>JCB</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualJCB")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>HDD</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualHDD")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Poklane</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualPoklane")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualRockBreaker")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Compressor</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualExcavator")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Duct Rodder</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualBackhoeLoader")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Blowing</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualBlowing")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Splicing Team</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualSlicer")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Labour</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResLbData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualManpower")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>
                          
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 20, fontWeight: "bold" }}>Civil Work</Text>
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Activity</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.activities}
                            dropdownPosition={1}
                            value={this.state.activity}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "activity")}
                            disabled= {this.state.activityDisabled}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    {
                      (this.state.activityChmbrInst)?(
                        <View style ={ styles.paddingBottomMore }>
                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Chamber Type</Text></View>
                          </View>

                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ width: "100%" }}>
                              <Dropdown
                                label= ""
                                data={chamberType}
                                dropdownPosition={0}
                                dropdownOffset = {{ top: 8, left: 0 }}
                                onChangeText={(value) => this.getData(value, "chamberType")}
                                error = ""
                              />
                            </View>
                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Issues Report</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={issuesReport}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.IssuesReportValue}
                            onChangeText={(value) => this.getData(value, "IssuesReport")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    {
                      (this.state.activityTD === true || this.state.activityBlowing === true)?(
                        <View style ={ styles.paddingBottomMore }>
                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ flex: 1, paddingBottom: 5 }}>
                              <Text style={{ fontSize: 16 }}>Field Type</Text>
                            </View>
                          </View>

                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ width: "100%" }}>
                              <Dropdown
                                label= ""
                                data={blowingType}
                                dropdownPosition={0}
                                dropdownOffset = {{ top: 8, left: 0 }}
                                onChangeText={(value) => this.getData(value, "fieldType")}
                                error = ""
                              />
                            </View>
                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.activityTDDrtBookNo)?(
                        <View style ={ styles.paddingBottomMore }>
                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ flex: 1, paddingBottom: 5 }}>
                              <Text style={{ fontSize: 16 }}>DRT Book No</Text>
                            </View>
                          </View>

                          <View style ={{ flex:1, flexDirection:'row', }}>
                            <View style ={{ width: "100%" }}>
                              <TextInput
                                style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                                onChangeText={(value) => this.getData(value, "drt_book_num")}
                                // editable = {false}
                              />
                            </View>
                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.activityTD === true || this.state.activityChmbrInst === true )?(
                        <View>
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                              <View style ={{ flex: 1, paddingBottom: 8 }}>
                                <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                              </View>
                            </View>


                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= "Milestone From"
                                  data={this.state.milestoneSpansLen}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  value={this.state.milestoneFromTD}
                                  onChangeText={(value) => this.getData(value, "milestoneFromTD")}
                                  error = ""
                                />
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ width: "100%" }}>
                                <Text style={{ color: "#999", fontSize: 10 }}>Milestone To</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:6 }}>
                              <View style ={{ width: "100%" }}>
                                <TextInput
                                  style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value={this.state.milestoneToTD}
                                  editable = {false}
                                />
                              </View>
                            </View>

                            {
                              (this.state.activityTD)?(
                                <View> 
                                  <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                                    <View style ={{ flex: 1, paddingBottom: 8 }}>
                                      <Text style={{ fontSize: 16 }}>Location Tracking:</Text>
                                    </View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row', }}>
                                    <View style={ styles.milestoneView }>

                                      <View style={{ flex: 0.5 }}>
                                        <TouchableOpacity style={[ styles.milestoneBtns, (this.state.milestoneStartStop)? { backgroundColor: "#fff" }: {}]} onPress={() => this.milestoneProcessStart(true)}>
                                          <Text style={[ styles.milestoneBtnsTxt, (this.state.milestoneStartStop)? { }:{ color: "#FFFFFF" }]}>Start</Text>
                                        </TouchableOpacity>
                                      </View>

                                      <View style={{ flex: 0.5 }}>
                                        <TouchableOpacity style={[styles.milestoneBtns, (!this.state.milestoneStartStop)? { backgroundColor: "#fff" }: {}]} onPress={() => this.milestoneProcessStop(false)}>
                                          <Text style={[ styles.milestoneBtnsTxt, (!this.state.milestoneStartStop)? {}: { color: "#FFFFFF" }]}>Stop</Text>
                                        </TouchableOpacity>
                                      </View>

                                    </View>
                                  </View>
                                  <View style ={{ flex:1, flexDirection:'column', paddingTop: 10 }}>
                                    <Text style={{ color: "#333", fontSize: 16, fontWeight:"900"}}>
                                      Distance - {this.state.distance} meters
                                    </Text>
                                  </View>

                                  {
                                    (this.state.milestoneStartStop)?(
                                      <View>
                                        <View style ={{ flex:1, flexDirection:'column', paddingTop: 10 }}>

                                          <Text style={{ color: "#333", fontSize: 16, fontWeight:"900"}}>
                                            Progress -
                                          </Text>
                                        </View>
                                        <View style ={{ flex:1, flexDirection:'column', paddingTop: 10 }}>
                                          <Progress.Bar progress={this.state.countedMint} animated={true} animationType="spring" width={screenWidth-30} height={10} />
                                        </View>
                                      </View>
                                    ):(
                                      <View></View>
                                    )
                                  }
                                </View>
                              ):(
                                <View></View>
                              )
                            }

                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.activityTD)?(
                        <View>
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 10 }}>
                                <Text style={{ fontSize: 16 }}>Chainage (MTS)</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row' }}>
                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.25 }}>
                                  <Text style={{ height: 40, textAlignVertical: "center" }} >Start</Text>
                                </View>
                                <View style={{ flex: 0.75 }}>
                                  <TextInput
                                    style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.chainageFrom}
                                    onChangeText={(value) => this.getData(value, "ChainageStart")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>

                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.25 }}>
                                  <Text style={{ height: 40, textAlignVertical: "center", textAlign: "right" }} >End</Text>
                                </View>
                                <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                                  <TextInput
                                    style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.chainageTo}
                                    onChangeText={(value) => this.getData(value, "ChainageEnd")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Soil strata</Text></View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={soilStrata}
                                  dropdownPosition={0}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "SoilStrata")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.activityBlowing)?(
                        <View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                              <View style ={{ flex: 1, paddingBottom: 8 }}>
                                <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                              </View>
                            </View>


                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ flex: 0.7,  }}>
                                <Dropdown
                                  label= "Milestone From"
                                  data={this.state.milestoneSpansLen}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 33, left: 0 }}
                                  value={this.state.milestoneFromBlowing}
                                  onChangeText={(value) => this.getData(value, "milestoneFromBlowing")}
                                  error = ""
                                />
                              </View>
                              <View style ={{ flex: 0.3, paddingLeft: 10 }}>
                                <TextField
                                  label='MTS'
                                  keyboardType='default'
                                  autoCapitalize='none'
                                  autoCorrect={false}
                                  enablesReturnKeyAutomatically={true}
                                  returnKeyType='next'
                                  value ={this.state.milestoneFromBlowingMTS}
                                  onChangeText={(value) => this.getData(value, "milestoneFromBlowingMTS")}
                                  error	= ""
                                />
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ flex: 0.7,  }}>
                                <Dropdown
                                  label= "Milestone To"
                                  data={this.state.milestoneSpansLen}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 33, left: 0 }}
                                  value={this.state.milestoneToBlowing}
                                  onChangeText={(value) => this.getData(value, "milestoneToBlowing")}
                                  error = ""
                                />
                              </View>
                              <View style ={{ flex: 0.3, paddingLeft: 10 }}>
                                <TextField
                                  label='MTS'
                                  keyboardType='default'
                                  autoCapitalize='none'
                                  autoCorrect={false}
                                  enablesReturnKeyAutomatically={true}
                                  returnKeyType='next'
                                  value ={this.state.milestoneToBlowingMTS}
                                  onChangeText={(value) => this.getData(value, "milestoneToBlowingMTS")}
                                  error	= ""
                                />
                              </View>
                            </View>
                          </View>
                        </View>
                      ):(
                        <View></View>
                      )
                    }
                    
                    {
                      (this.state.activityChmbrInst)?(
                        <View>
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom: 10 }}>
                              <Text style={{ fontSize: 16 }}>Chainage Point (MTS)</Text>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row' }}>
                              <TextInput
                                style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                value={this.state.chainageFrom}
                                onChangeText={(value) => this.getData(value, "ChainageStart")}
                                keyboardType='phone-pad'
                              />
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>

                            <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                              <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong()}>
                                  <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                              </TouchableOpacity>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                <Text style={{ fontSize: 16 }}>Lattitude</Text>
                              </View>
                              <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                <Text style={{ fontSize: 16 }}>Longitude</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex:0.5, marginRight: 15 }}>
                                <TextInput
                                  style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value= {this.state.lattitude}
                                  // onChangeText={(value) => this.getData(value, "lattitude")}
                                  editable = {false}
                                />
                              </View>

                              <View style ={{ flex:0.5 }}>
                                <TextInput
                                  style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value= {this.state.longitude}
                                  // onChangeText={(value) => this.getData(value, "longitude")}
                                  editable = {false}
                                />
                              </View>
                            </View>
                          </View>


                          {/* <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>GP Name</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={gPName}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "gPName")}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View> */}
                          
                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    
                    {
                      (this.state.activityInfraEquipment)?(
                        <View>
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>PoP name</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={pOpName}
                                  dropdownPosition={0}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  value={this.state.pOpNameValue}
                                  onChangeText={(value, index) => this.popDataChanges(value, index)}
                                  error = ""
                                />
                              </View>
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom: 5}}>
                              <Text style={{ fontSize: 16 }}>PoP Type</Text>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ width: "100%" }}>
                                <TextInput
                                  style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value={this.state.pOpType}
                                  editable = {false}
                                />
                              </View>
                            </View>
                          </View>


                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.IssuesReportChange)?(
                      <View>
                        {
                          (this.state.activityTD)?(
                            <View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <Text style={{ fontSize: 16 }}>Excavation Type</Text>
                                </View>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={excavationType}
                                      dropdownPosition={0}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "excavationType")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>

                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', paddingBottom: 10 }}>
                                  <Text style={{ fontSize: 16 }}>Approx Avg Depth (MTS)</Text>
                                </View>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.approxAvgDepth}
                                    onChangeText={(value) => this.getData(value, "approx_Avg_Depth")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>

                              <View style ={ styles.paddingBottomMore }>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 10 }}>
                                    <Text style={{ fontSize: 16 }}>Milestone status</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row' }}>
                                  <View style={{ flex: 0.5, flexDirection:'row' }}>
                                    <View style={{ flex: 0.23 }}>
                                      <Radio
                                        selectedColor= {"#1ab679"}
                                        standardStyle={true}
                                        style={{ paddingLeft: 6, paddingTop: 4 }}
                                        selected={ milestoneChecked === 'WIP' ? true : false }
                                        onPress={() => {
                                          this.setState({ milestoneChecked: 'WIP' })
                                          selectedData.milestoneStatus = "WIP";
                                        }}
                                      />
                                    </View>
                                    <View style={{ flex: 0.77 }}>
                                      <TouchableOpacity activeOpacity={1} onPress={() => {
                                        this.setState({ milestoneChecked: 'WIP' })
                                        selectedData.milestoneStatus = "WIP";
                                      }}>
                                        <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                          WIP (In Progress)
                                        </Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>

                                  <View style={{ flex: 0.5, flexDirection:'row' }}>
                                    <View style={{ flex: 0.23 }}>
                                      <Radio
                                        selectedColor= {"#1ab679"}
                                        standardStyle={true}
                                        style={{ paddingLeft: 6, paddingTop: 4 }}
                                        selected={milestoneChecked === 'Complete' ? true : false}
                                        onPress={() => {
                                          this.setState({ milestoneChecked: 'Complete' });
                                          selectedData.milestoneStatus = "Complete";
                                        }}
                                      />
                                    </View>
                                    <View style={{ flex: 0.77 }}>
                                      <TouchableOpacity activeOpacity={1} onPress={() => {
                                        this.setState({ milestoneChecked: 'Complete' });
                                        selectedData.milestoneStatus = "Complete";
                                      }} >
                                        <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Complete</Text>
                                      </TouchableOpacity>
                                    </View>
                                  </View>
                                </View>

                              </View>

                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Protection</Text></View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={protection}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "protection")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View> 
                            </View>
                          ):(this.state.activityBlowing)?(
                            <View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                    <Text style={{ fontSize: 16 }}>Fiber Reading From</Text>
                                  </View>
                                  <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                    <Text style={{ fontSize: 16 }}>Fiber Reading To</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex:0.5, marginRight: 15 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value= {this.state.fiberReadingFrom}
                                      onChangeText={(value) => this.getData(value, "fiberReadingFrom")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>

                                  <View style ={{ flex:0.5 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value= {this.state.fiberReadingTo}
                                      onChangeText={(value) => this.getData(value, "fiberReadingTo")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>
                                </View>
                              </View>

                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Fiber Type</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={fiberType}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "fiberType")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          ):(this.state.activityChmbrInst)?(
                            <View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                    <Text style={{ fontSize: 16 }}>Fiber Reading In</Text>
                                  </View>
                                  <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                    <Text style={{ fontSize: 16 }}>Fiber Reading Out</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex:0.5, marginRight: 15 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      onChangeText={(value) => this.getData(value, "fiberReadingIn")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>

                                  <View style ={{ flex:0.5 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      onChangeText={(value) => this.getData(value, "fiberReadingOut")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          ):(this.state.activityInfraEquipment)?(
                            <View>
                              {
                                (this.state.popTypeGP)?(
                                  <View>
                                    <View style ={ styles.paddingBottomMore }>
                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                                          <Text style={{ fontSize: 16 }}>
                                            Active Equipment Installation & Power
                                          </Text>
                                        </View>
                                      </View>
      
                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ width: "100%" }}>
                                          <Dropdown
                                            label= ""
                                            data={popTypeVal}
                                            dropdownPosition={0}
                                            dropdownOffset = {{ top: 8, left: 0 }}
                                            onChangeText={(value) => this.getData(value, "pop_active_equipment_inst_power")}
                                            error = ""
                                          />
                                        </View>
                                      </View>
                                    </View>
                                    
                                    <View style ={ styles.paddingBottomMore }>
                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                                          <Text style={{ fontSize: 16 }}>
                                            Infra Installation & Power
                                          </Text>
                                        </View>
                                      </View>

                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ width: "100%" }}>
                                          <Dropdown
                                            label= ""
                                            data={popTypeVal}
                                            dropdownPosition={0}
                                            dropdownOffset = {{ top: 8, left: 0 }}
                                            onChangeText={(value) => this.getData(value, "pop_infra_installation_power")}
                                            error = ""
                                          />
                                        </View>
                                      </View>
                                    </View>

                                  </View>
                                ):(
                                  <View></View>
                                )
                              }


                              {
                                (this.state.popTypeMandal === true || this.state.popTypeZone === true)?(
                                  <View>
                                    <View style ={ styles.paddingBottomMore }>
                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                                          <Text style={{ fontSize: 16 }}>
                                            Active Equipment & Infa Installation
                                          </Text>
                                        </View>
                                      </View>
                                      <View style ={{ flex:1, flexDirection:'row', }}>
                                        <View style ={{ width: "100%" }}>
                                          <Dropdown
                                            label= ""
                                            data={popTypeVal}
                                            dropdownPosition={0}
                                            dropdownOffset = {{ top: 8, left: 0 }}
                                            onChangeText={(value) => this.getData(value, "pop_active_equipment_infa_installation")}
                                            error = ""
                                          />
                                        </View>
                                      </View>
                                    </View>
                                    
                                  {
                                    (this.state.popTypeMandal)?(
                                      <View>
                                        <View style ={ styles.paddingBottomMore }>
                                          <View style ={{ flex:1, flexDirection:'row', }}>
                                            <View style ={{ flex: 1, paddingBottom: 5 }}>
                                              <Text style={{ fontSize: 16 }}>
                                                Last Mile connectivity
                                              </Text>
                                            </View>
                                          </View>
                                          <View style ={{ flex:1, flexDirection:'row', }}>
                                            <View style ={{ width: "100%" }}>
                                              <Dropdown
                                                label= ""
                                                data={popTypeVal}
                                                dropdownPosition={0}
                                                dropdownOffset = {{ top: 8, left: 0 }}
                                                onChangeText={(value) => this.getData(value, "pop_last_mile_connectivity")}
                                                error = ""
                                              />
                                            </View>
                                          </View>
                                        </View>
                                        <View style ={ styles.paddingBottomMore }>
                                          <View style ={{ flex:1, flexDirection:'row', }}>
                                            <View style ={{ flex: 1, paddingBottom: 5 }}>
                                              <Text style={{ fontSize: 16 }}>
                                                BSNL Exchange Connectivity
                                              </Text>
                                            </View>
                                          </View>
                                          <View style ={{ flex:1, flexDirection:'row', }}>
                                            <View style ={{ width: "100%" }}>
                                              <Dropdown
                                                label= ""
                                                data={popTypeVal1}
                                                dropdownPosition={0}
                                                dropdownOffset = {{ top: 8, left: 0 }}
                                                onChangeText={(value) => this.getData(value, "pop_bsnl_exchange_connectivity")}
                                                error = ""
                                              />
                                            </View>
                                          </View>
                                        </View>                                    
                                      </View>
                                    ):(
                                      <View></View>
                                    )
                                  }

                                  </View>
                                ):(
                                  <View></View>
                                )
                              }

                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>
                                      Pre AT
                                    </Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={popTypeVal}
                                      dropdownPosition={0}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "pop_pre_at")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>
                                      Commissioning
                                    </Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={popTypeVal}
                                      dropdownPosition={0}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "pop_commissioning")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>
                              
                              
                            </View>
                          ):(
                            <View></View>
                          )
                        }

                        {
                          (this.state.activityBlowing === true || this.state.activityChmbrInst === true)?(
                            <View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>
                                      Splicing {(this.state.activityBlowing)?"Identification":"Done"}
                                    </Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={splicingTermination}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "splicing")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>

                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>
                                      Termination {(this.state.activityBlowing)?"Identification":"Done"}
                                    </Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={splicingTermination}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "termination")}
                                      error = ""
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          ):(
                            <View></View>
                          )
                        }
                      </View>
                      ):(
                        <View>
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 20, fontWeight: "bold" }}>Issues</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>

                            <View style ={{ flex:1, flexDirection:'row' }}>
                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.23 }}>
                                  <Radio
                                    selectedColor= {"#1ab679"}
                                    standardStyle={true}
                                    style={{ paddingLeft: 6, paddingTop: 4 }}
                                    selected={(IssuesChecked === 'Internal')? true : false}
                                    onPress={() => {
                                      this.setState({ IssuesChecked: 'Internal' });
                                      selectedData["issuesType"] = "Internal";
                                      selectedData["issues"] = "";
                                      this.state.internalIssues.map((item, i)=>{
                                          item.isSelect = false;
                                      })
                                    }}
                                  />
                                </View>
                                <View style={{ flex: 0.77 }}>
                                  <TouchableOpacity activeOpacity={1} onPress={() => {
                                    this.setState({ IssuesChecked: 'Internal' });
                                    selectedData["issuesType"] = "Internal";
                                    selectedData["issues"] = "";
                                    this.state.internalIssues.map((item, i)=>{
                                        item.isSelect = false;
                                    })
                                  }}>
                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                      Internal Issues
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              </View>

                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.23 }}>
                                  <Radio
                                    selectedColor= {"#1ab679"}
                                    standardStyle={true}
                                    style={{ paddingLeft: 6, paddingTop: 4 }}
                                    selected={(IssuesChecked === 'Customer')? true : false}
                                    onPress={() => {
                                      this.setState({ IssuesChecked: 'Customer' });
                                      selectedData["issuesType"] = "Customer";
                                      selectedData["issues"] = "";
                                      this.state.customerIssues.map((item, i)=>{
                                          item.isSelect = false;
                                      })
                                    }}
                                  />
                                </View>
                                <View style={{ flex: 0.77 }}>
                                  <TouchableOpacity activeOpacity={1} onPress={() => {
                                    this.setState({ IssuesChecked: 'Customer' });
                                    selectedData["issuesType"] = "Customer";
                                    selectedData["issues"] = "";
                                    this.state.customerIssues.map((item, i)=>{
                                        item.isSelect = false;
                                    })
                                  }} >
                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Customer Issues</Text>
                                  </TouchableOpacity>
                                </View>
                              </View>
                            </View>

                          </View>

                          <View style ={ styles.paddingBottomMore }>

                            <View style ={{ flex:1, flexDirection:'row', }}>
                              <View style ={{ flex: 1, paddingBottom: 10 }}>
                                <Text style={{ fontSize: 16 }}>Type of Issue</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row' }}>

                              {
                                // INTERNAL ISSUES LIST
                                (IssuesChecked === "Internal")?(
                                  <FlatList
                                    data={ this.state.internalIssues }
                                    extraData={this.state}
                                    renderItem={({item}) => {
                                      return(
                                        <View style={{ flex: 1, flexDirection: "row" }}>

                                          <View style={{ paddingRight: 16, paddingTop: 5 }}>
                                            <CheckBox
                                              color= {(item.isSelect)? "#1ab679" : "#000" }
                                              checked= {(item.isSelect)? true : false }
                                              onPress={() => this.internalIssuesCheckbox(item.value)}
                                            />
                                          </View>
                                          <View style={{ flex: 1, paddingLeft: 5, paddingRight: 1 }}>
                                            <TouchableOpacity activeOpacity={1}>
                                              <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                                {item.value}
                                              </Text>
                                            </TouchableOpacity>
                                          </View>

                                        </View>
                                      )
                                    }
                                    }
                                    keyExtractor={item => item.value}
                                    numColumns={2}
                                  />

                                ):( // CUSTOMER ISSUES LIST

                                  <FlatList
                                    data={ this.state.customerIssues }
                                    extraData={this.state}
                                    renderItem={({item}) =>
                                      <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={{ paddingRight: 16, paddingTop: 5 }}>
                                          <CheckBox
                                            color= {(item.isSelect)? "#1ab679" : "#000" } 
                                            checked= {(item.isSelect)? true : false }
                                            onPress={() => this.customerIssuesCheckbox(item.value)}
                                          />
                                        </View>
                                        <View style={{ flex: 1, paddingLeft: 5 }}>
                                          <TouchableOpacity activeOpacity={1}>
                                            <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                              {item.value}
                                            </Text>
                                          </TouchableOpacity>
                                        </View>

                                      </View>

                                    }
                                    keyExtractor={item => item.value}
                                    numColumns={2}
                                  />
                                )
                              }

                            </View>

                          </View>

                        </View>
                      )
                    }

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Remarks</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Textarea
                            containerStyle={{ height: 160, padding: 10, backgroundColor: '#f0f0f0' }}
                            style={{textAlignVertical: 'top', height: 150, fontSize: 14, color: '#8e8e8e',}}
                            onChangeText={(value) => this.getData(value, "Remarks")}
                            // defaultValue={this.state.text}
                            maxLength={200}
                            placeholder={'Comment About Work'}
                            placeholderTextColor={'#8e8e8e'}
                            underlineColorAndroid={'transparent'}
                          />
                        </View>
                      </View>
                    </View>


                  </View>

                  <KeyboardSpacer topSpacing ={5} />

                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 5 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                  </View>

                  {
                    (this.state.submitBtnDisabled)?(
                      <View>
                        {
                          (this.state.isLoading)?
                          (
                            <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                              <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                            </TouchableOpacity>
                          ):(
                            <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                              <Text style={styles.loadMorebtnTxt}>Submit</Text>
                            </TouchableOpacity>
                          )
                        }
                      </View>
                    ):(
                      <View>
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", paddingHorizontal: 10  }}>Stop Location Tracking to save your report.</Text>
                        <TouchableOpacity activeOpacity={1} style={[styles.loadMorebtn, { backgroundColor: "gray" }]} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                      </TouchableOpacity>
                      </View>
                    )
                  }

                </View>
              </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }


    static navigationOptions = ({navigation}) => {
      return {
        title: 'Engineer Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center",
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  }
}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    paddingBottomMore: {
      paddingBottom: 15
    },

    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }

  });

export default EngineerForm;
