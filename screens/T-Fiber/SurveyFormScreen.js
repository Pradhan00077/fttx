import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, TextInput, AsyncStorage, ActivityIndicator} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import * as Network from 'expo-network';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Database from '../../Database';
import { MaterialIcons } from '@expo/vector-icons';
const db = new Database();
import {BASE_URL} from '../../basepath'
const screenHeight = Math.round(Dimensions.get('window').height);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

class EngineerForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        DistrictAPI: BASE_URL + "apis/userDistrictFilter",
        TalukaAPI: BASE_URL + "apis/userTalukaFilter",
        SpanLength: BASE_URL + "apis/surveySpanLength",

        checkLastRecordAPI: BASE_URL + "apis/checkLastRecordData",
        checkLastRecordsAPI: BASE_URL + "apis/checkLastRecordDatas",

        submitFormAPI: BASE_URL + "apis/surveyFormDataSubmit",
      }
      this.state = {
        isLoading: false,
        allZone: [],
        allMandal: [],
        zoneVal: '',
        mandalVal: '',
        mandalLength: "0",
        userName: 0,
        userRole: 0,
        userLoggedInId: 0,
        physical_survey: 'No',
        design_boq_report: 'No',
        customer_approval: 'No',
        physical_survey_disabled: false,
        design_boq_report_disabled: false,
        customer_approval_disabled: false,
        row_application: "",
        demand_note: "",
        permission: "",

        selectedData: {
          zone: "",
          mandal: "",
          row_application: "",
          demand_note: "",
          permission: "",
          physical_survey: 'No',
          design_boq_report: 'No',
          customer_approval: 'No',
          slctDate: todayDate
        }
      }
    }

    componentDidMount(){
      this.fetchUserDataState();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    // CALLING API TO GET AND CHECK
    fetchLastRecrdState = async(zone, mandal) => {
      let responseJson = [];
      
      let response = await fetch(this.config.checkLastRecordAPI+"/"+zone+"/"+mandal);
      try {
        responseJson = await response.json();
        
        //Successful response from the API Call
        let splitRs = responseJson.data.split(',');
        let splitRsLen = (splitRs.length)-1;
        let splitRsLen1 = splitRsLen+1;
        let splitRsLen2 = splitRsLen+2;

        if (splitRs[splitRsLen-splitRsLen] == 'physical_survey' || splitRs[splitRsLen1-splitRsLen] == 'physical_survey' || splitRs[splitRsLen2-splitRsLen] == 'physical_survey') {
          this.state.selectedData.physical_survey = "Yes";
          this.setState({ physical_survey: "Yes", physical_survey_disabled: true });
        }else{
          this.setState({ physical_survey: "No", physical_survey_disabled: false });
        }

        if (splitRs[splitRsLen-splitRsLen] == 'design_boq_report' || splitRs[splitRsLen1-splitRsLen] == 'design_boq_report' || splitRs[splitRsLen2-splitRsLen] == 'design_boq_report') {
          this.state.selectedData.design_boq_report = "Yes";
          this.setState({ design_boq_report: "Yes", design_boq_report_disabled: true });
        }else{
          this.setState({design_boq_report: "No", design_boq_report_disabled: false });
        }

        if (splitRs[splitRsLen-splitRsLen] == 'customer_approval' || splitRs[splitRsLen1-splitRsLen] == 'customer_approval' || splitRs[splitRsLen2-splitRsLen] == 'customer_approval') {
          this.state.selectedData.customer_approval = "Yes";
          this.setState({ customer_approval: "Yes", customer_approval_disabled: true });
        }else{
          this.setState({ customer_approval: "No", customer_approval_disabled: false });
        }

        this.setState({ isLoading: false });
      }catch (error) {
        console.log("Response error - ",error);
        this.setState({ isLoading: false });
      }
    }

    fetchUserDataState = async() => {
      this.setState({ isLoading: true })

      const userid = await AsyncStorage.getItem('user_id');
      const usern = await AsyncStorage.getItem('username');
      const userr = await AsyncStorage.getItem('user_role');
      const ipAddress = await Network.getIpAddressAsync();
      // const macAddress = await Network.getMacAddressAsync();

      this.state.selectedData["ipAddress"] = ipAddress;
      // this.state.selectedData["macAddress"] = macAddress;

      this.state.selectedData["user_id"] = userid;
      this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
      this.fetchZoneAPI();
    }

    // CALLING API TO GET ALL DISTRICT
    async fetchZoneAPI(){
      let responseJson = [];
      let response = await fetch(this.config.DistrictAPI+"/"+this.state.userRole+"/"+this.state.userName);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ allZone: responseJson.data, zoneVal: responseJson.data[0].value, allMandal: [], mandalLength: "0" });
        this.fetchMandalAPI(responseJson.data[0].value);
        this.state.selectedData.zone = responseJson.data[0].value;
      }catch (error) {
        console.log("Response error - ",error);
        this.setState({ isLoading: false });
      }
    }

    // CALLING API TO GET ALL TALUKAS
    fetchMandalAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.TalukaAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ allMandal: responseJson.data, mandalVal: responseJson.data[0].value });
        this.fetchMandalLengthAPI(responseJson.data[0].value);
        this.state.selectedData.mandal = responseJson.data[0].value;
      }catch (error) {
        console.log("Response error - ",error);
        this.setState({ isLoading: false });
      }
    }

    // CALLING API TO GET ALL SPAN
    fetchMandalLengthAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.SpanLength+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        this.fetchLastRecrdState(this.state.zoneVal, this.state.mandalVal);
        this.setState({ mandalLength: responseJson.data[0].span_length });
        this.state.selectedData.mandalLength = responseJson.data[0].span_length;
      }catch (error) {
        console.log("Response error - ",error);
        this.setState({ isLoading: false });
      }
    }
    
    // GETING DATA FROM FIELDS AND USING VALIDATION
    async getData(data, fieldName){
      if(fieldName === "zone"){
        this.state.selectedData.zone = data;
        this.fetchMandalAPI(data);
        this.setState({ zoneVal: data, isLoading: true })
      }else if(fieldName === "mandal"){
        this.state.selectedData.mandal = data;
        this.fetchMandalLengthAPI(data);
        this.setState({ mandalVal: data, isLoading: true })
      }else{
        this.state.selectedData[fieldName] = data;
      }
    }

  // USING FOLLOWING COMPONENT TO VALIDATE FORM
    fnSubmitValidateForm = async() => {
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;
      const mndLength = Math.ceil(this.state.mandalLength)+5;
      this.setState({ errorMsg: "", successMsg: "", isLoading: true });

      if(selectedData.row_application > mndLength){
        alert('You are only allowed to enter up to '+mndLength+' in row application.');
        this.setState({ errorMsg: "You are only allowed to enter up to "+mndLength+" in row application.", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.demand_note > mndLength){
        alert('You are only allowed to enter up to '+mndLength+' in demand note.');
        this.setState({ errorMsg: "You are only allowed to enter up to "+mndLength+" in demand note.", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.permission > mndLength){
        alert('You are only allowed to enter up to '+mndLength+' in permission.');
        this.setState({ errorMsg: "You are only allowed to enter up to "+mndLength+" in permission.", successMsg: "", isLoading: false });
        return false;
      }else {
        let response = await fetch(this.config.checkLastRecordsAPI, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: selectedData
          })
        });
        try {
          responseJson = await response.json();
          console.log(" responseJson - ", responseJson)
          let splitedRs = responseJson.data.split('-');
          if (splitedRs[0] === 'row_application') {
              this.setState({ errorMsg: 'You have entered '+splitedRs[1]+' Row application, You are only allowed to enter up to '+mndLength+' in Row application.', successMsg: "", isLoading: false });
          }else if (splitedRs[0] === 'demand_note') {
              this.setState({ errorMsg: 'You have entered '+splitedRs[1]+' Row application, You are only allowed to enter up to '+mndLength+' in Demand note.', successMsg: "", isLoading: false });
          }else if (splitedRs[0] === 'permission') {
              this.setState({ errorMsg: 'You have entered '+splitedRs[1]+' Row application, You are only allowed to enter up to '+mndLength+' in Permission.', successMsg: "", isLoading: false });
          }else{
            // this.setState({ isLoading: false });
            this.submitFormData();
          }
        } catch (error) {
          console.log("Response error - ", error);
          this.setState({ errorMsg: "", successMsg: "", isLoading: false });
        }

      }
    }

    // FOLLOWING COMPONENT USING TO SUBMIT FORM
    submitFormData = async() => {
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      let response = await fetch(this.config.submitFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        responseJson = await response.json();
        alert(responseJson.data[0].success);
        this.setState({ successMsg: responseJson.data[0].success, isLoading: false });
      } catch (error) {
        console.log("Response error - ", error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }
    }


    render() {

      let Survey_Design_BOQ = [{
        "value": "Yes"
      },{
        "value": "No"
      }]
      
      const { selectedData } = this.state;
      return (
          <Container style={styles.Container}>

            {/* START SECTION */}
            {
              this.state.isLoading ? (
                <View style={ styles.ActivityIndicatorContainer }>
                  <View style={{  alignItems: 'center',justifyContent: 'space-around' }}>
                    <ActivityIndicator size="large" color="#2157c4" style={ styles.activityIndicator } />
                  </View>
                </View>
              ) : (
                <View></View>
              )
            }
            
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >

                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Zone</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allZone}
                            // dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.zoneVal}
                            onChangeText={(value) => this.getData(value, "zone")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Mandal</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allMandal}
                            dropdownPosition={0}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.mandalVal}
                            onChangeText={(value) => this.getData(value, "mandal")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Mandal length</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            value={this.state.mandalLength}
                            editable = {false}
                          />
                        </View>
                      </View>

                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={styles.headingFormStyle}>
                        <Text style={{ fontSize: 16 }}>Survey, Design and BOQ</Text>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Physical survey</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={Survey_Design_BOQ}
                            dropdownPosition={0}
                            value={this.state.physical_survey}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "physical_survey")}
                            disabled= {this.state.physical_survey_disabled}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Design & BOQ Report sumbitted</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={Survey_Design_BOQ}
                            dropdownPosition={0}
                            value={this.state.design_boq_report}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "design_boq_report")}
                            disabled= {this.state.design_boq_report_disabled}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Customer approval</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={Survey_Design_BOQ}
                            dropdownPosition={0}
                            value={this.state.customer_approval}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "customer_approval")}
                            disabled= {this.state.customer_approval_disabled}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={styles.headingFormStyle}>
                        <Text style={{ fontSize: 16 }}>ROW</Text>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>ROW Application (km)</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "row_application")}
                            keyboardType="number-pad"
                            maxLength = {10}
                            // editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Demand Note(km)</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "demand_note")}
                            keyboardType="number-pad"
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Permission(km)</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "permission")}
                            keyboardType="number-pad"
                          />
                        </View>
                      </View>
                    </View>


                  </View>

                  <KeyboardSpacer topSpacing ={5} />

                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 5 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                  </View>

                  <View>
                    {
                      (this.state.isLoading)?
                      (
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                        </TouchableOpacity>
                      ):(
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                        </TouchableOpacity>
                      )
                    }
                  </View>

                </View>
              </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }


    static navigationOptions = ({navigation}) => {
      return {
        title: 'Survey Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center",
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  }
}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    ActivityIndicatorContainer: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#00000040',
      position: "absolute",
      width: "100%",
      height: screenHeight-60
    },
    activityIndicator: {
      backgroundColor: '#fff',
      height: 60,
      width: 60,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      zIndex: 999,
      justifyContent: 'space-around'
    },
    headingFormStyle: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#adb9ca",
      paddingBottom: 10,
      paddingTop: 10,
      paddingLeft: 5,
      borderRadius: 10,
      borderColor: "#517bc5",
      borderWidth: 1
    },
    paddingBottomMore: {
      paddingBottom: 15
    },

    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }

  });

export default EngineerForm;
