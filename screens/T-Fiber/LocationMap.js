import React from 'react';
import { StyleSheet, View, Dimensions, Text, Image, ActivityIndicator, Platform, TouchableOpacity } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { BASE_URL } from '../../basepath'
import MultiSelect from 'react-native-multiple-select';
import Modal from "react-native-modal";
const GOOGLE_MAPS_APIKEY = 'AIzaSyDvVj296ZycDdW4wPFR8wIUsT18nKSkQiA';
const screenHeight = Math.round(Dimensions.get('screen').height);
const screenWidth = Math.round(Dimensions.get('window').width);
const AspectRatio= screenWidth/screenHeight;
export default class LocationMap extends React.Component {

  constructor(props) {
    super(props);
    this.config = {
      apiUrl: BASE_URL + "apis/locationmap_data",
      ug_aerialData: BASE_URL + "apis/ug_aerial",
      zoneData: BASE_URL + "apis/zone",
      mandalData: BASE_URL + "apis/mandal",
      span_nameData: BASE_URL + "apis/span_name",
      locationTrackingDATA: BASE_URL + "apis/locationTrackingActivity"
    }
    this.state = {
      isLoading: true,
      coordinates: [],
      trackingCoordinates: [],
      errorMsg: "",
      latitudeDelta: 0.9,
      longitudeDelta: 0.6,
      centerlat: 0,
      centerlong: 0,
      totalrecords: 0,
      trackingData_TD: [],
      trackingData_Blowing: [],
      ug_aerial:[],
      selectedUg_aerial:[],
      zone: [],
      selectedZone:[],
      mandal: [],
      selectedMandal: [],
      span_name: [],
      selectedSpan_name: [],
      confirmModalVisible:false,
    }

    
  }

  ug_aerialChange = (items) => {
    this.setState({ selectedUg_aerial:items, selectedZone:[],  zone: [], selectedMandal:[], mandal:[], selectedSpan_name:[], span_name:[] });
    this.fetchzone();
  };

  zoneChange = async(items) => {
    await this.setState({ selectedZone:items, selectedMandal:[], mandal:[], selectedSpan_name:[], span_name:[] });
    this.fetchmandal();
  };
  mandalChange = async (items) => {
    await this.setState({ selectedMandal:items, selectedSpan_name:[], span_name:[] });
    this.fetchSpan();
  };
  spanChange = (items) => {
    this.setState({ selectedSpan_name:items });
  };
  
  filterRecordByAPI = async() => {
    this.setState({ confirmModalVisible:!this.state.confirmModalVisible });
  }


  componentDidMount() {
    this.fetchDataOnLoad();
    this.fetchUgAerial();
    this.fetchzone();
  }


  fetchUgAerial = async () => {
    let responseJson = [];
    let response = await fetch(this.config.ug_aerialData);
    try {
      responseJson = await response.json();
      //Successful response from the API Call

      if (responseJson.hasOwnProperty('error')) {
        this.setState({ errorMsg: responseJson.error, isLoading: false });
      } else {
        await this.setState({ ug_aerial: responseJson.data });
      }
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  fetchzone = async () => {

    return await fetch(this.config.zoneData, {
      method: "POST",
      headers: {
        Accept: 'application/json',
    'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        "ug":this.state.selectedUg_aerial,
      })
    }).then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.hasOwnProperty('error')) {
        this.setState({ errorMsg: responseJson.error, isLoading: false });
      } else {
         this.setState({ zone: responseJson.data });
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }
  fetchmandal = async () => {
   return fetch(this.config.mandalData, {
      method: "POST",
      headers: {
        Accept: 'application/json',
    'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        "zone":this.state.selectedZone,
        "ug":this.state.selectedUg_aerial,
      })
    }).then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.hasOwnProperty('error')) {
            this.setState({ errorMsg: responseJson.error, isLoading: false });
          } else {
            this.setState({ mandal: responseJson.data });
          }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  fetchSpan = async () => {
    return fetch(this.config.span_nameData, {
      method: "POST",
      headers: {
        Accept: 'application/json',
    'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        "zone":this.state.selectedZone,
        "mandal":this.state.selectedMandal,
        "ug":this.state.selectedUg_aerial,
      })
    }).then((response) => response.json())
    .then((responseJson) => {
      if (responseJson.hasOwnProperty('error')) {
            this.setState({ errorMsg: responseJson.error, isLoading: false });
          } else {
            this.setState({ span_name: responseJson.data });
          }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  fetchDataOnLoad = async () => {
    this.setState({ confirmModalVisible: false });
    this.setState({ isLoading: true})
   return await fetch(this.config.apiUrl, {
      method: "POST",
      headers: {
        Accept: 'application/json',
    'Content-Type': 'application/json',
      },
      body:JSON.stringify({
        "ug":this.state.selectedUg_aerial,
        "zone":this.state.selectedZone,
        "mandal":this.state.selectedMandal,
        "span":this.state.selectedSpan_name,
      })
    }).then((response) =>  response.json())
    .then((responseJson) => {
      
      if (responseJson.hasOwnProperty('error')) {
        this.setState({ errorMsg: responseJson.error, isLoading: false });
        console.log(responseJson.query);
        this.setState({ coordinates:[]});
        alert(responseJson.error);
      } else {
         this.setState({ coordinates: responseJson.data, totalrecords: responseJson.total_data });
         this.setState({centerlat:this.state.coordinates[parseInt(this.state.totalrecords/2)].lat, centerlong:this.state.coordinates[parseInt(this.state.totalrecords/2)].lon, isLoading: false})
         this.fetchDatafortracking('T&D');
         this.fetchDatafortracking('Blowing');
      }
    })
    .catch((error) => {
      console.error(error);
    });
  }

  fetchDatafortracking = async (activity) => {
    let responseJson = [];
    let response = await fetch(this.config.locationTrackingDATA + "/" + activity);
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      if (responseJson.hasOwnProperty('error')) {
        this.setState({ errorMsg: responseJson.error, isLoading: false });
      } else {
        if (activity === 'T&D') {
          this.setState({ trackingData_TD: responseJson.data, isLoading: false });
        } else if (activity === 'Blowing') {
          this.setState({ trackingData_Blowing: responseJson.data, isLoading: false }); 
        }

      }
    } catch (error) {
      console.log("Response error - ", error);
    }
  }


  render() {
    let total_length_TD = 0;
    let total_length_Blowing = 0;
    let marker_pin_number=1;
    return (

      (this.state.isLoading) ? (
        <View style={ styles.ActivityIndicatorContainer }>
          <View style={{  alignItems: 'center',justifyContent: 'space-around' }}>
            <ActivityIndicator size="large" color="#2157c4" style={ styles.activityIndicator } />
            <Text style = {{color:'#fff'}}>Loading...</Text>
          </View>
        </View>
      ) : (
          <View style={styles.container}>
            <MapView style={styles.map} initialRegion={{
              latitude: parseFloat(this.state.centerlat),
              longitude: parseFloat(this.state.centerlong),
              latitudeDelta: this.state.latitudeDelta,
              longitudeDelta: this.state.longitudeDelta,

            }
            }
              provider={PROVIDER_GOOGLE}
            >
            {
              (this.state.coordinates.length>0)?
              (
                  <MapView.Marker
                    coordinate={{
                      latitude: parseFloat(this.state.coordinates[0].lat),
                      longitude: parseFloat(this.state.coordinates[0].lon)
                    }}
                  
                    title="Start Tracking"
                    description={"Span Name: " + this.state.coordinates[0].span_name}
                  />
                   
              ):
              (<View></View>)
            }
              
              {
                this.state.coordinates.map((Markers, key) => {
                  if (key < this.state.totalrecords - 1) {
                      return (
                        <MapViewDirections
                            key={key}
                              origin={{
                                latitude:parseFloat(this.state.coordinates[key].lat), 
                                longitude:parseFloat(this.state.coordinates[key].lon)
                                }}
                                  
                                destination={{
                                    latitude:parseFloat(this.state.coordinates[key + 1].lat),
                                    longitude:parseFloat(this.state.coordinates[key + 1].lon)
                                  }}
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={6}
                            strokeColor='#ea4a4a'
                          />
                        )
                    
                  }
                })
              }
              {
                this.state.coordinates.map((Markers, key) => {
                  total_length_TD = total_length_TD + parseInt(Markers.route_length);
                  if (this.state.trackingData_TD.length > 0) {
                    let item_data = this.state.trackingData_TD.find(item => item.span === Markers.span_name);
                    if (item_data !== undefined) {
                      if (parseInt(item_data.distance) >= parseInt(total_length_TD)) {
                        if (key < this.state.totalrecords - 1) {
                            return (
                              <MapViewDirections
                                  key={key}
                                  origin={{
                                    latitude:parseFloat(this.state.coordinates[key].lat), 
                                    longitude:parseFloat(this.state.coordinates[key].lon)
                                  }}
                                    
                                  destination={{
                                      latitude:parseFloat(this.state.coordinates[key + 1].lat),
                                      longitude:parseFloat(this.state.coordinates[key + 1].lon)
                                  }}
                                  apikey={GOOGLE_MAPS_APIKEY}
                                  strokeWidth={4}
                                  strokeColor='green'
                              />
                            )
                        }
                      }
                    }
                    if (key < this.state.totalrecords - 1) {
                      if (this.state.coordinates[key].span_name !== this.state.coordinates[key + 1].span_name) {
                        marker_pin_number=marker_pin_number+1;
                        total_length_TD = 0;
                        return (
                          <MapView.Marker
                            coordinate={{
                              latitude: parseFloat(this.state.coordinates[key].lat),
                              longitude: parseFloat(this.state.coordinates[key].lon)
                            }}
                            key={key}
                            title={"Span Name: " + this.state.coordinates[key].span_name}
                          />
                        )
                      }
                    }
                  }

                })
              }

              {
                marker_pin_number=marker_pin_number+1,
              (this.state.coordinates.length>0)?
              (
                  <MapView.Marker
                    coordinate={{
                      latitude: parseFloat(this.state.coordinates[this.state.totalrecords - 1].lat),
                      longitude: parseFloat(this.state.coordinates[this.state.totalrecords - 1].lon)
                    }}
                    title="End Tracking"
                    description={"Span Name: " + this.state.coordinates[this.state.totalrecords - 1].span_name}
                  />
                   
              ):
              (<View></View>)
            }

              {
                this.state.coordinates.map((Markers, key) => {
                  total_length_Blowing = total_length_Blowing + parseInt(Markers.route_length);
                  if (this.state.trackingData_Blowing.length > 0) {
                    let item_data = this.state.trackingData_Blowing.find(item => item.span === Markers.span_name);
                    if (item_data !== undefined) {
                      if (parseInt(item_data.distance) >= parseInt(total_length_Blowing)) {
                        if (key < this.state.totalrecords - 1) {
                          if(item_data.field_type==='Greenfield'){
                              return (
                                <MapViewDirections
                                  key={key}
                                  origin={{
                                    latitude:parseFloat(this.state.coordinates[key].lat), 
                                    longitude:parseFloat(this.state.coordinates[key].lon)
                                  }}
                                    
                                  destination={{
                                      latitude:parseFloat(this.state.coordinates[key + 1].lat),
                                      longitude:parseFloat(this.state.coordinates[key + 1].lon)
                                  }}
                                  apikey={GOOGLE_MAPS_APIKEY}
                                  strokeWidth={2}
                                  strokeColor='blue'
                                />
                              )  
                          }
                          else{
                              return (
                                <MapViewDirections
                                  key={key}
                                  origin={{
                                    latitude:parseFloat(this.state.coordinates[key].lat), 
                                    longitude:parseFloat(this.state.coordinates[key].lon)
                                  }}
                                    
                                  destination={{
                                      latitude:parseFloat(this.state.coordinates[key + 1].lat),
                                      longitude:parseFloat(this.state.coordinates[key + 1].lon)
                                  }}
                                  apikey={GOOGLE_MAPS_APIKEY}
                                  strokeWidth={2}
                                  strokeColor='#964B00'
                                />
                            )
                            }
                          
                        }
                      }
                    }
                    if (key < this.state.totalrecords - 1) {
                      if (this.state.coordinates[key].span_name !== this.state.coordinates[key + 1].span_name) {
                        total_length_Blowing = 0;

                      }
                    }
                  }
                })
              }
            </MapView> 
            
              {
                (!this.state.confirmModalVisible)?
                (
                  <View style={styles.filter}>
                    <View style={{alignSelf:'center' , width:50, height:32,paddingTop:4,paddingRight:13}}>
                      <TouchableOpacity style={{width:'100%',height:'100%' }} onPress={ this.filterRecordByAPI} >
                          <Image source = {require("../../assets/images/filter_icon.png")} resizeMode='contain'  style={{ alignSelf: "center", tintColor: "#13c16a", width: 24, height: 24 }}/>
                      </TouchableOpacity>
                    </View>
                  </View>
                ):
                (
                  <View>
                    <Text></Text>
                  </View>
                )
              }
            
            {/* START MODAL SECTION */}
            <Modal isVisible={this.state.confirmModalVisible}
             hideModalContentWhileAnimating={true}
             onTouchOutside={() => {
              this.setState({ confirmModalVisible: false });
            }}
             useNativeDriver={false}
             style={[ styles.modal,{ padding: 10, }]}  animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={500} animationOutTiming={500}  ackdropTransitionInTiming={500} backdropTransitionOutTiming={500} >

              <View style={{ width:'100%', paddingBottom:30}}>
                <MultiSelect
                    hideTags
                    items={this.state.ug_aerial}
                    uniqueKey="name"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.ug_aerialChange}
                    selectedItems={this.state.selectedUg_aerial}
                    selectText="Select UG Aerial"
                    searchInputPlaceholderText="Search UG Aerial..."
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    fixedHeight={true}
                    selectedItemTextColor="#13c16a"
                    selectedItemIconColor="#13c16a"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Done"
                    styleListContainer={{height:180}}
                />
                 <MultiSelect
                    hideTags
                    items={this.state.zone}
                   uniqueKey="name"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.zoneChange}
                    selectedItems={this.state.selectedZone}
                    selectText="Select Zone"
                    searchInputPlaceholderText="Search Zone..."
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    fixedHeight={true}
                    selectedItemTextColor="#13c16a"
                    selectedItemIconColor="#13c16a"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Done"
                    styleListContainer={{height:180}}
                />
                <MultiSelect
                    hideTags
                    items={this.state.mandal}
                    uniqueKey="name"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.mandalChange}
                    selectedItems={this.state.selectedMandal}
                    selectText="Select Mandal"
                    searchInputPlaceholderText="Search Mandal..."
                    tagRemoveIconColor="#CCC"
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    selectedItemTextColor="#13c16a"
                    selectedItemIconColor="#13c16a"
                    fixedHeight={true}
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Done"
                    styleListContainer={{height:180}}
                />
                 <MultiSelect
                    hideTags
                    items={this.state.span_name}
                    uniqueKey="name"
                    ref={(component) => { this.multiSelect = component }}
                    onSelectedItemsChange={this.spanChange}
                    selectedItems={this.state.selectedSpan_name}
                    tagRemoveIconColor="#CCC"
                    selectText="Select Span"
                    searchInputPlaceholderText="Search Span..."
                    tagBorderColor="#CCC"
                    tagTextColor="#CCC"
                    fixedHeight={true}
                    selectedItemTextColor="#13c16a"
                    selectedItemIconColor="#13c16a"
                    itemTextColor="#000"
                    displayKey="name"
                    searchInputStyle={{ color: '#CCC' }}
                    submitButtonColor="#CCC"
                    submitButtonText="Done"
                    styleListContainer={{height:180}}
                />
                 
              </View>
              <TouchableOpacity style={{  width:90 , position:'absolute', bottom:0, left:10}} onPress={ this.filterRecordByAPI} >
                <Text style={{ width: "100%", textAlign:"left", padding: 10, fontWeight: "bold" }}> CANCEL </Text>
              </TouchableOpacity>
              <TouchableOpacity style={{  width:50 , position:'absolute', bottom:0, right:10}} onPress={ this.fetchDataOnLoad} >
                <Text style={{ width: "100%", textAlign:"right", padding: 10, fontWeight: "bold" }}> OK </Text>
              </TouchableOpacity>
            </Modal>
            {/* END MODAL SECTION */}
          </View>
        )
    )
  }

}

LocationMap.navigationOptions = {
  header: null,
};



const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#00000040',
      position: "absolute",
      width: "100%",
      height: screenHeight-60
  },
  activityIndicator: {
    backgroundColor: '#fff',
    height: 60,
    width: 60,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    zIndex: 999,
    justifyContent: 'space-around'
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    alignContent: 'flex-start'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
  },
  LocationDetail: {
    alignSelf: "flex-start",
    paddingBottom: 40,
  },
  filter: {
    position: 'absolute',
    width: 50,
    height:32,
    top: 40,
    right: 20,
    paddingLeft: 15,
    backgroundColor: '#fff',
    alignContent:'center',
    alignItems:'center',
    borderRadius:2
  },
  paddingBottomMore: {
    marginBottom: 10,
  },
  modal: {
    alignItems: 'center',
    width: "90%",
    marginLeft: "5%",
    top:0,
    backgroundColor: "#ffffff",
    position: "absolute",
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 6,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 10,
      },
    }),

    borderRadius: 5
  },
});
