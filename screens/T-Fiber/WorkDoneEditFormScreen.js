import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, TextInput, AsyncStorage, FlatList, ActivityIndicator } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base'; 
import { Dropdown } from 'react-native-material-dropdown';
import { RadioButton, Checkbox } from 'react-native-paper';
import DatePicker from 'react-native-datepicker';
import * as Network from 'expo-network';
import Textarea from 'react-native-textarea';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import LocationTracking from './LocationTracking';
import * as Progress from 'react-native-progress';
import { TextField } from 'react-native-material-textfield';
import {BASE_URL} from '../../basepath'
import Database from '../../Database';
import { MaterialIcons } from '@expo/vector-icons';
const db = new Database();

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

class EngineerForm extends Component {

    constructor(props) {
      super(props);
      this.paramsDatas = this.props.navigation.state.params;
      this.paramsData = this.paramsDatas.reportID;
      this.config = {
        DistrictAPI: BASE_URL + "apis/userDistrictFilter",
        TalukaAPI: BASE_URL + "apis/userTalukaFilter",
        SpanAPI: BASE_URL + "apis/userSpanFilter",
        SpanLength:BASE_URL + "/apis/userGpLengthSpanFilter",
        GetRingNo: BASE_URL + "apis/userRingNoFilter",
        ActivitiesAPI: BASE_URL + "apis/userActivitiesFilter",
        IssuesAPI: BASE_URL + "apis/userIssuesFilter",
        CheckWipTransAPI: BASE_URL + "apis/userWipTransFilter",
        CheckTransAPI:BASE_URL + "apis/userTransactionFilter",
        submitFormAPI: BASE_URL + "apis/workDoneFormSubmit",
        detailsAPI: BASE_URL + "apis/workReportsDetails",
      }
      this.state = {
        isLoading: true,
        usersData: [],
        allDistrict: [],
        allTaluka: [],
        allSpan: [],
        ringNoVal: [],
        activities: [],
        internalIssues: [],
        customerIssues: [],
        userName: 0,
        userRole: 0,
        userLoggedInId: 0,
        milestoneStartStop: false,
        IssuesReportChange: true,
        IssuesReportValue: "No",
        rignTypeVal: "Zonal Ring",
        ringNoValue: "",
        soilStrataVal: "Normal",
        spanLength: "",
        activity: "T&D",
        IssuesChecked: 'Internal',
        milestoneChecked: 'WIP',
        mbChecked: 'Yes',
        chainageFrom: "0",
        chainageTo: "1",
        typeTD: "",
        depthTD: "",
        isCheckedInternalIssues: [],
        isCheckedCustomerIssues: [],
        activityDisabled: false,
        activityTD: true,
        activityBlowing: false,
        activityHHMH: false,
        activityInfraEquipment: false,
        activityMachineResource: false,
        fiberReadingFrom: "",
        fiberReadingTo: "",
        fiberReadingIn: "",
        fiberReadingOut: "",
        lattitude: "0.000",
        longitude: "0.000",
        errorMsg: "",
        splitedGPName: [],
        slctDateVal: new Date(),
        seconds: 0,
        submitBtnDisabled: true,
        countedMint: 0,
        milestoneToTD: "1",
        milestoneFromTD: "0",
        milestoneToHHMH: "1",
        milestoneFromHHMH: "0",
        milestoneFromBlowing: "0",
        milestoneFromBlowingMTS: "0",
        milestoneToBlowing: "0",
        milestoneToBlowingMTS: "0",

        milestoneSpansLen: [],

        selectedData: {
          district: "",
          taluka: "",
          span: "",
          locFrom: "Zone",
          locTo: "Zone",
          activity: "T&D",
          issuesReportTD: "No",
          ringType: "Zonal Ring",
          chainageFrom: "0",
          chainageTo: "1",
          soilStrata: "Normal",
          typeTD: "",
          depthTD: "",
          milestoneStatus: "WIP",
          protection: "",
          mbTD: "Yes",
          issuesType: "Internal",
          issues_TD_Hndl: "No",
          protection: "",
          blowingType: "",
          fiberReadingFrom: "",
          fiberReadingTo: "",
          fiberType: "",
          splicing: "",
          termination: "",
          chainagePointBlow: "",
          fiberReadingIn: "",
          fiberReadingOut: "",
          lattitude: "",
          longitude: "",
          milestoneFrom: "0",
          milestoneTo: "0",
          milestoneFromM: "0",
          milestoneToM: "0",
          gPName: "",
          GPFiberReady: "No",
          GPInfraReadiness: "No",
          GPCommissioning: "No",
          mRPlannedJCB: "", mRActualJCB: "", mRIdleJCB: "",
          mRPlannedHDD: "", mRActualHDD: "", mRIdleHDD: "",
          mRPlannedPoklane: "", mRActualPoklane: "", mRIdlePoklane: "",
          mRPlannedRockBreaker: "", mRActualRockBreaker: "", mRIdleRockBreaker: "",
          mRPlannedExcavator: "", mRActualExcavator: "", mRIdleExcavator: "",
          mRPlannedBackhoeLoader: "", mRActualBackhoeLoader: "", mRIdleBackhoeLoader: "",
          mRPlannedBlowing: "", mRActualBlowing: "", mRIdleBlowing: "",
          mRPlannedSlicer: "", mRActualSlicer: "", mRIdleSlicer: "",
          mRPlannedManpower: "", mRActualManpower: "", mRIdleManpower: "",
          slctDate: date

        }
      }
      this.milestoneProcessHandler = this.milestoneProcessHandler.bind(this);
    }

    
    componentDidMount(){
      this.fetchUserDataState();
      this.fetchActivities();
      this.fetchInternalIssues();
      this.fetchCustomerIssues();
      this.fetchDataOnLoad();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    // GETING DATA ON LOAD OF SCREEN
    fetchDataOnLoad = async() => {
      let responseJson = [];
      let response = await fetch(this.config.detailsAPI+"/"+this.paramsData);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.state.selectedData.district = responseJson.data.district;
        this.state.selectedData.taluka = responseJson.data.taluka;
        this.state.selectedData.span = responseJson.data.gp;
        this.state.selectedData.locFrom = responseJson.data.location_from;
        this.state.selectedData.locTo = responseJson.data.location_to;
        this.state.selectedData.ringNoValue = responseJson.data.ring_no;
        this.state.selectedData.rignTypeVal = responseJson.data.ring_type;
        this.state.selectedData.activity = responseJson.data.activity;
        this.state.selectedData.chainageFrom = responseJson.data.cspfrom;
        this.state.selectedData.chainageTo = responseJson.data.cspto;
        this.state.selectedData.soilStrataVal = responseJson.data.soil_strata;
        this.state.selectedData.typeTD = responseJson.data.type_TD_Act;
        this.state.selectedData.depthTD = responseJson.data.defth_TD_Act;
        this.state.selectedData.milestoneChecked = responseJson.data.cstatus;
        this.state.selectedData.mbChecked = responseJson.data.mb_TD_Act;
        this.state.selectedData.fiberReadingFrom = responseJson.data.fiberReadBlow_from;
        this.state.selectedData.fiberReadingTo = responseJson.data.fiberReadBlow_To;
        this.state.selectedData.chainagePointBlow = responseJson.data.cspfrom;
        this.state.selectedData.fiberReadingTo = responseJson.data.fiberReadingTo;
        this.state.selectedData.fiberReadingIn = responseJson.data.fiberReading_In;
        this.state.selectedData.fiberReadingOut = responseJson.data.fiberReading_Out;
        this.state.selectedData.fiberType = responseJson.data.fiber_type_blw;        
        this.state.selectedData.lattitude = responseJson.data.lattitude;
        this.state.selectedData.longitude = responseJson.data.longitude;

        this.state.selectedData.protection = responseJson.data.protection_TD;
        this.state.selectedData.Remarks = responseJson.data.remarks;

        this.state.selectedData.blowingType = responseJson.data.blowingType;
        this.state.selectedData.splicing = responseJson.data.splicingBlowing;
        this.state.selectedData.termination = responseJson.data.terminationBlowing;

        this.state.selectedData.chainagePointBlow = responseJson.data.cspfrom;
        this.state.selectedData.fiberReadingIn = responseJson.data.fiberReading_In;
        this.state.selectedData.lattitude = responseJson.data.lattitude;
        this.state.selectedData.longitude = responseJson.data.longitude;

        this.state.selectedData.gPName = responseJson.data.gp_name;
        this.state.selectedData.GPFiberReady = responseJson.data.gp_fiber;
        this.state.selectedData.GPInfraReadiness = responseJson.data.gp_infra;
        this.state.selectedData.GPCommissioning = responseJson.data.gp_commissioning;
        
        this.state.selectedData.mRPlannedJCB = responseJson.data.planned_jcb;
        this.state.selectedData.mRPlannedHDD = responseJson.data.planned_hdd;
        this.state.selectedData.mRPlannedPoklane = responseJson.data.planned_Poklane;
        this.state.selectedData.mRPlannedRockBreaker = responseJson.data.planned_rock_breaker;
        this.state.selectedData.mRPlannedExcavator = responseJson.data.planned_excavator;
        this.state.selectedData.mRPlannedBackhoeLoader = responseJson.data.planned_backhoe_loader;
        this.state.selectedData.mRPlannedBlowing = responseJson.data.planned_blowing;
        this.state.selectedData.mRPlannedSlicer = responseJson.data.planned_splicer;
        this.state.selectedData.mRPlannedManpower = responseJson.data.planned_manpower;
        // -------------
        this.state.selectedData.mRActualJCB = responseJson.data.deploy_jcb;
        this.state.selectedData.mRActualHDD = responseJson.data.deploy_hdd;
        this.state.selectedData.mRActualPoklane = responseJson.data.deploy_Poklane;
        this.state.selectedData.mRActualRockBreaker = responseJson.data.deploy_rock_breaker;
        this.state.selectedData.mRActualExcavator = responseJson.data.deploy_excavator;
        this.state.selectedData.mRActualBackhoeLoader = responseJson.data.deploy_backhoe_loader;
        this.state.selectedData.mRActualBlowing = responseJson.data.deploy_blowing;
        this.state.selectedData.mRActualSlicer = responseJson.data.deploy_splicer;
        this.state.selectedData.mRActualManpower = responseJson.data.deploy_manpower;
        // -------------
        this.state.selectedData.mRIdleJCB = responseJson.data.actual_jcb;
        this.state.selectedData.mRIdleHDD = responseJson.data.actual_hdd;
        this.state.selectedData.mRIdlePoklane = responseJson.data.actual_Poklane;
        this.state.selectedData.mRIdleRockBreaker = responseJson.data.actual_rock_breaker;
        this.state.selectedData.mRIdleExcavator = responseJson.data.actual_excavator;
        this.state.selectedData.mRIdleBackhoeLoader = responseJson.data.actual_backhoe_loader;
        this.state.selectedData.mRIdleBlowing = responseJson.data.actual_blowing;
        this.state.selectedData.mRIdleSlicer = responseJson.data.actual_splicer;
        this.state.selectedData.mRIdleManpower = responseJson.data.actual_manpower;
        
        
        if(responseJson.data.activity !== ''){
          if(responseJson.data.activity === "T&D"){
            this.state.selectedData.milestoneFrom = responseJson.data.mfrom;
            this.setState({ 
              activityTD: true,
              activityBlowing: false,
              activityHHMH: false,
              activityInfraEquipment: false,
              activityMachineResource: false,
              milestoneFromTD: responseJson.data.mfrom,
              milestoneToTD: responseJson.data.mto,
            });
          }else if(responseJson.data.activity === "Blowing"){

            let mfromMTS = responseJson.data.mfrom.split('.');
            let mtoMTS = responseJson.data.mto.split('.');
            this.state.selectedData.milestoneFrom = mfromMTS[0],
            this.state.selectedData.milestoneFromM = mfromMTS[1],
            this.state.selectedData.milestoneTo = mtoMTS[0],
            this.state.selectedData.milestoneToM =  mtoMTS[1],

            this.setState({
              activityBlowing: true,
              activityTD: false,
              activityHHMH: false,
              activityInfraEquipment: false,
              activityMachineResource: false,
              milestoneFromBlowing: mfromMTS[0],
              milestoneFromBlowingMTS: mfromMTS[1],
              milestoneToBlowing: mtoMTS[0],
              milestoneToBlowingMTS: mtoMTS[1],
            });
          }else if(responseJson.data.activity === "HH/MH"){
            this.state.selectedData.milestoneFrom = responseJson.data.mfrom,
            this.setState({ 
              activityHHMH: true,
              activityTD: false,
              activityBlowing: false,
              activityInfraEquipment: false,
              activityMachineResource: false,
              milestoneFromHHMH: responseJson.data.mfrom,
              milestoneToHHMH: responseJson.data.mto,
            });
          }else if(responseJson.data.activity === "Infra/Equipment Readiness"){
            this.setState({
              activityInfraEquipment: true,
              activityTD: false,
              activityBlowing: false,
              activityHHMH: false,
              activityMachineResource: false
            });
          }else{
            this.setState({
              activityMachineResource: true,
              activityTD: false,
              activityBlowing: false,
              activityHHMH: false,
              activityInfraEquipment: false
            });
          }
        }
        
        // CALL FOLLOWING COMPONENTS TO GET DATA DEFAULT
          this.fetchTalukaAPI(responseJson.data.district);
          this.fetchSpanAPI(responseJson.data.taluka);
          this.fetchSpanLengthAPI(responseJson.data.gp);
          this.fetchRingNoAPI(responseJson.data.gp);

        // SET DATA
          this.setState({ 
            usersData: responseJson.data,
            isLoading: false,
            ringNoValue: responseJson.data.ring_no,
            rignTypeVal: responseJson.data.ring_type,
            activity: responseJson.data.activity,
            chainageFrom: responseJson.data.cspfrom,
            chainageTo: responseJson.data.cspto,
            soilStrataVal: responseJson.data.soil_strata,
            typeTD: responseJson.data.type_TD_Act,
            depthTD: responseJson.data.defth_TD_Act,
            milestoneChecked: responseJson.data.cstatus,
            mbChecked: responseJson.data.mb_TD_Act,
            fiberReadingFrom: responseJson.data.fiberReadBlow_from,
            fiberReadingTo: responseJson.data.fiberReadBlow_To,
            chainagePointBlow: responseJson.data.cspfrom,
            fiberReadingIn: responseJson.data.fiberReading_In,
            fiberReadingOut: responseJson.data.fiberReading_Out,
            lattitude: responseJson.data.lattitude,
            longitude: responseJson.data.longitude,
          });          

      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    fetchUserDataState = async() => {
      const userid = await AsyncStorage.getItem('user_id');
      const usern = await AsyncStorage.getItem('username');
      const userr = await AsyncStorage.getItem('user_role');
      const ipAddress = await Network.getIpAddressAsync();
      // const macAddress = await Network.getMacAddressAsync();
      
      this.state.selectedData["ipAddress"] = ipAddress;
      // this.state.selectedData["macAddress"] = macAddress;
      
      this.state.selectedData["user_id"] = userid;
      this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
      this.fetchDistrictAPI();
    }
    
    // CALLING API TO GET ALL ACTIVITIES 
    async fetchActivities(){
      let responseJson = [];
      let response = await fetch(this.config.ActivitiesAPI);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ activities: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
  
    // CALLING API TO GET ALL Internal ISSUES 
    async fetchInternalIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/1");
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ internalIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }


    // CALLING API TO GET ALL Customer ISSUES 
    async fetchCustomerIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/2");
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ customerIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
  
    // CALLING API TO GET ALL DISTRICT 
    async fetchDistrictAPI(){
      let responseJson = [];
      let response = await fetch(this.config.DistrictAPI+"/"+this.state.userRole+"/"+this.state.userName);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ allDistrict: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL TALUKAS 
    fetchTalukaAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.TalukaAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ allTaluka: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN 
    fetchSpanAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.SpanAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ allSpan: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
        
    // CALLING API TO GET ALL SPAN 
    fetchSpanLengthAPI = async(value) => {
      let gpName = [];
      let responseJson = [];
      let gpNameVals = value.split("-");

      let response = await fetch(this.config.SpanLength+"/"+value);
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        let spLength = responseJson.data[0].length;
        let milestoneSpan = parseInt(spLength, 10)+5;
        let milestoneSpanVal = [];
        for (var i=0 ; i <= milestoneSpan; i++) {
          milestoneSpanVal.push({ 'value': i });
        }

        gpName.push(gpNameVals.map((item, i) => {
          return {
            "value": gpNameVals[i]
          }
        }))

        this.setState({ spanLength: spLength, milestoneSpansLen: milestoneSpanVal, splitedGPName: gpName[0] });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
        
    // CALLING API TO GET RING NO
    fetchRingNoAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.GetRingNo+"/"+this.state.userRole+"/"+this.state.userName+"/"+value+"/2");
      try {
        //Successful response from the API Call
        responseJson = await response.json();        
        this.setState({ ringNoVal: responseJson.data, ringNoValue: responseJson.data[0].value });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
    
    // USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE ISSUES
    internalIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.internalIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedInternalIssues: checkedIssuesList });
    }

    // CUSTOMER ISSUES CHECKBOXES
    customerIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.customerIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedCustomerIssues: checkedIssuesList });
    }

    // GETING DATA FROM FIELDS AND USING VALIDATION
    getData(data, fieldName){

      if(fieldName === "District"){
        this.state.selectedData.district = data;
        this.fetchTalukaAPI(data);
      }else if(fieldName === "Taluka"){
        this.state.selectedData.taluka = data;
        this.fetchSpanAPI(data);
      }else if(fieldName === "Span"){
        this.state.selectedData.span = data;
        this.fetchSpanLengthAPI(data);
        this.fetchRingNoAPI(data);        
      }else if(fieldName === "activity"){
        
        if(data === "T&D"){
          this.setState({ activityTD: true, activityBlowing: false, activityHHMH: false, activityInfraEquipment: false, activityMachineResource: false});
        }else if(data === "Blowing"){
          this.setState({ activityBlowing: true, activityTD: false, activityHHMH: false, activityInfraEquipment: false, activityMachineResource: false });
        }else if(data === "HH/MH"){
          this.setState({ activityHHMH: true, activityTD: false, activityBlowing: false, activityInfraEquipment: false, activityMachineResource: false});
        }else if(data === "Infra/Equipment Readiness"){
          this.setState({ activityInfraEquipment: true, activityTD: false, activityBlowing: false, activityHHMH: false, activityMachineResource: false });
        }else{
          this.setState({ activityMachineResource: true, activityTD: false, activityBlowing: false, activityHHMH: false, activityInfraEquipment: false });
        }

        this.state.selectedData.activity = data;
      }else if(fieldName === "IssuesReport"){
        if(data === "Yes"){
          this.state.selectedData['issues_TD_Hndl'] = "Yes";
          this.setState({ IssuesReportChange: false, IssuesReportValue: "Yes", activityDisabled: true });
        }else{
          this.state.selectedData['issues_TD_Hndl'] = "No";
          this.setState({ IssuesReportChange: true, IssuesReportValue: "No", activityDisabled: false });
        }
        this.state.selectedData.issuesReportTD = data;
      }else if(fieldName === "ChainageStart"){
        this.setState({ chainageFrom: data })
        this.state.selectedData.chainageFrom = data;
      }else if(fieldName === "ChainageEnd"){
        this.setState({ chainageTo: data })
        this.state.selectedData.chainageTo = data;
      }else if(fieldName === "Depth"){
        this.setState({ depthTD: data })
        this.state.selectedData.depthTD = data;
      }else if(fieldName === "fiberReadingFrom"){
        this.setState({ fiberReadingFrom: data })
        this.state.selectedData.fiberReadingFrom = data;
      }else if(fieldName === "fiberReadingTo"){
        this.setState({ fiberReadingTo: data })
        this.state.selectedData.fiberReadingTo = data;
      }else if(fieldName === "fiberReadingIn"){
        this.setState({ fiberReadingIn: data })
        this.state.selectedData.fiberReadingIn = data;
      }else if(fieldName === "fiberReadingOut"){
        this.setState({ fiberReadingOut: data })
        this.state.selectedData.fiberReadingOut = data;
      }else if(fieldName === "lattitude"){
        this.setState({ lattitude: data })
        this.state.selectedData.lattitude = data;
      }else if(fieldName === "longitude"){
        this.setState({ longitude: data })
        this.state.selectedData.longitude = data;
      }else if(fieldName === "chainagePointBlow"){
        this.setState({ chainagePointBlow: data })
        this.state.selectedData.chainagePointBlow = data;
      }else if(fieldName === "RingType"){
        this.setState({ rignTypeVal: data })
        this.state.selectedData.ringType = data;
      }else if(fieldName === "SoilStrata"){
        this.setState({ soilStrataVal: data })
        this.state.selectedData.soilStrata = data;
      }else if(fieldName === "slctDate"){
        this.setState({ slctDateVal: data })
        this.state.selectedData.slctDate = data;
      }else if(fieldName === "milestoneFromTD"){
        this.setState({ milestoneToTD: (data + 1).toString(), milestoneFromTD: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromHHMH"){
        this.setState({ milestoneToHHMH: (data + 1).toString(), milestoneFromHHMH: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromBlowing"){
        this.setState({ milestoneFromBlowing: data })
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneFromBlowingMTS"){
        this.setState({ milestoneFromBlowingMTS: data.toString() })
        this.state.selectedData.milestoneFromM = data;
      }else if(fieldName === "milestoneToBlowing"){
        this.setState({ milestoneToBlowing: data })
        this.state.selectedData.milestoneTo = data;
      }else if(fieldName === "milestoneToBlowingMTS"){
        this.setState({ milestoneToBlowingMTS: data.toString() })
        this.state.selectedData.milestoneToM = data;
      }else if(fieldName === "typeTD"){
        this.setState({ typeTD: data })
        this.state.selectedData.typeTD = data;
      }else{
        this.state.selectedData[fieldName] = data;        
      }
      
    }

    milestoneProcessStart = async(value) => {
      db.transaction(
        tx => {
          tx.executeSql(`delete from workdoneActivity;`);
        }
      );
      new LocationTracking().startTrackingInterval(this.milestoneProcessHandler, this.startProgressInterval);
      this.setState({ milestoneStartStop: value, countedMint: 0, submitBtnDisabled: false });
    };

    async milestoneProcessStop(value=null){
      new LocationTracking().clearIntervalMtd();
      this.setState({ milestoneStartStop: value, submitBtnDisabled: true });
    }

    milestoneProcessHandler = async() => {
      this.setState({ milestoneStartStop: false, submitBtnDisabled: true });
    }

    startProgressInterval = async() => {
      this.setState({ countedMint: this.state.countedMint + 0.05 });
    }

  // USING FOLLOWING COMPONENT TO VALIDATE FORM 
    fnSubmitValidateForm = async() => {
      
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;
      
      let milst_frm_validation = parseFloat(selectedData.milestoneFrom+"."+selectedData.milestoneFromM);
      let milst_to_validation = parseFloat(selectedData.milestoneTo+"."+selectedData.milestoneToM);

      if(selectedData.district === '' || selectedData.taluka === ''){
        this.setState({ errorMsg: "Please select Zone and Mandal.", successMsg: "" });
      }else if(selectedData.span === ''){
        this.setState({ errorMsg: "Please select Span.", successMsg: "" });
      }else if(selectedData.activity === 'Infra/Equipment Readiness' && selectedData.gPName === ''){
        this.setState({ errorMsg: "Please select GP Name.", successMsg: "" });
      }else if(selectedData.activity === ''){
        this.setState({ errorMsg: "Please select Acivity.", successMsg: "" });
      }else if( (selectedData.chainageTo < selectedData.chainageFrom || selectedData.chainageTo === selectedData.chainageFrom ) && selectedData.activity !== 'HH/MH'){
        this.setState({ errorMsg: "Chainage End value must be higher than Chaninage From value.", successMsg: "" });
      }else if (( selectedData.chainageTo - selectedData.chainageFrom ) > 1000) {
        this.setState({ errorMsg: "You are only allowed to enter up to 1000 meters Chainage.", successMsg: "" });
      }else if ( selectedData.typeTD === '' && selectedData.activity === 'T&D' && this.state.activityDisabled === false ) {
        this.setState({ errorMsg: "Please select Type.", successMsg: "" });
      }else if ( selectedData.issues === '' && this.state.activityDisabled === true ) {
        this.setState({ errorMsg: "Please select at least one Issue.", successMsg: "" });
      }else if((selectedData.fiberReadingFrom === '' || selectedData.fiberReadingTo === '') && selectedData.activity === 'Blowing'){
        this.setState({ errorMsg: "Please enter fiber reading From and To.", successMsg: "" });
      }else if(selectedData.fiberType === '' && selectedData.activity === 'Blowing'){
        this.setState({ errorMsg: "Please select Fiber Type.", successMsg: "" });
      }else if(selectedData.chainageFrom === 0 && selectedData.activity === 'HH/MH'){
        this.setState({ errorMsg: "Please change Chainage Point.", successMsg: "" });
      }else if((selectedData.fiberReadingIn === '' || selectedData.fiberReadingOut === '') && selectedData.activity === 'HH/MH'){
        this.setState({ errorMsg: "Please select Fiber Reading In and Out.", successMsg: "" });
      }else if(( (selectedData.lattitude === '' || selectedData.lattitude === 0.000) || (selectedData.longitude === '' || selectedData.longitude === 0.000)) && selectedData.activity === 'HH/MH'){
        this.setState({ errorMsg: "Please select Lattitude and Longitude.", successMsg: "" });
      }else if ( (milst_frm_validation > milst_to_validation || milst_frm_validation === milst_to_validation) && selectedData.activity === 'Blowing'  ) {
        this.setState({ errorMsg: "Milestone To value must be higher than Milestone From value.", successMsg: "" });
      }else{
        
        let apiCall = '';

        // CALLING APIS ACCORDING TO ISSUES REPORTES SELECTION
        if(this.state.activityDisabled === true){
          apiCall = this.config.CheckWipTransAPI+"/"+this.paramsData;
        }else{
          apiCall = this.config.CheckTransAPI+"/"+this.paramsData;
        }

        // LET'S CALL API
        let response = await fetch(apiCall, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: selectedData
          })
        })
        try {
          // GOT SUCCESSFULLY RESPONSE FROM API
          responseJson = await response.json();

          if(responseJson.data[0].value === ''){
            this.setState({ errorMsg: "" });
            // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
            this.submitFormData();
          }else{

            let respVal1 = responseJson.data[0].value;
            respVal = respVal1.split('-');

            // RESPONSE BASED ERROR MESSAGES
            if(respVal[0] === 'range'){
                
              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal[1]+' to '+respVal[2]+', please select different range of Chainage Start & End Point.', successMsg: "" });

            }else{

              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal1+', You have not permission more than 1000 with these entries. \n\nPlease enter with another entry!', successMsg: "" });

            }

          }
        } catch (error) {
          console.log("Response error - ",error);
          this.setState({ errorMsg: "", successMsg: "" });
        }

      }
    }

    // FOLLOWING COMPONENT USING TO SUBMIT FORM 
    submitFormData = async() => {
      
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      let response = await fetch(this.config.submitFormAPI+"/"+this.paramsData, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      })
      try {
        responseJson = await response.json();
        this.setState({ successMsg: responseJson.data[0].success });
      } catch (error) {
        console.log("Response error - ",error);
        this.setState({ errorMsg: "", successMsg: "" });
      }

    }


    render() {
      
      let fromTo = [{
        "value": 'Zone',
      }, {
        "value": 'Mandal',
      }, {
        "value": 'GP',
      }];

      let ringType = [{
        "value": 'Zonal Ring',
      }, {
        "value": 'Mandal Ring',
      }, {
        "value": 'GP Ring',
      }];

      let data = [{
        "value": 'Zone',
      }, {
        "value": 'Mandal',
      }, {
        "value": 'GP',
      }];
      
      let issuesReport = [{
        "value": "No"
      },{
        "value": "Yes"
      }]

      let typeTD = [{
        "value": "Open"
      },{
        "value": "HDD"
      }]

      let soilStrata = [{
        "value": "Normal"
      },{
        "value": "Hard"
      },{
        "value": "Soft"
      }];

      let protection = [{
        "value": "GI"
      },{
        "value": "DWC"
      },{
        "value": "RCC"
      }];

      let blowingType = [{
        "value": "Green field"
      },{
        "value": "Brown field"
      }]

      let fiberType = [{
        "value": "24F"
      },{
        "value": "48F"
      },{
        "value": "96F"
      }]

      let splicingTermination = [{
        "value": "1"
      },{
        "value": "2"
      },{
        "value": "3"
      },{
        "value": "4"
      },{
        "value": "5"
      }]
      
      let gPName = this.state.splitedGPName;

      let gpConfirm = [{
        "value": "Yes"
      },{
        "value": "No"
      }]

      let machineResourceData = [{
        "value": "1"
      },{
        "value": "2"
      },{
        "value": "3"
      },{
        "value": "4"
      },{
        "value": "5"
      }]
 
      const { milestoneChecked, mbChecked, selectedData, IssuesChecked, usersData } = this.state;
      
      return (
          <Container style={styles.Container}>

            {/* START SECTION FOR LIST OF ALL USER */}
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
              ):(
                <ScrollView>
                  <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >
                    <View style ={[ styles.mainEventsClass ]} >

                      {/* <View style ={ styles.paddingBottomMore } >
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 0.3 }}><Text style={{ fontSize: 16 }}>Area Manager: </Text></View>
                          <View style ={{ flex: 0.7 }}>
                            <Text style={{ fontSize: 16 }}>Chiranjeet Sharma</Text>
                          </View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 0.3 }}><Text></Text></View>                        
                          <View style ={{ flex: 0.7 }}>
                            <Text style={{ fontSize: 16 }}>(9812423544)</Text>
                          </View>                          
                        </View>
                      </View> */}

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Date</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>
                          <DatePicker
                            style={{width: 200 }}
                            mode="date"
                            date= {usersData.date_reported}
                            format="YYYY-MM-DD"
                            minDate={fourDaysBack}
                            maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            iconSource = {require("../../assets/images/date.png")}
                            customStyles={{
                              dateIcon: {
                                position: 'absolute',
                                left: 0,
                                top: 4,
                                marginLeft: 0,
                              },
                              dateInput: {
                                marginLeft: 36,
                                backgroundColor: "#f0f0f0",
                                borderWidth: 0,
                                color: "#888888",
                              }
                            }}
                            onDateChange={(value) => this.getData(value, "slctDate")}
                          />
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Zone</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={this.state.allDistrict}
                              // dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={selectedData.district}
                              onChangeText={(value) => this.getData(value, "District")}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 16 }}>Mandal</Text>
                          </View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={this.state.allTaluka}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={selectedData.taluka}
                              onChangeText={(value) => this.getData(value, "Taluka")}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>


                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Span</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={this.state.allSpan}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={selectedData.span}
                              onChangeText={(value) => this.getData(value, "Span")}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>

                        <View style ={{ flex:1, flexDirection:'row', }}>
                          <View style ={{ flex: 0.35, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 16 }}>From</Text>
                          </View>

                          <View style ={{ flex: 0.31, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 16 }}>To</Text>
                          </View>

                          <View style ={{ flex: 0.33, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 16 }}>Span Length</Text>
                          </View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>
                          <View style ={{ width: "33%" }}>
                            <Dropdown
                              label= ""
                              data={fromTo}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={selectedData.locFrom}
                              onChangeText={(value) => this.getData(value, "locFrom")}
                              error = ""
                            />
                          </View>
                          <View style ={{ width: "33%", paddingHorizontal: 10 }}>
                            <Dropdown
                              label= ""
                              data={fromTo}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={selectedData.locTo}
                              onChangeText={(value) => this.getData(value, "locTo")}
                              error = ""
                            />
                          </View>
                          <View style ={{ width: "33%" }}>
                            <TextInput
                              style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                              value={this.state.spanLength}
                              editable = {false}
                            />
                          </View>
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Ring Type</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={ringType}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={this.state.rignTypeVal}
                              onChangeText={(value) => this.getData(value, "RingType")}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Ring No</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={this.state.ringNoVal}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              value={this.state.ringNoValue}
                              onChangeText={(value) => this.getData(value, "RingNo")}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>


                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 20, fontWeight: "bold" }}>Civil Work</Text>
                          </View>
                        </View>
                      </View>


                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Activity</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Dropdown
                              label= ""
                              data={this.state.activities}
                              dropdownPosition={1}
                              value={this.state.activity}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              onChangeText={(value) => this.getData(value, "activity")}
                              disabled= {this.state.activityDisabled}
                              error = ""
                            />
                          </View>                          
                        </View>
                      </View>
                      
                      {
                        (this.state.activityTD)?(
                          <View>

                            {/* <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Issues Report</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={issuesReport}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={this.state.IssuesReportValue}
                                    onChangeText={(value) => this.getData(value, "IssuesReport")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View> */}


                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                                <View style ={{ flex: 1, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                                </View>
                              </View>
                              
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= "Milestone From"
                                    data={this.state.milestoneSpansLen}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={this.state.milestoneFromTD}
                                    onChangeText={(value) => this.getData(value, "milestoneFromTD")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ width: "100%" }}>
                                  <Text style={{ color: "#999", fontSize: 10 }}>Milestone To</Text>
                                </View>                          
                              </View>
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:6 }}>
                                <View style ={{ width: "100%" }}>
                                  <TextInput
                                    style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.milestoneToTD}
                                    editable = {false}
                                  />
                                </View>                          
                              </View>

                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 10 }}>
                                  <Text style={{ fontSize: 16 }}>Chainage (MTS)</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row' }}>
                                <View style={{ flex: 0.5, flexDirection:'row' }}>
                                  <View style={{ flex: 0.25 }}>
                                    <Text style={{ height: 40, textAlignVertical: "center" }} >Start</Text>
                                  </View>
                                  <View style={{ flex: 0.75 }}>
                                    <TextInput
                                      style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value={this.state.chainageFrom}
                                      onChangeText={(value) => this.getData(value, "ChainageStart")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>
                                </View>

                                <View style={{ flex: 0.5, flexDirection:'row' }}>
                                  <View style={{ flex: 0.25 }}>
                                    <Text style={{ height: 40, textAlignVertical: "center", textAlign: "right" }} >End</Text>
                                  </View>
                                  <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                                    <TextInput
                                      style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value={this.state.chainageTo}
                                      onChangeText={(value) => this.getData(value, "ChainageEnd")}
                                      keyboardType='phone-pad'
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Soil strata</Text></View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={soilStrata}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={this.state.soilStrataVal}
                                    onChangeText={(value) => this.getData(value, "SoilStrata")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                            {
                              (this.state.IssuesReportChange)?(
                              <View>
                                <View style ={ styles.paddingBottomMore }>

                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                      <Text style={{ fontSize: 16 }}>Type</Text>
                                    </View>
                                    <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                      <Text style={{ fontSize: 16 }}>Depth (MTS)</Text>
                                    </View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row', }}>
                                    <View style ={{ flex:0.5, marginRight: 15 }}>                            
                                      <Dropdown
                                        label= ""
                                        data={typeTD}
                                        dropdownPosition={1}
                                        dropdownOffset = {{ top: 8, left: 0 }}
                                        value={this.state.typeTD}
                                        onChangeText={(value) => this.getData(value, "typeTD")}
                                        error = ""
                                      />
                                    </View>
                                    
                                    <View style ={{ flex:0.5 }}>                            
                                      <TextInput
                                          style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                          value= {this.state.depthTD}
                                          onChangeText={(value) => this.getData(value, "Depth")}
                                          keyboardType='phone-pad'
                                      />
                                    </View>
                                  </View>

                                </View>

                                <View style ={ styles.paddingBottomMore }>

                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 1, paddingBottom: 10 }}>
                                      <Text style={{ fontSize: 16 }}>Milestone status</Text>
                                    </View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row' }}>
                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.25 }}>
                                        <RadioButton
                                          value="WIP"
                                          color="#1ab679"
                                          status={ milestoneChecked === 'WIP' ? 'checked' : 'unchecked' }
                                        />
                                      </View>
                                      <View style={{ flex: 0.75 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => {
                                          this.setState({ milestoneChecked: 'WIP' }) 
                                          selectedData.milestoneStatus = "WIP";
                                        }}>
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                            WIP (In Progress)
                                          </Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>

                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.25 }}>
                                        <RadioButton
                                          value="Complete"
                                          // status='unchecked'
                                          color="#1ab679"
                                          status={milestoneChecked === 'Complete' ? 'checked' : 'unchecked'}
                                        />
                                      </View>
                                      <View style={{ flex: 0.75 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => { 
                                          this.setState({ milestoneChecked: 'Complete' }); 
                                          selectedData.milestoneStatus = "Complete";
                                        }} >
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Complete</Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>
                                  </View>

                                </View>
                                
                                <View style ={ styles.paddingBottomMore }>
                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Protection</Text></View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ width: "100%" }}>
                                      <Dropdown
                                        label= ""
                                        data={protection}
                                        dropdownPosition={1}
                                        dropdownOffset = {{ top: 8, left: 0 }}
                                        value={selectedData.protection}
                                        onChangeText={(value) => this.getData(value, "protection")}
                                        error = ""
                                      />
                                    </View>                          
                                  </View>
                                </View>


                                <View style ={ styles.paddingBottomMore }>
                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 1, paddingBottom: 10 }}>
                                      <Text style={{ fontSize: 16 }}>MB</Text>
                                    </View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row' }}>
                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.25 }}>
                                        <RadioButton
                                          value="Yes"
                                          status={(mbChecked === 'Yes')? 'checked' : 'unchecked' }
                                          color="#1ab679"
                                        />
                                      </View>
                                      <View style={{ flex: 0.75 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => { 
                                          this.setState({ mbChecked: 'Yes' }); 
                                          selectedData.mbTD = "Yes";
                                        }} >
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Yes</Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>

                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.25 }}>
                                        <RadioButton
                                          value="No"
                                          status={(mbChecked === 'No')? 'checked' : 'unchecked'}
                                          color="#1ab679"
                                        />
                                      </View>
                                      <View style={{ flex: 0.75 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => { 
                                          this.setState({ mbChecked: 'No' }); 
                                          selectedData.mbTD = "No";
                                        }} >
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >No</Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>
                                  </View>

                                </View>

                              </View>

                              ):(

                                <View>
                                  <View style ={ styles.paddingBottomMore }>
                                    <View style ={{ flex:1, flexDirection:'row', }}>                          
                                      <View style ={{ flex: 1, paddingBottom: 5 }}>
                                        <Text style={{ fontSize: 20, fontWeight: "bold" }}>Issues</Text>
                                      </View>
                                    </View>
                                  </View>
                                  
                                  <View style ={ styles.paddingBottomMore }>

                                    <View style ={{ flex:1, flexDirection:'row' }}>
                                      <View style={{ flex: 0.5, flexDirection:'row' }}>
                                        <View style={{ flex: 0.25 }}>
                                          <RadioButton
                                            value="Internal"
                                            color="#1ab679"
                                            status={ IssuesChecked === 'Internal' ? 'checked' : 'unchecked' }
                                          />
                                        </View>
                                        <View style={{ flex: 0.75 }}>
                                          <TouchableOpacity activeOpacity={1} onPress={() => { 
                                            this.setState({ IssuesChecked: 'Internal' });
                                            selectedData["issuesType"] = "Internal";
                                            selectedData["issues"] = "";
                                          }}>
                                            <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                              Internal Issues
                                            </Text>
                                          </TouchableOpacity>
                                        </View>
                                      </View>

                                      <View style={{ flex: 0.5, flexDirection:'row' }}>
                                        <View style={{ flex: 0.25 }}>
                                          <RadioButton
                                            value="Customer"
                                            color="#1ab679"
                                            status={IssuesChecked === 'Customer' ? 'checked' : 'unchecked'}
                                          />
                                        </View>
                                        <View style={{ flex: 0.75 }}>
                                          <TouchableOpacity activeOpacity={1} onPress={() => {
                                            this.setState({ IssuesChecked: 'Customer' }); 
                                            selectedData["issuesType"] = "Customer";
                                            selectedData["issues"] = "";
                                          }} >
                                            <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Customer Issues</Text>
                                          </TouchableOpacity>
                                        </View>
                                      </View>
                                    </View>

                                  </View>
                                  
                                  <View style ={ styles.paddingBottomMore }>

                                    <View style ={{ flex:1, flexDirection:'row', }}>                          
                                      <View style ={{ flex: 1, paddingBottom: 10 }}>
                                        <Text style={{ fontSize: 16 }}>Type of Issue</Text>
                                      </View>
                                    </View>

                                    <View style ={{ flex:1, flexDirection:'row' }}>
                                      {
                                        // INTERNAL ISSUES LIST
                                        (IssuesChecked === "Internal")?(
                                          <FlatList
                                            data={ this.state.internalIssues }
                                            extraData={this.state}
                                            renderItem={({item}) => {
                                              return(
                                                <View style={{ flex: 1, flexDirection: "row" }}>

                                                  <View style={{ paddingRight: 5 }}>
                                                    <Checkbox
                                                      color="#1ab679"
                                                      status= {(item.isSelect)? 'checked' : 'unchecked' }
                                                      onPress={() => {this.internalIssuesCheckbox(item.value)}}
                                                    />
                                                  </View>
                                                  <View style={{ paddingLeft: 5 }}>
                                                    <TouchableOpacity activeOpacity={1}>
                                                      <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                                        {item.value}
                                                      </Text>
                                                    </TouchableOpacity>
                                                  </View>

                                                </View>
                                              )
                                            }
                                            }
                                            keyExtractor={item => item.value}
                                            numColumns={2}
                                          />

                                        ):( // CUSTOMER ISSUES LIST

                                          <FlatList
                                            data={ this.state.customerIssues }
                                            extraData={this.state}
                                            renderItem={({item}) =>
                                              <View style={{ flex: 1, flexDirection: "row" }}>
                                                <View style={{ paddingRight: 5 }}>
                                                  <Checkbox
                                                    color="#1ab679"
                                                    status= {(item.isSelect)? 'checked' : 'unchecked'} 
                                                    onPress={() => { this.customerIssuesCheckbox(item.value) }}
                                                  />
                                                </View>
                                                <View style={{ paddingLeft: 5 }}>
                                                  <TouchableOpacity activeOpacity={1}>
                                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                                      {item.value}
                                                    </Text>
                                                  </TouchableOpacity>
                                                </View>

                                              </View>

                                            }
                                            keyExtractor={item => item.value}
                                            numColumns={2}
                                          />
                                        )
                                      }
                                      
                                    </View>

                                  </View>

                                </View>
                              )
                            }

                          </View>
                        ):(
                          <View></View>
                        )
                      }
                      
                      {
                        (this.state.activityBlowing)?(
                          <View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Blowing Type</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={blowingType}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.blowingType}
                                    onChangeText={(value) => this.getData(value, "blowingType")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                                <View style ={{ flex: 1, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                                </View>
                              </View>
                              
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ flex: 0.7,  }}>
                                  <Dropdown
                                    label= "Milestone From"
                                    data={this.state.milestoneSpansLen}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 33, left: 0 }}
                                    value={this.state.milestoneFromBlowing}
                                    onChangeText={(value) => this.getData(value, "milestoneFromBlowing")}
                                    error = ""
                                  />
                                </View>
                                <View style ={{ flex: 0.3, paddingLeft: 10 }}>                                
                                  <TextField
                                    label='MTS'
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    returnKeyType='next'
                                    value ={this.state.milestoneFromBlowingMTS}
                                    onChangeText={(value) => this.getData(value, "milestoneFromBlowingMTS")}
                                    error	= ""
                                  />
                                </View>
                              </View>
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ flex: 0.7,  }}>
                                  <Dropdown
                                    label= "Milestone To"
                                    data={this.state.milestoneSpansLen}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 33, left: 0 }}
                                    value={this.state.milestoneToBlowing}
                                    onChangeText={(value) => this.getData(value, "milestoneToBlowing")}
                                    error = ""
                                  />
                                </View>
                                <View style ={{ flex: 0.3, paddingLeft: 10 }}>                                
                                  <TextField
                                    label='MTS'
                                    keyboardType='default'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    returnKeyType='next'
                                    value ={this.state.milestoneToBlowingMTS}
                                    onChangeText={(value) => this.getData(value, "milestoneToBlowingMTS")}
                                    error	= ""
                                  />
                                </View>
                              </View>

                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                  <Text style={{ fontSize: 16 }}>Fiber Reading From</Text>
                                </View>
                                <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Fiber Reading To</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>
                                <View style ={{ flex:0.5, marginRight: 15 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.fiberReadingFrom}
                                    onChangeText={(value) => this.getData(value, "fiberReadingFrom")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                                
                                <View style ={{ flex:0.5 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.fiberReadingTo}
                                    onChangeText={(value) => this.getData(value, "fiberReadingTo")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Fiber Type</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={fiberType}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.fiberType}
                                    onChangeText={(value) => this.getData(value, "fiberType")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Splicing</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={splicingTermination}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.splicing}
                                    onChangeText={(value) => this.getData(value, "splicing")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Termination</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={splicingTermination}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.termination}
                                    onChangeText={(value) => this.getData(value, "termination")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                          </View>
                        ):(
                          <View></View>
                        )
                      }
                      
                      {
                        (this.state.activityHHMH)?(
                          <View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                                <View style ={{ flex: 1, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                                </View>
                              </View>
                              
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= "Milestone From"
                                    data={this.state.milestoneSpansLen}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={this.state.milestoneFromHHMH}
                                    onChangeText={(value) => this.getData(value, "milestoneFromHHMH")}
                                    error = ""
                                  />
                                </View>
                              </View>
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                                <View style ={{ width: "100%" }}>
                                  <Text style={{ color: "#999", fontSize: 10 }}>Milestone To</Text>
                                </View>                          
                              </View>
                              
                              <View style ={{ flex:1, flexDirection:'row', paddingBottom:6 }}>
                                <View style ={{ width: "100%" }}>
                                  <TextInput
                                    style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.milestoneToHHMH}
                                    editable = {false}
                                  />
                                </View>                          
                              </View>

                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 8, marginRight: 15 }}>
                                  <Text style={{ fontSize: 16 }}>Chainage Point (MTS)</Text>
                                </View>
                              </View>

                              <View style ={{ flex: 1, flexDirection:'row', }}>
                                <View style ={{ flex: 1, marginRight: 15 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.chainagePointBlow}
                                    onChangeText={(value) => this.getData(value, "chainagePointBlow")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                  <Text style={{ fontSize: 16 }}>Fiber Reading In</Text>
                                </View>
                                <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Fiber Reading Out</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>
                                <View style ={{ flex:0.5, marginRight: 15 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.fiberReadingIn}
                                    onChangeText={(value) => this.getData(value, "fiberReadingIn")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                                
                                <View style ={{ flex:0.5 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.fiberReadingOut}
                                    onChangeText={(value) => this.getData(value, "fiberReadingOut")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                  <Text style={{ fontSize: 16 }}>Lattitude</Text>
                                </View>
                                <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                  <Text style={{ fontSize: 16 }}>Longitude</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>
                                <View style ={{ flex:0.5, marginRight: 15 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.lattitude}
                                    onChangeText={(value) => this.getData(value, "lattitude")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                                
                                <View style ={{ flex:0.5 }}>
                                  <TextInput
                                    style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value= {this.state.longitude}
                                    onChangeText={(value) => this.getData(value, "longitude")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>
                            </View>
                            
                          </View>
                        ):(
                          <View></View>
                        )
                      }
                      
                      {
                        (this.state.activityInfraEquipment)?(
                          <View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>GP Name</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={gPName}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.gPName}
                                    onChangeText={(value) => this.getData(value, "gPName")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>GP Fiber Ready?</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={gpConfirm}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.GPFiberReady}
                                    onChangeText={(value) => this.getData(value, "GPFiberReady")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>                          
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>GP Infra Readiness?</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={gpConfirm}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.GPInfraReadiness}
                                    onChangeText={(value) => this.getData(value, "GPInfraReadiness")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>                          
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>GP Commissioning?</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={gpConfirm}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.GPCommissioning}
                                    onChangeText={(value) => this.getData(value, "GPCommissioning")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                          </View>
                        ):(
                          <View></View>
                        )
                      }
                      
                      {
                        (this.state.activityMachineResource)?(
                          <View>
                            {/* PLANNED SECTION IN MACHINE RESOURCE */}
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16, fontWeight: "600" }}>Planned</Text>
                                </View>
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>JCB</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedJCB}
                                    onChangeText={(value) => this.getData(value, "mRPlannedJCB")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>HDD</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedHDD}
                                    onChangeText={(value) => this.getData(value, "mRPlannedHDD")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Poklane</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedPoklane}
                                    onChangeText={(value) => this.getData(value, "mRPlannedPoklane")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedRockBreaker}
                                    onChangeText={(value) => this.getData(value, "mRPlannedRockBreaker")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Excavator</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedExcavator}
                                    onChangeText={(value) => this.getData(value, "mRPlannedExcavator")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedBackhoeLoader}
                                    onChangeText={(value) => this.getData(value, "mRPlannedBackhoeLoader")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Blowing</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedBlowing}
                                    onChangeText={(value) => this.getData(value, "mRPlannedBlowing")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Slicer</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedSlicer}
                                    onChangeText={(value) => this.getData(value, "mRPlannedSlicer")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Manpower</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRPlannedManpower}
                                    onChangeText={(value) => this.getData(value, "mRPlannedManpower")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                            {/* ACTUAL SECTION IN MACHINE RESOURCE */}

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16, fontWeight: "600" }}>Actual</Text>
                                </View>
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>JCB</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualJCB}
                                    onChangeText={(value) => this.getData(value, "mRActualJCB")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>HDD</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualHDD}
                                    onChangeText={(value) => this.getData(value, "mRActualHDD")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Poklane</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualPoklane}
                                    onChangeText={(value) => this.getData(value, "mRActualPoklane")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualRockBreaker}
                                    onChangeText={(value) => this.getData(value, "mRActualRockBreaker")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Excavator</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualExcavator}
                                    onChangeText={(value) => this.getData(value, "mRActualExcavator")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualBackhoeLoader}
                                    onChangeText={(value) => this.getData(value, "mRActualBackhoeLoader")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Blowing</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualBlowing}
                                    onChangeText={(value) => this.getData(value, "mRActualBlowing")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Slicer</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualSlicer}
                                    onChangeText={(value) => this.getData(value, "mRActualSlicer")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Manpower</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRActualManpower}
                                    onChangeText={(value) => this.getData(value, "mRActualManpower")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                            
                            {/* IDLE SECTION IN MACHINE RESOURCE */}

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16, fontWeight: "600" }}>Idle</Text>
                                </View>
                              </View>
                            </View>

                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>JCB</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleJCB}
                                    onChangeText={(value) => this.getData(value, "mRIdleJCB")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>HDD</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleHDD}
                                    onChangeText={(value) => this.getData(value, "mRIdleHDD")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Poklane</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdlePoklane}
                                    onChangeText={(value) => this.getData(value, "mRIdlePoklane")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleRockBreaker}
                                    onChangeText={(value) => this.getData(value, "mRIdleRockBreaker")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Excavator</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleExcavator}
                                    onChangeText={(value) => this.getData(value, "mRIdleExcavator")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleBackhoeLoader}
                                    onChangeText={(value) => this.getData(value, "mRIdleBackhoeLoader")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Blowing</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleBlowing}
                                    onChangeText={(value) => this.getData(value, "mRIdleBlowing")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Slicer</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleSlicer}
                                    onChangeText={(value) => this.getData(value, "mRIdleSlicer")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>
                            
                            <View style ={ styles.paddingBottomMore }>
                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ flex: 1, paddingBottom: 5 }}>
                                  <Text style={{ fontSize: 16 }}>Manpower</Text>
                                </View>
                              </View>

                              <View style ={{ flex:1, flexDirection:'row', }}>                          
                                <View style ={{ width: "100%" }}>
                                  <Dropdown
                                    label= ""
                                    data={machineResourceData}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    value={selectedData.mRIdleManpower}
                                    onChangeText={(value) => this.getData(value, "mRIdleManpower")}
                                    error = ""
                                  />
                                </View>                          
                              </View>
                            </View>

                          </View>
                        ):(
                          <View></View>
                        )
                      }
                      

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Remarks</Text></View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row', }}>                          
                          <View style ={{ width: "100%" }}>
                            <Textarea
                              containerStyle={{ height: 160, padding: 10, backgroundColor: '#f0f0f0' }}
                              style={{textAlignVertical: 'top', height: 150, fontSize: 14, color: '#8e8e8e',}}
                              onChangeText={(value) => this.getData(value, "Remarks")}
                              defaultValue={selectedData.Remarks}
                              maxLength={200}
                              placeholder={'Comment About Work'}
                              placeholderTextColor={'#8e8e8e'}
                              underlineColorAndroid={'transparent'}
                            />
                          </View>                          
                        </View>
                      </View>


                    </View>

                    <KeyboardSpacer topSpacing ={5} />

                    {/* DISPLAY ERROR MESSAGE */}
                    <View style={{ paddingVertical: 5 }}>
                      {
                        (this.state.errorMsg != "")?(
                          <Text style={{ color: "red", fontSize: 14 }}>{this.state.errorMsg}</Text>
                        ):(
                          <Text></Text>
                        )
                      }

                      {
                        (this.state.successMsg != "")?(
                          <Text style={{ color: "green", fontSize: 14 }}>{this.state.successMsg}</Text>
                        ):(
                          <Text></Text>
                        )
                      }

                    </View>
                    
                    {
                      (this.state.submitBtnDisabled)?(
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                        </TouchableOpacity>
                      ):(
                        <TouchableOpacity activeOpacity={1} style={[styles.loadMorebtn, { backgroundColor: "gray" }]} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                        </TouchableOpacity>
                      )
                    }

                  </View> 
                </ScrollView>
              )
            }
            
          </Container>
        )
    }
  
    static navigationOptions = ({navigation}) => {
      return {
        title: 'Engineer Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  
  }
}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    paddingBottomMore: {
      paddingBottom: 15
    },
    
    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }
  
  });
  
export default EngineerForm;
