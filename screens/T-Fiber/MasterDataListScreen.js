import React, { Component } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  ActivityIndicator,
  RefreshControl,
  Platform
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Container } from "native-base";
import {BASE_URL} from '../../basepath'
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get("window").height);

class MasterDataList extends Component {
  constructor(props) {
    super(props);
    this.config = {
      apiUrl:  BASE_URL + "apis/masterDataLists"
    };
    this.offset = 0;
    this.state = {
      //Loading state used while loading more data
      isLoading: true,
      loadMoreAct: false,
      usersList: [],
      displayMoreBtn: false,
      errorMsg: "",
      refreshing: false,
    };
  }

  componentDidMount() {
    this.fetchDataOnLoad();
    this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
  }

  // USING METHOD TO OPEN DRAWER
  openControlPanel = () => {
    this.props.navigation.openDrawer();
  }

  fetchDataOnLoad = async () => {
    let responseJson = [];
    let response = await fetch(this.config.apiUrl + "/" + this.offset);
    try {
      responseJson = await response.json();
      if (responseJson.hasOwnProperty("error")) {
        this.setState({ errorMsg: responseJson.error, isLoading: false });
      } else {
        this.setState({ usersList: [] });

        //Successful response from the API Call
        this.offset = this.offset + 1;
        if (responseJson.total_data > 10) {
          this.setState({
            usersList: [...this.state.usersList, ...responseJson.data],
            displayMoreBtn: true,
            isLoading: false,
            refreshing: false
          });
        } else {
          this.setState({
            usersList: [...this.state.usersList, ...responseJson.data],
            displayMoreBtn: false,
            isLoading: false,
            refreshing: false
          });
        }
      }
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  btnPressToLoadMoreData = () => {
    this.setState({ loadMoreAct: true });
    this.loadMoreData();
  };

  loadMoreData = async () => {
    let responseJson = [];
    let response = await fetch(
      this.config.apiUrl + "/" + this.offset
    );
    try {
      responseJson = await response.json();

      //Successful response from the API Call
      if (responseJson.hasOwnProperty("error")) {
        this.setState({ displayMoreBtn: false });
      } else {
        this.offset = this.offset + 1;
        if (responseJson.total_data > 10) {
          this.setState({
            usersList: [...this.state.usersList, ...responseJson.data],
            displayMoreBtn: true,
            loadMoreAct: false
          });
        } else {
          this.setState({
            usersList: [...this.state.usersList, ...responseJson.data],
            displayMoreBtn: false,
            loadMoreAct: false
          });
        }
      }
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  _handleUsersDetails(data) {
    this._detailsStateChange1();
    this.props.navigation.navigate("TFB_MasterDataDetails", { reportID: data });
  }

  _onRefresh = () => {
    this.offset = 0;
    this.setState({refreshing: true});
    this.fetchDataOnLoad();
  }

  _detailsStateChange = () => {
    this.setState({ refreshStop: false});
  }
  _detailsStateChange1 = () => {
    this.setState({ refreshStop: true});
  }

  render() {
    return (
      <Container style={styles.Container}>
        {/* START SECTION FOR LIST OF ALL USER */}
        {this.state.isLoading ? (
          <View
            style={[
              styles.ActivityIndicatorContainer,
              styles.ActivityIndicatorHorizontal
            ]}
          >
            <ActivityIndicator size="large" color="#2157c4" />
          </View>
        ) : (
          <ScrollView refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }>
            <View
              style={{ paddingVertical: 20, paddingLeft: 15, paddingRight: 15 }}
            >
              {this.state.errorMsg != "" ? (
                <View
                  style={[
                    styles.ActivityIndicatorContainer,
                    styles.ActivityIndicatorHorizontal
                  ]}
                >
                  <Text style={{ color: "red", fontSize: 16 }}>
                    {this.state.errorMsg}
                  </Text>
                </View>
              ) : (
                <View>
                  {this.state.usersList.map((item, i) => {
                    let engineerName = item.engineer_name;
                    engineerName = engineerName.toLowerCase();
                    engineerName = engineerName[0].toUpperCase() + engineerName.slice(1);

                    let spanName = item.span_name;
                    spanName = spanName.toLowerCase();
                    spanName = spanName[0].toUpperCase() + spanName.slice(1);

                    return (
                      <TouchableOpacity
                        key={i}
                        activeOpacity={1}
                        style={[styles.mainEventsClass]}
                        underlayColor="white"
                        onPress={() => this._handleUsersDetails(item.id)}
                      >
                        <View style={styles.reportsView}>
                          <View style={{ flex: 0.7 }}>
                            <View style={{ paddingLeft: 10 }}>
                              <Text style={styles.reportsTitleText}>
                                {engineerName}
                              </Text>
                              <Text style={styles.reportsSubTitleText}>
                                {spanName}
                              </Text>
                              <Text style={styles.reportsSubTitleText}>
                                {item.span_id}
                              </Text>
                            </View>
                          </View>

                          <View style={{ flex: 0.22, justifyContent: "center" }} >
                            <View style={styles.reportsGrpText}>
                              <Text
                                style={[
                                  styles.ageGroupbtn,
                                  { alignSelf: "center" }
                                ]}
                              > {item.span_length} </Text>
                            </View>
                          </View>

                          <View
                            style={{ flex: 0.08, justifyContent: "center" }}
                          >
                            <View style={styles.reportsNxtViewImg}>
                              <Image
                                style={{
                                  width: 8,
                                  height: "100%",
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/right_events.png")}
                              />
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    );
                  })}

                  {this.state.displayMoreBtn ? (
                    <View>
                      {this.state.loadMoreAct ? (
                        <TouchableOpacity
                          activeOpacity={1}
                          style={styles.loadMorebtn}
                          underlayColor="white"
                        >
                          <Text style={styles.loadMorebtnTxt}>
                            Load More....
                          </Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          activeOpacity={1}
                          style={styles.loadMorebtn}
                          underlayColor="white"
                          onPress={this.btnPressToLoadMoreData}
                        >
                          <Text style={styles.loadMorebtnTxt}>Load More</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  ) : (
                    <View></View>
                  )}
                </View>
              )}
            </View>
          </ScrollView>
        )}
        {/* END SECTION */}
      </Container>
    );
  }

  static navigationOptions = ({navigation}) => {
    return {
    title: "Master Data",
    headerTintColor: "#FFFFFF",
    headerTitleStyle: {
      alignSelf: "center",
      textAlign: "center",
      flex: 0.8,
      fontSize: 22,
      lineHeight: 25
    },
    headerStyle: {
      height: 84,
      borderBottomWidth: 0
    },
    headerLeft:(
      <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
        <MaterialIcons
          name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
      </TouchableOpacity>
    ),
    headerBackground: (
      <View>
        <LinearGradient
          colors={["#2157c4", "#3b5998", "#1ab679"]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{
            paddingTop: screenHeight - (screenHeight - screenHeight / 25),
            ...Platform.select({
              ios: {
                shadowOpacity: 0.3,
                shadowRadius: 10,
                shadowOffset: {
                    height: 1,
                    width: 0
                },
              },
              android: {
                elevation: 10,
              },
            })
          }}
        >
          <View style={{ width: "60%", marginLeft: "20%" }}>
            <Image
              style={{
                width: "100%",
                height: "100%",
                alignSelf: "center",
                zIndex: 1,
                opacity: 0.2
              }}
              source={require("../../assets/images/logo.png")}
              resizeMode="contain"
            />
          </View>
        </LinearGradient>
      </View>
    )
  };
  }
}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: "center"
  },
  ActivityIndicatorHorizontal: {
    flexDirection: "row",
    justifyContent: "space-around"
  },
  Container: {
    backgroundColor: "#eeeeee"
  },
  body: {
    paddingBottom: 20
  },
  sectionContainer: {
    marginTop: screenHeight - (screenHeight - screenHeight / 8.5),
    marginLeft: 20,
    marginRight: 20,
    paddingHorizontal: 12,
    // paddingVertical: 12,
    backgroundColor: "#FFFFFF",
    flex: 1,
    flexDirection: "column",
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 4,
      },
    }),
    borderRadius: 2
  },
  profileSection: {
    backgroundColor: "#ffffff",
    position: "absolute",
    top: 60,
    left: 0,
    zIndex: 2,
    width: "90%",
    marginLeft: "5%",
    alignSelf: "center",
    borderRadius: 4,
    flex: 1,
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 5,
      },
    }),
  },
  mainEventsClass: {
    marginTop: 4,
    marginBottom: 4,
    borderLeftColor: "#146FA9",
    borderLeftWidth: 4,
    borderRadius: 4,
    justifyContent: "center",
    ...Platform.select({
      ios: {
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 1,
      },
    })
    // height: (screenHeight-(screenHeight-(screenHeight/12)))
    // ...Platform.select({
    //   ios:{
    //     shadowColor:'#000000',
    //     shadowOffset:{width:0,height:1},
    //     shadowOpacity:0.5,
    //     shadowRadius:2
    //   }
    // })
  },
  reportsView: {
    flexDirection: "row",
    flex: 1,
    backgroundColor: "#FFFFFF",
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    paddingVertical: 15
  },
  reportsTimeText: {
    alignSelf: "center",
    // paddingLeft:3,
    flex: 0.5,
    fontWeight: "900"
  },
  reportsSubTimeText: {
    textAlign: "center",
    lineHeight: 20,
    fontSize: 14,
    paddingLeft: 3,
    paddingRight: 3,
    color: "rgba(0, 0, 0, 0.54)",
    flex: 0.5
  },
  reportsGrpText: {
    flex: 1,
    width: "100%",
    alignSelf: "center",
    textAlign: "center",
    maxHeight: 24,
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#25AE88",
    borderRadius: 12
  },
  reportsTitleText: {
    fontSize: 16,
    lineHeight: 21,
    color: "rgba(0, 0, 0, 0.87)"
  },
  reportsSubTitleText: {
    fontSize: 13,
    lineHeight: 18,
    color: "rgba(0, 0, 0, 0.54)"
  },
  reportsNxtViewImg: {
    alignSelf: "center",
    justifyContent: "center",
    marginRight: 5
  },
  loadMorebtn: {
    borderRadius: 50,
    flex: 1,
    backgroundColor: "#25AE88",
    paddingVertical: 15,
    marginTop: 15
  },
  loadMorebtnTxt: {
    fontWeight: "bold",
    color: "#fff",
    textAlign: "center"
  },

  ageGroupbtn: {
    marginLeft: 0,
    marginRight: 0,
    fontSize: 10,
    lineHeight: 25,
    textAlign: "right",
    color: "#ffffff"
  },
  profileImage: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: "center",
    position: "absolute",
    top: -50,
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 6,
      },
    }),
    borderWidth: 1,
    borderColor: "#ffffff"
  },
  profileImageStyle: {
    width: 100,
    height: 100,
    borderRadius: 50,
    alignSelf: "center"
  },
  flexColumn: {
    marginTop: screenHeight / 8,
    flex: 1,
    marginBottom: 20
  },
  userProfileName: {
    fontSize: 24,
    lineHeight: 16,
    alignSelf: "center",
    paddingTop: 8,
    paddingBottom: 8
  },
  itemsSection: {
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#dadada",
    borderBottomWidth: 1,
    paddingVertical: 14
  },
  sectionIcon: {
    flex: 0.4,
    justifyContent: "center"
  },
  sectionTitleIcon: {
    flex: 0.16,
    paddingVertical: 5
  },
  sectionTitle: {
    flex: 0.83,
    fontSize: 16,
    fontWeight: "600",
    color: "#898989",
    alignSelf: "center"
  },
  sectionUserDet: {
    alignSelf: "flex-end",
    fontWeight: "600",
    fontSize: 16,
    color: "#898989",
    paddingTop: 4
  }
});

export default MasterDataList;
