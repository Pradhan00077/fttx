import React from 'react';
import { View, AsyncStorage } from 'react-native';
import {BASE_URL} from '../../basepath'
import Database from '../../Database';
const db = new Database();
let interval = 0;


class LocationTracking extends React.Component {

  constructor(props) {
    super(props);
    this.config = {
      submitLatLongAPI:  BASE_URL + "apis/trackedDataSubmit/",
    }
    this.state = {
      trackedData: {},
      isLoading: true
    }
  }
  
  startTrackingInterval(callMtd, callInterval, callIntervalDist) {
    let userID = '';
    let totalCountedTime = 0;
    let stopTime = false;

    AsyncStorage.getItem('user_id')
    .then((value) => {
      const data = JSON.parse(value);
      userID = data;
    });
    

    interval = setInterval(() => {
      let twoMint = 10000*6*2;
      // CALLING PROGRESS BAR INTERVAL EVERY 10 SECOND
      callInterval();

      if(twoMint === totalCountedTime){
        if(stopTime === true){
          callMtd();
          alert("Tracking Stopped!");
          this.clearIntervalMtd();
        }else{
          totalCountedTime = totalCountedTime-60000;
          alert("Lat Long tracking runing from last 2 minutes. ");
          stopTime = true;
        }
      }else{
        totalCountedTime = totalCountedTime+10000;
      }

      navigator.geolocation.getCurrentPosition(
          position => {
            const lattitude = JSON.stringify(position.coords.latitude);
            const longitude = JSON.stringify(position.coords.longitude);
            let {self} = this;

            // GETTING DISTANCE 
            callIntervalDist(lattitude, longitude);

            let data = {
              userId: userID,
              lattitude: lattitude,
              longitude: longitude,
            }

            db.transaction( tx => {
                tx.executeSql("insert into workdoneActivity (userid, lattitude, longitude, date) values (?, ?, ?, ?)", [data.userId, data.lattitude, data.longitude, "dates"]);
              }
            );
          },
          error => {
            console.log(" error - ", error)
            callMtd();
            alert("Tracking Stopped!");
            this.clearIntervalMtd();
          },
          { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );

      }, 10000);
      return () => clearInterval(interval);
  }

  clearIntervalMtd(){
    clearInterval(interval);
    console.log(" Stoped...... 1")
  }

  storeTrackedData= async(recordID) => {
    let rowsData = [];
    db.transaction( tx => {
      rowsData = rowsData;
        tx.executeSql("select * from workdoneActivity", [], (_, { rows }) =>{
            rowsData = rows._array;
            this.storeTrackedDataCall(recordID, rowsData);
          }
        );
      }
    );
  }

  // CALLBACK FUNCTION TO STORE DATA ON SERVER 
  storeTrackedDataCall= async(recordID, rowsData) => {
    let responseJson = [];

    let response = await fetch(this.config.submitLatLongAPI+recordID, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        data: rowsData
      })
    })
    try {
      responseJson = await response.json();
      console.log("Location Tracking:-", responseJson);
    } catch (error) {
      console.log("Response error - ",error);
    }
  }
  

  render() {

    return (
      <View></View>
    );

  }
}

export default LocationTracking;
