import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, ActivityIndicator, AsyncStorage, Platform } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';
import {BASE_URL} from '../../basepath'
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class MyTeam extends Component {

    constructor(props) {
      super(props);
      // this.paramsData = this.props.navigation.state.params;
      this.config = {
        apiUrl:  BASE_URL + "apis/TFB/asmTeamData"
      }
      this.offset = 0;
      this.state = {
        //Loading state used while loading more data
        isLoading: true,
        loadMoreAct: false,
        usersList: [],
        displayMoreBtn: false,
        errorMsg: ""
      };
      
    }

    componentDidMount(){
      this.fetchDataOnLoad();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }
  

    fetchDataOnLoad = async() => {
      const userId = await AsyncStorage.getItem('user_id');
      let responseJson = [];
        
      let response = await fetch(this.config.apiUrl+"/"+userId+"/"+this.offset);
      try {
        responseJson = await response.json();
        this.offset = this.offset + 1;

        //Successful response from the API Call
        if(responseJson.hasOwnProperty('error')){
          this.setState({ errorMsg: responseJson.error, isLoading: false });
        }else{
          
          if(responseJson.total_data > 10 ){
            this.setState({ usersList: [...this.state.usersList, ...responseJson.data], displayMoreBtn: true, isLoading: false });
          }else{
            this.setState({ usersList: [...this.state.usersList, ...responseJson.data], displayMoreBtn: false, isLoading: false  });
          }
          
        }
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    btnPressToLoadMoreData = () => {
      this.setState({ loadMoreAct: true });
      this.loadMoreData();
    }

    loadMoreData = async() => {
      const userId = await AsyncStorage.getItem('user_id');
      let responseJson = [];
        
      let response = await fetch(this.config.apiUrl+"/"+userId+"/"+this.offset);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        if(responseJson.hasOwnProperty('error')){
          this.setState({ displayMoreBtn: false, isLoading: false });
        }else{

          this.offset = this.offset + 1;  
          if(responseJson.total_data > 10 ){
            this.setState({ usersList: [...this.state.usersList, ...responseJson.data], displayMoreBtn: true, loadMoreAct: false });
          }else{
            this.setState({ usersList: [...this.state.usersList, ...responseJson.data], displayMoreBtn: false, loadMoreAct: false  });
          }

        }

      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    _handleUsersDetails(data){
      // alert("Comming Soon!");
      this.props.navigation.navigate('TeamUserDetails', {'userID': data, 'ProjectID':'TFB'});
    }

    render() {
        return (
          <Container style={styles.Container}>
            
            {/* START SECTION FOR LIST OF ALL USER */}
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView>
                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >
                {
                    (this.state.errorMsg != '')?(
                        <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                          <Text style={{ color: "red", fontSize: 16 }}>{this.state.errorMsg}</Text>
                        </View>
                      ):(
                        <View>
                        {
                          this.state.usersList.map((item, i) => {   
                            let userRole = '';
                            let userProject = '';
      
                            if(item.user_role === 'adm'){ userRole = 'Admin'; }
                            else if(item.user_role === 'asm'){ userRole = 'Area manager'; }
                            else if(item.user_role === 'sdm'){ userRole = 'Sub Admin'; }
                            else{ userRole = 'Engineer'; }
                            
                            if(item.project_id === 'RJO'){ userProject = 'R-Jio'; }
                            else if(item.project_id === 'MNT'){ userProject = 'MahaNet'; }
                            else if(item.project_id === 'TFB'){ userProject = 'T-Fiber'; }
                            else{ userProject = 'MP4'; }

                              return(
                                <TouchableOpacity key={i} activeOpacity={1} style ={[ styles.mainEventsClass ]} underlayColor="white" onPress={() => this._handleUsersDetails(item.user_id)} >
                                  <View style={styles.reportsView}>
      
                                    <View style ={{borderColor:'#146FA9',borderRightWidth:2, flex:0.20 }}>
                                      <View style={{ flex: 1, flexDirection: "row" }}>
                                        <View style={[styles.reportsTimeText, { paddingLeft: 5}]} >
                                          <Image style={{ width: 19, height: 19, marginRight: 3, alignSelf: "flex-end" }} source={require("../../assets/images/email-1.png")} />
                                        </View>
                                        <View style={[styles.reportsTimeText]} >
                                          <Image style={{ width: 19, height: 19, marginLeft: 3, }} source={require("../../assets/images/call.png")} />
                                        </View>
                                      </View>
                                    </View>
      
                                    <View style ={{flex:0.72}}>
                                      <View style = {{paddingLeft:10}}>
                                        <Text style ={styles.reportsTitleText}>{item.first_name}</Text>
                                        <Text style ={styles.reportsSubTitleText}>{userRole} | {userProject}</Text>
                                      </View>
                                    </View>
      
                                    <View style ={{ flex:0.08, justifyContent:'center'}}>
                                      <View style ={styles.reportsNxtViewImg}>
                                        <Image
                                          style={{width:8, height:'100%', resizeMode:'contain'}}
                                          source = {require('../../assets/images/right_events.png')}
                                        />
                                      </View>
                                    </View>
      
                                  </View>
                                </TouchableOpacity>
                              );
                          })
                        }
                        
                        {
                          (this.state.displayMoreBtn)?(
                            <View>
                              {
                                (this.state.loadMoreAct)?(
                                  <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                                    <Text style={styles.loadMorebtnTxt}>Load More....</Text>
                                  </TouchableOpacity>
                                ):(
                                  <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress={this.btnPressToLoadMoreData}>
                                    <Text style={styles.loadMorebtnTxt}>Load More</Text>
                                  </TouchableOpacity>
                                )
                              }
                            </View>
                          ):(
                            <View></View>
                          )
                        }
                        </View>                        
                      )
                  }

                </View> 
              </ScrollView>
              )
            }
            {/* END SECTION */}

          </Container>
        )
    }
  
  
    static navigationOptions = ({navigation}) => {
      return {
        title: 'My Team',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign:"center", 
          flex:0.8,
          fontSize:22,
          lineHeight:25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
      }
    }
}

const styles = StyleSheet.create({
    ActivityIndicatorContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    ActivityIndicatorHorizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
    },
    Container: {
      backgroundColor: "#eeeeee"
    },
    body: {
      paddingBottom: 20,
    },
    sectionContainer: {
      marginTop: (screenHeight-(screenHeight-(screenHeight/8.5))),
      marginLeft: 20,
      marginRight: 20,
      paddingHorizontal: 12,
      // paddingVertical: 12,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "column",
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 4,
        },
      }),
      borderRadius: 2,
    },
    profileSection: {
      backgroundColor:'#ffffff',
      position:'absolute',
      top: 60,
      left: 0,
      zIndex: 2,
      width:'90%',
      marginLeft:'5%',
      alignSelf:'center',
      borderRadius: 4,
      flex: 1,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 5,
        },
      }),
    },
    mainEventsClass:{
      marginTop:4,
      marginBottom:4, 
      borderLeftColor:'#146FA9',
      borderLeftWidth:4,
      borderRadius:4, 
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 3,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 1,
        },
      }),
      justifyContent:'center',

    },
    reportsView: {
      flexDirection:'row',
      flex:1,
      backgroundColor: "#FFFFFF",
      borderTopRightRadius: 4,
      borderBottomRightRadius: 4,
      paddingVertical: 15
    },
    reportsTimeText: {
      alignSelf:'center',

      flex: 0.5,
    },
    reportsSubTimeText: {
      textAlign:'center',
      lineHeight:20,
      fontSize:14,
      paddingLeft:3,
      paddingRight:3,
      color:'rgba(0, 0, 0, 0.54)',
      flex: 0.5
    },
    reportsGrpText: {
      flex:1,
      width:'100%',
      alignSelf:'center',
      textAlign:'center',
      maxHeight:24,
      flex:1,
      justifyContent:'center',
      backgroundColor:'#25AE88',
      borderRadius:12,
    },
    reportsTitleText: {
      fontSize:16,
      lineHeight:21,
      color:'rgba(0, 0, 0, 0.87)'
    },
    reportsSubTitleText: {
      fontSize:13,
      lineHeight:18,
      color:'rgba(0, 0, 0, 0.54)'
    },
    reportsNxtViewImg: {
      alignSelf:'center',
      justifyContent:'center',
      marginRight:5
    },
    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center"
    },

    ageGroupbtn:{
      marginLeft:0,
      marginRight:0,
      fontSize:10,
      lineHeight:25, 
      textAlign:'right', 
      color: '#ffffff'
    },
    profileImage:{
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center',
      position:'absolute',
      top:-50,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 4,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 6,
        },
      }),
      borderWidth:1,
      borderColor: "#ffffff",    
    },
    profileImageStyle: {
      width:100,
      height:100,
      borderRadius:50,
      alignSelf:'center'
    },
    flexColumn:{
      marginTop:(screenHeight/8),
      flex:1,
      marginBottom:20,
    },
    userProfileName: {
      fontSize: 24,
      lineHeight: 16,
      alignSelf: "center",
      paddingTop:8,
      paddingBottom:8,
    },    
    itemsSection: {
      flex: 1,
      flexDirection: "row",
      borderBottomColor: "#dadada",
      borderBottomWidth: 1,
      paddingVertical: 14
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitleIcon: {
      flex: 0.16,
      paddingVertical: 5
    },
    sectionTitle: {
      flex: 0.83,
      fontSize: 16,
      fontWeight: '600',
      color: "#898989",
      alignSelf:"center",
    },
    sectionUserDet: {
      alignSelf: "flex-end",
      fontWeight: "600",
      fontSize: 16,
      color: "#898989",
      paddingTop: 4
    },
  
  });
  
export default MyTeam;
