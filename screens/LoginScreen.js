import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator, } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import axios from 'axios';
import {BASE_URL} from '../basepath';
const screenHeight = Math.round(Dimensions.get('window').height);


class Login extends Component {

    constructor(props) {
      super(props);
      // ALL API
      this.config = {
        apiUrl: BASE_URL + "apis/login"
      }
      this.state = {
          isLoading: false,
          emailErrorMsg: "",
          passErrorMsg: "",
          username: "",
          password: ""
      };
     }

    // USING METHOD TO CALL LOGIN USER METHOD 
     _handleButtonPressLogin = async () =>{
      this.setState({ isLoading: true });
      this.loginUser();
    }

    // USING METHOD TO SET LOGIN USER DETAILS IN LOCAL STORAGE 
    async saveItem(item, selectedValue) {
      try {
        await AsyncStorage.setItem(item, selectedValue); 
      } catch (error) {
        console.error('AsyncStorage error: ' + error.message);
      }
    }

    // USING METHOD TO CALL API FOR LOGIN USER
    async loginUser(){
        let self = this;
        let responseJson = [];
        
        let response = await fetch(this.config.apiUrl, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: self.state.username,//'9320036741',
            password: self.state.password,
          })
        })
        try {
          responseJson = await response.json();
          // Successful response from the API Call

          if(responseJson.hasOwnProperty('error')){
            if(responseJson.error.hasOwnProperty('username')){
              this.setState({isLoading: false, emailErrorMsg: responseJson.error.username, passErrorMsg: ""});
            }else if(responseJson.error.hasOwnProperty('password')){
              this.setState({isLoading: false, emailErrorMsg: "", passErrorMsg: responseJson.error.password});
            }else{
              this.setState({isLoading: false, emailErrorMsg: responseJson.error, passErrorMsg: ""});
            }
          }else{
            self.saveItem('userToken', responseJson.success.api_token);
            self.saveItem('user_id', responseJson.success.user_id);
            self.saveItem('user_project_id', responseJson.success.user_project_id);
            self.saveItem('user_role', responseJson.success.user_role);
            self.saveItem('username', responseJson.success.username);
            self.saveItem('useremail', responseJson.success.useremail );
            self.saveItem('name', responseJson.success.user_name );
            
            self.setState({ isLoading: false, emailErrorMsg: "", passErrorMsg: "" });
            self.props.navigation.navigate('App')
          }
        } catch (error) {
          alert(error);
          console.log("Response error - ",error);
          this.setState({ isLoading: false });
        }
    }

    // USING METHOD TO GET & SET DATA FROM FIELDS IN STATE
    cmpSetTextState = ( txtVal, txtName ) => {
      if(txtName === 'email'){
        this.setState({ username: txtVal });
      }else if(txtName === 'password'){
        this.setState({ password: txtVal });
      }
    }

    render() {

        return (
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4" animating={true}/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                <LinearGradient colors={['#2157c4', '#3b5998', '#1ab679']} style={styles.body}>
                  
                  <View style={styles.sectionContainer}>
                      <View style={styles.logoSection}>
                          <Image source = {require('../assets/images/logo.png')} resizeMode='contain' style={{ alignSelf: "center"}} />
                      </View>

                      <View style={{ marginBottom: 4 }}>
                        <Text style={styles.sectionTitle}>Welecome!</Text>
                        <Text style={styles.sectionDescription}>
                          Login and start reporting
                        </Text>
                      </View>

                      <View style={{ marginBottom: 25 }}>
                        
                        <View style={{ marginLeft: 10, marginRight: 10 }}>
                            <TextField
                              label='Mobile No.'
                              style={{ color: "#FFF" }}
                              tintColor = "#FFFFFF"
                              baseColor = "#FFFFFF"
                              keyboardType='email-address'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.username}
                              onChangeText={(value) => this.cmpSetTextState(value, "email")}
                              error	= {this.state.emailErrorMsg}
                            />
                            
                            <TextField
                              label='Password'
                              style={{ color: "#FFF" }}
                              tintColor = "#FFFFFF"
                              baseColor = "#FFFFFF"
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              clearTextOnFocus={true}
                              secureTextEntry={true}
                              maxLength={35}
                              returnKeyType='done'
                              value={this.state.password}
                              onChangeText={(value) => this.cmpSetTextState(value, "password")}
                              error	= {this.state.passErrorMsg}
                            />
                            <Text onPress={() => this.props.navigation.navigate("ForgetPassword")} style ={ (this.state.passErrorMsg != "")?styles.labelForgetColor1:styles.labelForgetColor} >
                              Forget Password?
                            </Text>

                        </View>

                      </View>

                    
                      <KeyboardSpacer topSpacing ={10} />

                      <Button style={styles.buttonText} onPress={this._handleButtonPressLogin}>
                        <Text style={{ flex: 1, textAlign: "center", fontSize: 18, fontWeight: 'bold' }}>Login</Text>
                      </Button>

                  </View>
                
                </LinearGradient>
              </ScrollView>
              
            )
            }
          </Container>
        )
    }
  
  // USING navigationOptions TO SET HEADER OF SCREEN
  static navigationOptions = {
    header: null
  }

}

// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#2157c4',
    backgroundColor: '#FFFFFF',
  },
  labelForgetColor1:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },

});
  
export default Login;
