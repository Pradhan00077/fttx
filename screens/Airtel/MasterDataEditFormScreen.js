import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, TextInput, AsyncStorage, ActivityIndicator} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import * as Network from 'expo-network';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Database from '../../Database';
const db = new Database();
import {BASE_URL} from '../../basepath'
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class MasterDataForm extends Component {
    constructor(props) {
      super(props);
      this.paramsData = this.props.navigation.state.params;
      this.config = {
        getFormDataAPI: BASE_URL + "apis/airtel/masterDataDetails",
        submitFormAPI: BASE_URL + "apis/airtel/masterDataEditFormSubmit",
        apiAllEngsUrl: BASE_URL + "apis/airtel/allEngineers",
        apiAllAsmUrl: BASE_URL + "apis/airtel/allEngineersASM",        
      }
      this.state = {
        isLoading: false,
        userLoggedInId: 0,
        allMasterDataDetails: [],
        allEngs: [],
        zone: "",
        state: "",
        project: "",
        span_name: "",
        span_length: "",
        area_manager_name: "",
        area_manager_mobile: "",
        engineer_mobile: "",

        selectedData: {
          project: "",
          state: "",
          zone: "",
          span_name: "",
          span_length: "",
          engineer_name: "",
        }
      }
    }

    componentDidMount(){
      this.fetchUserDataState();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    fetchUserDataState = async() => {
      const userid = await AsyncStorage.getItem('user_id');

      const ipAddress = await Network.getIpAddressAsync();
      // const macAddress = await Network.getMacAddressAsync();
      this.state.selectedData["ipAddress"] = ipAddress;
      // this.state.selectedData["macAddress"] = macAddress;
      this.state.selectedData["user_id"] = userid;
      this.setState({ userLoggedInId: userid });
      this.fetchAllEngsOnLoad();
      this.fetchDataOnLoad();
    }

    fetchDataOnLoad = async () => {
      let responseJson = [];
      let response = await fetch( this.config.getFormDataAPI+"/"+this.paramsData.reportID);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ 
          engineer_mobile: responseJson.data.engineer_mobile,
          area_manager_mobile: responseJson.data.area_manager_mobile,
          area_manager_name: responseJson.data.area_manager_name,
          zone: responseJson.data.zone,
          state: responseJson.data.state,
          project: responseJson.data.project,
          span_name: responseJson.data.span_name,
          span_length: responseJson.data.span_length,
        });
        this.state.selectedData.engineer_mobile =  responseJson.data.engineer_mobile;
        this.state.selectedData.engineer_name =  responseJson.data.engineer_name;
        this.state.selectedData.area_manager_mobile = responseJson.data.area_manager_mobile;
        this.state.selectedData.area_manager_name = responseJson.data.area_manager_name;
        this.state.selectedData.zone = responseJson.data.zone;
        this.state.selectedData.state = responseJson.data.state;
        this.state.selectedData.project = responseJson.data.project;
        this.state.selectedData.span_name = responseJson.data.span_name;
        this.state.selectedData.span_length = responseJson.data.span_length;
        this.state.selectedData.reportID = responseJson.data.sl_no;
      } catch (error) {
        console.log("Response error - ", error);
      }
    };

    fetchAllEngsOnLoad = async () => {
      let responseJson = [];
      let response = await fetch( this.config.apiAllEngsUrl );
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ allEngs: responseJson.data });
      } catch (error) {
        console.log("Response error - ", error);
      }
    };
    
    fetchAllAsmOnSelection= async (mobileNo) => {
      let responseJson = [];
      let response = await fetch( this.config.apiAllAsmUrl+"/"+mobileNo);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
        this.setState({ area_manager_name: responseJson.data[0].asm_name, area_manager_mobile: responseJson.data[0].asm_mobile });
        
        this.state.selectedData.area_manager_name = responseJson.data[0].asm_name;
        this.state.selectedData.area_manager_mobile = responseJson.data[0].asm_mobile;
      } catch (error) {
        console.log("Response error - ", error);
      }
    };

    
    // GETING DATA FROM SELECT BOX AND USING VALIDATION
    async getDatas(value, index, data, fieldName){
      if(fieldName === 'engineer_name'){
        this.fetchAllAsmOnSelection(value);
        
        this.setState({ engineer_mobile: value });
        this.state.selectedData.engineer_mobile = value;
        this.state.selectedData[fieldName] = data[index].label;
      }
    }

    // GETING DATA FROM FIELDS AND USING VALIDATION
    async getData(data, fieldName){
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });
    }

    // USING FOLLOWING COMPONENT TO VALIDATE FORM
    fnSubmitValidateForm = async() => {
      let self = this;
      const { selectedData } = this.state;
      this.setState({ errorMsg: "", successMsg: "", isLoading: true });

      if(selectedData.project === ''){
        this.setState({ errorMsg: "Please enter Project!", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.state === ''){
        this.setState({ errorMsg: "Please enter State!", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.zone === ''){
        this.setState({ errorMsg: "Please enter Zone!", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.span_name === ''){
        this.setState({ errorMsg: "Please enter Span Name!", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.span_length === ''){
        this.setState({ errorMsg: "Please enter Span Length!", successMsg: "", isLoading: false });
        return false;
      }else if(selectedData.engineer_name === ''){
        this.setState({ errorMsg: "Please select Engineer!", successMsg: "", isLoading: false });
        return false;
      }else{
        this.setState({ errorMsg: "", successMsg: "" });
        this.submitFormData();
      }
    }

    // FOLLOWING COMPONENT USING TO SUBMIT FORM
    submitFormData = async() => {
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      let response = await fetch(this.config.submitFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        responseJson = await response.json();
        alert(responseJson.success);
        this.setState({ successMsg: responseJson.success, isLoading: false });
      } catch (error) {
        console.log("Response error - ", error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }
    }

    render() {
      return (
          <Container style={styles.Container}>
            {/* START SECTION */}            
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >
                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Project</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "project")}
                            value={this.state.project}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>State</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "state")}
                            value={this.state.state}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Zone</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "zone")}
                            value={this.state.zone}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Span Name</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "span_name")}
                            value={this.state.span_name}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Span Length</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, borderBottomWidth: 1, borderBottomColor: "#878787" }}
                            onChangeText={(value) => this.getData(value, "span_length")}
                            keyboardType="number-pad"
                            value={this.state.span_length}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Engineer Name</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allEngs}
                            dropdownPosition={0}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value, index, data) => this.getDatas(value, index, data, 'engineer_name')}
                            value={this.state.engineer_mobile}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Engineer Mobile</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, backgroundColor: "#e4e4e4" }}
                            value = {this.state.engineer_mobile}
                            editable={false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Area Manager Name</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, backgroundColor: "#e4e4e4" }}
                            value = {this.state.area_manager_name}
                            editable={false}
                          />
                        </View>
                      </View>
                    </View>
                    
                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Area Manager Mobile</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, color: "#878787", paddingLeft: 3, borderRadius: 2, backgroundColor: "#e4e4e4"}}
                            value = {this.state.area_manager_mobile}
                            editable={false}
                          />
                        </View>
                      </View>
                    </View>

                  </View>

                  <KeyboardSpacer topSpacing ={5} />

                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 5 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                  </View>

                  <View>
                    {
                      (this.state.isLoading)?
                      (
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                        </TouchableOpacity>
                      ):(
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                        </TouchableOpacity>
                      )
                    }
                  </View>

                </View>
              </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }


    static navigationOptions = ({navigation}) => ({
        title: 'Master Data Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center",
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    })

}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    ActivityIndicatorContainer: {
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#00000040',
      position: "absolute",
      width: "100%",
      height: screenHeight-60
    },
    activityIndicator: {
      backgroundColor: '#fff',
      height: 60,
      width: 60,
      borderRadius: 10,
      display: 'flex',
      alignItems: 'center',
      zIndex: 999,
      justifyContent: 'space-around'
    },
    headingFormStyle: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#adb9ca",
      paddingBottom: 10,
      paddingTop: 10,
      paddingLeft: 5,
      borderRadius: 10,
      borderColor: "#517bc5",
      borderWidth: 1
    },
    paddingBottomMore: {
      paddingBottom: 15
    },

    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }

  });

export default MasterDataForm;
