import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Dimensions , RefreshControl } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container } from 'native-base';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class IssueReports extends Component {

    constructor(props) {
      super(props);
      this.state={
        refreshing: false,
      }
    }

    _handleButtonPress = () => {
      this.setState({ loginSuccess: "Success Login!" });
      this.props.navigation.navigate('ReportDetails');
    }

    componentDidMount(){
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    _onRefresh = () => {
      this.offset = 0;
      this.setState({ refreshing: true, loadMoreAct: false });
      this.fetchDataOnLoad();
    }

    render() {

      let data = [{
        value: 'Akola',
      }, {
        value: 'Betul',
      }, {
        value: 'Bhawanipatna',
      }];

      return (
          <Container style={styles.Container}>

          {/* START SECTION FOR PROFILE MIDLLE WITH DETAILS OF USER */}
          <ScrollView refreshControl={
                <RefreshControl
                  refreshing={this.state.refreshing}
                  onRefresh={this._onRefresh}
                  contentInsetAdjustmentBehavior="automatic"
                />
              }>
            <View style={styles.body}>
              <View style={styles.sectionContainer}>

                <View style={styles.itemsSection}>
                  <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-start" }}>
                    <Text style={[styles.sectionTitle, { textAlign: "center" }]}>Comming Soon</Text>
                  </View>
                </View>

              </View>
            </View>
          </ScrollView>
          {/* END SECTION */}

        </Container>
        )
    }
  
  
    static navigationOptions = ({navigation}) => ({
        title: 'Issue Reports',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    })

}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: "#eeeeee"
  },
  body: {
    paddingBottom: 20,
  },
  sectionContainer: {
    marginTop: (screenHeight-(screenHeight-(screenHeight/8.5))),
    marginLeft: 20,
    marginRight: 20,
    paddingHorizontal: 12,
    // paddingVertical: 12,
    backgroundColor: "#FFFFFF",
    flex: 1,
    flexDirection: "column",
    elevation: 4,
    borderRadius: 2,
  },
  profileSection: {
    backgroundColor:'#ffffff',
    position:'absolute',
    top: 60,
    left: 0,
    zIndex: 2,
    width:'90%',
    marginLeft:'5%',
    alignSelf:'center',
    borderRadius: 4,
    flex: 1,
    elevation: 5
  },
  profileImage:{
    width:100,
    height:100,
    borderRadius:50,
    alignSelf:'center',
    position:'absolute',
    top:-50,
    elevation: 6,
    borderWidth:1,
    borderColor: "#ffffff",    
  },
  profileImageStyle: {
    width:100,
    height:100,
    borderRadius:50,
    alignSelf:'center'
  },
  flexColumn:{
    marginTop:(screenHeight/8),
    flex:1,
    marginBottom:20,
  },
  userProfileName: {
    fontSize: 24,
    lineHeight: 16,
    alignSelf: "center",
    paddingTop:8,
    paddingBottom:8,
  },    
  itemsSection: {
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#dadada",
    borderBottomWidth: 1,
    paddingVertical: 14
  },
  sectionIcon: {
    flex: 0.4,
    justifyContent: "center",
  },
  sectionTitleIcon: {
    flex: 0.16,
    paddingVertical: 5
  },
  sectionTitle: {
    flex: 0.83,
    fontSize: 16,
    fontWeight: '600',
    color: "#898989",
    alignSelf:"center",
  },
  sectionUserDet: {
    alignSelf: "flex-end",
    fontWeight: "600",
    fontSize: 16,
    color: "#898989",
    paddingTop: 4
  },

});
  
export default IssueReports;
