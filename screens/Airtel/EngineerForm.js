import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Button, TouchableOpacity, Dimensions, TextInput, AsyncStorage, FlatList } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container, Radio, CheckBox } from 'native-base'; 
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import * as Network from 'expo-network';
import Textarea from 'react-native-textarea';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import * as Progress from 'react-native-progress';
import {BASE_URL} from '../../basepath'
import Database from '../../Database';
const db = new Database();
import { MaterialIcons } from '@expo/vector-icons';
const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

class EngineerForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        totalZONEAPI:  BASE_URL + "apis/airtel/totalzone",
        SpanAPI:  BASE_URL + "apis/airtel/totalSpan",
        SpanLength:  BASE_URL + "apis/airtel/spanLength",
        ActivitiesAPI:  BASE_URL + "apis/airtel/activities",
        IssuesAPI:  BASE_URL + "apis/airtel/issues",
        CheckTransAPI: BASE_URL + "apis/airtel/checkOldTrans",

        submitFormAPI:  BASE_URL + "apis/airtel/insertReportData",
      }
      this.state = {
        isLoading: false,
        allzone: [],
        allSpan: [],
        activities: [],
        internalIssues: [],
        customerIssues: [],
        userName: 0,
        userRole: 0,
        userLoggedInId: 0,
        IssuesReportChange: true,
        IssuesReportValue: "No",
        spanLength: "",
        activity: "T&D",
        IssuesChecked: 'Internal',
        milestoneChecked: 'WIP',
        milestoneTo: "1",
        milestoneFrom: "0",
        chainageFrom: "0",
        chainageTo: "1",
        isCheckedInternalIssues: [],
        isCheckedCustomerIssues: [],
        activityDisabled: false,
        activityTD: true,
        activityBlowing: false,
        activityChamberInst: false,
        activityMachineResource: false,
        activityAcceptanceTest: false,
        activityHoTo: false,
        lattitude: "0.000",
        longitude: "0.000",
        errorMsg: "",
        slctDateVal: new Date(),
        submitBtnDisabled: true,
        countedMint: 0,
        Depth:'',
        milestoneSpansLen: [],


        selectedData: {
          span: "",
          zone:"",
          activity: "T&D",
          issuesReportTD: "No",
         
          chainageFrom: "0",
          chainageTo: "1",
          milestoneStatus: "WIP",
          issues: [],
          issuesType: "Internal",
          issues_TD_Hndl: "No",
          lattitude: "0.000",
          longitude: "0.000",
          milestoneFrom: "0",
          milestoneTo: "1",
          mRPlannedJCB: "0", mRActualJCB: "0", mRIdleJCB: "0",
          mRPlannedHDD: "0", mRActualHDD: "0", mRIdleHDD: "0",
          mRPlannedPoklane: "0", mRActualPoklane: "0", mRIdlePoklane: "0",
          mRPlannedRockBreaker: "0", mRActualRockBreaker: "0", mRIdleRockBreaker: "0",
          mRPlannedExcavator: "0", mRActualExcavator: "0", mRIdleExcavator: "0",
          mRPlannedBackhoeLoader: "0", mRActualBackhoeLoader: "0", mRIdleBackhoeLoader: "0",
          mRPlannedBlowing: "0", mRActualBlowing: "0", mRIdleBlowing: "0",
          mRPlannedSlicer: "0", mRActualSlicer: "0", mRIdleSlicer: "0",
          mRPlannedManpower: "0", mRActualManpower: "0", mRIdleManpower: "0",
          slctDate: todayDate,
          Remarks: "",
          Soil_strata:"",
          Protection:"",
          Type:"",
          Depth:"0",
        }
      }
    }

    
    componentDidMount(){
      this.fetchUserDataState();
      this.fetchActivities();
      this.fetchInternalIssues();
      this.fetchCustomerIssues();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }
    
    fetchUserDataState = async() => {
      const userid = await AsyncStorage.getItem('user_id');
      const usern = await AsyncStorage.getItem('username');
      const userr = await AsyncStorage.getItem('user_role');
      const ipAddress = await Network.getIpAddressAsync();
      //  const macAddress = await Network.getMacAddressAsync();
      
      this.state.selectedData["ipAddress"] = ipAddress;
    //  this.state.selectedData["macAddress"] = macAddress;
      
      this.state.selectedData["user_id"] = userid;
      this.state.selectedData["user_role"] = userr;
      this.state.selectedData["user_name"] = usern;

      this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
      this.fetchCmpAPI();
    }
    
    // CALLING API TO GET ALL ACTIVITIES 
    async fetchActivities(){
      let responseJson = [];
      let response = await fetch(this.config.ActivitiesAPI);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ activities: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
  
    // CALLING API TO GET ALL Internal ISSUES 
    async fetchInternalIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/1");
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ internalIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }


    // CALLING API TO GET ALL Customer ISSUES 
    async fetchCustomerIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI+"/2");
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ customerIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
  
    // CALLING API TO GET ALL ZONE 
    async fetchCmpAPI(){
      let responseJson = [];
      let response = await fetch(this.config.totalZONEAPI+"/"+this.state.userRole+"/"+this.state.userName);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ allzone: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    
    // CALLING API TO GET ALL SPAN 
    fetchSpanAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.SpanAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call          
          this.setState({ allSpan: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN 
    fetchSpanLengthAPI = async(value) => {
      let responseJson = [];
      
      let response = await fetch(this.config.SpanLength+"/"+value);
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        let spLength = responseJson.data[0].length;
        let milestoneSpan = parseInt(spLength, 10)+5;
        let milestoneSpanVal = [];
        for (var i=0 ; i <= milestoneSpan; i++) {
          milestoneSpanVal.push({ 'value': i });
        }

        this.setState({ spanLength: spLength, milestoneSpansLen: milestoneSpanVal });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }
    
    // USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE ISSUES
    async internalIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.internalIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedInternalIssues: checkedIssuesList, internalIssues: checkedIssues });
    }

    // CUSTOMER ISSUES CHECKBOXES
    async customerIssuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.customerIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ isCheckedCustomerIssues: checkedIssuesList });
    }


    // GETING DATA FROM FIELDS AND USING VALIDATION
    getData(data, fieldName){
      if(fieldName === "zone"){
        this.state.selectedData.zone = data;
        this.fetchSpanAPI(data);
      }else if(fieldName === "Span"){
        this.state.selectedData.span = data;
        this.fetchSpanLengthAPI(data);
      }else if(fieldName === "activity"){
        
        if(data === "T&D"){
          this.setState({ activityTD: true, activityBlowing: false, activityChamberInst: false, activityMachineResource: false, activityAcceptanceTest:false, activityHoTo:false});
        }else if(data === "Blowing"){
          this.setState({ activityBlowing: true, activityTD: false, activityChamberInst: false, activityMachineResource: false, activityAcceptanceTest:false, activityHoTo:false });
        }else if(data === "Chamber Installation"){
          this.setState({ activityChamberInst: true, activityTD: false, activityBlowing: false,  activityMachineResource: false, activityAcceptanceTest:false, activityHoTo:false});
        }else if(data === "Machine Resource"){
          this.setState({ activityMachineResource: true, activityTD: false, activityBlowing: false, activityChamberInst: false, activityAcceptanceTest:false, activityHoTo:false });
        }else if(data === "Acceptance Test"){
          this.setState({ activityMachineResource: false, activityTD: false, activityBlowing: false, activityChamberInst: false, activityAcceptanceTest:true, activityHoTo:false });
        }else if(data === "HoTo"){
          this.setState({ activityMachineResource: false, activityTD: false, activityBlowing: false, activityChamberInst: false, activityAcceptanceTest:false, activityHoTo:true });
        }

        this.state.selectedData.activity = data;
        this.setState({milestoneFrom:'0', milestoneTo:'1'})
        this.state.selectedData.milestoneFrom = '0';
        this.state.selectedData.milestoneTo = '1';
      }else if(fieldName === "IssuesReport"){
        if(data === "Yes"){
          this.state.selectedData['issues_TD_Hndl'] = "Yes";
          this.setState({ IssuesReportChange: false, IssuesReportValue: "Yes", activityDisabled: true });
        }else{
          this.state.selectedData['issues_TD_Hndl'] = "No";
          this.setState({ IssuesReportChange: true, IssuesReportValue: "No", activityDisabled: false });
        }
        this.state.selectedData.issuesReportTD = data;
      }else if(fieldName === "ChainageStart"){
        this.setState({ chainageFrom: data })
        this.state.selectedData.chainageFrom = data;
      }else if(fieldName === "ChainageEnd"){
        this.setState({ chainageTo: data })
        this.state.selectedData.chainageTo = data;
      }else if(fieldName === "lattitude"){
        this.setState({ lattitude: data })
        this.state.selectedData.lattitude = data;
      }else if(fieldName === "longitude"){
        this.setState({ longitude: data })
        this.state.selectedData.longitude = data;
      }else if(fieldName === "slctDate"){
        this.setState({ slctDateVal: data })
        this.state.selectedData.slctDate = data;
      }else if(fieldName === "Depth"){
        this.setState({ Depth: data })
        this.state.selectedData.Depth = data;
      }else if(fieldName === "milestoneFrom"){
        if(this.state.activityAcceptanceTest || this.state.activityHoTo){
          this.setState({ milestoneFrom: data })
        }
        else{
          this.setState({ milestoneTo: (data + 1).toString(), milestoneFrom: data })
        }
        this.state.selectedData.milestoneFrom = data;
      }else if(fieldName === "milestoneTo"){
          this.setState({ milestoneTo: data})
        this.state.selectedData.milestoneTo = data;
      }else{
        this.state.selectedData[fieldName] = data;      
      }
      
    }

    // USING FOLLOWING COMPONENT TO VALIDATE FORM 
    fnSubmitValidateForm = async() => {
      
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      this.setState({ isLoading: true });
      let milst_frm_validation = parseFloat(selectedData.milestoneFrom+"."+selectedData.milestoneFromM);
      let milst_to_validation = parseFloat(selectedData.milestoneTo+"."+selectedData.milestoneToM);

      let chainageFrom = parseInt(selectedData.chainageFrom);
      let chainageTo = parseInt(selectedData.chainageTo);

      if(selectedData.zone === '' || selectedData.span === ''){
        if(selectedData.zone === '' && selectedData.span === ''){
          this.setState({ errorMsg: "Please select Zone and Span.", successMsg: "", isLoading: false });
        }else if(selectedData.zone === ''){
          this.setState({ errorMsg: "Please select Zone ", successMsg: "", isLoading: false });
        }else {
          this.setState({ errorMsg: "Please select Span.", successMsg: "", isLoading: false });
        }
      }else if(selectedData.activity === ''){

        this.setState({ errorMsg: "Please select Acivity.", successMsg: "", isLoading: false });

      }else if( (chainageTo < chainageFrom || chainageTo === chainageFrom ) && (selectedData.activity !== 'Machine Resource' || selectedData.activity !== 'Acceptance Test' || selectedData.activity !== 'HoTo')){

        this.setState({ errorMsg: "Chainage End value must be higher than Chaninage From value.", successMsg: "", isLoading: false });

      }else if (( chainageTo - chainageFrom ) > 1000) {

        this.setState({ errorMsg: "You are only allowed to enter up to 1000 meters Chainage.", successMsg: "", isLoading: false });

      }else if (selectedData.issues.length === 0 && selectedData.issues_TD_Hndl === "Yes" ) {

        this.setState({ errorMsg: "Please select at least one Issue.", successMsg: "", isLoading: false });

      }else if(( (selectedData.lattitude === '' || selectedData.lattitude === 0.000) || (selectedData.longitude === '' || selectedData.longitude === 0.000)) && selectedData.activity === 'Chamber Installation'){

        this.setState({ errorMsg: "Please select Lattitude and Longitude.", successMsg: "", isLoading: false });

      }else if(selectedData.activity==='T&D' && selectedData.Soil_strata===''){

          this.setState({ errorMsg: "Please select Soil Strata", successMsg: "", isLoading: false });
      }
        else if(selectedData.activity==='T&D' && selectedData.Protection==='' && selectedData.issues_TD_Hndl === "No"){
          this.setState({ errorMsg: "Please select Protection", successMsg: "", isLoading: false });
        }
       else if(selectedData.activity==='T&D' && selectedData.Type===' && selectedData.issues_TD_Hndl === "No"'){
          this.setState({ errorMsg: "Please select Type", successMsg: "", isLoading: false });
        }
        else if(selectedData.activity==='T&D' && selectedData.Depth==='' && selectedData.issues_TD_Hndl === "No"){
          this.setState({ errorMsg: "Please Enter Depth", successMsg: "", isLoading: false });
      }else if((parseInt(this.state.milestoneTo)==='' || parseInt(this.state.milestoneFrom)==='' ) && (selectedData.activity === 'Acceptance Test' || selectedData.activity === 'HoTo')){
        this.setState({ errorMsg: "Please enter Milestone From and Milestone To values", successMsg: "", isLoading: false });
      }
      else if((parseInt(this.state.milestoneTo) <= parseInt(this.state.milestoneFrom) ) && (selectedData.activity === 'Acceptance Test' || selectedData.activity === 'HoTo')){
        this.setState({ errorMsg: "Milestoneto value should be grater then Milestonefrom", successMsg: "", isLoading: false });
    }
      else{
        // LET'S CALL API
        let response = await fetch(this.config.CheckTransAPI, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: selectedData
          })
        })
        try {
          // GOT SUCCESSFULLY RESPONSE FROM API
          responseJson = await response.json();
          
          if(responseJson.data[0].value === ''){
            this.setState({ errorMsg: "", successMsg: "", isLoading: false });
            // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
            this.submitFormData();
          }else{

            let respVal1 = responseJson.data[0].value;
            respVal = respVal1.split('-');

            // RESPONSE BASED ERROR MESSAGES
            if(respVal[0] === 'range'){
                
              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal[1]+' to '+respVal[2]+', please select different range of Chainage Start & End Point.', successMsg: "", isLoading: false });

            }else{

              if (isNumber(respVal1)) {
                this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal1+', You are not allowed to submit more than 1000 meter chainage for Blowing and T&D activity!', successMsg: "", isLoading: false });
              }

              if (respVal1 === 'yes') {
                if(confirm('You have already entered data for Activity:' + selectedData.activity + ' for Span: ' + selectedData.span + '. \n\n Would you like to enter another entry?')){
                  // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
                  this.submitFormData();
                }else{
                  this.setState({ isLoading: false });
                  return false;
                }
              }else{
                // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
                this.submitFormData();
              }

            }

          }
        } catch (error) {
          console.log("Response error - ",error);
          
          this.setState({ errorMsg: "", successMsg: "", isLoading: false });
        }

      }
    }

    // FOLLOWING COMPONENT USING TO SUBMIT FORM 
    submitFormData = async() => {
      let responseJson = [];
      const { selectedData } = this.state;
      
      let response = await fetch(this.config.submitFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      })
      try {
        responseJson = await response.json();
        console.log(responseJson);
        alert(responseJson.success);
        this.setState({ successMsg: responseJson.success, isLoading: false });
      } catch (error) {
        console.log("Response error - ",error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }

    }

    async getCurrentLatLong(type){
      
      navigator.geolocation.getCurrentPosition(
        position => {
          const lattitude = JSON.stringify(position.coords.latitude);
          const longitude = JSON.stringify(position.coords.longitude);
  
          this.state.selectedData["lattitude"] = lattitude;
          this.state.selectedData["longitude"] = longitude;
          this.setState({ lattitude: lattitude, longitude: longitude });
          
        },
        error => console.log(" error - ", error.message),
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
  
    }
  
    render() {
     
      let issuesReport = [{
        "value": "No"
      },{
        "value": "Yes"
      }]

      let Dit = [{
        "value": "No"
      },{
        "value": "Yes"
      }];
      let Offer_Type = [{
        "value": "Civil"
      },{
        "value": "Optical"
      }];


     let Soil_strata = [{
        value:"Normal"
      },{
        value:"Hard"
      },{
        value:"Soft"
      }]
      
     let Protection = [{
      value:"GL"
    },{
      value:"DWC"
    },{
      value:"RCC"
    }]
    
    let Type = [{
      value:"Open"
    },{
      value:"HDD"
    }]

      let machineResourceData = [{
        "value": "1"
      },{
        "value": "2"
      },{
        "value": "3"
      },{
        "value": "4"
      },{
        "value": "5"
      }]
 
      const { milestoneChecked, selectedData, IssuesChecked } = this.state;
      return (
          <Container style={styles.Container}>

            {/* START SECTION FOR PROFILE TOP WITH IMAGE AND NAME OF USER */}
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >

                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Date</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <DatePicker
                          style={{width: 200 }}
                          // date={this.state.date}
                          mode="date"
                          date= {this.state.slctDateVal}
                          format="YYYY-MM-DD"
                          minDate={fourDaysBack}
                          maxDate={new Date()}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          iconSource = {require("../../assets/images/date.png")}
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              marginLeft: 36,
                              backgroundColor: "#f0f0f0",
                              borderWidth: 0,
                              color: "#888888",
                            }
                          }}
                          onDateChange={(value) => this.getData(value, "slctDate")}
                        />
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Zone</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allzone}
                            // dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "zone")}
                            error = ""
                          />
                        </View>                          
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Span</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allSpan}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "Span")}
                            error = ""
                          />
                        </View>                          
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>

                      <View style ={{ flex:1, flexDirection:'row', paddingBottom: 5, }}>
                          <Text style={{ fontSize: 16 }}>Span Length</Text>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <TextInput
                          style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                          value={this.state.spanLength}
                          editable = {false}
                        />
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Activity</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.activities}
                            dropdownPosition={1}
                            value={this.state.activity}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "activity")}
                            disabled= {this.state.activityDisabled}
                            error = ""
                          />
                        </View>                          
                      </View>
                    </View>
                    
                    {
                      (this.state.activityTD === true || this.state.activityBlowing === true || this.state.activityChamberInst === true || this.state.activityAcceptanceTest === true || this.state.activityHoTo === true)?(
                        <View>
                          {
                            (this.state.activityTD)?(
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Issues Report</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={issuesReport}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      value={this.state.IssuesReportValue}
                                      onChangeText={(value) => this.getData(value, "IssuesReport")}
                                      error = ""
                                    />
                                  </View>                          
                                </View>
                              </View>
                            ):(this.state.activityBlowing)?(
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>DIT</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={Dit}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "DIT")}
                                      error = ""
                                    />
                                  </View>                          
                                </View>
                              </View>
                            ):(this.state.activityAcceptanceTest || this.state.activityHoTo)?(
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Offer Type</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={Offer_Type}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "Offer_Type")}
                                      error = ""
                                    />
                                  </View>                          
                                </View>
                              </View>
                            ):(
                              <View></View>
                            )
                          }

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                              <View style ={{ flex: 1, paddingBottom: 8 }}>
                                <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                              </View>
                            </View>
                            
                            
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ width: "100%" }}>
                              {
                                (this.state.activityHoTo || this.state.activityAcceptanceTest)?
                                (
                                  <TextInput
                                  style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value={this.state.milestoneFrom}
                                  onChangeText={(value) => this.getData(value, "milestoneFrom")}
                                  keyboardType='phone-pad'
                                />
                                ):
                                (
                                  <Dropdown
                                  label= "Milestone From"
                                  data={this.state.milestoneSpansLen}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  value={this.state.milestoneFrom}
                                  onChangeText={(value) => this.getData(value, "milestoneFrom")}
                                  error = ""
                                />
                                )
                              }
                               
                              </View>
                            </View>
                            
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                              <View style ={{ width: "100%" }}>
                                <Text style={{ color: "#999", fontSize: 10 }}>Milestone To</Text>
                              </View>                          
                            </View>
                            
                            <View style ={{ flex:1, flexDirection:'row', paddingBottom:6 }}>
                              <View style ={{ width: "100%" }}>
                                <TextInput
                                  style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                  value={this.state.milestoneTo}
                                  onChangeText={(value) => this.getData(value, "milestoneTo")}
                                  keyboardType='phone-pad'
                                  editable = {(this.state.activityHoTo || this.state.activityAcceptanceTest)?true:false}
                                />
                              </View>                          
                            </View>

                          </View>
                          
                          {
                            (this.state.activityChamberInst)?(
                              <View style ={ styles.paddingBottomMore }>
                                
                                <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                                  <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong()}>
                                      <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                                  </TouchableOpacity>
                                </View>
                                
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}>
                                    <Text style={{ fontSize: 16 }}>Lattitude</Text>
                                  </View>
                                  <View style ={{ flex: 0.5, paddingBottom: 8 }}>
                                    <Text style={{ fontSize: 16 }}>Longitude</Text>
                                  </View>
                                </View>
    
                                <View style ={{ flex:1, flexDirection:'row', }}>
                                  <View style ={{ flex:0.5, marginRight: 15 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value= {this.state.lattitude}
                                      // onChangeText={(value) => this.getData(value, "lattitude")}
                                      editable = {false}
                                    />
                                  </View>
                                  
                                  <View style ={{ flex:0.5 }}>
                                    <TextInput
                                      style={{ height: 40, width:"100%", textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                      value= {this.state.longitude}
                                      // onChangeText={(value) => this.getData(value, "longitude")}
                                      editable = {false}
                                    />
                                  </View>
                                </View>
                              </View>
                            ):(
                              <View></View>
                            )
                          }

                          {
                                (this.state.activityHoTo || this.state.activityAcceptanceTest)?
                                (
                                  <View></View>
                                ):
                                (
                                  <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 10 }}>
                                <Text style={{ fontSize: 16 }}>Chainage Point(MTS)</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row' }}>
                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.25 }}>
                                  <Text style={{ height: 40, textAlignVertical: "center" }} >Start</Text>
                                </View>
                                <View style={{ flex: 0.75 }}>
                                  <TextInput
                                    style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.chainageFrom}
                                    onChangeText={(value) => this.getData(value, "ChainageStart")}
                                    keyboardType='phone-pad'
                                  />
                                </View>
                              </View>

                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.25 }}>
                                  <Text style={{ height: 40, textAlignVertical: "center", textAlign: "right" }} >End</Text>
                                </View>
                                <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                                  <TextInput
                                    style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                                    value={this.state.chainageTo}
                                    onChangeText={(value) => this.getData(value, "ChainageEnd")}
                                    keyboardType='phone-pad'
                                    editable = {!this.state.activityChamberInst}
                                  />
                                </View>
                              </View>
                            </View>
                          </View>
                                )
                          }

                          
                          {
                            (!this.state.activityTD)?(
                              <View></View>
                            ):(
                              <View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Soil strata</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={Soil_strata}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "Soil_strata")}
                                      error = ""
                                      value = {this.state.selectedData.Soil_strata}
                                    />
                                  </View>                          
                                </View>
                              </View>
                              {
                            (!this.state.IssuesReportChange)?(
                              <View></View>
                              ):(
                                <View>
                                <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Protection</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={Protection}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "Protection")}
                                      error = ""
                                      value = {this.state.selectedData.Protection}
                                    />
                                  </View>                          
                                </View>
                              </View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Type</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                    <Dropdown
                                      label= ""
                                      data={Type}
                                      dropdownPosition={1}
                                      dropdownOffset = {{ top: 8, left: 0 }}
                                      onChangeText={(value) => this.getData(value, "Type")}
                                      error = ""
                                      value = {this.state.selectedData.Type}
                                    />
                                  </View>                          
                                </View>
                              </View>
                              <View style ={ styles.paddingBottomMore }>
                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ flex: 1, paddingBottom: 5 }}>
                                    <Text style={{ fontSize: 16 }}>Depth (MTS)</Text>
                                  </View>
                                </View>

                                <View style ={{ flex:1, flexDirection:'row', }}>                          
                                  <View style ={{ width: "100%" }}>
                                  <TextInput
                                  style={{ height: 40, color: "#878787", borderRadius: 2, backgroundColor: "#f0f0f0", textAlign:"center" }}
                                  onChangeText={(value) => this.getData(value, "Depth")}
                                  keyboardType='phone-pad'
                                  value = {this.state.Depth}
                                />
                                  </View>                          
                                </View>
                              </View>
                                </View>
                              )
                              }
                              </View>
                            )
                          }
                          <View style ={ styles.paddingBottomMore }>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 10 }}>
                                <Text style={{ fontSize: 16 }}>Chainage status</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row' }}>
                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.23 }}>
                                  <Radio 
                                    selectedColor= {"#1ab679"}
                                    standardStyle={{ color: "#1ab679" }}
                                    style={{ paddingLeft: 6, paddingTop: 4 }}
                                    selected={ milestoneChecked === 'WIP' ? true : false } 
                                    onPress={() => {
                                      this.setState({ milestoneChecked: 'WIP' }) 
                                      selectedData.milestoneStatus = "WIP";
                                    }}
                                  />
                                </View>
                                <View style={{ flex: 0.77 }}>
                                  <TouchableOpacity activeOpacity={1} onPress={() => {
                                    this.setState({ milestoneChecked: 'WIP' }) 
                                    selectedData.milestoneStatus = "WIP";
                                  }}>
                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                      WIP (In Progress)
                                    </Text>
                                  </TouchableOpacity>
                                </View>
                              </View>

                              <View style={{ flex: 0.5, flexDirection:'row' }}>
                                <View style={{ flex: 0.23 }}>
                                  <Radio 
                                    selectedColor= {"#1ab679"}
                                    standardStyle={{ color: "#1ab679" }}
                                    style={{ paddingLeft: 6, paddingTop: 4 }}
                                    selected={ milestoneChecked === 'Complete' ? true : false } 
                                    onPress={() => {
                                      this.setState({ milestoneChecked: 'Complete' }) 
                                      selectedData.milestoneStatus = "Complete";
                                    }}
                                  />
                                </View>
                                <View style={{ flex: 0.77 }}>
                                  <TouchableOpacity activeOpacity={1} onPress={() => { 
                                    this.setState({ milestoneChecked: 'Complete' }); 
                                    selectedData.milestoneStatus = "Complete";
                                  }} >
                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Complete</Text>
                                  </TouchableOpacity>
                                </View>
                              </View>
                            </View>

                          </View>

                          {
                            (this.state.IssuesReportChange)?(
                              <View></View>
                            ):(
                              <View>
                                <View style ={ styles.paddingBottomMore }>
                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 1, paddingBottom: 5 }}>
                                      <Text style={{ fontSize: 20, fontWeight: "bold" }}>Issues</Text>
                                    </View>
                                  </View>
                                </View>
                                
                                <View style ={ styles.paddingBottomMore }>

                                  <View style ={{ flex:1, flexDirection:'row' }}>
                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.23 }}>
                                        <Radio 
                                          selectedColor= {"#1ab679"}
                                          standardStyle={{ color: "#1ab679" }}
                                          style={{ paddingLeft: 6, paddingTop: 4 }}
                                          selected={ IssuesChecked === 'Internal' ? true : false } 
                                          onPress={() => { 
                                            this.setState({ IssuesChecked: 'Internal' });
                                            selectedData["issuesType"] = "Internal";
                                            selectedData["issues"] = "";
                                            this.state.internalIssues.map((item, i)=>{
                                                item.isSelect = false;
                                            })
                                          }}
                                        />
                                      </View>
                                      <View style={{ flex: 0.77 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => { 
                                          this.setState({ IssuesChecked: 'Internal' });
                                          selectedData["issuesType"] = "Internal";
                                          selectedData["issues"] = "";
                                          this.state.internalIssues.map((item, i)=>{
                                              item.isSelect = false;
                                          })
                                        }}>
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                            Internal Issues
                                          </Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>

                                    <View style={{ flex: 0.5, flexDirection:'row' }}>
                                      <View style={{ flex: 0.23 }}>
                                        <Radio 
                                          selectedColor= {"#1ab679"}
                                          standardStyle={{ color: "#1ab679" }}
                                          style={{ paddingLeft: 6, paddingTop: 4 }}
                                          selected={ IssuesChecked === 'Customer' ? true : false } 
                                          onPress={() => {
                                            this.setState({ IssuesChecked: 'Customer' }); 
                                            selectedData["issuesType"] = "Customer";
                                            selectedData["issues"] = "";
                                            this.state.customerIssues.map((item, i)=>{
                                                item.isSelect = false;
                                            })
                                          }}
                                        />
                                      </View>
                                      <View style={{ flex: 0.77 }}>
                                        <TouchableOpacity activeOpacity={1} onPress={() => {
                                          this.setState({ IssuesChecked: 'Customer' }); 
                                          selectedData["issuesType"] = "Customer";
                                          selectedData["issues"] = "";
                                          this.state.customerIssues.map((item, i)=>{
                                              item.isSelect = false;
                                          })
                                        }} >
                                          <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >Customer Issues</Text>
                                        </TouchableOpacity>
                                      </View>
                                    </View>
                                  </View>

                                </View>
                                
                                <View style ={ styles.paddingBottomMore }>

                                  <View style ={{ flex:1, flexDirection:'row', }}>                          
                                    <View style ={{ flex: 1, paddingBottom: 10 }}>
                                      <Text style={{ fontSize: 16 }}>Type of Issue</Text>
                                    </View>
                                  </View>

                                  <View style ={{ flex:1, flexDirection:'row' }}>
                                    
                                    {
                                      // INTERNAL ISSUES LIST
                                      (IssuesChecked === "Internal")?(
                                        <FlatList
                                          data={ this.state.internalIssues }
                                          extraData={this.state}
                                          renderItem={({item}) => {
                                            return(
                                              <View style={{ flex: 1, flexDirection: "row" }}>

                                                <View style={{ paddingRight: 16, paddingTop: 5 }}>
                                                  <CheckBox
                                                   color= {(item.isSelect)? "#1ab679" : "#000" }
                                                   checked= {(item.isSelect)? true : false }
                                                    onPress={() => this.internalIssuesCheckbox(item.value)}
                                                  />
                                                </View>
                                                <View style={{ paddingLeft: 5 }}>
                                                  <TouchableOpacity activeOpacity={1}>
                                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                                      {item.value}
                                                    </Text>
                                                  </TouchableOpacity>
                                                </View>

                                              </View>
                                            )
                                          }
                                          }
                                          keyExtractor={item => item.value}
                                          numColumns={2}
                                        />

                                      ):( // CUSTOMER ISSUES LIST

                                        <FlatList
                                          data={ this.state.customerIssues }
                                          extraData={this.state}
                                          renderItem={({item}) =>
                                            <View style={{ flex: 1, flexDirection: "row" }}>
                                              <View style={{ paddingRight: 16, paddingTop: 5 }}>
                                                <CheckBox
                                                  color= {(item.isSelect)? "#1ab679" : "#000" }
                                                  checked= {(item.isSelect)? true : false }
                                                  onPress={() => this.customerIssuesCheckbox(item.value)}
                                                />
                                              </View>
                                              <View style={{ paddingLeft: 5 }}>
                                                <TouchableOpacity activeOpacity={1}>
                                                  <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                                    {item.value}
                                                  </Text>
                                                </TouchableOpacity>
                                              </View>

                                            </View>

                                          }
                                          keyExtractor={item => item.value}
                                          numColumns={2}
                                        />
                                      )
                                    }
                                    
                                  </View>

                                </View>

                              </View>
                            )
                          }

                        </View>
                      ):(
                        <View></View>
                      )
                    }

                    {
                      (this.state.activityMachineResource)?(
                        <View>
                          {/* PLANNED SECTION IN MACHINE RESOURCE */}
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16, fontWeight: "600" }}>Planned</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>JCB</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedJCB")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>HDD</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedHDD")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Poklane</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedPoklane")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedRockBreaker")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Excavator</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedExcavator")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedBackhoeLoader")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Blowing</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedBlowing")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Slicer</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedSlicer")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Manpower</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRPlannedManpower")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>

                          {/* ACTUAL SECTION IN MACHINE RESOURCE */}

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16, fontWeight: "600" }}>Actual</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>JCB</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualJCB")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>HDD</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualHDD")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Poklane</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualPoklane")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualRockBreaker")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Excavator</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualExcavator")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualBackhoeLoader")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Blowing</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualBlowing")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Slicer</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualSlicer")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Manpower</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRActualManpower")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>

                          
                          {/* IDLE SECTION IN MACHINE RESOURCE */}

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16, fontWeight: "600" }}>Idle</Text>
                              </View>
                            </View>
                          </View>

                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>JCB</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleJCB")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>HDD</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleHDD")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Poklane</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdlePoklane")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleRockBreaker")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Excavator</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleExcavator")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleBackhoeLoader")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Blowing</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleBlowing")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Slicer</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleSlicer")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>
                          
                          <View style ={ styles.paddingBottomMore }>
                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ flex: 1, paddingBottom: 5 }}>
                                <Text style={{ fontSize: 16 }}>Manpower</Text>
                              </View>
                            </View>

                            <View style ={{ flex:1, flexDirection:'row', }}>                          
                              <View style ={{ width: "100%" }}>
                                <Dropdown
                                  label= ""
                                  data={machineResourceData}
                                  dropdownPosition={1}
                                  dropdownOffset = {{ top: 8, left: 0 }}
                                  onChangeText={(value) => this.getData(value, "mRIdleManpower")}
                                  error = ""
                                />
                              </View>                          
                            </View>
                          </View>

                        </View>
                      ):(
                        <View></View>
                      )
                    }
                    

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Remarks</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>                          
                        <View style ={{ width: "100%" }}>
                          <Textarea
                            containerStyle={{ height: 160, padding: 10, backgroundColor: '#f0f0f0' }}
                            style={{textAlignVertical: 'top', height: 150, fontSize: 14, color: '#8e8e8e',}}
                            onChangeText={(value) => this.getData(value, "Remarks")}
                            // defaultValue={this.state.text}
                            maxLength={200}
                            placeholder={'Comment About Work'}
                            placeholderTextColor={'#8e8e8e'}
                            underlineColorAndroid={'transparent'}
                          />
                        </View>                          
                      </View>
                    </View>


                  </View>

                  <KeyboardSpacer topSpacing ={5} />

                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 5 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center" }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                  </View>
                  
                  {/* SUBMIT FORM BUTTON */}
                  <View>
                    {
                      (this.state.isLoading)?
                      (
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                        </TouchableOpacity>
                      ):(
                        <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                        </TouchableOpacity>
                      )
                    }
                  </View>                  

                </View>
              </ScrollView>
            {/* END SECTION */}

        </Container>
        )
    }
  
  
  
    static navigationOptions = ({navigation}) => {
      return {
        title: 'Engineer Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  }
}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    paddingBottomMore: {
      paddingBottom: 15
    },
    
    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }
  
  });
  
export default EngineerForm;
