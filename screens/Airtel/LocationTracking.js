import React from 'react';
import { View, StyleSheet, ActivityIndicator, Text, ScrollView, Dimensions } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
const GOOGLE_MAPS_APIKEY = 'AIzaSyDvVj296ZycDdW4wPFR8wIUsT18nKSkQiA';
const screenHeight = Math.round(Dimensions.get('screen').height);

let firstlat = 0;
let firstlon = 0;
let lastlat = 0;
let lastlon = 0;

class LocationTracking extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      trackedData: [],
      isLoading: true,
      latitudeDelta: 0.095,
      longitudeDelta: 0.04,
      centerlat: 0,
      centerlong: 0,
      distance: 0
    }
  }


  componentDidMount() {
    this.startTrackingInterval();
    // set Interval
    this.interval = setInterval(this.startTrackingInterval, 5000);
  }
  startTrackingInterval = () => {
    navigator.geolocation.getCurrentPosition(
      position => {
        const lattitude = JSON.stringify(position.coords.latitude);
        const longitude = JSON.stringify(position.coords.longitude);

        let data = {
          lattitude: lattitude,
          longitude: longitude,
        }
        var joined = this.state.trackedData.concat(data);
        this.setState({ trackedData: joined, isLoading: false })
        if (firstlat === 0 && firstlon === 0) {
          this.setState({ centerlat: parseFloat(lattitude), centerlong: parseFloat(longitude) })
          firstlat = lattitude;
          firstlon = longitude;
        } else {
          firstlat = lastlat;
          firstlon = lastlon;
        }
        lastlat = lattitude;
        lastlon = longitude;


        distance = this.getDistance(firstlat, firstlon, lastlat, lastlon);
      },

      error => console.log(" error - ", error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );
  }

  getDistance = (lat, lon, lat2, long2) => {
    var R = 6371;
    var dLat = (lat2 - lat) * Math.PI / 180;
    var dLon = (long2 - lon) * Math.PI / 180;
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(lat * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    if (parseFloat(d * 1000, 2) >= 4) {
      let dist = this.state.distance + d * 1000;
      this.setState({ distance: dist })
    }
  }


  render() {


    return (

      (this.state.isLoading) ? (
        <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
          <ActivityIndicator size="large" color="#2157c4" />
        </View>
      ) : (
          <ScrollView>
            <View style={styles.container}>

              <MapView style={styles.map} initialRegion={{
                latitude: this.state.centerlat,
                longitude: this.state.centerlong,
                latitudeDelta: this.state.latitudeDelta,
                longitudeDelta: this.state.longitudeDelta,
              }
              }
                provider={PROVIDER_GOOGLE}
              >
                {
                  this.state.trackedData.map((Markers, key) => {
                    if (key < this.state.trackedData.length - 1) {
                      return (
                        <MapView.Polyline
                          key={key}
                          coordinates={
                            [
                              { latitude: parseFloat(this.state.trackedData[key].lattitude), longitude: parseFloat(this.state.trackedData[key].longitude), },
                              { latitude: parseFloat(this.state.trackedData[key + 1].lattitude), longitude: parseFloat(this.state.trackedData[key + 1].longitude), }
                            ]
                          }
                          strokeWidth={4}
                          strokeColor='hotpink'

                        />
                      )
                    }
                  })
                }
              </MapView>
              <View style={styles.Dist}>

                <Text>Distance From Origin - {parseFloat(this.state.distance, 2)} Meter</Text>

              </View>
            </View>
          </ScrollView>
        )
    );

  }
}



const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    height: (screenHeight - ((screenHeight * 0) / 3))
  },
  Dist: {
    width: '100%',
    height: (screenHeight - ((screenHeight * 1) / 3)),
    justifyContent: 'flex-end',
  },
});


export default LocationTracking;


