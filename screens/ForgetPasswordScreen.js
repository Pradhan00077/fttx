import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator, } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import axios from 'axios';
import {BASE_URL} from '../basepath';
const screenHeight = Math.round(Dimensions.get('window').height);

class ForgetPassword extends Component {

    constructor(props) {
      super(props);
      // ALL APIS 
      this.config = {
        apiUrl: BASE_URL + "apis/forget_password"
      }
      this.state = {
          isLoading: false,
          emailErrorMsg: "",
          successMsg: "",
          useremail: "",
      };
     }

    // USING METHOD TO CALL FORGET PASSWORD METHOD 
     _handleForgetPassword = async () =>{
      this.setState({ isLoading: true });
      this.forgetUserPassword();
    }

    // USING METHOD TO CALL API FOR FORGET PASSWORD
    async forgetUserPassword(){

        let self = this;
        let responseJson = [];
        
        let response = await fetch(this.config.apiUrl, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            useremail: self.state.useremail,//'anil.kumar1@sterlite.com',
          })
        })
        try {
          responseJson = await response.json();
          // Successful response from the API Call
          
          console.log(" Data - ", responseJson)

          if(responseJson.hasOwnProperty('error')){
            this.setState({ isLoading: false, emailErrorMsg: responseJson.error.useremail, successMsg: "" });
          }else{            
            self.setState({ isLoading: false, emailErrorMsg: "", successMsg: responseJson.success });
          }
        } catch (error) {
          alert(error);
          console.log("Response error - ",error);
          this.setState({ isLoading: false, successMsg: "" });
        }

    }

    // USING METHOD TO GET & SET DATA FROM FIELDS IN STATE
    cmpSetTextState = ( txtVal, txtName ) => {
      if(txtName === 'email'){
        this.setState({ useremail: txtVal });
      }
    }

    render() {
        return (
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
                <LinearGradient colors={['#2157c4', '#3b5998', '#1ab679']} style={styles.body}>
                  
                  <View style={styles.sectionContainer}>
                      <View style={styles.logoSection}>
                          <Image source = {require('../assets/images/logo.png')} resizeMode='contain' style={{ alignSelf: "center"}} />
                      </View>

                      <View style={{ marginBottom: 4 }}>
                        <Text style={styles.sectionTitle}>Forgot Password!</Text>
                        <Text style={styles.sectionDescription}>
                          {this.state.successMsg}
                        </Text>
                      </View>

                      <View style={{ marginBottom: 25 }}>
                        
                        <View style={{ marginLeft: 10, marginRight: 10 }}>
                            <TextField
                              label='E-mail'
                              style={{ color: "#FFF" }}
                              tintColor = "#FFFFFF"
                              baseColor = "#FFFFFF"
                              keyboardType='email-address'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.username}
                              onChangeText={(value) => this.cmpSetTextState(value, "email")}
                              error	= {this.state.emailErrorMsg}
                            />
                            
                        </View>

                      </View>

                    
                      <KeyboardSpacer topSpacing ={10} />

                      <Button style={styles.buttonText} onPress={this._handleForgetPassword}>
                        <Text style={{flex:1, textAlign:"center", fontSize:18, fontWeight:'bold'}}>
                          Submit
                        </Text>
                      </Button>

                  </View>
                
                </LinearGradient>
              </ScrollView>
              
            )
            }
          </Container>
        )
    }
  
  // USING navigationOptions TO SET HEADER OF SCREEN
  static navigationOptions = {
    header: null
  }

}

// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#2157c4',
    backgroundColor: '#FFFFFF',
  },
  labelForgetColor1:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#ffffff',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },

});
  
export default ForgetPassword;
