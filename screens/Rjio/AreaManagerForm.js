import React, { Component } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  AsyncStorage
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Container } from "native-base";
import { Dropdown } from "react-native-material-dropdown";
import DatePicker from "react-native-datepicker";
import * as Network from "expo-network";
import Textarea from "react-native-textarea";
import KeyboardSpacer from "react-native-keyboard-spacer";
import {BASE_URL} from '../../basepath'
import Database from "../../Database";
import { MaterialIcons } from '@expo/vector-icons';
const db = new Database();

const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3; // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

let statusQCS = [{
  value: "Complete"
  },{
    value: "Partial"
  }];

let status = [{ value: "Complete" }];

class AreaManagerForm extends Component {
  constructor(props) {
    super(props);
    this.config = {
      totalCMPAPI:  BASE_URL + "apis/rjo/totalCMP",
      SpanAPI:  BASE_URL + "apis/rjo/totalSpan",
      SpanLength:  BASE_URL + "apis/rjo/spanLength",
      ActivitiesAPI:  BASE_URL + "apis/rjo/ASM/activities",
      CheckTransAPI:  BASE_URL + "apis/rjo/ASM/checkOldTrans",
      submitFormAPI:  BASE_URL + "apis/rjo/ASM/insertReportData"
    };
    this.state = {
      isLoading: false,
      allcmp: [],
      allSpan: [],
      activities: [],
      allStatus: statusQCS,
      userName: 0,
      userRole: 0,
      userLoggedInId: 0,
      IssuesReportChange: true,
      IssuesReportValue: "No",
      spanLength: "",
      activity: "QCS",
      status: "Complete",
      IssuesChecked: "Internal",
      milestoneChecked: "WIP",
      milestoneTo: "1",
      milestoneFrom: "0",
      chainageFrom: "0",
      chainageTo: "1",
      errorMsg: "",
      slctDateVal: new Date(),
      submitBtnDisabled: true,
      countedMint: 0,
      milestoneSpansLen: [],

      selectedData: {
        cmp: "",
        span: "",
        activity: "QCS",
        status: "Complete",
        milestoneFrom: "0",
        milestoneTo: "1",
        slctDate: todayDate,
        Remarks: ""
      }
    };
  }

  componentDidMount() {
    this.fetchUserDataState();
    this.fetchActivities();
    this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
  }

  // USING METHOD TO OPEN DRAWER
  openControlPanel = () => {
    this.props.navigation.openDrawer();
  }

  fetchUserDataState = async () => {
    const userid = await AsyncStorage.getItem("user_id");
    const usern = await AsyncStorage.getItem("username");
    const userr = await AsyncStorage.getItem("user_role");
    const ipAddress = await Network.getIpAddressAsync();

    this.state.selectedData["ipAddress"] = ipAddress;
    // this.state.selectedData["macAddress"] = macAddress;

    this.state.selectedData["user_id"] = userid;
    this.state.selectedData["user_role"] = userr;
    this.state.selectedData["user_name"] = usern;

    this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
    this.fetchCmpAPI();
  };

  // CALLING API TO GET ALL ACTIVITIES
  async fetchActivities() {
    let responseJson = [];
    let response = await fetch(this.config.ActivitiesAPI);
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ activities: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL CMP
  async fetchCmpAPI() {
    let responseJson = [];
    let response = await fetch(
      this.config.totalCMPAPI +
        "/" +
        this.state.userRole +
        "/" +
        this.state.userName
    );
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ allcmp: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL SPAN
  fetchSpanAPI = async value => {
    let responseJson = [];
    let response = await fetch( this.config.SpanAPI + "/" + this.state.userRole + "/" + this.state.userName + "/" + value );
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ allSpan: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // CALLING API TO GET ALL SPAN
  fetchSpanLengthAPI = async value => {
    let responseJson = [];

    let response = await fetch(this.config.SpanLength + "/" + value);
    try {
      //Successful response from the API Call
      responseJson = await response.json();
      let spLength = responseJson.data[0].length;
      let milestoneSpan = parseInt(spLength, 10) + 2;
      let milestoneSpanVal = [];
      for (var i = 0; i <= milestoneSpan; i++) {
        milestoneSpanVal.push({ value: i });
      }

      this.setState({
        spanLength: spLength,
        milestoneSpansLen: milestoneSpanVal,
        milestoneFrom: '0',
        milestoneTo: '1',
      });
      this.state.selectedData.milestoneFrom = "0";
      this.state.selectedData.milestoneTo = "1";
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // GETING DATA FROM FIELDS AND USING VALIDATION
  getData(data, fieldName) {
    if (fieldName === "cmp") {
      this.state.selectedData.cmp = data;
      this.fetchSpanAPI(data);
    } else if (fieldName === "Span") {
      this.state.selectedData.span = data;
      this.fetchSpanLengthAPI(data);
    } else if (fieldName === "activity") {
      if(data === "QCS"){
        this.setState({ allStatus: statusQCS })
      } else {
        this.setState({ allStatus: status, status: "Complete" })
        this.state.selectedData.status = "Complete";
      }
      this.state.selectedData.activity = data;
    } else if (fieldName === "status") {
      this.setState({ status: data });
      this.state.selectedData.status = data;
    } else if (fieldName === "slctDate") {
      this.setState({ slctDateVal: data });
      this.state.selectedData.slctDate = data;
    } else if (fieldName === "milestoneFrom") {
      this.setState({ milestoneFrom: data });
      this.state.selectedData.milestoneFrom = data;
    } else if (fieldName === "milestoneTo") {
      this.setState({ milestoneTo: data });
      this.state.selectedData.milestoneTo = data;
    } else {
      this.state.selectedData[fieldName] = data;
    }
  }

  // USING FOLLOWING COMPONENT TO VALIDATE FORM
  fnSubmitValidateForm = async () => {
    this.setState({ errorMsg: "", successMsg: "", isLoading: true });
    let responseJson = [];
    const { selectedData } = this.state;

    if (selectedData.cmp === "" || selectedData.span === "") {
      this.setState({ errorMsg: "Please select CMP and Span.", successMsg: "", isLoading: false });
    } else if (selectedData.activity === "") {
      this.setState({ errorMsg: "Please select Acivity.", successMsg: "", isLoading: false });
    } else if ((selectedData.milestoneTo < selectedData.milestoneFrom)|| (selectedData.milestoneTo === selectedData.milestoneFrom)) {
      this.setState({ errorMsg: "Milestone To value must be higher than Milestone From value.", successMsg: "", isLoading: false });
    } else {

      // LET'S CALL API
      let response = await fetch(this.config.CheckTransAPI, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        // GOT SUCCESSFULLY RESPONSE FROM API
        responseJson = await response.json();
        let respVal1 = responseJson.data[0].value;
        let respVal = respVal1.split("-");

        // RESPONSE BASED ERROR MESSAGES
        if (respVal[0] === "range") {
          this.setState({
            errorMsg:
            'You have already entered Milestones From: ' + respVal[1] + ' to: ' + respVal[2] + ', please select different range of Milestone Start & End Point. You are not allowed to add duplicate entry.',
            successMsg: "", isLoading: false
          });
        } else {

          if (respVal1 === "yes") {
            if ( confirm( "You have already entered data for Activity:" + selectedData.activity + " for Span: " + selectedData.span + ". \n\n Would you like to enter another entry?" ) ) {
              // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
              this.submitFormData();
            } else {
              this.setState({ errorMsg: "", successMsg: "", isLoading: false });
              return false;
            }
          } else {
            // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
            this.submitFormData();
          }
        }
      } catch (error) {
        console.log("Response error - ", error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }
    }
  };

  // FOLLOWING COMPONENT USING TO SUBMIT FORM
  submitFormData = async () => {
    let responseJson = [];
    const { selectedData } = this.state;

    let response = await fetch(this.config.submitFormAPI, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        data: selectedData
      })
    });
    try {
      responseJson = await response.json();
      alert(responseJson.success);
      this.setState({ errorMsg: "", successMsg: responseJson.success, isLoading: false });
    } catch (error) {
      console.log("Response error - ", error);
      this.setState({ errorMsg: "", successMsg: "", isLoading: false });
    }
  };

  render() {

    const { milestoneChecked, selectedData, IssuesChecked } = this.state;
    return (
      <Container style={styles.Container}>
        {/* START SECTION FOR PROFILE TOP WITH IMAGE AND NAME OF USER */}
        <ScrollView>
          <View
            style={{ paddingVertical: 20, paddingLeft: 15, paddingRight: 15 }}
          >
            <View style={[styles.mainEventsClass]}>
              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Date</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <DatePicker
                    style={{ width: 200 }}
                    // date={this.state.date}
                    mode="date"
                    date={this.state.slctDateVal}
                    format="YYYY-MM-DD"
                    minDate={fourDaysBack}
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource={require("../../assets/images/date.png")}
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36,
                        backgroundColor: "#f0f0f0",
                        borderWidth: 0,
                        color: "#888888"
                      }
                    }}
                    onDateChange={value => this.getData(value, "slctDate")}
                  />
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>
                      CMP(Common Maintenance Point)
                    </Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allcmp}
                      // dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "cmp")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Span</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allSpan}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "Span")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row", paddingBottom: 5 }}>
                  <Text style={{ fontSize: 16 }}>Span Length</Text>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <TextInput
                    style={{
                      height: 40,
                      width: "100%",
                      textAlign: "center",
                      backgroundColor: "#f0f0f0",
                      color: "#878787",
                      borderRadius: 2
                    }}
                    value={this.state.spanLength}
                    editable={false}
                  />
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Activity</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.activities}
                      dropdownPosition={1}
                      value={this.state.activity}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "activity")}
                      disabled={this.state.activityDisabled}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Status</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allStatus}
                      value={this.state.status}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "status")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View
                  style={{ flex: 1, flexDirection: "row", paddingBottom: 10 }}
                >
                  <View style={{ flex: 1, paddingBottom: 8 }}>
                    <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                  </View>
                </View>

                <View
                  style={{ flex: 1, flexDirection: "row", paddingBottom: 10 }}
                >
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label="Milestone From"
                      data={this.state.milestoneSpansLen}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={this.state.milestoneFrom}
                      onChangeText={value =>
                        this.getData(value, "milestoneFrom")
                      }
                      error=""
                    />
                  </View>
                </View>

                <View
                  style={{ flex: 1, flexDirection: "row", paddingBottom: 6 }}
                >
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label="Milestone To"
                      data={this.state.milestoneSpansLen}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={this.state.milestoneTo}
                      onChangeText={value =>
                        this.getData(value, "milestoneTo")
                      }
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Remarks</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Textarea
                      containerStyle={{
                        height: 160,
                        padding: 10,
                        backgroundColor: "#f0f0f0"
                      }}
                      style={{
                        textAlignVertical: "top",
                        height: 150,
                        fontSize: 14,
                        color: "#8e8e8e"
                      }}
                      onChangeText={value => this.getData(value, "Remarks")}
                      // defaultValue={this.state.text}
                      maxLength={200}
                      placeholder={"Comment About Work"}
                      placeholderTextColor={"#8e8e8e"}
                      underlineColorAndroid={"transparent"}
                    />
                  </View>
                </View>
              </View>
            </View>

            <KeyboardSpacer topSpacing={5} />

            {/* DISPLAY ERROR MESSAGE */}
            <View style={{ paddingVertical: 5 }}>
              {this.state.errorMsg != "" ? (
                <Text style={{ color: "red", fontSize: 14, textAlign: "center" }}>
                  {this.state.errorMsg}
                </Text>
              ) : (
                <Text></Text>
              )}

              {this.state.successMsg != "" ? (
                <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>
                  {this.state.successMsg}
                </Text>
              ) : (
                <Text></Text>
              )}
            </View>

            {/* SUBMIT FORM BUTTON */}
            
            {
              (this.state.isLoading)?
              (
                <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                  <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                </TouchableOpacity>
              ):(
                <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                  <Text style={styles.loadMorebtnTxt}>Submit</Text>
                </TouchableOpacity>
              )
            }

          </View>
        </ScrollView>
        {/* END SECTION */}
      </Container>
    );
  }

  static navigationOptions = ({navigation}) => ({
    title: "Area Manager Form",
    headerTintColor: "#FFFFFF",
    headerTitleStyle: {
      alignSelf: "center",
      textAlign: "center",
      flex: 0.8,
      fontSize: 22,
      lineHeight: 25
    },
    headerStyle: {
      height: 84,
      borderBottomWidth: 0
    },
    headerLeft:(
      <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
        <MaterialIcons
          name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
      </TouchableOpacity>
    ),
    headerBackground: (
      <View>
        <LinearGradient
          colors={["#2157c4", "#3b5998", "#1ab679"]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{
            paddingTop: screenHeight - (screenHeight - screenHeight / 25),
            elevation: 10
          }}
        >
          <View style={{ width: "60%", marginLeft: "20%" }}>
            <Image
              style={{
                width: "100%",
                height: "100%",
                alignSelf: "center",
                zIndex: 1,
                opacity: 0.2
              }}
              source={require("../../assets/images/logo.png")}
              resizeMode="contain"
            />
          </View>
        </LinearGradient>
      </View>
    )
  })
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: "#FFFFFF"
  },
  mainEventsClass: {
    marginVertical: 4
    //height: (screenHeight-(screenHeight-(screenHeight/12)))
  },
  paddingBottomMore: {
    paddingBottom: 15
  },

  loadMorebtn: {
    borderRadius: 50,
    flex: 1,
    backgroundColor: "#25AE88",
    paddingVertical: 15,
    marginTop: 15
  },
  loadMorebtnTxt: {
    fontWeight: "bold",
    color: "#fff",
    textAlign: "center",
    fontSize: 16
  },
  milestoneBtns: {
    borderRadius: 20,
    paddingVertical: 8
  },
  milestoneView: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#1e5aca",
    borderRadius: 20,
    padding: 2
  },
  milestoneBtnsTxt: {
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center"
  }
});

export default AreaManagerForm;
