import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, Dimensions, TouchableOpacity, ActivityIndicator , Platform} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Root, Container, Button, Toast } from 'native-base';
import Modal from "react-native-modal";
import axios from 'axios';
import {BASE_URL} from '../../../basepath'
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class ReportDetails extends Component {

  constructor(props) {
    super(props);
    this.paramsData = this.props.navigation.state.params;
    this.config = {
      apiUrl: BASE_URL + "apis/Fttx/Survey_data_Detail",
      deleteAPI: BASE_URL + "apis/Fttx/removeSurveyReport/Delete"
    }
    this.state = {
      isLoading: true,
      usersData: [],
      confirmModalVisible: false,
      successModalVisible: false,
      FDC_Details:[],
      FAT_Details:[],
    };
  }

  componentDidMount(){
    this.fetchDataOnLoad();
    this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
  }
  // USING METHOD TO OPEN DRAWER
  openControlPanel = () => {
    this.props.navigation.openDrawer();
  }

  fetchDataOnLoad = async() => {
    let responseJson = [];
    let response = await fetch(this.config.apiUrl+"/"+this.paramsData.reportID);
    try {
      responseJson = await response.json();
      //Successful response from the API Call
        this.setState({ usersData: responseJson.data,FAT_Details: responseJson.FAT_Details,FDC_Details: responseJson.FDC_Details, isLoading: false });
    }catch (error) {
      console.log("Response error - ",error);
    }
  }

  deleteRecordByAPI = async() => {
    let self = this;
    console.log(this.config.deleteAPI+"/"+this.paramsData.reportID);
    let response = axios.delete(this.config.deleteAPI+"/"+this.paramsData.reportID)
    .then(function (response) {
      Toast.show({
        text: "Your entry deleted successfully!",
       //  buttonText: "Okay",
        position: "bottom",
        type: "success",
        textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
      });
      self.setState({ confirmModalVisible: false, successModalVisible: !self.state.successModalVisible });
    })
    .catch(function (error) {
      console.log("Response error - ",error);
    });
  }

  attentionModalVisible = () => {
    this.setState({confirmModalVisible: !this.state.confirmModalVisible});
  }

  // editTransactionRecord = () => {
  //   this.props.navigation.navigate('TFB_WorkDoneEditForm', {"reportID": this.paramsData.reportID});
  // }

  successModalVisibleFn = () => {
    this.setState({ confirmModalVisible: false, successModalVisible: !this.state.successModalVisible});
  }

  render() {
      return (
        <Root>
        <Container style={styles.Container}>

          {/* START SECTION FOR PROFILE MIDLLE WITH DETAILS OF USER */}
          <ScrollView  contentInsetAdjustmentBehavior="automatic">
            <View style={styles.body}>

            {
            (this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <View>

                {/* FIRST SECTION */}
                <View style={styles.sectionContainer}>

                  {/* START SECTION */}
                  <View style={styles.itemsSection} >
                    <View style={{flex:1, flexDirection:"row", alignItems:"flex-start", paddingTop: 12, paddingBottom: 8}}>

                      <View style={styles.sectionUserTitle}>
                        <Text style={{ fontWeight: "bold", fontSize: 18 }}>ID: {this.state.usersData.id}/Form_id: {this.state.usersData.Form_id} </Text>
                      </View>

                      <View style={styles.sectionUserActionIcon}>

                        <TouchableOpacity
                            activeOpacity={1}
                            underlayColor="white"
                            // onPress={this.editTransactionRecord}
                            style={{ flex:0.50, alignItems: "flex-end" }}
                          >
                          {/* <Image source={require('../../assets/images/edit.png')} /> */}
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity={1}
                            underlayColor="white"
                            onPress={this.attentionModalVisible}
                            style={{ flex:0.50, alignItems: "flex-end"  }}
                          >
                          <Image source={require('../../../assets/images/delete.png')} />
                        </TouchableOpacity>

                      </View>


                    </View>
                  </View>
                  {/* END SECTION */}


                  {/* START SECTION */}
                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 12 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>Created Date </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.Date_created}</Text>
                      </View>
                    </View>

                  </View>

                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 7 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>Building Name </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize:16, color:"#959595", textAlign:"right" }}>{this.state.usersData.Building_name}</Text>
                      </View>
                    </View>

                  </View>

                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 7 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>State </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.State}</Text>
                      </View>
                    </View>

                  </View>


                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 7 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>City </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.City}</Text>
                      </View>
                    </View>

                  </View>


                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 7 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>JC </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.JC}</Text>
                      </View>
                    </View>

                  </View>


                  <View style={[styles.itemsSection, { borderBottomWidth: 0, paddingTop: 7 }]} >

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>FSA </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.FSA}</Text>
                      </View>
                    </View>

                  </View>


                  <View style={[styles.itemsSection, { borderBottomWidth:0,paddingTop:7 }]}>

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>Type of connection</Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.Type_of_connection} </Text>
                      </View>
                    </View>

                  </View>

                  <View style={[styles.itemsSection, { borderBottomWidth:0,paddingTop:7 }]}>

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>No. of shaft in building </Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.No_of_shaft_in_building}</Text>
                      </View>
                    </View>

                  </View>

                  <View style={[styles.itemsSection, { borderBottomWidth:0,paddingTop:7 }]}>

                    <View style={{ flex:1, flexDirection:"row" }}>
                      <View style={styles.sectionUserTitleField}>
                        <Text style={{ fontWeight: "800", fontSize: 16 }}>Space for FDC</Text>
                      </View>
                      <View style={styles.sectionUserInfo}>
                        <Text style={{ fontSize: 16, color: "#959595", textAlign: "right" }}>{this.state.usersData.Space_for_FDC} </Text>
                      </View>
                    </View>

                  </View>

                  {/* END SECTION */}

                </View>
                {/* END FIRST SECTION */}
                </View>
              )
            }
            </View>
          </ScrollView>
          {/* END SECTION */}


          {/* START MODAL SECTION */}
          <Modal isVisible={this.state.confirmModalVisible}
           hideModalContentWhileAnimating={true}
           useNativeDriver={false}
           onModalHide={() => {
             if (this.state.successModalVisible) {
               this.props.navigation.pop(2);
               this.props.navigation.navigate('SurveyReports', { "refresh_Screen": true });
             }
           }} style={[ styles.modal,{ padding: 20 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >

            <View style={{ alignContent: "stretch" }}>

              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{ height: 100, }} >

                  <Text style={{ color:"#727272", alignSelf:"center", fontSize:20, paddingTop:15 }}>
                    ATTENTION!
                  </Text>

                  <View style={{ marginLeft: 25, marginRight: 25, marginBottom: 10 , marginTop: 5 }}>
                    <Text style={[ styles.modalMessage , { color: "#727272", fontSize: 14 } ]}>
                      Do you really want to delete this record?
                    </Text>
                  </View>

                </View>
              </View>


              <View style={{ flex: 1, flexDirection: "row", position: "relative", top: 0, marginTop: 75 }} >

                <View style={{ flex:0.5, marginLeft: 0 , marginRight: 8, marginTop: 0, marginBottom: 0 }} >
                  <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.attentionModalVisible } >
                    <Text style={{ width: "100%", textAlign:"center", padding: 10, fontWeight: "bold" }}> NO </Text>
                  </TouchableOpacity>
                </View>

                <View style={{ flex:0.5, marginLeft: 8 , marginRight: 0, marginTop: 0, marginBottom: 0 }}>
                  <TouchableOpacity style={{ backgroundColor: "#DADADA", borderRadius: 2 }} onPress={ this.deleteRecordByAPI} >
                    <Text style={{ width: "100%", textAlign:"center", padding: 10, fontWeight: "bold" }}> YES </Text>
                  </TouchableOpacity>
                </View>

              </View>

            </View>
          </Modal>
          {/* END MODAL SECTION */}

          {/* MODAL USING TO DISPLAY SUCCESS POPUP */}
          <Modal isVisible={false} style={[ styles.modal, { paddingHorizontal: 10 }]} animationIn={'zoomInDown'} animationOut={'zoomOutUp'} animationInTiming={1000} animationOutTiming={1000}  ackdropTransitionInTiming={1000} backdropTransitionOutTiming={1000} >
            <View style={{ flex: 1, flexDirection: "row" }}>

              <View style={styles.rightImgView} >
                <Image style={styles.rightImgSelf} source={require('../../../assets/images/success_icon.png')}/>
              </View>

              <View style={ styles.closeModalBtnView }>
                <Button style={[styles.closeModalBtn ]} onPress={this.successModalVisibleFn}>
                  <Text style={[ styles.closeModalBtnX, {position:'absolute'}]}>X</Text>
                </Button>
              </View>

              <View style={{ flex: 1, position: "relative", top: 70, left: 0 }} >
                <Text style={[ styles.modalHeading ]}>SUCCESS!</Text>
                <View style={{ marginLeft: 15, marginRight: 15 }}>
                  <Text style={[ styles.modalMessage ]}>Your entry deleted successfully </Text>
                </View>
              </View>

            </View>
          </Modal>
          {/* END MODAL SECTION */}

        </Container>
        </Root>
      )
  }


  static navigationOptions = ({navigation}) => ({
      title: 'Report Details',
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        alignSelf: 'center',
        textAlign:"center",
        flex: 0.8,
        fontSize:22,
        lineHeight:25,
      },
      headerStyle: {
        height:84,
        borderBottomWidth: 0,
      },
      headerLeft:(
        <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
          <MaterialIcons
            name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
        </TouchableOpacity>
      ),
      headerBackground: (
        <View>
          <LinearGradient
            colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
          >
            <View style={{ width: '60%', marginLeft: "20%" }}>
              <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
            </View>
          </LinearGradient>
        </View>
      ),

  })

}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  Container: {
    backgroundColor: "#eeeeee"
  },
  body: {
    paddingBottom: 20,
  },
  sectionContainer: {
    marginTop: (screenHeight-(screenHeight-(screenHeight/28))),
    marginLeft: 20,
    marginRight: 20,
    backgroundColor: "#FFFFFF",
    flex: 1,
    flexDirection: "column",
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 4,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 4,
      },
    }),
    borderRadius: 8,
  },
  flexColumn:{
    marginTop:(screenHeight/8),
    flex:1,
    marginBottom:20,
  },
  itemsSection: {
    flex: 1,
    flexDirection: "row",
    borderBottomColor: "#e5e5e5",
    borderBottomWidth: 1,
    paddingHorizontal: 15
  },
  sectionIcon: {
    flex: 0.4,
    justifyContent: "center",
  },
  sectionUserTitle: {
    flex: 0.85,
    paddingTop: 2
  },
  sectionUserActionIcon: {
    flex: 0.15,
    flexDirection: "row",
    paddingVertical: 5
  },

  sectionTitleIcon: {
    flex: 0.16,
    paddingVertical: 5
  },
  sectionTitle: {
    flex: 0.83,
    fontSize: 16,
    fontWeight: '600',
    color: "#898989",
    alignSelf:"center",
  },
  sectionUserTitleField: {
    flex: 0.4,
  },
  sectionUserInfo: {
    flex: 0.6,
  },
  sectionUserDet: {
    alignSelf: "flex-end",
    fontWeight: "600",
    fontSize: 16,
    color: "#898989",
    paddingTop: 4
  },
  modal: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 190,
    width: "70%",
    marginLeft: "15%",
    backgroundColor: "#ffffff",
    position: "absolute",
    top: (screenHeight-(screenHeight-(screenHeight/3))),
    ...Platform.select({
      ios: {
        shadowOpacity: 0.3,
        shadowRadius: 7,
        shadowOffset: {
            height: 0,
            width: 0
        },
      },
      android: {
        elevation: 10,
      },
    }),
    borderRadius: 5
  },
  modalMessage: {
    textAlign: "center",
    fontSize: 16,
    justifyContent: "center",
    color: "gray",
  },
  rightImgView: {
    position: "absolute",
    top: 0,
    left: 0,
    width: "90%",
    marginLeft: "5%",
  },

  rightImgSelf: {
    alignSelf:'center',
    backgroundColor:'transparent',
    borderColor: "#ffffff",
    borderWidth: 8,
    borderRadius: 100,
    height: 90,
    width: 90,
    position: 'relative',
    top: -40,
  },
  closeModalBtnView: {
    position: "absolute",
    top: 5,
    right: 0,
    width: 50,
  },

  closeModalBtn: {
    alignItems: "flex-end",
    elevation:0,
    width: "100%",
    backgroundColor: "#ffffff",
  },

  closeModalBtnX: {
    paddingBottom: 10,
    width: "100%",
    fontSize:20,
    fontWeight: "bold",
    color: "gray",
    elevation: 0,
    backgroundColor: "#ffffff",
    textAlign: "right",
    alignSelf: "flex-end",
  },

  modalHeading: {
    fontSize: 24,
    textAlign: "center",
  },
  modalMessage: {
    textAlign: "center",
    fontSize: 16,
    justifyContent: "center",
    color: "gray",
  },

});

export default ReportDetails;
