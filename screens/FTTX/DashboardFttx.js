import React, { Component } from "react";
import { AsyncStorage, StyleSheet, ScrollView, View, Text,ImageBackground, Image, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { Container } from 'native-base';
import { LinearGradient } from 'expo-linear-gradient';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class Dashboard extends Component {

    constructor(props) {
      super(props);
      this.state = {
        userId: "",
        userProjectId: "",
        userRole: "",
        errorMsg: ""
      };
    }

    componentDidMount(){
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    // USING METHOD TO CHECK SCREENS PERMISSION ACCORNING TO USER ASSIGNED PROJECTS & ROLES
    _handleButtonPress = async(link) => {
      
      let self = this;
      const user_id = await AsyncStorage.getItem('user_id');
      const userProjectId = await AsyncStorage.getItem('user_project_id');
      const userRole = await AsyncStorage.getItem('user_role');
      self.props.navigation.navigate(link);
    }

    render() {

      const dashboardBoxes = [
        {
          "Image_path": require("../../assets/images/icon-5-1.png"),
          "Name": "OLT TO FDC or Chamber",
          "link": "Execution_OLT_TO_FDCScreen",
        },{
          "Image_path": require("../../assets/images/icon-6-1.png"),
          "Name": "FDC or Chamber TO FAT",
          "link": "Execution_FDC_TO_FATScreen",
        },{
          "Image_path": require("../../assets/images/icon-4.png"),
          "Name": "FAT to OTB(SDU)",
          "link": "Execution_FAT_TO_OTBScreen",
        }
      ];
    
      return (
        <Container>

          <ScrollView  contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>

            <View style={styles.body}>
            <View style={{marginLeft:20, marginRight:20, marginTop:20}}>
              <ImageBackground
                  source={require("../../assets/images/Execution.jpg")}
                  style={{
                    height: 200,
                    width: "100%",
                    position: 'relative', // because it's parent
                    borderRadius:4,
                    backgroundColor: "#E5E5E5",
                  }}
                  resizeMode="cover"
                  imageStyle={{ borderRadius: 4, borderWidth:0 }}
                ></ImageBackground>
            </View>
              
              {
                (this.state.errorMsg !='')?(
                  <View>
                    <Text style={styles.errorDesign}>{this.state.errorMsg}</Text>
                  </View>
                ):(
                  <View><Text></Text></View>
                )
              }
              

              {
                dashboardBoxes.map((item, i) => {
                    return(
                      <TouchableOpacity key={i} activeOpacity={1} style={styles.sectionContainer} onPress={()=> this._handleButtonPress(item.link)} >
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={styles.sectionIcon}>
                            <Image source = {item.Image_path} resizeMode='contain' 
                            style={{ alignSelf: "auto" }} />
                          </View>
                          <Text style={styles.sectionTitle}>
                            {item.Name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                })
              }
                
            </View>
            
          </ScrollView>
        </Container>
      )
    }
  
    // USING navigationOptions TO SET HEADER OF SCREEN
    static navigationOptions = ({ navigation }) => ({
        title: 'Execution Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign:"center", 
          flex:0.8,
          fontSize:20,
          lineHeight:25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),

    })

}


// ADDING CSS TO DESIGN SCREEN
const styles = StyleSheet.create({
  scrollView:{
    backgroundColor: '#eeeeee',
  },
    container: {
      padding: 20,
      flex: 1,
    },
    body: {
      backgroundColor: '#eeeeee',
     
      paddingBottom: 5,
    },
    sectionContainer: {
      // marginTop: 32,
      marginBottom: 15,
      marginLeft: 20,
      marginRight: 20,
      paddingHorizontal: 15,
      paddingVertical:10,
      backgroundColor: "#FFFFFF",
      flex: 1,
      flexDirection: "row",
      
      borderRadius: 2,
      ...Platform.select({
        ios: {
          shadowOpacity: 0.3,
          shadowRadius: 3,
          shadowOffset: {
              height: 0,
              width: 0
          },
        },
        android: {
          elevation: 4,
        },
      })
    },
    sectionIcon: {
      flex: 0.4,
      justifyContent: "center",
    },
    sectionTitle: {
      flex: 0.6,
      fontSize: 22,
      fontWeight: '600',
      color: "#1949c7",
      alignSelf:"center"
    },
    highlight: {
      fontWeight: '700',
    },
    footer: {
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    errorDesign: {
      paddingVertical: 10,
      paddingHorizontal: 25,
      textAlign: 'center',
      color: "red",
      fontSize: 14,
      fontWeight: '600'
    }
  
  });
  
export default Dashboard;
