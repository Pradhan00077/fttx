import React, { Component } from "react";
import {Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator, Platform} from 'react-native';
//import ImagePicker from 'react-native-image-picker'
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {BASE_URL} from '../../../basepath';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

class ExecutionForm extends Component {
    constructor(props) {
        super(props);
        this.config = {
          BuildingNameApi: BASE_URL+'apis/Fttx/building_name',
        }
        this.state = {
            Error:"",
            loginbutton:false,
            isLoading: true,
            Building_Name_DATA:[],
            Building_name:"",
            Building_NameErrorMsg:"",
            Vender_id:"",
            Vender_idErrorMsg:"",
            Vender_name:"",
            Vender_nameErrorMsg:"",
            Work_start_date:"",
            Work_start_dateErrorMsg:"",
            Work_end_date: todayDate,
            Work_end_dateErrorMsg:"",
            Activity:"",
            ActivityErrorMsg:"",
        }

    }
    SetBuildingName = async( textValue, value,) => {
      this.setState({Error:""});
        this.setState({ Building_name: textValue, Building_nameErrorMsg:"" });
  }

  onchangeTxt = async(textValue, txtName) =>{
    this.setState({Error:""});
    if(txtName === 'Vender_id'){
      this.setState({ Vender_id: textValue, Vender_idErrorMsg:"" });
    }else if(txtName === 'Vender_name'){
      this.setState({ Vender_name: textValue, Vender_nameErrorMsg:"" });
    }else if(txtName==='Work_start_date'){
      this.setState({ Work_start_date: textValue, Work_start_dateErrorMsg:"", Work_end_date:"" });
    }else if(txtName==='Work_end_date'){
      this.setState({ Work_end_date: textValue, Work_end_dateErrorMsg:"" });
    } else if(txtName==='Activity'){
      this.setState({ Activity: textValue, ActivityErrorMsg:"" });
    } 
  }
  // CALLING METHOD ON LOAD OF PAGE
  fatchBuildingName = async() => {
    let responseJson = [];

    let response = await fetch(this.config.BuildingNameApi);
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      if(responseJson.hasOwnProperty('error')){
        this.setState({ Building_NameError: responseJson.error, isLoading: false });
      }else{
        //Successful response from the API Call
          this.setState({ Building_Name_DATA: responseJson.data, isLoading: false });
      }
    }catch (error) {
      console.log("Response error - ",error);
    }
  }

  _handleButtonPressLogin = async () =>{
    this.setState({ loginbutton: true, });
    this.SubmitForm();
  }

  async SubmitForm(){
    if(!this.state.Building_name.trim()){
      this.setState({loginbutton:false, Building_nameErrorMsg:"Building name is required.", Error:"Building name is required."})
      return 0;
    }
    

    if(!this.state.Vender_id.trim()){
      this.setState({loginbutton:false, Vender_idErrorMsg:"Vender id is required.", Error:"Vender id is required."})
      return 0;
    }

    if(!this.state.Vender_name.trim()){
      this.setState({loginbutton:false, Vender_nameErrorMsg:"Vender name is required.", Error:"Vender name is required."})
      return 0;
    }
    if(!this.state.Work_start_date.trim()){
      this.setState({loginbutton:false, Work_start_dateErrorMsg:"Work start date is required.", Error:"Work start date is required."})
      return 0;
    }
    if(!this.state.Activity.trim()){
      this.setState({loginbutton:false, ActivityErrorMsg:"Activity is required.", Error:"Activity is required."})
      return 0;
    }
    
    this.props.navigation.navigate('ExecutionFormitem', {ExecutionFormInfo:this.state});

  }

    componentDidMount(){
      this.fatchBuildingName();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }
    render() {
      let Activity = [
        {
          value:"Activity-1"
        },
        {
          value:"Activity-2"
        },
        {
          value:"Activity-3"
        }
      ]
        return (
            <Container style = {{height:screenHeight}}>
              {(this.state.isLoading)?(
                <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                  <ActivityIndicator size="large" color="#2157c4" />
                </View>
              ):(
                <View style = {{height:'100%'}}>
                <ScrollView contentInsetAdjustmentBehavior="automatic">  
                    <View style={styles.sectionContainer}>
  
                        <View style={{ marginBottom: 25, marginTop:25 }}>
                          <View style ={ styles.paddingBottomMore }>
                            <Dropdown
                              label= "Building Name"
                              data={this.state.Building_Name_DATA}
                              dropdownPosition={1}
                              dropdownOffset = {{ top: 8, left: 0 }}
                              onChangeText={(value, index) => this.SetBuildingName(value, index, "Building_Name")}
                              valueExtractor={({value})=> value}
                              textColor="#000000"
                              style={{ color: "#000000" }}
                              baseColor = "#000000"
                              error	= {this.state.Building_nameErrorMsg}
                            />                        
                          </View>
                          <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Vender ID'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.Vender_id}
                              onChangeText={(value) => this.onchangeTxt(value, "Vender_id")}
                              error	= {this.state.Vender_idErrorMsg}
                            />
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Vender Name'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.Vender_name}
                              onChangeText={(value) => this.onchangeTxt(value, "Vender_name")}
                              error	= {this.state.Vender_nameErrorMsg}
                            />
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                        <View><Text style ={[(this.state.Work_start_date)?({fontSize:12}):(!this.state.Work_start_dateErrorMsg)?({fontSize:12}):({fontSize:16, color:'red'})]}>Work Start date</Text></View>
                          <DatePicker
                          style={[{width:'100%', borderBottomWidth:0.333, borderColor:"rgba(0, 0, 0, 1)"}, (!this.state.Work_start_dateErrorMsg)?({}):({borderColor:'red', borderBottomWidth:1})]}
                            mode="date"
                            date ={this.state.Work_start_date}
                            format="YYYY-MM-DD"
                            placeholderText=""
                            maxDate={new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            iconSource = {require("../../../assets/images/calendar-2.png")}
                            customStyles={{
                              dateIcon: {
                                position: 'absolute',
                                right: 0,
                                top: 4,
                                marginLeft: 0,
                              },
                              dateText:{
                                color:'#000000',
                                textAlign:'left',
                                alignSelf:'flex-start'
                              },
                              dateInput: {
                                backgroundColor: "#fff",
                                borderWidth: 0,
                                color: "#000000",
                                alignItems:'flex-start',
                              }
                            }}
                            onDateChange={(value) => this.onchangeTxt(value, "Work_start_date")}
                          />
                          <View><Text style ={[(!this.state.Work_start_dateErrorMsg)?({fontSize:12}):({fontSize:12, color:'red'})]}>{this.state.Work_start_dateErrorMsg}</Text></View>
                        </View> 
                      </View>
                      <View style ={ styles.paddingBottomMore }>
                        <View><Text style ={[(this.state.Work_end_date)?({fontSize:12}):(!this.state.Work_end_dateErrorMsg)?({fontSize:12}):({fontSize:16, color:'red'})]}>Work End date</Text></View>
                          <DatePicker
                          style={[{width:'100%', borderBottomWidth:0.333, borderColor:"rgba(0, 0, 0, 1)"}, (!this.state.Work_end_dateErrorMsg)?({}):({borderColor:'red', borderBottomWidth:1})]}
                            mode="date"
                            date = {this.state.Work_end_date}
                            format="YYYY-MM-DD"
                            placeholderText=""
                            minDate={(this.state.Work_start_date)?this.state.Work_start_date:new Date()}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            iconSource = {require("../../../assets/images/calendar-1.png")}
                            customStyles={{
                              dateIcon: {
                                position: 'absolute',
                                right: 0,
                                top: 4,
                                marginLeft: 0,
                              },
                              dateText:{
                                color:'#000000',
                                textAlign:'left',
                                alignSelf:'flex-start'
                              },
                              dateInput: {
                                backgroundColor: "#fff",
                                borderWidth: 0,
                                color: "#000000",
                                alignItems:'flex-start',
                              }
                            }}
                            onDateChange={(value) => this.onchangeTxt(value, "Work_end_date")}
                          />
                          <View><Text style ={[(!this.state.Work_end_dateErrorMsg)?({fontSize:12}):({fontSize:12, color:'red'})]}>{this.state.Work_end_dateErrorMsg}</Text></View>
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                        <Dropdown
                            label= "Activity"
                            data={Activity}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.onchangeTxt(value, "Activity")}
                            value = {this.state.Activity}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.ActivityErrorMsg}
                          />       
                        </View>  
                        <KeyboardSpacer topSpacing ={10} />
                    </View>
                </ScrollView>
                
                        {(this.state.Error!=='')?
                        (
                          <View style = { styles.cartButtonView } >
                            <Button style = {[styles.buttonDisabled] } >
                                <Image  style = {styles.imageStyle} source = {require('../../../assets/images/Shape.png')} />
                            </Button>
                          </View>
                        )
                        :
                        (
                          <View style = { styles.cartButtonView } >
                            <Button style = {[ styles.cartButton] } onPress={this._handleButtonPressLogin} >
                                <Image  style = {styles.imageStyle} source = {require('../../../assets/images/Shape.png')} />
                            </Button>
                          </View>
                        )}
                </View>
              )
            }
            </Container>
        )
    }

    static navigationOptions = ({navigation}) => {
      return {
        title: 'Execution Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
      }
    }
}
const styles = StyleSheet.create({
    ActivityIndicatorContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    ActivityIndicatorHorizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      // padding: 10,
    },
     buttonDisabled:{
      backgroundColor:'#cccccc',
      alignItems: 'center',
      width: 57,
      height: 57,
      borderRadius: 33,
      color: '#ffffff',
      alignSelf: 'flex-end',
      zIndex: 11,
      elevation: 10,
      ...Platform.select({
        ios:{
          shadowOpacity: 0.4,
          shadowRadius: 7,
          shadowOffset: {
              height: 0,
              width: 0
          }
        }
      }),
     },
    body: {
      flex: 1,
      backgroundColor: "#2157c4",
      justifyContent: "center",
      //height: (screenHeight-23),
      height: (screenHeight + 33),
      paddingHorizontal: 24,
    },
    logoSection: {
      marginBottom: 50
    },
    sectionTitle: {
      fontSize: 26,
      color: "#FFF",
      alignSelf: "center",
    },
    paddingBottomMore: {
      paddingBottom: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      color: "#FFF",
      alignSelf: "center",
    },
  
    footer: {
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    
    buttonText: {
      // fontFamily: 'Gill Sans',
      alignItems: "center",
      margin: 10,
      color: '#2157c4',
      backgroundColor: '#eeeeee',
    },
    labelForgetColor1:{
      color:'#eeeeee',
      flex:1,
      textAlign:'right',
      position:'absolute',
      right:5,
      bottom:28,
      fontSize:12,
      lineHeight:16,
    },
    labelForgetColor:{
      color:'#eeeeee',
      flex:1,
      textAlign:'right',
      position:'absolute',
      right:5,
      bottom:12,
      fontSize:12,
      lineHeight:16,
    },

    cartButtonView: {
      position: "absolute",
      bottom: 10,
      right: 10,
      borderRadius:33,
      width:66,
      height:66,
    },
   
    cartButton:{
      alignItems: 'center',
      width: 57,
      height: 57,
      borderRadius: 33,
      backgroundColor: '#0099EF',
      color: '#ffffff',
      alignSelf: 'flex-end',
      zIndex: 11,
      elevation: 10,
      ...Platform.select({
        ios:{
          shadowOpacity: 0.4,
          shadowRadius: 7,
          shadowOffset: {
              height: 0,
              width: 0
          }
        }
      }),
    },
  
    imageStyle:{
        position: "relative",
        left: 20,
        top: 1,
        alignSelf:'center', 
        width:21,
        height:21
    },
  
  });
    
  export default ExecutionForm;
  