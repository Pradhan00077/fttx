import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button, Root, Toast} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { MaterialIcons } from '@expo/vector-icons';
import {BASE_URL} from '../../../basepath';
import { TouchableOpacity } from "react-native-gesture-handler";

const screenHeight = Math.round(Dimensions.get('window').height);



class FAT_TO_OTBExecutionForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        APIurl: BASE_URL+'apis/Fttx/Execution_FAT_TO_OTB_Insertion',
      }
      this.state = {
          Error:"",
          isLoading: false,
          loginbutton:false,
          user_id:null,
          CSA_Name:"",
          CSA_NameErrorMsg:"",
          Building_Name:"",
          Building_NameErrorMsg:"",
          FAT_Name:"",
          FAT_NameErrorMsg:"",
          FAT_SAP_ID:"",
          FAT_SAP_IDErrorMsg:"",
          OTB_Name:"",
          OTB_NameErrorMsg:"",
          OTB_SAP_ID:"",
          OTB_SAP_IDErrorMsg:"",
          Duct_Conduit_Type:"",
          Duct_Conduit_TypeErrorMsg:"",
          Duct_Conduit_Length:"",
          Duct_Conduit_LengthErrorMsg:"",
          Fiber_Cable_Type:"",
          Fiber_Cable_TypeErrorMsg:"",
          No_Of_Home_Pass:"",
          No_Of_Home_PassErrorMsg:"",
          Cable_Length:"",
          Cable_LengthErrorMsg:"",
        };
     }

     componentDidMount =async()=>{
      const userid = await AsyncStorage.getItem('user_id');
      this.setState({user_id:userid});
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

     _handleButtonPressLogin = async () =>{
      this.setState({ loginbutton: true, });
      this.SubmitForm();
    }


    async SubmitForm(){

      if(!this.state.CSA_Name.trim()){
        this.setState({loginbutton:false, CSA_NameErrorMsg:"CSA Name is required.", Error:"CSA Name is required."})
        return 0;
      }
      if(!this.state.Building_Name.trim()){
        this.setState({loginbutton:false, Building_NameErrorMsg:"Building Name is required.", Error:"Building Name is required."})
        return 0;
      }
      if(!this.state.FAT_Name.trim()){
        this.setState({loginbutton:false, FAT_NameErrorMsg:"FAT Name is required.", Error:"FAT Name is required."})
        return 0;
      }
      if(!this.state.FAT_SAP_ID.trim()){
        this.setState({loginbutton:false, FAT_SAP_IDErrorMsg:"FAT SAP ID is required.", Error:"FAT SAP ID is required."})
        return 0;
      }
      if(!this.state.OTB_Name.trim()){
        this.setState({loginbutton:false, OTB_NameErrorMsg:"OTB Name is required.", Error:"OTB Name is required."})
        return 0;
      }
      if(!this.state.OTB_SAP_ID.trim()){
        this.setState({loginbutton:false,OTB_SAP_IDErrorMsg:"OTB SAP ID is required.", Error:"OTB SAP ID is required."})
        return 0;
      }
     

      if(!this.state.Duct_Conduit_Type.trim()){
        this.setState({loginbutton:false, Duct_Conduit_TypeErrorMsg:"Duct/Conduit type is required.", Error:"Duct/Conduit type is required."})
        return 0;
      }

      
     if(!this.state.Duct_Conduit_Length.trim()){
      this.setState({loginbutton:false, Duct_Conduit_LengthErrorMsg:" Duct/Conduit length is required.", Error:"Duct/Conduit length required."})
      return 0;
    }


      if(!this.state.Fiber_Cable_Type.trim()){
        this.setState({loginbutton:false, Fiber_Cable_TypeErrorMsg:"Fiber Cable Type is required.", Error:"Fiber Cable Type is required."})
        return 0;
      }
      if(!this.state.Cable_Length.trim()){
        this.setState({loginbutton:false, Cable_LengthErrorMsg:"Cable Length is required.", Error:"Cable Length is required."})
        return 0;
      }
      if(!this.state.No_Of_Home_Pass.trim()){
        this.setState({loginbutton:false, No_Of_Home_PassErrorMsg:"No. of home pass is required.", Error:"No. of home pass is required."})
        return 0;
      }


      
      
      this.setState({loginbutton:true});
      let response = await fetch(this.config.APIurl, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: this.state,
        })
      })
         
          try {
            responseJson = await response.json();
            this.setState({ loginbutton: false, isLoading: false });
            console.log("Data:", responseJson);
            if(responseJson.hasOwnProperty('error')){
              this.setState({ Error: responseJson.error});
            }else{
              //Successful response from the API Call
              Toast.show({
                text: responseJson.success,
               //  buttonText: "Okay",
                position: "bottom",
                type: "success",
                textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
              });
              this.setState({Error:responseJson.success,
                CSA_Name:"",
                Building_Name:"",
                FAT_Name:"",
                FAT_SAP_ID:"",
                OTB_Name:"",
                OTB_SAP_ID:"",
                Duct_Conduit_Type:"",
                Duct_Conduit_Length:"",
                Fiber_Cable_Type:"",
                No_Of_Home_Pass:"",
                Cable_Length:"",
              })
            }
          }catch (error) {
            this.setState({ isLoading: false });
            console.log("Response error - ",error);
          }
    }

    cmpSetTextState = async( textValue, txtName, index ) => {
      this.setState({Error:"", loginbutton:false});
      const txtVal = textValue;
      if(txtName === 'CSA_Name'){
        this.setState({ CSA_Name: txtVal, CSA_NameErrorMsg:"" });
      }else if(txtName==='Building_Name'){
        this.setState({Building_Name: txtVal, Building_NameErrorMsg:""})
      }else if(txtName==='FAT_Name'){
        this.setState({FAT_Name: txtVal, FAT_NameErrorMsg:""})
      }else if(txtName==='FAT_SAP_ID'){
        this.setState({FAT_SAP_ID:txtVal, FAT_SAP_IDErrorMsg:""})
      }else if(txtName==='OTB_Name'){
        this.setState({OTB_Name: txtVal, OTB_NameErrorMsg:""})
      }else if(txtName==='OTB_SAP_ID'){
        this.setState({OTB_SAP_ID:txtVal,OTB_SAP_IDErrorMsg:""})
      }else if(txtName==='Duct_Conduit_Type'){
        this.setState({Duct_Conduit_Type:txtVal, Duct_Conduit_TypeErrorMsg:""})
      }else if(txtName==='Duct_Conduit_Length'){
        this.setState({Duct_Conduit_Length:txtVal,Duct_Conduit_LengthErrorMsg:""})
      }else if(txtName==='Fiber_Cable_Type'){
        this.setState({Fiber_Cable_Type:txtVal, Fiber_Cable_TypeErrorMsg:""})
      }else if(txtName==='Cable_Length'){
        this.setState({Cable_Length:txtVal, Cable_LengthErrorMsg:""})
      }else if(txtName==='No_Of_Home_Pass'){
        this.setState({No_Of_Home_Pass:txtVal, No_Of_Home_PassErrorMsg:""})
      }
    }
    


    render() {
      let Duct_Conduit_Type =[
        {
          value:"Duct-1"
        },
        {
          value:"Duct-2"
        },
        {
          value:"Duct-3"
        },
        {
          value:"Duct-4"
        },
        {
          value:"Duct-5"
        },
        {
          value:"Duct-6"
        },
        {
          value:"Duct-7"
        },
        {
          value:"Duct-8"
        }
      ];
      let Cable_Type = [
        {
          value:"Armoured"
        },
        {
          value:"ADSS"
        }
      ]
      let Fiber_Cable_Type = [
        {
          value:"Fiber Cable Type-1"
        },{
          value:"Fiber Cable Type-2"
        },{
          value:"Fiber Cable Type-3"
        },{
          value:"Fiber Cable Type-4"
        },{
          value:"Fiber Cable Type-5"
        },{
          value:"Fiber Cable Type-6"
        },
      ]
        return (
          <Root>
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic">  
                  <View style={styles.sectionContainer}>

                      <View style={{ marginBottom: 25 }}>
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="CSA Name"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.CSA_Name}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "CSA_Name")}
                              error	= {this.state.CSA_NameErrorMsg}
                            />
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Building Name"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Building_Name}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Building_Name")}
                              error	= {this.state.Building_NameErrorMsg}
                            />
                        </View>  
                        <View style = {{flex:1,  marginLeft: 10, marginRight: 10,marginBottom:0}}>
                          <Text style = {{fontWeight:'900', fontSize:14}}>FAT:</Text>
                        </View>
                        <View style ={[ styles.paddingBottomMore, {flex:1, flexDirection:'row'} ]}>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="Name"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='default'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.FAT_Name}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "FAT_Name")}
                                error	= {this.state.FAT_NameErrorMsg}
                              />
                          </View>
                          <View style={{flex:0.06}}></View>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="SAP ID"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='number-pad'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.FAT_SAP_ID}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "FAT_SAP_ID")}
                                error	= {this.state.FAT_SAP_IDErrorMsg}
                              />
                          </View>
                        </View>  
                        <View style = {{flex:1,  marginLeft: 10, marginRight: 10,marginBottom:0}}>
                          <Text style = {{fontWeight:'900', fontSize:14}}>OTB:</Text>
                        </View>
                        <View style ={[ styles.paddingBottomMore, {flex:1, flexDirection:'row'} ]}>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="Name"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='default'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.OTB_Name}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "OTB_Name")}
                                error	= {this.state.OTB_NameErrorMsg}
                              />
                          </View>
                          <View style={{flex:0.06}}></View>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="SAP ID"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='number-pad'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.OTB_SAP_ID}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "OTB_SAP_ID")}
                                error	= {this.state.OTB_SAP_IDErrorMsg}
                              />
                          </View>
                        </View>   

                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Duct/Conduit Type"
                            data={Duct_Conduit_Type}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Duct_Conduit_Type")}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            value = {this.state.Duct_Conduit_Type}
                            error	= {this.state.Duct_Conduit_TypeErrorMsg}
                          />                        
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Duct/Conduit length(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Duct_Conduit_Length}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Duct_Conduit_Length")}
                              error	= {this.state.Duct_Conduit_LengthErrorMsg}
                            />
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Fiber Cable Type"
                            data={Fiber_Cable_Type}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Fiber_Cable_Type")}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            value = {this.state.Fiber_Cable_Type}
                            error	= {this.state.Fiber_Cable_TypeErrorMsg}
                          />                        
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Cable Length(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Cable_Length}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Cable_Length")}
                              error	= {this.state.Cable_LengthErrorMsg}
                            />
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="No. of Home Pass"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.No_Of_Home_Pass}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "No_Of_Home_Pass")}
                              error	= {this.state.No_Of_Home_PassErrorMsg}
                            />
                        </View>
                      </View>
                      <KeyboardSpacer topSpacing ={10} />

                      {
                        (this.state.Error!='')?
                        (
                          <View style ={ styles.paddingBottomMore }>
                            <Text style={{ flex: 1, fontSize: 14,color:'red' }}>{this.state.Error}</Text>
                          </View>
                        ):
                        (
                          <View></View>
                        )
                      }
                      <View style ={ styles.paddingBottomMore }>
                      {
                        (this.state.loginbutton)?
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Loading...</Text>
                            </TouchableOpacity>
                        )
                        :
                        (

                            <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}  onPress={this._handleButtonPressLogin}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Submit</Text>
                            </TouchableOpacity>
                        )
                      }
                      </View>
                  </View>
              </ScrollView>
              
            )
            }
          </Container>
          </Root>
        )
    }
  
  
    static navigationOptions = ({navigation}) => {
      return {
      title: 'FAT TO OTB(SDU)',
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        alignSelf: 'center',
        textAlign: "center", 
        flex: 0.8,
        fontSize: 22,
        lineHeight: 25,
      },
      headerStyle: {
        height:84,
        borderBottomWidth: 0,
      },
      headerLeft:(
        <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
          <MaterialIcons
            name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
        </TouchableOpacity>
      ),
      headerBackground: (
        <View>
          <LinearGradient
            colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
          >
            <View style={{ width: '60%', marginLeft: "20%" }}>
              <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
            </View>
          </LinearGradient>
        </View>
      ),
  }
}

}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    // padding: 10,
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    //height: (screenHeight-23),
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  paddingBottomMore: {
    paddingBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#fff',
  },
  deletebuttonText: {
    alignItems: "center",
    color: '#fff',
  },
  labelForgetColor1:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },
  submitbtn:{
    borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
  }

});
  
export default FAT_TO_OTBExecutionForm;
