import React, { Component } from "react";
import { ImageEditor, Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator, Platform, PermissionsAndroid} from 'react-native';
//import ImagePicker from 'react-native-image-picker'
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button, Root, Toast} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import KeyboardSpacer from 'react-native-keyboard-spacer';
const States = require('../../../assets/states.json');
const Cities = require('../../../assets/cities.json');
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {BASE_URL} from '../../../basepath';
import { TouchableOpacity } from "react-native-gesture-handler";
import { MaterialIcons } from '@expo/vector-icons';
let Images = new FormData();
const screenHeight = Math.round(Dimensions.get('window').height);



class SurveyForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        SurveyFormAPI: BASE_URL+'apis/Fttx/Survey_Data_Insertion',
      }
      this.state = {
          Error:"",
          isLoading: false,
          form_id: "",
          loginbutton:false,
          State: '',
          city: '', 
          selectedCity:[],
          formErrorMsg: "",
          stateErrorMsg:"",
          cityErrorMsg:"",
          jc:'',
          jcErrorMsg:"",
          fsa:'',
          fsaErrorMsg:"",
          typeofconnection:"",
          typeconnectionErrorMsg:'',
          buildingname:'',
          buildingnameErrorMsg:"",
          Buildingphoto: null,
          BuildingphotoErrorMsg:"",
          buildingpermissionlatter:null,
          buildingpermissionlatterErrorMsg:"",
          No_of_shaft:"",
          No_of_shaftErrorMsg:"",
          total_no_of_FDC:"",
          total_no_of_FDCErrorMsg:"",
          Space_for_fdc:"",
          Space_for_fdcErrorMsg:"",
          FDC_detail:[],
          total_no_of_FAT:"",
          total_no_of_FATErrorMsg:"",
          Space_for_fat:"",
          Space_for_fatErrorMsg:"",
          FAT_detail:[],
          Vertical_conduct_Methodology:"",
          Vertical_conduct_MethodologyErrorMsg:"",
          Serving_building:"",
          Serving_buildingErrorMsg:"",
          Incoming_Office_From:"",
          Incoming_Office_FromErrorMsg:"",
          Building_Approach:"",
          Building_ApproachErrorMsg:"",
          Type_of_Ricer:"",
          Type_of_RicerErrorMsg:"",
          Route_length_of_Ricer:"",
          Route_length_of_RicerErrorMsg:"",
          user_id:null
        };
     }

     componentDidMount =async()=>{
      const userid = await AsyncStorage.getItem('user_id');
      this.setState({user_id:userid});
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

     s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1); // substring return all chars except char at index=0;
    };
    
    uniqueId = () => {
      // create uniqueId for image as Alphabetical
      return (
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4()
      );
    };

     handleChoosePhoto = async(fieldname, index) => {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      let response = [];
      if(fieldname==='buildingpermission'){
         response = await ImagePicker.launchImageLibraryAsync({ // this is from Gallery
          mediaTypes: "Images",
          allowsEditing: true,
          quality: 1,
          isCamera: true,
        });
      }else{
      if(status === 'granted') {
        response = await ImagePicker.launchCameraAsync({ // this is capture image only
          mediaTypes: "Images",
          allowsEditing: true,
          quality: 1,
          isCamera: true,
          }).catch(error => alert("Status 1" +error));
       }
      }
    
      

      if (!response.cancelled) {
        let localUri = response.uri;
        let filename = localUri.split('/').pop();
  
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
   
        if(fieldname==='buildingphoto'){
          // Assume "photo" is the name of the form field the server expects
          Images.append('file', { uri: localUri, name: filename, type:type  });


          this.setState({Buildingphoto: response });
        }else if(fieldname==='buildingpermission'){
          // Assume "photo" is the name of the form field the server expects
          Images.append('buildingPermission', { uri: localUri, name: filename, type:type  });
          this.setState({buildingpermissionlatter: response });
        }else if(fieldname==='FDC_Detail'){
          var FDC_Data =[ ...this.state.FDC_detail]
          FDC_Data[index].image = response;
          Images.append('FDC[]', { uri: localUri, name: filename, type:type  });
           await this.setState({FDC_detail:FDC_Data});
        } else if(fieldname==='FAT_Detail'){
          var FAT_Data =[ ...this.state.FAT_detail]
          FAT_Data[index].image = response;
          Images.append('FAT[]', { uri: localUri, name: filename, type:type  });
           await this.setState({FAT_detail:FAT_Data});
        }    
      }
    }

    handleRemovePhoto = async(fieldname, index) => {
      if(fieldname==='buildingphoto'){
        this.setState({Buildingphoto: null })
      }else if(fieldname==='buildingpermission'){
        this.setState({buildingpermissionlatter: null })
      }else if(fieldname==='FDC_Detail'){
        var FDC_Data_remove =[ ...this.state.FDC_detail]
        FDC_Data_remove[index].image = {};
           await this.setState({FDC_detail:FDC_Data_remove});
      } else if(fieldname==='FAT_Detail'){
        var FAT_Data_remove =[ ...this.state.FAT_detail]
        FAT_Data_remove[index].image = {};
         await this.setState({FAT_detail:FAT_Data_remove});
      }  
     
    }

     _handleButtonPressLogin = async () =>{
      this.setState({ loginbutton: true, });
      this.SubmitForm();
    }


    async SubmitForm(){

      if(!this.state.form_id.trim()){
        this.setState({loginbutton:false, formErrorMsg:"Form Id is required.", Error:"Form Id is required."})
        return 0;
      }
      if(!this.state.State){
        this.setState({loginbutton:false, stateErrorMsg:"State is required.", Error:"State is required."})
        return 0;
      }

      if(!this.state.city){
        this.setState({loginbutton:false, cityErrorMsg:"City is required.", Error:"City is required."})
        return 0;
      }

      if(!this.state.jc){
        this.setState({loginbutton:false, jcErrorMsg:"JC is required.", Error:"JC is required."})
        return 0;
      }

      if(!this.state.fsa){
        this.setState({loginbutton:false, fsaErrorMsg:"FSA is required.", Error:"FSA is required."})
        return 0;
      }

      if(!this.state.typeofconnection){
        this.setState({loginbutton:false, typeconnectionErrorMsg:"Type of connection is required.", Error:"Type of connection is required."})
        return 0;
      }

      if(!this.state.buildingname.trim()){
        this.setState({loginbutton:false, buildingnameErrorMsg:"Building name is required.", Error:"Building name is required."})
        return 0;
      }

      if(!this.state.Buildingphoto){
        this.setState({loginbutton:false, BuildingphotoErrorMsg:"Building Photo is required.", Error:"Building photo is required."})
        return 0;
      }

      if(!this.state.buildingpermissionlatter){
        this.setState({loginbutton:false, buildingpermissionlatterErrorMsg:"Building permission latter is required.", Error:"Building permission latter is required."})
        return 0;
      }


      if(!this.state.No_of_shaft){
        this.setState({loginbutton:false, No_of_shaftErrorMsg:"No of shaft in building is required.", Error:"No of shaft in building is required."})
        return 0;
      }
      if(!this.state.Space_for_fdc){
        this.setState({loginbutton:false, Space_for_fdcErrorMsg:"Space for FDC is required.", Error:"Space for FDC is required."})
        return 0;
      }
      if(!this.state.total_no_of_FDC){
        this.setState({loginbutton:false, total_no_of_FDCErrorMsg:"Total no of FDC is required.", Error:"Total no of FDC is required."})
        return 0;
      }
      
      if(this.state.FDC_detail.length>0){
        for(i=0;i<this.state.FDC_detail.length ;i++){
          this.state.FDC_detail[i].floore_no_Error="";
          this.state.FDC_detail[i].image_Error="";
          if(this.state.FDC_detail[i].floore_no ==''){
            this.state.FDC_detail[i].floore_no_Error= "FDC floore no "+( i+1)+" is required";
            this.setState({loginbutton:false, Error:"FDC floore no "+ (i+1)+" is required"});
            return 0;
          }
          if(!this.state.FDC_detail[i].image.hasOwnProperty('uri')){
       this.state.FDC_detail[i].image_Error= "FDC detail "+( i+1)+" image  is required";
            this.setState({loginbutton:false, Error:"FDC detail "+ (i+1)+" image no  is required"});
            return 0;
          }
        }
      }
         
      
      
      if(!this.state.Space_for_fat){
        this.setState({loginbutton:false, Space_for_fatErrorMsg:"Space for FAT is required.", Error:"Space for FAT is required."})
        return 0;
      }
      if(!this.state.total_no_of_FAT){
        this.setState({loginbutton:false, total_no_of_FATErrorMsg:"Total no of FAT is required.", Error:"Total no of FAT is required."})
        return 0;
      }

      if(this.state.FAT_detail.length>0){
        for(i=0;i<this.state.FAT_detail.length ;i++){
          this.state.FAT_detail[i].floore_no_Error="";
          this.state.FAT_detail[i].image_Error="";
          if(this.state.FAT_detail[i].floore_no ==''){
            this.state.FAT_detail[i].floore_no_Error= "FAT floore no "+( i+1)+" is required";
            this.setState({loginbutton:false, Error:"FAT floore no "+ (i+1)+" is required"});
            return 0;
          }
          if(!this.state.FAT_detail[i].image.hasOwnProperty('uri')){
            this.state.FAT_detail[i].image_Error= "FAT detail "+( i+1)+" image  is required";
            this.setState({loginbutton:false, Error:"FAT detail "+ (i+1)+" image no  is required"});
            return 0;
          }
        }
      }
      
      if(!this.state.Vertical_conduct_Methodology){
        this.setState({loginbutton:false, Vertical_conduct_MethodologyErrorMsg:"Vertical conduct methodology is required.", Error:"Vertical conduct methodology is required."})
        return 0;
      }
      
      if(!this.state.Serving_building.trim()){
        this.setState({loginbutton:false, Serving_buildingErrorMsg:"Serving P1 building is required.", Error:"Serving P1 building is required."})
        return 0;
      }

      if(!this.state.Incoming_Office_From.trim()){
        this.setState({loginbutton:false, Incoming_Office_FromErrorMsg:"Incoming office From is required.", Error:"Incoming office From is required."})
        return 0;
      }

      if(!this.state.Building_Approach){
        this.setState({loginbutton:false, Building_ApproachErrorMsg:"Building approach is required.", Error:"Building approach is required."})
        return 0;
      }

      if(!this.state.Type_of_Ricer.trim()){
        this.setState({loginbutton:false, Type_of_RicerErrorMsg:"Type of ricer is required.", Error:"Type of ricer is required."})
        return 0;
      }

      if(!this.state.Route_length_of_Ricer.trim()){
        this.setState({loginbutton:false, Route_length_of_RicerErrorMsg:"Route length of ricer is required.", Error:"Route length of ricer is required."})
        return 0;
      }



     Images.append('data', JSON.stringify(this.state)) ;
      this.setState({loginbutton:true,isLoading: true })
      let response = await fetch(this.config.SurveyFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body:Images,
      })
      try {
        responseJson = await response.json();
        console.log(responseJson);
        this.setState({ loginbutton: false, isLoading: false });
        if(responseJson.hasOwnProperty('error')){
          this.setState({ Error: responseJson.error});
        }else{
          Toast.show({
            text: "data submitted successfully!",
           //  buttonText: "Okay",
            position: "bottom",
            type: 'Survey form data submitted successfully!',
            textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
          });
          this.setState({form_id:'',State:"",city:"",selectedCity:[],jc:"",fsa:"",typeofconnection:"",
          buildingname:"",Buildingphoto:null,buildingpermissionlatter:null,No_of_shaft:"",
          total_no_of_FDC:"",
          Space_for_fdc:"",
          FDC_detail:[],
          total_no_of_FAT:"",
          Space_for_fat:"",
          FAT_detail:[],
          Vertical_conduct_Methodology:"",
          Serving_building:"",
          Incoming_Office_From:"",
          Building_Approach:"",
          Type_of_Ricer:"",
          Route_length_of_Ricer:"",
        });
        }
      }catch (error) {
        this.setState({ isLoading: false });
        console.log("Response error - ",error);
      }
    }

    cmpSetTextState = async( textValue, txtName, index ) => {
      this.setState({Error:""});
      const txtVal = textValue;
      if(txtName === 'form_id'){
        this.setState({ form_id: txtVal, formErrorMsg:"" });
      }else if(txtName === 'password'){
        this.setState({ password: txtVal });
      }
      else if(txtName === 'State'){
        this.setState({ State: txtVal , city:'', stateErrorMsg:""});
        let StateArray =  States['states'].filter(function(obj) {
          return obj.value === txtVal;
        });
         let Selected =  Cities['cities'].filter(function(obj) {
           return obj.state_id === StateArray[0].id;
        });
        this.setState({selectedCity:Selected})
      }
      else if(txtName === 'City'){
        this.setState({ city: txtVal , cityErrorMsg:""});
      }
      else if(txtName === 'jc'){
        this.setState({ jc: txtVal , jcErrorMsg:""});
      }
      else if(txtName === 'fsa'){
        this.setState({ fsa: txtVal , fsaErrorMsg:""});
      }
      else if(txtName === 'TypeofConnection'){
        this.setState({ typeofconnection: txtVal , typeconnectionErrorMsg:""});
      }
      else if(txtName === 'buildingname'){
        this.setState({ buildingname: txtVal , buildingnameErrorMsg:""});
      }
      else if(txtName === 'Building Permission Latter'){
        this.setState({ buildingpermissionlatter: txtVal , buildingpermissionlatterErrorMsg:""});
      }
      else if(txtName === 'No Of Shaft'){
        this.setState({ No_of_shaft: txtVal, No_of_shaftErrorMsg:""});
      }
      else if(txtName === 'Space for FDC'){
        this.setState({ Space_for_fdc: txtVal, Space_for_fdcErrorMsg:""});
      }
      else if(txtName === 'Total no of FDC'){
        this.setState({FDC_detail:[]});
         for(let i = 0; i < parseInt(txtVal); i++) {
         await this.state.FDC_detail.push({
            'floore_no': "",
            'floore_no_Error': "",
            'image_Error':"",
            'image':{}
          });
        }
        this.setState({ total_no_of_FDC: txtVal, total_no_of_FDCErrorMsg:""});
      }else if(txtName==='floor_number'){
        this.state.FDC_detail[index].floore_no=txtVal;
      }
      else if(txtName === 'Space for FAT'){
        this.setState({ Space_for_fat: txtVal, Space_for_fatErrorMsg:""});
      }
      else if(txtName === 'Total no of FAT'){
        this.setState({FAT_detail:[]});
         for(let i = 0; i < parseInt(txtVal); i++) {
         await this.state.FAT_detail.push({
            'floore_no': "",
            'floore_no_Error': "",
            'image_Error':"",
            'image':{}
          });
        }
        this.setState({ total_no_of_FAT: txtVal, total_no_of_FATErrorMsg:""});
      }else if(txtName==='floor_number_fat'){
        this.state.FAT_detail[index].floore_no=txtVal;
      }
      else if(txtName === 'Vertical conduct Methodology'){
        this.setState({ Vertical_conduct_Methodology: txtVal, Vertical_conduct_MethodologyErrorMsg:""});
      }
      else if(txtName === 'Serving building'){
        this.setState({Serving_building: txtVal, Serving_buildingErrorMsg:""});
      }
      else if(txtName === 'Incoming Office From'){
        this.setState({ Incoming_Office_From: txtVal, Incoming_Office_FromErrorMsg:""});
      }
      else if(txtName === 'Building Approach'){
        this.setState({ Building_Approach: txtVal, Building_ApproachErrorMsg:""});
      }
      else if(txtName === 'Type of Ricer'){
        this.setState({ Type_of_Ricer: txtVal, Type_of_RicerErrorMsg:""});
      }
      else if(txtName === 'Route length of Ricer'){
        this.setState({ Route_length_of_Ricer: txtVal, Route_length_of_RicerErrorMsg:""});
      }
      else if(txtName === 'Survey Done'){
        this.setState({ Survey_Done: txtVal, Survey_DoneErrorMsg:""});
      }
    }



    render() {
      const { Buildingphoto, buildingpermissionlatter } = this.state
      let typeOfConnection = [
        {
          value:"First"
        },
        {
          value:"Second"
        }
      ];
      let No_of_shaft =[ ];
      for (let i = 1; i <= 10; i++) {
        No_of_shaft.push({
          value: i.toString(),
        });
      }
      let total_no_of_FDC =[ ];
      for (let i = 1; i <= 10; i++) {
        total_no_of_FDC.push({
          value: i.toString(),
        });
      }
      let total_no_of_FAT =[ ];
      for (let i = 1; i <= 10; i++) {
        total_no_of_FAT.push({
          value: i.toString(),
        });
      }
      
      let JC =[
        {
          value:"JC-1"
        },
        {
          value:"JC-2"
        },
        {
          value:"JC-3"
        },
      ];
      let FSA =[
        {
          value:"FSA-1"
        },
        {
          value:"FSA-2"
        },
        {
          value:"FSA-3"
        },
      ];
      let Space_for_fdc =[
        {
          value:"shaft"
        },
        {
          value:"outside wall"
        },
        {
          value:"pole"
        },
        {
          value:"staircase"
        },
        {
          value:"terrace"
        },
      ]
      let Space_for_fat =[
        {
          value:"shaft"
        },
        {
          value:"outside wall"
        },
        {
          value:"pole"
        },
        {
          value:"staircase"
        },
        {
          value:"terrace"
        },
      ]
      let Vertical_conduct_Methodology =[
        {
          value:"Scaffolding"
        },
        {
          value:"shaft"
        },
        {
          value:"core cutting"
        },
        {
          value:"staircase conduiting"
        }
      ]

      let Building_Approach =[
        {
          value:"Aerial"
        },
        {
          value:"existing route(tray/hume/pipe etc)"
        },
        {
          value:"(hand trenching/micro slitting chipping)"
        },
        {
          value:"micro trenching"
        },
        {
          value:"shallow trenching"
        },
        {
          value:"wall clamping"
        }
      ]

      let Type_of_Ricer = [
        {
          value:"12 F Riser"
        },
        {
          value:"06 F Riser"
        }
      ]

        return (
          <Root>
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic">  
                  <View style={styles.sectionContainer}>

                      <View style={{ marginBottom: 25 }}>
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Form id'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.form_id}
                              onChangeText={(value) => this.cmpSetTextState(value, "form_id")}
                              error	= {this.state.formErrorMsg}
                            />
                        </View>                                
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "State"
                            data={States['states']}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "State")}
                            value = {this.state.State}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.stateErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "City"
                            data={this.state.selectedCity}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "City")}
                            value = {this.state.city}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.cityErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                        <Dropdown
                            label= "JC"
                            data={JC}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "jc")}
                            value = {this.state.jc}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.jcErrorMsg}
                          />       
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                        <Dropdown
                            label= "FSA"
                            data={FSA}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "fsa")}
                            value = {this.state.fsa}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.fsaErrorMsg}
                          />  
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Type of connection"
                            data={typeOfConnection}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "TypeofConnection")}
                            value = {this.state.typeofconnection}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.typeconnectionErrorMsg}
                          />                        
                        </View>   
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Building name'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              maxLength={70}
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.buildingname}
                              onChangeText={(value) => this.cmpSetTextState(value, "buildingname")}
                              error	= {this.state.buildingnameErrorMsg}
                            />
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                        {(Buildingphoto )? (
                          <View>
                            <Image
                              source={{ uri: Buildingphoto.uri }}
                              style={{ flex:1, width:'100%', height:'100%', minHeight:320,minWidth:320}}
                            />
                            <View style = {{flex:1, flexDirection:'row', marginTop:10}}>
                            
                            <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/edit-7-24.png')} />
                                  </View>
                                  <TouchableOpacity style={[styles.buttonText, {flex:0.7}]} onPress={()=>this.handleChoosePhoto( "buildingphoto")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Change Picture</Text>
                                  </TouchableOpacity>
                                </View>
                                <View style = {{flex:0.1}}></View>
                                <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/trash-6-24.png')} />
                                  </View>
                                  <TouchableOpacity style={styles.buttonText} onPress={()=>this.handleRemovePhoto( "buildingphoto")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Remove Picture</Text>
                                  </TouchableOpacity>
                                </View>
                            </View>
                          </View>
                        ):
                        (
                          <TouchableOpacity style={[{borderWidth:2, borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a', padding:10}]} onPress={()=>this.handleChoosePhoto( "buildingphoto")}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Building Picture</Text>
                            </TouchableOpacity>
                        )}
                      </View>
                      <View style ={ styles.paddingBottomMore }>
                          {(buildingpermissionlatter )? (
                          <View>
                            <Image
                              source={{ uri: buildingpermissionlatter.uri }}
                              style={{ flex:1, width:'100%', height:'100%', minHeight:320,minWidth:320}}
                            />
                            <View style = {{flex:1, flexDirection:'row', marginTop:10}}>

                            <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/edit-7-24.png')} />
                                  </View>
                                  <TouchableOpacity style={[styles.buttonText, {flex:0.7}]} onPress={()=>this.handleChoosePhoto( "buildingpermission")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Change Picture</Text>
                                  </TouchableOpacity>
                                </View>
                                <View style = {{flex:0.1}}></View>
                                <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/trash-6-24.png')} />
                                  </View>
                                  <TouchableOpacity style={styles.buttonText} onPress={()=>this.handleRemovePhoto( "buildingpermission")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Remove Picture</Text>
                                  </TouchableOpacity>
                                </View>
                            </View>
                          </View>
                        ):
                        (
                          <TouchableOpacity style={[{borderWidth:2, borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a', padding:10}]} onPress={()=>this.handleChoosePhoto( "buildingpermission")}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Building Permission Letter Picture</Text>
                            </TouchableOpacity>
                        )}
                      </View>                               
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "No of shaft in building"
                            data={No_of_shaft}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "No Of Shaft")}
                            value = {this.state.No_of_shaft}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.No_of_shaftErrorMsg}
                          />                        
                        </View>

                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Space for FDC"
                            data={Space_for_fdc}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Space for FDC")}
                            value = {this.state.Space_for_fdc}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.Space_for_fdcErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Total no of FDC"
                            data={total_no_of_FDC}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Total no of FDC")}
                            value = {this.state.total_no_of_FDC}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.total_no_of_FDCErrorMsg}
                          />                        
                        </View>   
                        {
                          (this.state.FDC_detail.length>0)?
                          (
                            
                            this.state.FDC_detail.map((item, key) =>{
                              return(
                                <View key ={key}>
                                <View style = {{marginLeft: 10,marginRight:10}}><Text style = {{fontWeight:'900'}}>FDC Details {key+1}:</Text></View>
                                <View style ={ styles.paddingBottomMore} >
                                  <TextField
                                    label={'Floor no ' }
                                    style={{ color: "#000000" }}
                                    tintColor = "#000000"
                                    baseColor = "#000000"
                                    keyboardType='number-pad'
                                    maxLength={4}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    returnKeyType='next'
                                    value={this.state.FDC_detail[key].floore_no}
                                    onChangeText={(value) => this.cmpSetTextState(value, "floor_number", key)}
                                    error	= {this.state.FDC_detail[key].floore_no_Error}
                                  />
                                </View>
                                <View style ={ styles.paddingBottomMore }>


                                    {(this.state.FDC_detail[key].image.hasOwnProperty('uri') )? (
                                      <View>
                                        <Image
                                          source={{ uri: this.state.FDC_detail[key].image.uri }}
                                          style={{ flex:1, width:'100%', height:'100%', minHeight:320,minWidth:320}}
                                        />
                                        <View style = {{flex:1, flexDirection:'row', marginTop:10}}>

                                        <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                          <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                            <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/edit-7-24.png')} />
                                          </View>
                                          <TouchableOpacity style={[styles.buttonText, {flex:0.7}]}  onPress={()=>this.handleChoosePhoto( "FDC_Detail", key)}>
                                          <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Change Picture</Text>
                                          </TouchableOpacity>
                                        </View>
                                        <View style = {{flex:0.1}}></View>
                                        <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                          <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                            <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/trash-6-24.png')} />
                                          </View>
                                          <TouchableOpacity style={styles.buttonText} onPress={()=>this.handleRemovePhoto("FDC_Detail", key)}>
                                          <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Remove Picture</Text>
                                          </TouchableOpacity>
                                        </View>
                                        </View>
                                      </View>
                                    ):
                                    (
                                      <TouchableOpacity style={[{borderWidth:2, borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a', padding:10}]}  onPress={()=>this.handleChoosePhoto( "FDC_Detail", key)}>
                                        <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>FDC floor Picture</Text>
                                        </TouchableOpacity>
                                    )}

                                  </View>
                                </View>
                              )
                            })
                            
                          ):
                          (
                            <View></View>
                          )
                        }
                        
                          
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Space for FAT"
                            data={Space_for_fat}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Space for FAT")}
                            value = {this.state.Space_for_fat}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.Space_for_fatErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                        <Dropdown
                            label= "Total no of FAT"
                            data={total_no_of_FAT}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Total no of FAT")}
                            value = {this.state.total_no_of_FAT}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.total_no_of_FATErrorMsg}
                          />   
                        </View>  
                        {
                          (this.state.FAT_detail.length>0)?
                          (
                            
                            this.state.FAT_detail.map((item, key) =>{
                              return(
                                <View key ={key}>
                                <View style = {{marginLeft: 10,marginRight:10, }}><Text style = {{fontWeight:'900'}}>FAT Details {key+1}:</Text></View>
                                <View style ={ styles.paddingBottomMore} >
                                  <TextField
                                    label={'Floor no ' }
                                    style={{ color: "#000000" }}
                                    tintColor = "#000000"
                                    baseColor = "#000000"
                                    keyboardType='number-pad'
                                    maxLength={4}
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    enablesReturnKeyAutomatically={true}
                                    returnKeyType='next'
                                    value={this.state.FAT_detail[key].floore_no}
                                    onChangeText={(value) => this.cmpSetTextState(value, "floor_number_fat", key)}
                                    error	= {this.state.FAT_detail[key].floore_no_Error}
                                  />
                                </View>
                                <View style ={ styles.paddingBottomMore }>
                                    {(this.state.FAT_detail[key].image.hasOwnProperty('uri') )? (
                                      <View>
                                        <Image
                                          source={{ uri: this.state.FAT_detail[key].image.uri }}
                                          style={{ flex:1, width:'100%', height:'100%', minHeight:320,minWidth:320}}
                                        />
                                        <View style = {{flex:1, flexDirection:'row', marginTop:10}}>
                                        <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                              <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                                <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/edit-7-24.png')} />
                                              </View>
                                              <TouchableOpacity style={[styles.buttonText, {flex:0.7}]} onPress={()=>this.handleChoosePhoto( "FAT_Detail", key)}>
                                              <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Change Picture</Text>
                                              </TouchableOpacity>
                                            </View>
                                            <View style = {{flex:0.1}}></View>
                                            <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                              <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                                <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/trash-6-24.png')} />
                                              </View>
                                              <TouchableOpacity style={styles.buttonText} onPress={()=>this.handleRemovePhoto("FAT_Detail", key)}>
                                              <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Remove Picture</Text>
                                              </TouchableOpacity>
                                            </View>

                                        </View>
                                      </View>
                                    ):
                                    (
                                      <TouchableOpacity style={[{borderWidth:2, borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a', padding:10}]} onPress={()=>this.handleChoosePhoto( "FAT_Detail", key)}>
                                        <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>FAT floor Picture</Text>
                                        </TouchableOpacity>
                                    )}
                                  </View>
                                </View>
                              )
                            })
                            
                          ):
                          (
                            <View></View>
                          )
                        } 
                        
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Vertical conduit Methodology"
                            data={Vertical_conduct_Methodology}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Vertical conduct Methodology")}
                            value = {this.state.Vertical_conduct_Methodology}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.Vertical_conduct_MethodologyErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Serving P1 Building'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              maxLength={60}
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.Serving_building}
                              onChangeText={(value) => this.cmpSetTextState(value, "Serving building")}
                              error	= {this.state.Serving_buildingErrorMsg}
                            />
                        </View>   
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Incoming OFC From'
                              style={{ color: "#000000" }}
                              maxLength={50}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.Incoming_Office_From}
                              onChangeText={(value) => this.cmpSetTextState(value, "Incoming Office From")}
                              error	= {this.state.Incoming_Office_FromErrorMsg}
                            />
                        </View>   
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Building approach to FDC or FAT"
                            data={Building_Approach}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Building Approach")}
                            value = {this.state.Building_Approach}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.Building_ApproachErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Type of Riser OFC"
                            data={Type_of_Ricer}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Type of Ricer")}
                            value = {this.state.Type_of_Ricer}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            error	= {this.state.Type_of_RicerErrorMsg}
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                            <TextField
                              label='Route length of Riser OFC'
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              maxLength={3}
                              autoCapitalize='none'
                              autoCorrect={false}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              value={this.state.Route_length_of_Ricer}
                              onChangeText={(value) => this.cmpSetTextState(value, "Route length of Ricer")}
                              error	= {this.state.Route_length_of_RicerErrorMsg}
                            />
                        </View>   
                        
                      </View>
                      <KeyboardSpacer topSpacing ={10} />

                      {
                        (this.state.Error!='')?
                        (
                          <View style ={ styles.paddingBottomMore }>
                            <Text style={{ flex: 1, fontSize: 14,color:'red' }}>{this.state.Error}</Text>
                          </View>
                        ):
                        (
                          <View></View>
                        )
                      }
                      <View style ={ styles.paddingBottomMore }>
                      {
                        (this.state.loginbutton)?
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Loading...</Text>
                            </TouchableOpacity>
                        )
                        :
                        (

                            <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}  onPress={this._handleButtonPressLogin}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Submit</Text>
                            </TouchableOpacity>
                        )
                      }
                      </View>
                  </View>
              </ScrollView>
              
            )
            }
          </Container>
          </Root>
        )
    }
  
  
  
    static navigationOptions = ({navigation}) => {
      return {
      title: 'Survey Form',
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        alignSelf: 'center',
        textAlign: "center", 
        flex: 0.8,
        fontSize: 22,
        lineHeight: 25,
      },
      headerStyle: {
        height:84,
        borderBottomWidth: 0,
      },
      headerLeft:(
        <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
          <MaterialIcons
            name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
        </TouchableOpacity>
      ),
      headerBackground: (
        <View>
          <LinearGradient
            colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
          >
            <View style={{ width: '60%', marginLeft: "20%" }}>
              <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
            </View>
          </LinearGradient>
        </View>
      ),
  }
    }
}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    // padding: 10,
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    //height: (screenHeight-23),
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  paddingBottomMore: {
    paddingBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#fff',
  },
  deletebuttonText: {
    alignItems: "center",
    color: '#fff',
  },
  labelForgetColor1:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },
  submitbtn:{
    borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
  }

});
  
export default SurveyForm;
