import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container, Root, Toast} from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import {BASE_URL} from '../../../basepath';
import { TouchableOpacity } from "react-native-gesture-handler";

let Images = new FormData();
const screenHeight = Math.round(Dimensions.get('window').height);

class ImageUpload extends Component {

    constructor(props) {
      super(props);
      this.config = {
        SurveyFormAPI: BASE_URL+'apis/Fttx/Survey_Data_Insertion',
      }
      this.state = {
          Error:"",
          isLoading: false,
          loginbutton:false,
          Buildingphoto: null,
          BuildingphotoErrorMsg:"",
          user_id:null,
          data: null
          
     }

    }




     componentDidMount =async()=>{
      const userid = await AsyncStorage.getItem('user_id');
      this.setState({user_id:userid});
     }

     s4 = () => {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1); // substring return all chars except char at index=0;
    };
    
    uniqueId = () => {
      // create uniqueId for image as Alphabetical
      return (
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4() +
        "-" +
        this.s4()
      );
    };

     handleChoosePhoto = async(fieldname, index) => {
      const { status } = await Permissions.askAsync(Permissions.CAMERA);
      let response = [];
      if(status === 'granted') {
        response = await ImagePicker.launchCameraAsync({ // this is capture image only
          mediaTypes: "Images",
          allowsEditing: true,
          quality: 1,
          isCamera: true,
          }).catch(error => alert("Status 1" +error));
       }
      if (!response.cancelled) {
        let localUri = response.uri;
        let filename = localUri.split('/').pop();
  
        // Infer the type of the image
        let match = /\.(\w+)$/.exec(filename);
        let type = match ? `image/${match[1]}` : `image`;
   
        if(fieldname==='buildingphoto'){
          // Assume "photo" is the name of the form field the server expects
          Images.append('file', { uri: localUri, name: filename, type:type  });
          this.setState({Buildingphoto: response });
          const contents = "My text file contents";
        
        }
      }
    }

    handleRemovePhoto = async(fieldname, index) => {
      if(fieldname==='buildingphoto'){
        this.setState({Buildingphoto: null })
      }
    }

     _handleButtonPressLogin = async () =>{
      this.setState({ loginbutton: true, });
      this.SubmitForm();
    }


    async SubmitForm(){

      if(!this.state.Buildingphoto){
        this.setState({loginbutton:false, BuildingphotoErrorMsg:"Building Photo is required.", Error:"Building photo is required."})
        return 0;
      }

     Images.append('data', JSON.stringify(this.state)) ;
      this.setState({loginbutton:true,isLoading: true })
      let response = await fetch(this.config.SurveyFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'multipart/form-data',
        },
        body:Images,
      })
      try {
        responseJson = await response.json();
        console.log(responseJson);
        this.setState({ loginbutton: false, isLoading: false });
        if(responseJson.hasOwnProperty('error')){
          this.setState({ Error: responseJson.error});
        }else{
          Toast.show({
            text: "data submitted successfully!",
           //  buttonText: "Okay",
            position: "bottom",
            type: 'Survey form data submitted successfully!',
            textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
          });
          this.setState({Buildingphoto:null });
        }
      }catch (error) {
        this.setState({ isLoading: false });
        console.log("Response error - ",error);
      }
    }

    render() {
      const { Buildingphoto } = this.state
        return (
          <Root>
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic">  
                  <View style={styles.sectionContainer}>

                      <View style={{ marginBottom: 25 }}>
                          





                        <View style ={ styles.paddingBottomMore }>
                        {(Buildingphoto )? (
                          <View>
                            <Image
                              source={{ uri: Buildingphoto.uri }}
                              style={{ flex:1, width:'100%', height:'100%', minHeight:320,minWidth:320}}
                            />
                            <View style = {{flex:1, flexDirection:'row', marginTop:10}}>
                            
                            <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/edit-7-24.png')} />
                                  </View>
                                  <TouchableOpacity style={[styles.buttonText, {flex:0.7}]} onPress={()=>this.handleChoosePhoto( "buildingphoto")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Change Picture</Text>
                                  </TouchableOpacity>
                                </View>
                                <View style = {{flex:0.1}}></View>
                                <View style = {{flex:0.45,borderWidth:2, borderColor:'#ffffff', flexDirection:'row',borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a'}} >
                                  <View style={{flex:0.3, alignItems:'center', marginTop:10, marginLeft:10}}>
                                    <Image style={{marginLeft:5, maxWidth:18, maxHeight:18, width:18, height:18 }} source = {require('../../../assets/images/trash-6-24.png')} />
                                  </View>
                                  <TouchableOpacity style={styles.buttonText} onPress={()=>this.handleRemovePhoto( "buildingphoto")}>
                                  <Text style={{ flex: 1, textAlign: "center", fontSize: 14, color:'#fff'}}>Remove Picture</Text>
                                  </TouchableOpacity>
                                </View>
                            </View>
                          </View>
                        ):
                        (
                          <TouchableOpacity style={[{borderWidth:2, borderColor:'#ffffff', borderRadius:3, backgroundColor
                          :'#13c16a', padding:10}]} onPress={()=>this.handleChoosePhoto( "buildingphoto")}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Building Picture</Text>
                            </TouchableOpacity>
                        )}
                      </View>
                      </View>
                      <KeyboardSpacer topSpacing ={10} />

                      {
                        (this.state.Error!='')?
                        (
                          <View style ={ styles.paddingBottomMore }>
                            <Text style={{ flex: 1, fontSize: 14,color:'red' }}>{this.state.Error}</Text>
                          </View>
                        ):
                        (
                          <View></View>
                        )
                      }
                      <View style ={ styles.paddingBottomMore }>
                      {
                        (this.state.loginbutton)?
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Loading...</Text>
                            </TouchableOpacity>
                        )
                        :
                        (

                            <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}  onPress={this._handleButtonPressLogin}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Submit</Text>
                            </TouchableOpacity>
                        )
                      }
                      </View>
                  </View>
              </ScrollView>
              
            )
            }
          </Container>
          </Root>
        )
    }
  
  
    static navigationOptions = {
      title: 'Survey Form',
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        alignSelf: 'center',
        textAlign: "center", 
        flex: 0.8,
        fontSize: 22,
        lineHeight: 25,
      },
      headerStyle: {
        height:84,
        borderBottomWidth: 0,
      },
      headerBackground: (
        <View>
          <LinearGradient
            colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
          >
            <View style={{ width: '60%', marginLeft: "20%" }}>
              <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
            </View>
          </LinearGradient>
        </View>
      ),
  }

}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    // padding: 10,
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    //height: (screenHeight-23),
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  paddingBottomMore: {
    paddingBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#fff',
  },
  deletebuttonText: {
    alignItems: "center",
    color: '#fff',
  },
  labelForgetColor1:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },
  submitbtn:{
    borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
  }

});
  
export default ImageUpload;
