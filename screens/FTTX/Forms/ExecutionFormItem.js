import React, { Component } from "react";
import {Image, StyleSheet, ScrollView, View, Text,TouchableOpacity, AsyncStorage, Dimensions, ActivityIndicator, Platform} from 'react-native';
//import ImagePicker from 'react-native-image-picker'
import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import Autocomplete from 'react-native-autocomplete-input';
import { Container, Button, Toast, Root } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {BASE_URL} from '../../../basepath';
import { MaterialIcons } from '@expo/vector-icons';
const screenHeight = Math.round(Dimensions.get('window').height);

class ExecutionFormItem extends Component {
    constructor(props) {
        super(props);
        this.params = this.props.navigation.state.params;
        this.config = {
          BuildingNameApi: BASE_URL+'apis/Fttx/building_name_FSA/',
          filterAPI: BASE_URL+'apis/Fttx/item_Search/',
          ExecutionFormAPI: BASE_URL+'apis/Fttx/Execution_Data_Insertion',
        }
        this.state = {
          confirmModalVisible: false,
          successModalVisible: false,
            Error:"",
            loginbutton:false,
            isLoading: true,
            FSA_Data:[],
            FSA:"",
            FSAError:"",
            filterData:[],
            selectedItem:[],
            searchBox:true,
            user_id:null,
            
        }
    }

    fatchBuildingName_FSA = async(building_name) => {
        let responseJson = [];
        let response = await fetch(this.config.BuildingNameApi+building_name);
        try {
          responseJson = await response.json();
          
          //Successful response from the API Call
          if(responseJson.hasOwnProperty('error')){
            this.setState({ Error: responseJson.error, isLoading: false });
          }else{
            //Successful response from the API Call
              this.setState({ FSA_Data: responseJson.data, isLoading: false });
          }
        }catch (error) {
          console.log("Response error - ",error);
        }
      }

      _filterData = async(search) => {
        if(!this.state.FSA.trim()){
          this.setState({loginbutton:false, FSAError:"FSA is required.", Error:"FSA is required."})
          return 0;
        }
        var API='';
        let responseJson = [];
        if(search.trim()===''){
          API= this.config.filterAPI+this.state.FSA;
        }
        else{
          API= this.config.filterAPI+this.state.FSA+"/"+search.trim();
        }
        console.log(API)
        let response = await fetch(API);
        try {
          responseJson = await response.json();
          //Successful response from the API Call
          if(responseJson.hasOwnProperty('error')){
            this.setState({ Error: responseJson.error, isLoading: false });
          }else{
            //Successful response from the API Call
              this.setState({ filterData: responseJson.data, isLoading: false });
          }
        }catch (error) {
          console.log("Response error - ",error);
        }
      }

      SetFSA= async( textValue, value,) => {
        this.setState({Error:""});
          this.setState({ FSA: textValue, FSAError:"", filterData:[], selectedItem:[], searchBox:true });
    }

    ItemListed = async(items) =>{
      await this.state.selectedItem.push(items);
      this.setState({filterData:[],searchBox:false});
    }

    _handleButtonPressLogin = async (item) =>{

      if(!this.state.FSA.trim()){
        this.setState({loginbutton:false, FSAError:"FSA is required.", Error:"FSA is required."})
        return 0;
      }
        if(item==='yes'){
          
          if(this.state.selectedItem.length>0){
            for(i=0;i<this.state.selectedItem.length ;i++){
              this.state.selectedItem[i].QuantityErrorMsg="";
              if(this.state.selectedItem[i].Quantity ==''){
                this.state.selectedItem[i].QuantityErrorMsg= "Item "+( i+1)+" quantity is required";
                this.setState({loginbutton:false, Error:"Item "+( i+1)+" quantity is required"});
                return 0;
              }
            }
          }
            this.setState({searchBox:true});
        }
        else{
          this.setState({ loginbutton: true, });
            this.SubmitForm();
        }
        
      }

      async SubmitForm(value){
        this.setState({loginbutton:true,isLoading: true })
        if(this.state.selectedItem.length>0){
          let response = await fetch(this.config.ExecutionFormAPI, {
            method: 'POST',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              Execution_data: this.params.ExecutionFormInfo,
              Execution_item: this.state.selectedItem,
              Selected_FSA:this.state.FSA,
              User_id:this.state.user_id
            })
          })
             
              try {
                responseJson = await response.json();
                this.setState({ loginbutton: false, isLoading: false });
                console.log("Data:", responseJson);
                if(responseJson.hasOwnProperty('error')){
                  this.setState({ Error: responseJson.error});
                }else{
                  //Successful response from the API Call
                  Toast.show({
                    text: "Execution data submitted successfully!",
                   //  buttonText: "Okay",
                    position: "bottom",
                    type: "success",
                    textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
                  });
                  this.setState({selectedItem:'',FSA:"" })
                }
                 
              }catch (error) {
                this.setState({ isLoading: false });
                console.log("Response error - ",error);
              }
            
        }else{
          
          alert("Add atleast one item");
          this.setState({loginbutton:false, isLoading: false})
        }
          
        
    }

    AddQuantity = async(itemValue, ItemIndex) =>{
      this.state.selectedItem[ItemIndex].Quantity=itemValue;
      this.setState({Error:""});
    }


    componentDidMount =async()=>{
        this.fatchBuildingName_FSA(this.params.ExecutionFormInfo.Building_name);
          const userid = await AsyncStorage.getItem('user_id');
          this.setState({user_id:userid}); 
          this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
        }
      
        // USING METHOD TO OPEN DRAWER
        openControlPanel = () => {
          this.props.navigation.openDrawer();
        }

    render() {
      var ItemData=[];
      for(let i=0;i<this.state.selectedItem.length;i++){
        ItemData.push(
          <View key ={i}>
            <View style ={ styles.paddingBottomMore }>
              <View><Text style = {{fontWeight:'900', fontSize:14}}>Items No. {i+1}:</Text></View>
                  <View style ={ styles.paddingBottomMore }>
                      <TextField
                        label='Code'
                        style={{ color: "#000000" }}
                        tintColor = "#000000"
                        baseColor = "#000000"
                        keyboardType='number-pad'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        value={this.state.selectedItem[i].Code}
                        disabled={true}
                      />
                    </View>  
                    <View style ={ styles.paddingBottomMore }>
                      <TextField
                        label='Name'
                        style={{ color: "#000000" }}
                        tintColor = "#000000"
                        baseColor = "#000000"
                        keyboardType='default'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        value={this.state.selectedItem[i].Name}
                        disabled={true}
                      />
                    </View> 
                    <View style ={ styles.paddingBottomMore }>
                      <TextField
                        label='Description'
                        style={{ color: "#000000" }}
                        tintColor = "#000000"
                        baseColor = "#000000"
                        keyboardType='default'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        value={this.state.selectedItem[i].Description}
                        disabled={true}
                      />
                    </View> 
                    <View style ={ styles.paddingBottomMore }>
                      <TextField
                        label='Cost'
                        style={{ color: "#000000" }}
                        tintColor = "#000000"
                        baseColor = "#000000"
                        keyboardType='number-pad'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        value={this.state.selectedItem[i].Cost}
                        disabled={true}
                      />
                    </View> 
                    <View style ={ styles.paddingBottomMore }>
                      <TextField
                        label='Quantity'
                        style={{ color: "#000000" }}
                        tintColor = "#000000"
                        baseColor = "#000000"
                        keyboardType='number-pad'
                        autoCapitalize='none'
                        autoCorrect={false}
                        enablesReturnKeyAutomatically={true}
                        returnKeyType='next'
                        value={this.state.selectedItem[i].Quantity}
                        onChangeText={(value) => this.AddQuantity(value, i)}
                        error = {this.state.selectedItem[i].QuantityErrorMsg}
                      />
                    </View> 
            </View>
          </View>
        )
      }
          return (
            <Root>
              <Container style = {{height:screenHeight}}>
                {(this.state.isLoading)?(
                  <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                    <ActivityIndicator size="large" color="#2157c4" />
                  </View>
                ):(
                    <View style = {{height:"100%"}}>
                        <ScrollView contentInsetAdjustmentBehavior="automatic">  
                            <View style={{ marginBottom: 25, marginTop:25 }}>
                                <View style ={ styles.paddingBottomMore }>
                                    <Dropdown
                                    label= "Select FSA for items"
                                    data={this.state.FSA_Data}
                                    dropdownPosition={1}
                                    dropdownOffset = {{ top: 8, left: 0 }}
                                    onChangeText={(value, index) => this.SetFSA(value, index, "FSA")}
                                    valueExtractor={({value})=> value}
                                    textColor="#000000"
                                    style={{ color: "#000000" }}
                                    baseColor = "#000000"
                                    error	= {this.state.FSAError}
                                    />                        
                                </View>
                                <View>{ItemData}</View>
                                <View style ={ styles.paddingBottomMore }>
                                {
                                  (this.state.searchBox)?
                                  (
                                    <View>
                                      <View><Text>Add More Items:</Text></View>
                                      <Autocomplete
                                      keyExtractor={(item, index) => index.toString()}
                                      data={this.state.filterData}
                                      onChangeText={(text) => this._filterData(text)}
                                      renderItem={({ item, i }) => (
                                        <TouchableOpacity style={{paddingBottom:5,fontSize:16, botderWidth:0, paddingLeft:10, witdh:"100%"}} onPress={() => this.ItemListed(item)}>
                                            <Text style={{fontSize:15}}>{item.Name}</Text>
                                        </TouchableOpacity>
                                      )}
                                    />
                                   </View>
                                  ):
                                  (
                                    <View></View>
                                  )
                                }
                                  
                                </View>

                                
                            <KeyboardSpacer topSpacing ={10} />

                      {
                        (this.state.Error!='')?
                        (
                          <View style ={ styles.paddingBottomMore }>
                            <Text style={{ flex: 1, fontSize: 14,color:'red' }}>{this.state.Error}</Text>
                          </View>
                        ):
                        (
                          <View></View>
                        )
                      }

                      <View style ={[ styles.paddingBottomMore, {marginTop:30,} ]}>
                      {
                        (this.state.loginbutton)?
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Loading...</Text>
                            </TouchableOpacity>
                        )
                        :
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]} onPress={this._handleButtonPressLogin}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Submit</Text>
                            </TouchableOpacity>
                        )
                      }
                      </View>
                        </View>
                        </ScrollView>
                        <View style = { styles.cartButtonView } >
                            <Button style = {[ styles.cartButton] } onPress={()=>this._handleButtonPressLogin('yes')} >
                                <Image  style = {styles.imageStyle} source = {require('../../../assets/images/add.png')} />
                            </Button>
                        </View>
                    </View>
                    )
                }
                </Container>
                </Root>
          )
    }

    static navigationOptions = ({navigation}) => {
      return {
        title: 'Execution Form Items',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center", 
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
      }
    }
}
const styles = StyleSheet.create({
    ActivityIndicatorContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    ActivityIndicatorHorizontal: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      // padding: 10,
    },
    body: {
      flex: 1,
      backgroundColor: "#2157c4",
      justifyContent: "center",
      //height: (screenHeight-23),
      height: (screenHeight + 33),
      paddingHorizontal: 24,
    },
    logoSection: {
      marginBottom: 50
    },
    sectionTitle: {
      fontSize: 26,
      color: "#FFF",
      alignSelf: "center",
    },
    paddingBottomMore: {
      paddingBottom: 10,
      marginLeft: 10,
      marginRight: 10,
    },
    sectionDescription: {
      marginTop: 8,
      fontSize: 18,
      color: "#FFF",
      alignSelf: "center",
    },
    itemRow:{
      flexDirection:'row',
      flex:1
    },
    footer: {
      fontSize: 12,
      fontWeight: '600',
      padding: 4,
      paddingRight: 12,
      textAlign: 'right',
    },
    cartButtonView: {
        position: "absolute",
        bottom: 10,
        right: 10,
        borderRadius:33,
        width:66,
        height:66,
      },
      RemoveCartButtonView: {
        position: "absolute",
        bottom: 10,
        left: 10,
        borderRadius:33,
        width:66,
        height:66,
      },

      RemoveCartButton: {
        alignItems: 'center',
        width: 57,
        height: 57,
        borderRadius: 33,
        backgroundColor: '#2157c4',
        color: '#ffffff',
        alignSelf: 'flex-end',
        zIndex: 11,
        elevation: 10,
        ...Platform.select({
          ios:{
            shadowOpacity: 0.4,
            shadowRadius: 7,
            shadowOffset: {
                height: 0,
                width: 0
            }
          }
        }),
      },
     
      cartButton:{
        alignItems: 'center',
        width: 57,
        height: 57,
        borderRadius: 33,
        backgroundColor: '#1ab679',
        color: '#ffffff',
        alignSelf: 'flex-end',
        zIndex: 11,
        elevation: 10,
        ...Platform.select({
          ios:{
            shadowOpacity: 0.4,
            shadowRadius: 7,
            shadowOffset: {
                height: 0,
                width: 0
            }
          }
        }),
      },
    
      imageStyle:{
          position: "relative",
          left: 20,
          top: 1,
          alignSelf:'center', 
          width:20,
          height:20
      },
    buttonText: {
      // fontFamily: 'Gill Sans',
      alignItems: "center",
      margin: 10,
      color: '#2157c4',
    },
    labelForgetColor1:{
      color:'#eeeeee',
      flex:1,
      textAlign:'right',
      position:'absolute',
      right:5,
      bottom:28,
      fontSize:12,
      lineHeight:16,
    },
    labelForgetColor:{
      color:'#eeeeee',
      flex:1,
      textAlign:'right',
      position:'absolute',
      right:5,
      bottom:12,
      fontSize:12,
      lineHeight:16,
    },
    submitbtn:{
      borderRadius: 50,
        flex: 1,
        backgroundColor: "#25AE88",
        paddingVertical: 15,
        marginTop: 15
    }
  });
export default ExecutionFormItem;