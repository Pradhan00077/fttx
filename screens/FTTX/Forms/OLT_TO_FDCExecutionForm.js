import React, { Component } from "react";
import { Image, StyleSheet, ScrollView, View, Text, AsyncStorage, Dimensions, ActivityIndicator} from 'react-native';

import { LinearGradient } from 'expo-linear-gradient';
import { TextField } from 'react-native-material-textfield';
import { Container, Button, Root, Toast} from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { MaterialIcons } from '@expo/vector-icons';
import {BASE_URL} from '../../../basepath';
import { TouchableOpacity } from "react-native-gesture-handler";

const screenHeight = Math.round(Dimensions.get('window').height);



class OLT_TO_FDCExecutionForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        APIurl: BASE_URL+'apis/Fttx/Execution_OLT_TO_FDC_Insertion',
      }
      this.state = {
          Error:"",
          isLoading: false,
          loginbutton:false,
          user_id:null,
          FSA_Name:"",
          FSA_NameErrorMsg:"",
          OLT_Name:"",
          OLT_NameErrorMsg:"",
          OLT_SAP_ID:"",
          OLT_SAP_IDErrorMsg:"",
          FDC_Name:"",
          FDC_NameErrorMsg:"",
          FDC_SAP_ID:"",
          FDC_SAP_IDErrorMsg:"",
          SPAN_Name:"",
          SPAN_NameErrorMsg:"",
          SPAN_Length:"",
          SPAN_LengthErrorMsg:"",
          Depth:"",
          Constr_Method:"",
          Constr_MethodErrorMsg:"",
          Cable_Type:"",
          Length_Cable:"",
          Length_CableErrorMsg:"",

          Duct_Type:"",
          Duct_TypeErrorMsg:"",
          Duct_Length:"",
          Duct_LengthErrorMsg:"",
          Fiber_Cable_Type:"",
          Fiber_Cable_TypeErrorMsg:"",
          Cable_Length:"",
          Cable_LengthErrorMsg:"",
          No_Of_S1_FDC:"",
          No_Of_S1_FDCErrorMsg:"",
        };
     }

     componentDidMount =async()=>{
      const userid = await AsyncStorage.getItem('user_id');
      this.setState({user_id:userid}); 
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

     _handleButtonPressLogin = async () =>{
      this.setState({ loginbutton: true, });
      this.SubmitForm();
    }


    async SubmitForm(){

      if(!this.state.FSA_Name.trim()){
        this.setState({loginbutton:false, FSA_NameErrorMsg:"FSA Name is required.", Error:"FSA Name is required."})
        return 0;
      }
      if(!this.state.OLT_Name.trim()){
        this.setState({loginbutton:false, OLT_NameErrorMsg:"OLT Name is required.", Error:"OLT Name is required."})
        return 0;
      }
      if(!this.state.OLT_SAP_ID.trim()){
        this.setState({loginbutton:false, OLT_SAP_IDErrorMsg:"OLT SAP ID is required.", Error:"OLT SAP ID is required."})
        return 0;
      }
      if(!this.state.FDC_Name.trim()){
        this.setState({loginbutton:false, FDC_NameErrorMsg:"FDC or Chamber Name is required.", Error:"FDC or Chamber Name is required."})
        return 0;
      }
      if(!this.state.FDC_SAP_ID.trim()){
        this.setState({loginbutton:false, FDC_SAP_IDErrorMsg:"FDC or Chamber SAP ID is required.", Error:"FDC or Chamber SAP ID is required."})
        return 0;
      }
      if(!this.state.SPAN_Name.trim()){
        this.setState({loginbutton:false, SPAN_NameErrorMsg:"SPAN Name is required.", Error:"SPAN Name is required."})
        return 0;
      }
      if(!this.state.SPAN_Length.trim()){
        this.setState({loginbutton:false, SPAN_LengthErrorMsg:"SPAN length is required.", Error:"SPAN length is required."})
        return 0;
      }
      if(!this.state.Constr_Method.trim()){
        this.setState({loginbutton:false, Constr_MethodErrorMsg:"Constr. Method is required.", Error:"Constr. Method is required."})
        return 0;
      }
     if(!this.state.Length_Cable.trim()){
      this.setState({loginbutton:false, Length_CableErrorMsg:"Length of cable is required.", Error:"Length of cable is required."})
      return 0;
    }
      if(!this.state.Duct_Type.trim()){
        this.setState({loginbutton:false, Duct_TypeErrorMsg:"Duct Type is required.", Error:"Duct Type is required."})
        return 0;
      }
      if(!this.state.Duct_Length.trim()){
        this.setState({loginbutton:false, Duct_LengthErrorMsg:"Duct Length is required.", Error:"Duct Length is required."})
        return 0;
      }
      if(!this.state.Fiber_Cable_Type.trim()){
        this.setState({loginbutton:false, Fiber_Cable_TypeErrorMsg:"Fiber Cable Type is required.", Error:"Fiber Cable Type is required."})
        return 0;
      }
      if(!this.state.Cable_Length.trim()){
        this.setState({loginbutton:false, Cable_LengthErrorMsg:"Cable Length is required.", Error:"Cable Length is required."})
        return 0;
      }
      if(!this.state.No_Of_S1_FDC.trim()){
        this.setState({loginbutton:false, No_Of_S1_FDCErrorMsg:"No. of S1 in FDC or Chamber is required.", Error:"No. of S1 in FDC or Chamber is required."})
        return 0;
      }
      
      
      this.setState({loginbutton:true});

      let response = await fetch(this.config.APIurl, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: this.state,
        })
      })
         
          try {
            responseJson = await response.json();
            this.setState({ loginbutton: false, isLoading: false });
            if(responseJson.hasOwnProperty('error')){
              this.setState({ Error: responseJson.error});
            }else{
              //Successful response from the API Call
              Toast.show({
                text: responseJson.success,
               //  buttonText: "Okay",
                position: "bottom",
                type: "success",
                textStyle: { color: "#fff", fontSize: 14, textAlign: "center" },
              });
              this.setState({Error:responseJson.success,
              FSA_Name:"",
              OLT_Name:"",
              OLT_SAP_ID:"",
              FDC_Name:"",
              FDC_SAP_ID:"",
              SPAN_Name:"",
              SPAN_Length:"",
              Depth:"",
              Constr_Method:"",
              Cable_Type:"",
              Length_Cable:"",
              Duct_Type:"",
              Duct_Length:"",
              Fiber_Cable_Type:"",
              Cable_Length:"",
              No_Of_S1_FDC:"",
            })
            }
             
          }catch (error) {
            this.setState({ isLoading: false });
            console.log("Response error - ",error);
          }
    }

    cmpSetTextState = async( textValue, txtName, index ) => {
      this.setState({Error:"", loginbutton:false});
      const txtVal = textValue;
      if(txtName === 'FSA_Name'){
        this.setState({ FSA_Name: txtVal, FSA_NameErrorMsg:"" });
      }else if(txtName==='OLT_Name'){
        this.setState({OLT_Name: txtVal, OLT_NameErrorMsg:""})
      }else if(txtName==='OLT_SAP_ID'){
        this.setState({OLT_SAP_ID:txtVal, OLT_SAP_IDErrorMsg:""})
      }else if(txtName==='FDC_Name'){
        this.setState({FDC_Name: txtVal, FDC_NameErrorMsg:""})
      }else if(txtName==='FDC_SAP_ID'){
        this.setState({FDC_SAP_ID:txtVal, FDC_SAP_IDErrorMsg:""})
      }else if(txtName==='SPAN_Name'){
        this.setState({SPAN_Name:txtVal, SPAN_NameErrorMsg:""})
      }else if(txtName==='SPAN_Length'){
        this.setState({SPAN_Length:txtVal, SPAN_LengthErrorMsg:""})
      }else if(txtName==='Constr_Method'){
        this.setState({Constr_Method:txtVal, Constr_MethodErrorMsg:""})
      }else if(txtName==='Depth'){
        this.setState({Depth:txtVal,})
      }else if(txtName==='Cable_Type'){
        this.setState({Cable_Type:txtVal,})
      }else if(txtName==='Length_Cable'){
        this.setState({Length_Cable:txtVal, Length_CableErrorMsg:""})
      }else if(txtName==='Duct_Type'){
        this.setState({Duct_Type:txtVal, Duct_TypeErrorMsg:""})
      }else if(txtName==='Duct_Length'){
        this.setState({Duct_Length:txtVal, Duct_LengthErrorMsg:""})
      }else if(txtName==='Fiber_Cable_Type'){
        this.setState({Fiber_Cable_Type:txtVal, Fiber_Cable_TypeErrorMsg:""})
      }else if(txtName==='Cable_Length'){
        this.setState({Cable_Length:txtVal, Cable_LengthErrorMsg:""})
      }else if(txtName==='No_Of_S1_FDC'){
        this.setState({No_Of_S1_FDC:txtVal, No_Of_S1_FDCErrorMsg:""})
      }
    }
    


    render() {
      let Constr_Method =[
        {
          value:"Micro Trenching"
        },
        {
          value:"Open Trenching"
        },
        {
          value:"Shallow Trenching"
        },
        {
          value:"HDD"
        },
        {
          value:"Wall Clamping"
        },
        {
          value:"UG"
        },
        {
          value:"Moling"
        },
        {
          value:"Areial"
        },
        {
          value:"Bried"
        }
      ];
      let Cable_Type = [
        {
          value:"Armoured"
        },
        {
          value:"ADSS"
        }
      ]
      let Duct_Type = [
        {
          value:"Duct-1"
        },{
          value:"Duct-2"
        },{
          value:"Duct-3"
        },{
          value:"Duct-4"
        },{
          value:"Duct-5"
        },{
          value:"Duct-6"
        },
      ]
      let Fiber_Cable_Type = [
        {
          value:"Fiber Cable Type-1"
        },{
          value:"Fiber Cable Type-2"
        },{
          value:"Fiber Cable Type-3"
        },{
          value:"Fiber Cable Type-4"
        },{
          value:"Fiber Cable Type-5"
        },{
          value:"Fiber Cable Type-6"
        },
      ]
        return (
          <Root>
          <Container>
            {(this.state.isLoading)?(
              <View style={[styles.ActivityIndicatorContainer, styles.ActivityIndicatorHorizontal]}>
                <ActivityIndicator size="large" color="#2157c4"/>
              </View>
            ):(
              <ScrollView contentInsetAdjustmentBehavior="automatic">  
                  <View style={styles.sectionContainer}>

                      <View style={{ marginBottom: 25 }}>
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="FSA Name"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.FSA_Name}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "FSA_Name")}
                              error	= {this.state.FSA_NameErrorMsg}
                            />
                        </View>  
                        <View style = {{flex:1,  marginLeft: 10, marginRight: 10,marginBottom:0}}>
                          <Text style = {{fontWeight:'900', fontSize:14}}>OLT:</Text>
                        </View>
                        <View style ={[ styles.paddingBottomMore, {flex:1, flexDirection:'row'} ]}>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="Name"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='default'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.OLT_Name}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "OLT_Name")}
                                error	= {this.state.OLT_NameErrorMsg}
                              />
                          </View>
                          <View style={{flex:0.06}}></View>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="SAP ID"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='number-pad'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.OLT_SAP_ID}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "OLT_SAP_ID")}
                                error	= {this.state.OLT_SAP_IDErrorMsg}
                              />
                          </View>
                        </View>  
                        <View style = {{flex:1,  marginLeft: 10, marginRight: 10,marginBottom:0}}>
                          <Text style = {{fontWeight:'900', fontSize:14}}>FDC or Chamber:</Text>
                        </View>
                        <View style ={[ styles.paddingBottomMore, {flex:1, flexDirection:'row'} ]}>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="Name"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='default'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.FDC_Name}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "FDC_Name")}
                                error	= {this.state.FDC_NameErrorMsg}
                              />
                          </View>
                          <View style={{flex:0.06}}></View>
                          <View style = {{flex:0.47}}>
                            <TextField
                                label="SAP ID"
                                style={{ color: "#000000" }}
                                tintColor = "#000000"
                                baseColor = "#000000"
                                keyboardType='number-pad'
                                autoCapitalize='none'
                                autoCorrect={false}
                                value = {this.state.FDC_SAP_ID}
                                enablesReturnKeyAutomatically={true}
                                returnKeyType='next'
                                onChangeText={(value) => this.cmpSetTextState(value, "FDC_SAP_ID")}
                                error	= {this.state.FDC_SAP_IDErrorMsg}
                              />
                          </View>
                        </View>   
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="SPAN Name"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='default'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.SPAN_Name}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "SPAN_Name")}
                              error	= {this.state.SPAN_NameErrorMsg}
                            />
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="SPAN Length(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.SPAN_Length}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "SPAN_Length")}
                              error	= {this.state.SPAN_LengthErrorMsg}
                            />
                        </View>                                      
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Constr. Method"
                            data={Constr_Method}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Constr_Method")}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            value = {this.state.Constr_Method}
                            error	= {this.state.Constr_MethodErrorMsg}
                          />                        
                        </View>      
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Depth(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Depth}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Depth")}
                            />
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Cable Type"
                            data={Cable_Type}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Cable_Type")}
                            textColor="#000000"
                            value = {this.state.Cable_Type}
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                          />                        
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Length of Cable(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Length_Cable}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Length_Cable")}
                              error	= {this.state.Length_CableErrorMsg}
                            />
                        </View>
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Duct Type"
                            data={Duct_Type}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Duct_Type")}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            value = {this.state.Duct_Type}
                            baseColor = "#000000"
                            error	= {this.state.Duct_TypeErrorMsg}
                          />                        
                        </View> 
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Duct Length(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Duct_Length}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Duct_Length")}
                              error	= {this.state.Duct_LengthErrorMsg}
                            />
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <Dropdown
                            label= "Fiber Cable Type"
                            data={Fiber_Cable_Type}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.cmpSetTextState(value, "Fiber_Cable_Type")}
                            textColor="#000000"
                            style={{ color: "#000000" }}
                            baseColor = "#000000"
                            value = {this.state.Fiber_Cable_Type}
                            error	= {this.state.Fiber_Cable_TypeErrorMsg}
                          />                        
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="Cable Length(M)"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.Cable_Length}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "Cable_Length")}
                              error	= {this.state.Cable_LengthErrorMsg}
                            />
                        </View>  
                        <View style ={ styles.paddingBottomMore }>
                          <TextField
                              label="No. of S1 in FDC or Chamber"
                              style={{ color: "#000000" }}
                              tintColor = "#000000"
                              baseColor = "#000000"
                              keyboardType='number-pad'
                              autoCapitalize='none'
                              autoCorrect={false}
                              value = {this.state.No_Of_S1_FDC}
                              enablesReturnKeyAutomatically={true}
                              returnKeyType='next'
                              onChangeText={(value) => this.cmpSetTextState(value, "No_Of_S1_FDC")}
                              error	= {this.state.No_Of_S1_FDCErrorMsg}
                            />
                        </View>  
                      </View>
                      <KeyboardSpacer topSpacing ={10} />

                      {
                        (this.state.Error!='')?
                        (
                          <View style ={ styles.paddingBottomMore }>
                            <Text style={{ flex: 1, fontSize: 14,color:'red' }}>{this.state.Error}</Text>
                          </View>
                        ):
                        (
                          <View></View>
                        )
                      }
                      <View style ={ styles.paddingBottomMore }>
                      {
                        (this.state.loginbutton)?
                        (
                          <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Loading...</Text>
                            </TouchableOpacity>
                        )
                        :
                        (

                            <TouchableOpacity style={[{ padding:10}, styles.submitbtn]}  onPress={this._handleButtonPressLogin}>
                            <Text style={{ flex: 1, textAlign: "center", fontSize: 16, color:'#fff'}}>Submit</Text>
                            </TouchableOpacity>
                        )
                      }
                      </View>
                  </View>
              </ScrollView>
              
            )
            }
          </Container>
          </Root>
        )
    }
  
    static navigationOptions = ({navigation}) => {
      return {
      title: 'OLT TO FDC or Chamber',
      headerTintColor: '#FFFFFF',
      headerTitleStyle: {
        alignSelf: 'center',
        textAlign: "center", 
        flex: 0.8,
        fontSize: 22,
        lineHeight: 25,
      },
      headerStyle: {
        height:84,
        borderBottomWidth: 0,
      },
      headerLeft:(
        <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
          <MaterialIcons
            name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
        </TouchableOpacity>
      ),
      headerBackground: (
        <View>
          <LinearGradient
            colors={['#2157c4', '#3b5998', '#1ab679']}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
          >
            <View style={{ width: '60%', marginLeft: "20%" }}>
              <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../../assets/images/logo.png')} resizeMode= "contain" />
            </View>
          </LinearGradient>
        </View>
      ),
  }
}

}

const styles = StyleSheet.create({
  ActivityIndicatorContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  ActivityIndicatorHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    // padding: 10,
  },
  body: {
    flex: 1,
    backgroundColor: "#2157c4",
    justifyContent: "center",
    //height: (screenHeight-23),
    height: (screenHeight + 33),
    paddingHorizontal: 24,
  },
  logoSection: {
    marginBottom: 50
  },
  sectionTitle: {
    fontSize: 26,
    color: "#FFF",
    alignSelf: "center",
  },
  paddingBottomMore: {
    paddingBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    color: "#FFF",
    alignSelf: "center",
  },

  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
  
  buttonText: {
    alignItems: "center",
    margin: 10,
    color: '#fff',
  },
  deletebuttonText: {
    alignItems: "center",
    color: '#fff',
  },
  labelForgetColor1:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:28,
    fontSize:12,
    lineHeight:16,
  },
  labelForgetColor:{
    color:'#eeeeee',
    flex:1,
    textAlign:'right',
    position:'absolute',
    right:5,
    bottom:12,
    fontSize:12,
    lineHeight:16,
  },
  submitbtn:{
    borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
  }

});
  
export default OLT_TO_FDCExecutionForm;
