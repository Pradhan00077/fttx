import React, { Component } from "react";
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  TouchableOpacity,
  Dimensions,
  TextInput,
  AsyncStorage,
  FlatList
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Container } from "native-base";
import { Dropdown } from "react-native-material-dropdown";
import DatePicker from "react-native-datepicker";
import * as Network from "expo-network";
import Textarea from "react-native-textarea";
import KeyboardSpacer from "react-native-keyboard-spacer";
import {BASE_URL} from '../../basepath'
import Database from "../../Database";
import { MaterialIcons } from '@expo/vector-icons';
const db = new Database();

const screenWidth = Math.round(Dimensions.get("window").width);
const screenHeight = Math.round(Dimensions.get("window").height);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3; // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

let firstlat = 0;
let firstlon = 0;
let lastlat = 0;
let lastlon = 0;

class EngineerForm extends Component {
  constructor(props) {
    super(props);
    this.config = {
      DistrictAPI: BASE_URL + "apis/mnt/userDistrictFilter",
      TalukaAPI: BASE_URL + "apis/mnt/userTalukaFilter",
      SpanAPI: BASE_URL + "apis/mnt/userSpanFilter",
      SpanLength: BASE_URL + "apis/mnt/userGpLengthSpanFilter",
      GetRingNo: BASE_URL + "apis/mnt/userRingNoFilter",
      ActivitiesAPI: BASE_URL + "apis/mnt/userActivitiesFilter",
      IssuesAPI: BASE_URL + "apis/mnt/userIssuesFilter",

      CheckWipTransAPI: BASE_URL + "apis/mnt/userWipTransFilter",
      submitFormAPI: BASE_URL + "apis/mnt/workDoneFormSubmit/0"
    };
    this.state = {
      isLoading: false,
      allDistrict: [],
      allTaluka: [],
      allSpan: [],
      ringNoVal: [],
      activities: [],
      internalIssues: [],
      customerIssues: [],
      userName: 0,
      userRole: 0,
      userLoggedInId: 0,
      milestoneStartStop: false,
      IssuesReportChange: true,
      IssuesReportValue: "No",
      rignTypeVal: "Taluka Ring",
      soilStrataVal: "Normal",
      spanLength: "",
      ringNoValue: "",
      span_ID: "",
      activity: "T&D",
      IssuesChecked: "Internal",
      milestoneChecked: "WIP",
      mbChecked: "Yes",
      chainageFrom: "0",
      chainageTo: "1",
      depthTD: "",
      isCheckedInternalIssues: [],
      isCheckedCustomerIssues: [],
      displaySNT_no: false,
      activityDisabled: false,
      activityTD: true,
      activityBlowing: false,
      activityHHMH: false,
      activityInfraEquipment: false,
      activityMachineResource: false,
      fiberReadingFrom: "",
      fiberReadingTo: "",
      fiberReadingIn: "",
      fiberReadingOut: "",
      lattitude: "0.000",
      longitude: "0.000",
      errorMsg: "",
      splitedGPName: [],
      slctDateVal: new Date(),
      seconds: 0,
      submitBtnDisabled: true,
      countedMint: 0,
      milestoneToTD: "1",
      milestoneFromTD: "0",
      milestoneToHHMH: "1",
      milestoneFromHHMH: "0",
      milestoneToBlowing: "0",
      milestoneToBlowingMTS: "0",
      distance: 0,
      offRoad: "Left",
      ibd: "0.000",
      ductCoupler: "0.000",
      noDuct: "1",
      offset: "0.000",
      mtsDepth: "0.000",
      erm: "0.000",
      ermLat:"0.0000",
      ermLong:"0.0000",
      chamberTypeChainage: "0.000",
      chamberTypeLat: "0.0000",
      chamberTypeLong: "0.0000",
      chamberType: "Loop",
      referenceName1: "",
      distanceMtrs1: "",
      distanceMtrsLat1: "",
      distanceMtrsLong1: "",
      referenceName2: "",
      distanceMtrs2: "",
      distanceMtrsLat2: "",
      distanceMtrsLong2: "",
      referenceName3: "",
      distanceMtrs3: "",
      distanceMtrsLat3: "",
      distanceMtrsLong3: "",
      cableLength: "0",
      milestoneSpansLen: [],

      lat_start : "00.000000",
      long_start : "00.000000",

      lat_end : "00.000000",
      long_end : "00.000000",

      lat_start : "00.000000",
      long_start : "00.000000",

      lat_offset : "00.000000",
      long_offset : "00.000000",

      lat_chember : "00.000000",
      long_chember : "00.000000",

      lat_3_1 : "00.000000",
      long_3_1 : "00.000000",

      lat_3_2 : "00.000000",
      long_3_2 : "00.000000",

      lat_3_3 : "00.000000",
      long_3_3 : "00.000000",

      lat_start_ref : "00.000000",
      long_start_ref : "00.000000",

      lat_start_ref_to : "00.000000",
      long_start_ref_to : "00.000000",

      selectedData: {
        district: "",
        taluka: "",
        span: "",
        locFrom: "Taluka",
        locTo: "Taluka",
        activity: "T&D",
        issuesReportTD: "No",
        ringType: "Taluka Ring",
        chainageFrom: "0",
        chainageTo: "1",
        soilStrata: "Normal",
        typeTD: "",
        depthTD: "",
        milestoneStatus: "WIP",
        protection: "",
        mbTD: "Yes",
        issuesType: "Internal",
        issues_TD_Hndl: "No",
        offRoad: "left",
        span_ID: "",
        ibd: "0.000",
        ductCoupler: "0.000",
        noDuct: "1",
        offset: "0.000",
        mtsDepth: "0.000",
        erm: "0.000",
        ermLat:"0.0000",
        ermLong:"0.0000",
        chamberType: "Loop",
        chamberTypeChainage: "",
        chamberTypeLat: "",
        chamberTypeLong: "",
        referenceName1: "",
        distanceMtrs1: "",
        distanceMtrsLat1: "",
        distanceMtrsLong1: "",
        referenceName2: "",
        distanceMtrs2: "",
        distanceMtrsLat2: "",
        distanceMtrsLong2: "",
        referenceName3: "",
        distanceMtrs3: "",
        distanceMtrsLat3: "",
        distanceMtrsLong3: "",
        protection: "",
        blowingType: "",
        fiberReadingFrom: "",
        fiberReadingTo: "",
        fiberType: "",
        splicing: "",
        termination: "",
        chainagePointBlow: "",
        fiberReadingIn: "",
        fiberReadingOut: "",
        lattitude: "",
        longitude: "",
        milestoneFrom: "0",
        milestoneTo: "0",
        milestoneFromM: "0",
        milestoneToM: "0",
        gPName: "",
        gp_Name1: "",
        gp_Name2: "",
        GPFiberReady: "No",
        GPInfraReadiness: "No",
        GPCommissioning: "No",
        mRPlannedJCB: "0",
        mRActualJCB: "0",
        mRIdleJCB: "0",
        mRPlannedHDD: "0",
        mRActualHDD: "0",
        mRIdleHDD: "0",
        mRPlannedPoklane: "0",
        mRActualPoklane: "0",
        mRIdlePoklane: "0",
        mRPlannedRockBreaker: "0",
        mRActualRockBreaker: "0",
        mRIdleRockBreaker: "0",
        mRPlannedExcavator: "0",
        mRActualExcavator: "0",
        mRIdleExcavator: "0",
        mRPlannedBackhoeLoader: "0",
        mRActualBackhoeLoader: "0",
        mRIdleBackhoeLoader: "0",
        mRPlannedBlowing: "0",
        mRActualBlowing: "0",
        mRIdleBlowing: "0",
        mRPlannedSlicer: "0",
        mRActualSlicer: "0",
        mRIdleSlicer: "0",
        mRPlannedManpower: "0",
        mRActualManpower: "0",
        mRIdleManpower: "0",
        slctDate: todayDate, 
        chainage_from_m_start: "0.000",
        chainage_from_m_end: "0.000",
        chainage_to_m_start: "0.000",
        chainage_to_m_end: "0.000",
        fiberReadingFrom: "",
        fiberReadingTo: "",

        lat_start : "00.000000",
        long_start : "00.000000",

        lat_end : "00.000000",
        long_end : "00.000000",

        lat_start : "00.000000",
        long_start : "00.000000",

        lat_offset : "00.000000",
        long_offset : "00.000000",

        lat_chember : "00.000000",
        long_chember : "00.000000",

        lat_3_1 : "00.000000",
        long_3_1 : "00.000000",

        lat_3_2 : "00.000000",
        long_3_2 : "00.000000",

        lat_3_3 : "00.000000",
        long_3_3 : "00.000000",
      }
    };
    this.milestoneProcessHandler = this.milestoneProcessHandler.bind(this);
  }

  componentDidMount() {
    this.fetchUserDataState();
    this.fetchActivities();
    this.fetchInternalIssues();
    this.fetchCustomerIssues();
    this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
  }

  // USING METHOD TO OPEN DRAWER
  openControlPanel = () => {
    this.props.navigation.openDrawer();
  }

  async getCurrentLatLong(type){
      
    navigator.geolocation.getCurrentPosition(
      position => {
        const lattitude = JSON.stringify(position.coords.latitude);
        const longitude = JSON.stringify(position.coords.longitude);

        if(type==='c_start'){
          this.state.selectedData["lat_start"] = lattitude;
          this.state.selectedData["long_start"] = longitude;
          this.setState({ lat_start: lattitude, long_start: longitude });
        }else if( type ==="c_end"){
          this.state.selectedData["lat_end"] = lattitude;
          this.state.selectedData["long_end"] = longitude;
          this.setState({ lat_end: lattitude, long_end: longitude });
        }else if( type ==="offset"){
          this.state.selectedData["lat_offset"] = lattitude;
          this.state.selectedData["long_offset"] = longitude;
          this.setState({ lat_offset: lattitude, long_offset: longitude });
        }else if( type ==="chember"){
          this.state.selectedData["lat_chember"] = lattitude;
          this.state.selectedData["long_chember"] = longitude;
          this.setState({ lat_chember: lattitude, long_chember: longitude });
        }else if( type ==="points_1"){
          this.state.selectedData["lat_3_1"] = lattitude;
          this.state.selectedData["long_3_1"] = longitude;
          this.setState({ lat_3_1: lattitude, long_3_1: longitude });
        }else if( type ==="points_2"){
          this.state.selectedData["lat_3_2"] = lattitude;
          this.state.selectedData["long_3_2"] = longitude;
          this.setState({ lat_3_2: lattitude, long_3_2: longitude });
        }else if( type ==="points_3"){
          this.state.selectedData["lat_3_3"] = lattitude;
          this.state.selectedData["long_3_3"] = longitude;
          this.setState({ lat_3_3: lattitude, long_3_3: longitude });
        }else if( type ==="c_reference"){
          this.state.selectedData["lat_start_ref"] = lattitude;
          this.state.selectedData["long_start_ref"] = longitude;
          this.setState({ lat_start_ref: lattitude, long_start_ref: longitude });
        }else if( type ==="c_reference_to"){
          this.state.selectedData["lat_start_ref_to"] = lattitude;
          this.state.selectedData["long_start_ref_to"] = longitude;
          this.setState({ lat_start_ref_to: lattitude, long_start_ref_to: longitude });
        }
        
      },
      error => console.log(" error - ", error.message),
      { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
    );

  }

  fetchUserDataState = async () => {
    const userid = await AsyncStorage.getItem("user_id");
    const usern = await AsyncStorage.getItem("username");
    const userr = await AsyncStorage.getItem("user_role");
    const ipAddress = await Network.getIpAddressAsync();
    // const macAddress = await Network.getMacAddressAsync();

    this.state.selectedData["ipAddress"] = ipAddress;
    // this.state.selectedData["macAddress"] = macAddress;

    this.state.selectedData["user_id"] = userid;
    this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
    this.fetchDistrictAPI();
  };

  // CALLING API TO GET ALL ACTIVITIES
  async fetchActivities() {
    let responseJson = [];
    let response = await fetch(this.config.ActivitiesAPI);
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ activities: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL Internal ISSUES
  async fetchInternalIssues() {
    let responseJson = [];
    let response = await fetch(this.config.IssuesAPI + "/1");
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ internalIssues: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL Customer ISSUES
  async fetchCustomerIssues() {
    let responseJson = [];
    let response = await fetch(this.config.IssuesAPI + "/2");
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ customerIssues: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL DISTRICT
  async fetchDistrictAPI() {
    let responseJson = [];
    let response = await fetch(
      this.config.DistrictAPI +
        "/" +
        this.state.userRole +
        "/" +
        this.state.userName
    );
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ allDistrict: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  }

  // CALLING API TO GET ALL TALUKAS
  fetchTalukaAPI = async value => {
    let responseJson = [];
    let response = await fetch(
      this.config.TalukaAPI +
        "/" +
        this.state.userRole +
        "/" +
        this.state.userName +
        "/" +
        value
    );
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ allTaluka: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // CALLING API TO GET ALL SPAN
  fetchSpanAPI = async value => {
    let responseJson = [];
    let response = await fetch(
      this.config.SpanAPI +
        "/" +
        this.state.userRole +
        "/" +
        this.state.userName +
        "/" +
        value
    );
    try {
      responseJson = await response.json();
      //Successful response from the API Call
      this.setState({ allSpan: responseJson.data });
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // CALLING API TO GET ALL SPAN
  fetchSpanLengthAPI = async value => {
    let gpName = [];
    let responseJson = [];
    let gpNameVals = value.split("-");

    let response = await fetch(this.config.SpanLength + "/" + value);
    try {
      //Successful response from the API Call
      responseJson = await response.json();
      let spanId = responseJson.data[0].spanId;
      let spLength = responseJson.data[0].length;
      let spLength1 = Math.ceil(responseJson.data[0].length);
      let milestoneSpan = parseInt(spLength1, 10) + 5;
      let milestoneSpanVal = [];
      for (var i = 0; i <= milestoneSpan; i++) {
        milestoneSpanVal.push({ value: i });
      }

      gpName.push(
        gpNameVals.map((item, i) => {
          return {
            value: gpNameVals[i]
          };
        })
      );

      this.setState({
        spanLength: spLength,
        span_ID: spanId,
        milestoneSpansLen: milestoneSpanVal,
        splitedGPName: gpName[0]
      });
    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // CALLING API TO GET RING NO
  fetchRingNoAPI = async value => {
    let responseJson = [];
    let response = await fetch(
      this.config.GetRingNo +
        "/" +
        this.state.userRole +
        "/" +
        this.state.userName +
        "/" +
        value +
        "/2"
    );
    try {
      //Successful response from the API Call
      responseJson = await response.json();
      this.setState({ ringNoVal: responseJson.data, ringNoValue: responseJson.data[0].value });
      this.state.selectedData['RingNo'] = responseJson.data[0].value;

    } catch (error) {
      console.log("Response error - ", error);
    }
  };

  // USING FOLLOWING COMPONENT TO CHECK OR UNCHECK FOR SINGLE ISSUES
  async internalIssuesCheckbox(itemValue) {
    let checkedIssues = [];
    let checkedIssuesList = [];
    checkedIssues = this.state.internalIssues;

    checkedIssues.map((item, i) => {
      if (itemValue === item.value) {
        item.isSelect = !item.isSelect;
      }
      if (item.hasOwnProperty("isSelect")) {
        if (item.isSelect === true) {
          checkedIssuesList[i] = item.value;
        }
      }
    });

    checkedIssuesList = checkedIssuesList.filter(function(element) {
      return element !== undefined;
    });

    this.state.selectedData["issues"] = checkedIssuesList;
    this.setState({
      isCheckedInternalIssues: checkedIssuesList,
      internalIssues: checkedIssues
    });
  }

  // CUSTOMER ISSUES CHECKBOXES
  async customerIssuesCheckbox(itemValue) {
    let checkedIssues = [];
    let checkedIssuesList = [];
    checkedIssues = this.state.customerIssues;

    checkedIssues.map((item, i) => {
      if (itemValue === item.value) {
        item.isSelect = !item.isSelect;
      }
      if (item.hasOwnProperty("isSelect")) {
        if (item.isSelect === true) {
          checkedIssuesList[i] = item.value;
        }
      }
    });

    checkedIssuesList = checkedIssuesList.filter(function(element) {
      return element !== undefined;
    });

    this.state.selectedData["issues"] = checkedIssuesList;
    this.setState({ isCheckedCustomerIssues: checkedIssuesList });
  }

  // GETING DATA FROM FIELDS AND USING VALIDATION
  getData(data, fieldName) {
    if (fieldName === "District") {
      this.state.selectedData.district = data;
      this.fetchTalukaAPI(data);
    } else if (fieldName === "Taluka") {
      this.state.selectedData.taluka = data;
      this.fetchSpanAPI(data);
    } else if (fieldName === "Span") {
      this.state.selectedData.span = data;
      this.fetchSpanLengthAPI(data);
      this.fetchRingNoAPI(data);
    } else if (fieldName === "activity") {
      if (data === "T&D") {
        this.setState({
          activityTD: true,
          activityBlowing: false,
          activityInfraEquipment: false,
          activityMachineResource: false
        });
      } else if (data === "Blowing") {
        this.setState({
          activityBlowing: true,
          activityTD: false,
          activityInfraEquipment: false,
          activityMachineResource: false
        });
      } else if (data === "GP Work") {
        this.setState({
          activityInfraEquipment: true,
          activityTD: false,
          activityBlowing: false,
          activityMachineResource: false
        });
      } else {
        this.setState({
          activityMachineResource: true,
          activityTD: false,
          activityBlowing: false,
          activityInfraEquipment: false
        });
      }

      this.state.selectedData.activity = data;
    } else if (fieldName === "IssuesReport") {
      if (data === "Yes") {
        this.state.selectedData["issues_TD_Hndl"] = "Yes";
        this.setState({
          IssuesReportChange: false,
          IssuesReportValue: "Yes",
          activityDisabled: true
        });
      } else {
        this.state.selectedData["issues_TD_Hndl"] = "No";
        this.setState({
          IssuesReportChange: true,
          IssuesReportValue: "No",
          activityDisabled: false
        });
      }
      this.state.selectedData.issuesReportTD = data;
    } else if (fieldName === "ChainageStart") {
      this.setState({ chainageFrom: data });
      this.state.selectedData.chainageFrom = data;
    } else if (fieldName === "ChainageEnd") {
      this.setState({ chainageTo: data });
      this.state.selectedData.chainageTo = data;
    } else if (fieldName === "Depth") {
      this.setState({ depthTD: data });
      this.state.selectedData.depthTD = data;
    } else if (fieldName === "chainagePointBlow") {
      this.setState({ chainagePointBlow: data });
      this.state.selectedData.chainagePointBlow = data;
    } else if (fieldName === "RingType") {
      this.setState({ rignTypeVal: data });
      this.state.selectedData.ringType = data;
    } else if (fieldName === "SoilStrata") {
      this.setState({ soilStrataVal: data });
      this.state.selectedData.soilStrata = data;
    } else if (fieldName === "slctDate") {
      this.setState({ slctDateVal: data });
      this.state.selectedData.slctDate = data;
    } else if (fieldName === "milestoneFromTD") {
      this.setState({
        milestoneToTD: (data + 1).toString(),
        milestoneFromTD: data
      });
      this.state.selectedData.milestoneFrom = data;
    } else if (fieldName === "milestoneToBlowing") {
      this.setState({ milestoneToBlowing: data });
      this.state.selectedData.milestoneTo = data;
    } else if (fieldName === "milestoneToBlowingMTS") {
      this.setState({ milestoneToBlowingMTS: data.toString() });
      this.state.selectedData.milestoneToM = data;
    } else if (fieldName === "chainage_from_m_start" || fieldName === "chainage_from_m_end" ) {
      (data === '')? data = "0" : data = data;
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });      
      this.setState({chainage_from_m_calculate: (parseInt(this.state.selectedData.chainage_from_m_end)-parseInt(this.state.selectedData.chainage_from_m_start)).toString() });

    } else if (fieldName === "chainage_to_m_start" || fieldName === "chainage_to_m_end" ) {
      (data === '')? data = "0" : data = data;
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });
      this.setState({chainage_to_m_calculate: (parseInt(this.state.selectedData.chainage_to_m_end)-parseInt(this.state.selectedData.chainage_to_m_start)).toString() });

    } else if (fieldName === "fiberReadingTo" || fieldName === "fiberReadingFrom" ) {
      (data === '')? data = "0" : data = data;
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });
      this.setState({cableLength: (parseInt(this.state.selectedData.fiberReadingTo)-parseInt(this.state.selectedData.fiberReadingFrom)).toString() });

    } else if (fieldName === "mtsDepth" ) {
      (data === '')? data = "0" : data = data;
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });

      if(data >= 0.5 && data <= 1.199 ){
        this.setState({displaySNT_no: true });
      }else{
        this.setState({displaySNT_no: false });
      }

    } else {
      this.state.selectedData[fieldName] = data;
      this.setState({ [fieldName]: data });
    }
  }

  milestoneProcessStart = async value => {
    this.setState({ distance: 0 });
    db.transaction(tx => {
      tx.executeSql(`delete from workdoneActivity;`);
    });
    this.setState({
      milestoneStartStop: value,
      countedMint: 0,
      submitBtnDisabled: false
    });
  };

  async milestoneProcessStop(value = null) {
    this.setState({ milestoneStartStop: value, submitBtnDisabled: true });
  }

  milestoneProcessHandler = async () => {
    this.setState({ milestoneStartStop: false, submitBtnDisabled: true });
  };

  startProgressInterval = async () => {
    this.setState({ countedMint: this.state.countedMint + 0.05 });
  };

  startTrackingIntervals = async (lattitude, longitude) => {
    if (firstlat === 0 && firstlon === 0) {
      firstlat = lattitude;
      firstlon = longitude;
    } else {
      firstlat = lastlat;
      firstlon = lastlon;
    }

    lastlat = lattitude;
    lastlon = longitude;

    this.getDistance(firstlat, firstlon, lastlat, lastlon);
  };

  // CALCULATE DISTANCE FROM STARTING
  getDistance = (lat, lon, lat2, long2) => {
    var R = 6371;
    var dLat = ((lat2 - lat) * Math.PI) / 180;
    var dLon = ((long2 - lon) * Math.PI) / 180;
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos((lat * Math.PI) / 180) *
        Math.cos((lat2 * Math.PI) / 180) *
        Math.sin(dLon / 2) *
        Math.sin(dLon / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    if (parseFloat(d * 1000, 2) >= 4) {
      let distance = this.state.distance + d * 1000;
      let dist = distance.toPrecision(3);
      this.setState({ distance: dist });
    }
  };

  // USING FOLLOWING COMPONENT TO VALIDATE FORM
  fnSubmitValidateForm = async () => {
    
    this.setState({ errorMsg: "", successMsg: "", isLoading: true });

    let self = this;
    let responseJson = [];
    const { selectedData } = this.state;

    let chainageFrom = parseInt(selectedData.chainageFrom);
    let chainageTo = parseInt(selectedData.chainageTo);

    if (selectedData.district === "" || selectedData.taluka === "") {
      this.setState({
        errorMsg: "Please select District and Taluka.",
        successMsg: "", isLoading: false
      });
    } else if (selectedData.span === "") {
      this.setState({ errorMsg: "Please select Span.", successMsg: "", isLoading: false });
    } else if (
      selectedData.activity === "GP Work" &&
      selectedData.gp_Name2 === ""
    ) {
      this.setState({ errorMsg: "Please select GP Name.", successMsg: "", isLoading: false });
    } else if (
      selectedData.activity === "T&D" &&
      selectedData.gp_Name1 === ""
    ) {
      this.setState({ errorMsg: "Please select GP Name.", successMsg: "", isLoading: false });
    } else if ( selectedData.chainage_from_m_end < selectedData.chainage_from_m_start ) {
      this.setState({ errorMsg: "Chainage from M end should be greater then Chainage from M start.", successMsg: "", isLoading: false });
    } else if ( selectedData.chainage_to_m_end < selectedData.chainage_to_m_start ) {
      this.setState({ errorMsg: "Chainage to M end should be greater then Chainage to M start.", successMsg: "", isLoading: false });
    } else if (
      (chainageTo < chainageFrom ||
        chainageTo === chainageFrom) && (selectedData.activity !== 'GP Work' && selectedData.activity !== 'Machine Resource')
    ) {
      this.setState({
        errorMsg:
          "Chainage End value must be higher than Chaninage From value.",
        successMsg: "", isLoading: false
      });
    } else if (chainageTo - chainageFrom > 1000) {
      this.setState({
        errorMsg: "You are only allowed to enter up to 1000 meters Chainage.",
        successMsg: "", isLoading: false
      });
    } else if ((chainageTo - chainageFrom) > 50) {
      this.setState({
        errorMsg: "Chainage difference should be under 50 meter.",
        successMsg: "", isLoading: false
      });
    } else if (
      selectedData.typeTD === "" &&
      selectedData.activity === "T&D" &&
      this.state.activityDisabled === false
    ) {
      this.setState({ errorMsg: "Please select Type.", successMsg: "", isLoading: false });
    } else if (
      (selectedData.fiberReadingFrom === "" ||
        selectedData.fiberReadingTo === "") &&
      selectedData.activity === "Blowing"
    ) {
      this.setState({
        errorMsg: "Please select Fiber Reading From and To.",
        successMsg: "", isLoading: false
      });
    } else if (
      selectedData.fiberType === "" &&
      selectedData.activity === "Blowing"
    ) {
      this.setState({ errorMsg: "Please select Fiber Type.", successMsg: "", isLoading: false });
    } else {
      let apiCall = this.config.CheckWipTransAPI + "/0";

      // LET'S CALL API
      let response = await fetch(apiCall, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        // GOT SUCCESSFULLY RESPONSE FROM API
        responseJson = await response.json();
        
        console.log("Response - ", responseJson);

        if (responseJson.data[0].value === "") {
          this.setState({ errorMsg: "", successMsg: "", isLoading: false });
          // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
          this.submitFormData();
        } else {
          let respVal1 = responseJson.data[0].value;
          respVal = respVal1.split("-");

          // RESPONSE BASED ERROR MESSAGES
          if (respVal[0] === "range") {
            this.setState({
              errorMsg:
                "You have already entered Chainage Point " +
                respVal[1] +
                " to " +
                respVal[2] +
                ", please select different range of Chainage Start & End Point.",
              successMsg: "", isLoading: false
            });
          } else {
            this.setState({
              errorMsg:
                "You have already entered Chainage Point " +
                respVal1 +
                ", You have not permission more than 1000 with these entries. \n\nPlease enter with another entry!",
              successMsg: "", isLoading: false
            });
          }
        }
      } catch (error) {
        console.log("Response error - ", error);
        this.setState({ errorMsg: "", successMsg: "", isLoading: false });
      }
    }
  };

  // FOLLOWING COMPONENT USING TO SUBMIT FORM
  submitFormData = async () => {
    let self = this;
    let responseJson = [];
    const { selectedData } = this.state;

    let response = await fetch(this.config.submitFormAPI, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        data: selectedData
      })
    });
    try {
      responseJson = await response.json();

      console.log("resp - ", responseJson);

      if(responseJson.data[0].hasOwnProperty('error')){
        this.setState({ errorMsg: responseJson.data[0].error, successMsg: "", isLoading: false });
      }else{
        alert(responseJson.data[0].success);
        this.setState({ successMsg: responseJson.data[0].success, errorMsg: "", isLoading: false });
      }
    } catch (error) {
      console.log("Response error - ", error);
      this.setState({ errorMsg: "", successMsg: "", isLoading: false });
    }
  };

  render() {
    let fromTo = [
      {
        value: "taluka",
        label: "Taluka"
      },
      {
        value: "gp",
        label: "GP"
      }
    ];
    let offRoad = [
      {
        value: "left",
        label: "Left"
      },
      {
        value: "right",
        label: "Right"
      }
    ];
    let soilStrata = [{
        value: "Normal"
      },{
        value: "Hard"
      }
    ];
    let chamberType = [{
        value: "left",
        label: "Loop"
      },{
        value: "right",
        label: "Joint"
      }
    ];
    
    let crossingType = [{
        value: "bridge",
        label: "Bridge"
      },{
        value: "culvert",
        label: "Culvert"
      },{
        value: "road",
        label: "Road"
      },{
        value: "railway",
        label: "Railway"
      }
    ];
    let rowAuthority = [{
        value: "NH"
      },{
        value: "NHAI"
      },{
        value: "FOREST"
      },{
        value: "PWD"
      },{
        value: "MCD"
      },{
        value: "RAILWAY"
      },{
        value: "GP"
      },{
        value: "ZP"
      },{
        value: "MIDC"
      }
    ];
    
    let noDuct = [{
        value: "1",
      },{
        value: "2",
      },{
        value: "3",
      },{
        value: "4",
      },{
        value: "5",
      }
    ];

    let ringType = [{
        value: "Taluka Ring"
      },{
        value: "GP Ring"
      }
    ];

    let typeTD = [
      {
        value: "Open"
      },
      {
        value: "HDD"
      },
      {
        value: "Claimping"
      },
      {
        value: "Moiling"
      }
    ];

    let protection = [{
        value: "GI"
      },{
        value: "DWC"
      },{
        value: "RCC"
      },{
        value: "DWC+GI wrapping"
      },{
        value: "BOX PCC"
      }
    ];

    let chamber_type = [
      {
        value: "left",
        label: "Loop"
      },
      {
        value: "right",
        label: "Joint"
      }
    ];

    let fiberType = [
      {
        value: "24F"
      },
      {
        value: "48F"
      },
      {
        value: "96F"
      }
    ];

    let splicingTermination = [
      {
        value: "1"
      },
      {
        value: "2"
      },
      {
        value: "3"
      },
      {
        value: "4"
      },
      {
        value: "5"
      }
    ];

    let gPName = this.state.splitedGPName;

    let gpConfirm = [
      {
        value: "Yes"
      },
      {
        value: "No"
      }
    ];

    let machineResourceData = [
      {
        value: "1"
      },
      {
        value: "2"
      },
      {
        value: "3"
      },
      {
        value: "4"
      },
      {
        value: "5"
      }
    ];

    const { selectedData } = this.state;
    // console.log("spLength- ", this.state.spanLength)
    return (
      <Container style={styles.Container}>
        {/* START SECTION FOR PROFILE TOP WITH IMAGE AND NAME OF USER */}
        <ScrollView>
          <View
            style={{ paddingVertical: 20, paddingLeft: 15, paddingRight: 15 }}
          >
            <View style={[styles.mainEventsClass]}>
              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Date</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <DatePicker
                    style={{ width: 200 }}
                    // date={this.state.date}
                    mode="date"
                    date={this.state.slctDateVal}
                    format="YYYY-MM-DD"
                    minDate={fourDaysBack}
                    maxDate={new Date()}
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource={require("../../assets/images/date.png")}
                    customStyles={{
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 4,
                        marginLeft: 0
                      },
                      dateInput: {
                        marginLeft: 36,
                        backgroundColor: "#f0f0f0",
                        borderWidth: 0,
                        color: "#888888"
                      }
                    }}
                    onDateChange={value => this.getData(value, "slctDate")}
                  />
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>District</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allDistrict}
                      // dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "District")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Taluka</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allTaluka}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "Taluka")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Span</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.allSpan}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "Span")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <Text style={{ fontSize: 16 }}>Span ID</Text>
                </View>

                <View style={{ flex: 1 }}>
                  <TextInput
                    style={{
                      height: 40,
                      width: "100%",
                      textAlign: "center",
                      borderRadius: 2,
                      borderBottomWidth: 1,
                      borderBottomColor: "#878787"
                    }}
                    value={this.state.span_ID}
                    editable={false}
                  />
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 0.35, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>From</Text>
                  </View>

                  <View style={{ flex: 0.31, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>To</Text>
                  </View>

                  <View style={{ flex: 0.33, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Span Length</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "33%" }}>
                    <Dropdown
                      label=""
                      data={fromTo}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={selectedData.locFrom}
                      valueExtractor={({value})=> value}
                      onChangeText={value => this.getData(value, "locFrom")}
                      error=""
                    />
                  </View>
                  <View style={{ width: "33%", paddingHorizontal: 10 }}>
                    <Dropdown
                      label=""
                      data={fromTo}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={selectedData.locTo}
                      valueExtractor={({value})=> value}
                      onChangeText={value => this.getData(value, "locTo")}
                      error=""
                    />
                  </View>
                  <View style={{ width: "33%" }}>
                    <TextInput
                      style={{
                        height: 40,
                        width: 120,
                        textAlign: "center",
                        backgroundColor: "#f0f0f0",
                        color: "#878787",
                        borderRadius: 2
                      }}
                      value={this.state.spanLength}
                      editable={false}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Ring Type</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={ringType}
                      dropdownPosition={1}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={this.state.rignTypeVal}
                      onChangeText={value => this.getData(value, "RingType")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Ring No</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.ringNoVal}
                      dropdownOffset={{ top: 8, left: 0 }}
                      value={this.state.ringNoValue}
                      onChangeText={value => this.getData(value, "RingNo")}
                      error=""
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Vendor Name</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <TextInput
                      style={{
                        height: 40,
                        width: "100%",
                        backgroundColor: "#f0f0f0",
                        color: "#878787",
                        borderRadius: 2,
                        paddingLeft: 5
                      }}
                      onChangeText={value => this.getData(value, "vendorName")}
                    />
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>
                      Civil Work
                    </Text>
                  </View>
                </View>
              </View>

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Activity</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Dropdown
                      label=""
                      data={this.state.activities}
                      dropdownPosition={1}
                      value={this.state.activity}
                      dropdownOffset={{ top: 8, left: 0 }}
                      onChangeText={value => this.getData(value, "activity")}
                      disabled={this.state.activityDisabled}
                      error=""
                    />
                  </View>
                </View>
              </View>

              {this.state.activityTD ? (
                <View>
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Type</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={typeTD}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "typeTD")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage Start Point (MTS)</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainageFrom}
                          onChangeText={value =>
                            this.getData(value, "ChainageStart")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_start")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                          
                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_start}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_start}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage End Point (MTS)</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainageTo}
                          onChangeText={value =>
                            this.getData(value, "ChainageEnd")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_end")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_end}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_end}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Off Road</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={offRoad}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          value={this.state.offRoad}
                          onChangeText={value =>
                            this.getData(value, "offRoad")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Offset from centre of the road(Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.offset}
                          onChangeText={value =>
                            this.getData(value, "offset")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>No of Duct:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={noDuct}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          value={this.state.noDuct}
                          onChangeText={value =>
                            this.getData(value, "noDuct")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>IBD(Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.ibd}
                          onChangeText={value =>
                            this.getData(value, "ibd")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>GP Name:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={gPName}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value => this.getData(value, "gp_Name1")}
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Duct coupler:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.ductCoupler}
                          onChangeText={value =>
                            this.getData(value, "ductCoupler")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Depth (MTS):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.mtsDepth}
                          onChangeText={value =>
                            this.getData(value, "mtsDepth")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>
                  
                  {
                    (this.state.displaySNT_no)?(
                      <View style={styles.paddingBottomMore}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ flex: 1, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 16 }}>SNT NO:</Text>
                          </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ width: "100%" }}>
                            <TextInput
                              style={{
                                height: 40,
                                width: "100%",
                                textAlign: "center",
                                backgroundColor: "#f0f0f0",
                                color: "#878787",
                                borderRadius: 2
                              }}
                              value={this.state.snt_no}
                              onChangeText={value =>
                                this.getData(value, "snt_no")
                              }
                              keyboardType='phone-pad'
                            />
                          </View>
                        </View>
                      </View>
                    ):(
                      <View></View>
                    )
                  }
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>ERM Chainage(Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.erm}
                          onChangeText={value =>
                            this.getData(value, "erm")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("offset")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_offset}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_offset}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Protection</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={protection}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "protection")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Soil strata</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={soilStrata}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          value={this.state.soilStrataVal}
                          onChangeText={value =>
                            this.getData(value, "SoilStrata")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Crossing Type</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={crossingType}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "crossingType")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>ROW Authority</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={rowAuthority}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "rowAuthority")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Chamber Type</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={chamberType}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          value={this.state.chamberType}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "chamberType")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chamber Type Chainage(Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chamberTypeChainage}
                          onChangeText={value =>
                            this.getData(value, "chamberTypeChainage")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("chember")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_chember}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_chember}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 18, fontWeight: "600" }}>3 Points Refrence:</Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 14, fontWeight: "600" }}>Ist Point:</Text>
                      </View>
                    </View>
                  </View>
                  
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Reference name:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                       <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.referenceName1}
                          onChangeText={value =>
                            this.getData(value, "referenceName1")
                          }
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Distance (Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.distanceMtrs1}
                          onChangeText={value =>
                            this.getData(value, "distanceMtrs1")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("points_1")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_3_1}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_3_1}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 14, fontWeight: "600" }}>2nd Point:</Text>
                      </View>
                    </View>
                  </View>
                  
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Reference name:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                       <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.referenceName2}
                          onChangeText={value =>
                            this.getData(value, "referenceName2")
                          }
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Distance (Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.distanceMtrs2}
                          onChangeText={value =>
                            this.getData(value, "distanceMtrs2")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("points_2")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_3_2}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_3_2}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 14, fontWeight: "600" }}>3rd Point:</Text>
                      </View>
                    </View>
                  </View>
                  
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Reference name:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                       <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.referenceName3}
                          onChangeText={value =>
                            this.getData(value, "referenceName3")
                          }
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Distance (Mtrs):</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.distanceMtrs3}
                          onChangeText={value =>
                            this.getData(value, "distanceMtrs3")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("points_3")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_3_3}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_3_3}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                </View>
              ) : (
                <View></View>
              )}

              {this.state.activityBlowing ? (
                <View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage Start (MTS)</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainageFrom}
                          onChangeText={value =>
                            this.getData(value, "ChainageStart")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_start")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                          
                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_start}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_start}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage End (MTS)</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainageTo}
                          onChangeText={value =>
                            this.getData(value, "ChainageEnd")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_end")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>

                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_end}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_end}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View
                        style={{ flex: 0.5, paddingBottom: 8, marginRight: 15 }}
                      >
                        <Text style={{ fontSize: 16 }}>Fiber Reading From</Text>
                      </View>
                      <View style={{ flex: 0.5, paddingBottom: 8 }}>
                        <Text style={{ fontSize: 16 }}>Fiber Reading To</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, marginRight: 15 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.fiberReadingFrom}
                          onChangeText={value =>
                            this.getData(value, "fiberReadingFrom")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>

                      <View style={{ flex: 0.5 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.fiberReadingTo}
                          onChangeText={value =>
                            this.getData(value, "fiberReadingTo")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Total Cable Length: </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.cableLength}
                          editable = {false}
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Cable Drum NO:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.drum_no}
                          onChangeText={value =>
                            this.getData(value, "drum_no")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Fiber Type</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={fiberType}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "fiberType")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Splicing</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={splicingTermination}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "splicing")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Termination</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={splicingTermination}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "termination")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>No of Duct:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={splicingTermination}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "duct")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row", paddingBottom: 5 }}>
                      <Text style={{ fontSize: 18, fontWeight: "bold" }}>Slack Details</Text>
                    </View>
                  </View>                  
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Chamber Type:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={chamber_type}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "chamber_type")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage(Mtrs): </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chaninage_reference}
                          onChangeText={value =>
                            this.getData(value, "chaninage_reference")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_reference")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                          
                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_start_ref}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_start_ref}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                  
                  
                  <View style={styles.paddingBottomMore}>
                    
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>From: </Text>
                      </View>
                    </View>
                    
                    <View style={styles.paddingBottomMore}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ height: 40, textAlignVertical: "center" }} >
                          M Mark at Start
                        </Text>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value= {this.state.chainage_from_m_start}
                          onChangeText={value =>
                            this.getData(value, "chainage_from_m_start")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>

                    </View>

                    <View style={styles.paddingBottomMore}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{
                            height: 40,
                            textAlignVertical: "center",
                            textAlign: "right"
                          }}
                        >
                          M Mark at END
                        </Text>
                      </View>
                      <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-end" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value= {this.state.chainage_from_m_end}
                          onChangeText={value =>
                            this.getData(value, "chainage_from_m_end")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Cable Length:(Auto calculate (End-start)) </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainage_from_m_calculate}
                          editable = {false}
                          onChangeText={value =>
                            this.getData(value, "chainage_from_m_calculate")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                  </View>

                  {/* ----------------- NEW COPIED TO CHAMBER ------------------ */}
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row", borderBottomColor: "gray", borderBottomWidth: 1 }}></View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Chamber Type:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={chamber_type}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "chamber_type_to")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Chainage(Mtrs): </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chaninage_reference_to}
                          onChangeText={value =>
                            this.getData(value, "chaninage_reference_to")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style ={{ flex:1, flexDirection:'row', paddingBottom: 8 }}>
                        <TouchableOpacity style={[ styles.milestoneBtns, { backgroundColor: "#1e5aca", width: "100%", padding: 5 } ]} onPress={() => this.getCurrentLatLong("c_reference_to")}>
                            <Text style={[ styles.milestoneBtnsTxt, {color: "#fff", textAlign: "center"}]}>Get Current Lat-Long</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                          
                        <View style={{ flex: 0.25 }}>
                          <Text style={{ height: 40, textAlignVertical: "center" }} >
                            Lat
                          </Text>
                        </View>

                        <View style={{ flex: 0.75 }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.lat_start_ref_to}
                            editable = {false}
                          />
                        </View>

                      </View>

                      <View style={{ flex: 0.5, flexDirection: "row" }}>
                        <View style={{ flex: 0.25 }}>
                          <Text style={{
                              height: 40,
                              textAlignVertical: "center",
                              textAlign: "right"
                            }}
                          >
                            Long
                          </Text>
                        </View>
                        <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                          <TextInput
                            style={{
                              height: 40,
                              width: 120,
                              textAlign: "center",
                              backgroundColor: "#f0f0f0",
                              color: "#878787",
                              borderRadius: 2
                            }}
                            value= {this.state.long_start_ref_to}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>
                  </View>
                  
                  
                  <View style={styles.paddingBottomMore}>
                    
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>To: </Text>
                      </View>
                    </View>
                    
                    
                    <View style={styles.paddingBottomMore}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{ height: 40, textAlignVertical: "center" }} >
                          M Mark at Start
                        </Text>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value= {this.state.chainage_to_m_start}
                          onChangeText={value =>
                            this.getData(value, "chainage_to_m_start")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>

                    </View>
                    
                    <View style={styles.paddingBottomMore}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Text style={{
                            height: 40,
                            textAlignVertical: "center",
                            textAlign: "right"
                          }}
                        >
                          M Mark at END
                        </Text>
                      </View>

                      <View style={{ flex: 1, flexDirection: "row", alignItems: "flex-end" }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value= {this.state.chainage_to_m_end}
                          onChangeText={value =>
                            this.getData(value, "chainage_to_m_end")
                          }
                          keyboardType='phone-pad'
                        />
                      </View>
                    </View>
                    
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <Text style={{ fontSize: 16 }}>Cable Length: (Auto calculate (End-start)) </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 10 }}>
                        <TextInput
                          style={{
                            height: 40,
                            width: "100%",
                            textAlign: "center",
                            backgroundColor: "#f0f0f0",
                            color: "#878787",
                            borderRadius: 2
                          }}
                          value={this.state.chainage_to_m_calculate}
                          editable = {false}
                        />
                      </View>
                    </View>
                  </View>

                  {/* ----------------- NEW COPIED TO CHAMBER ------------------ */}
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row", borderBottomColor: "gray", borderBottomWidth: 1 }}></View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>EndPlug NOs:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={noDuct}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "endplug")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Simplex:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={noDuct}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "simplex")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Joint Closure Type:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={fiberType}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "joint_closure_type")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                  
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>FDMS Type:</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={fiberType}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          valueExtractor={({value})=> value}
                          onChangeText={value =>
                            this.getData(value, "FDMSType")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                </View>
              ) : (
                <View></View>
              )}

              {this.state.activityInfraEquipment ? (
                <View>
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>GP Name</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={gPName}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value => this.getData(value, "gp_Name2")}
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>GP Fiber Ready?</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={gpConfirm}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "GPFiberReady")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>
                          GP Infra Readiness?
                        </Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={gpConfirm}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "GPInfraReadiness")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>GP Commissioning?</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={gpConfirm}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "GPCommissioning")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <View></View>
              )}

              {this.state.activityMachineResource ? (
                <View>
                  {/* PLANNED SECTION IN MACHINE RESOURCE */}
                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16, fontWeight: "600" }}>
                          Planned
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>JCB</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedJCB")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>HDD</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedHDD")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Poklane</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedPoklane")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedRockBreaker")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Excavator</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedExcavator")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedBackhoeLoader")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Blowing</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedBlowing")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Slicer</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedSlicer")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Manpower</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRPlannedManpower")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  {/* ACTUAL SECTION IN MACHINE RESOURCE */}

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16, fontWeight: "600" }}>
                          Actual
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>JCB</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualJCB")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>HDD</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualHDD")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Poklane</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualPoklane")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualRockBreaker")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Excavator</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualExcavator")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualBackhoeLoader")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Blowing</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualBlowing")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Slicer</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualSlicer")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Manpower</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRActualManpower")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  {/* IDLE SECTION IN MACHINE RESOURCE */}

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16, fontWeight: "600" }}>
                          Idle
                        </Text>
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>JCB</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleJCB")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>HDD</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleHDD")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Poklane</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdlePoklane")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Rock Breaker</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleRockBreaker")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Excavator</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleExcavator")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Backhoe Loader</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleBackhoeLoader")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Blowing</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleBlowing")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Slicer</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleSlicer")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>

                  <View style={styles.paddingBottomMore}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, paddingBottom: 5 }}>
                        <Text style={{ fontSize: 16 }}>Manpower</Text>
                      </View>
                    </View>

                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "100%" }}>
                        <Dropdown
                          label=""
                          data={machineResourceData}
                          dropdownPosition={1}
                          dropdownOffset={{ top: 8, left: 0 }}
                          onChangeText={value =>
                            this.getData(value, "mRIdleManpower")
                          }
                          error=""
                        />
                      </View>
                    </View>
                  </View>
                </View>
              ) : (
                <View></View>
              )}

              <View style={styles.paddingBottomMore}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ flex: 1, paddingBottom: 5 }}>
                    <Text style={{ fontSize: 16 }}>Remarks</Text>
                  </View>
                </View>

                <View style={{ flex: 1, flexDirection: "row" }}>
                  <View style={{ width: "100%" }}>
                    <Textarea
                      containerStyle={{
                        height: 160,
                        padding: 10,
                        backgroundColor: "#f0f0f0"
                      }}
                      style={{
                        textAlignVertical: "top",
                        height: 150,
                        fontSize: 14,
                        color: "#8e8e8e"
                      }}
                      onChangeText={value => this.getData(value, "Remarks")}
                      // defaultValue={this.state.text}
                      maxLength={200}
                      placeholder={"Comment About Work"}
                      placeholderTextColor={"#8e8e8e"}
                      underlineColorAndroid={"transparent"}
                    />
                  </View>
                </View>
              </View>
            </View>

            <KeyboardSpacer topSpacing={5} />

            {/* DISPLAY ERROR MESSAGE */}
            <View style={{ paddingVertical: 5 }}>
              {this.state.errorMsg != "" ? (
                <Text style={{ color: "red", fontSize: 14 }}>
                  {this.state.errorMsg}
                </Text>
              ) : (
                <Text></Text>
              )}

              {this.state.successMsg != "" ? (
                <Text style={{ color: "green", fontSize: 14 }}>
                  {this.state.successMsg}
                </Text>
              ) : (
                <Text></Text>
              )}
            </View>

            {this.state.submitBtnDisabled ? (
              <View>
                {
                  (this.state.isLoading)?
                  (
                    <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                      <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                    </TouchableOpacity>
                  ):(
                    <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress={this.fnSubmitValidateForm}>
                      <Text style={styles.loadMorebtnTxt}>Submit</Text>
                    </TouchableOpacity>
                  )
                }
              </View>
            ) : (
              <TouchableOpacity
                activeOpacity={1}
                style={[styles.loadMorebtn, { backgroundColor: "gray" }]}
                underlayColor="white"
              >
                <Text style={styles.loadMorebtnTxt}>Submit</Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
        {/* END SECTION */}
      </Container>
    );
  }

  static navigationOptions = ({navigation}) => {
    return {
    title: "Engineer Form",
    headerTintColor: "#FFFFFF",
    headerTitleStyle: {
      alignSelf: "center",
      textAlign: "center",
      flex: 0.8,
      fontSize: 22,
      lineHeight: 25
    },
    headerStyle: {
      height: 84,
      borderBottomWidth: 0
    },
    headerLeft:(
      <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
        <MaterialIcons
          name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
      </TouchableOpacity>
    ),
    headerBackground: (
      <View>
        <LinearGradient
          colors={["#2157c4", "#3b5998", "#1ab679"]}
          start={{ x: 0, y: 0 }}
          end={{ x: 1, y: 0 }}
          style={{
            paddingTop: screenHeight - (screenHeight - screenHeight / 25),
            elevation: 10
          }}
        >
          <View style={{ width: "60%", marginLeft: "20%" }}>
            <Image
              style={{
                width: "100%",
                height: "100%",
                alignSelf: "center",
                zIndex: 1,
                opacity: 0.2
              }}
              source={require("../../assets/images/logo.png")}
              resizeMode="contain"
            />
          </View>
        </LinearGradient>
      </View>
    )
  };
}
}

const styles = StyleSheet.create({
  Container: {
    backgroundColor: "#FFFFFF"
  },
  mainEventsClass: {
    marginVertical: 4
    //height: (screenHeight-(screenHeight-(screenHeight/12)))
  },
  paddingBottomMore: {
    paddingBottom: 15
  },

  loadMorebtn: {
    borderRadius: 50,
    flex: 1,
    backgroundColor: "#25AE88",
    paddingVertical: 15,
    marginTop: 15
  },
  loadMorebtnTxt: {
    fontWeight: "bold",
    color: "#fff",
    textAlign: "center",
    fontSize: 16
  },
  milestoneBtns: {
    borderRadius: 20,
    paddingVertical: 8
  },
  milestoneView: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#1e5aca",
    borderRadius: 20,
    padding: 2
  },
  milestoneBtnsTxt: {
    fontWeight: "bold",
    fontSize: 18,
    textAlign: "center"
  }
});

export default EngineerForm;
