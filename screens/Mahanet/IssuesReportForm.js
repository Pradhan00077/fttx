import React, { Component } from "react";
import { StyleSheet, ScrollView, View, Text, Image, TouchableOpacity, Dimensions, TextInput, AsyncStorage, FlatList, Platform} from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { Container, CheckBox } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import DatePicker from 'react-native-datepicker';
import * as Network from 'expo-network';
import Textarea from 'react-native-textarea';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {BASE_URL} from '../../basepath';
import Database from '../../Database';
import { MaterialIcons } from '@expo/vector-icons';
const db = new Database();

const screenWidth = Math.round(Dimensions.get('window').width);
const screenHeight = Math.round(Dimensions.get('window').height);
const date = new Date();
const yesterday = date - 1000 * 60 * 60 * 24 * 3;   // current date's milliseconds - 1,000 ms * 60 s * 60 mins * 24 hrs * (# of days beyond one to go back)
const fourDaysBack = new Date(yesterday);

let todayDate = new Date();
let dd = String(todayDate.getDate()).padStart(2, '0');
let mm = String(todayDate.getMonth() + 1).padStart(2, '0'); //January is 0!
let yyyy = todayDate.getFullYear();
todayDate = yyyy + '-' + mm + '-' + dd;

class IssuesForm extends Component {

    constructor(props) {
      super(props);
      this.config = {
        DistrictAPI: BASE_URL + "apis/mnt/userDistrictFilter",
        TalukaAPI: BASE_URL + "apis/mnt/userTalukaFilter",
        SpanAPI: BASE_URL + "apis/mnt/userSpanFilter",
        SpanLength: BASE_URL + "apis/mnt/userGpLengthSpanFilter",
        GetRingNo: BASE_URL + "apis/mnt/userRingNoFilter",
        IssuesAPI: BASE_URL + "apis/mnt/userIssuesFilter",

        CheckTransAPI: BASE_URL + "apis/mnt/checkTransactionForWIP",
        submitFormAPI: BASE_URL + "apis/mnt/WIPformSubmit",
      }
      this.state = {
        isLoading: false,
        allDistrict: [],
        allTaluka: [],
        allSpan: [],
        ringNoVal: [],
        ringNoValue: "",
        internalIssues: [],
        userName: 0,
        userRole: 0,
        userLoggedInId: 0,
        rignTypeVal: "Taluka Ring",
        spanLength: "",
        activity: "T&D",
        chainageFrom: "0",
        chainageTo: "1",
        errorMsg: "",
        slctDateVal: new Date(),
        seconds: 0,
        submitBtnDisabled: true,
        countedMint: 0,
        milestoneToTD: "1",
        milestoneFromTD: "0",
        milestoneSpansLen: [],

        selectedData: {
          district: "",
          taluka: "",
          span: "",
          locFrom: "Taluka",
          locTo: "Taluka",
          activity: "T&D",
          ringType: "Taluka Ring",
          chainageFrom: "0",
          chainageTo: "1",
          milestoneFrom: "0",
          milestoneTo: "0",
          issues: [],
          slctDate: todayDate
        }
      }
    }


    componentDidMount(){
      this.fetchUserDataState();
      this.fetchIssues();
      this.props.navigation.setParams({ openControlPanel: this.openControlPanel });
    }
  
    // USING METHOD TO OPEN DRAWER
    openControlPanel = () => {
      this.props.navigation.openDrawer();
    }

    fetchUserDataState = async() => {
      const userid = await AsyncStorage.getItem('user_id');
      const usern = await AsyncStorage.getItem('username');
      const userr = await AsyncStorage.getItem('user_role');
      const ipAddress = await Network.getIpAddressAsync();
      // const macAddress = await Network.getMacAddressAsync();

      this.state.selectedData["ipAddress"] = ipAddress;
      // this.state.selectedData["macAddress"] = macAddress;

      this.state.selectedData["user_id"] = userid;
      this.setState({ userName: usern, userRole: userr, userLoggedInId: userid });
      this.fetchDistrictAPI();
    }


    // CALLING API TO GET ALL Internal ISSUES
    async fetchIssues(){
      let responseJson = [];
      let response = await fetch(this.config.IssuesAPI);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ internalIssues: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL DISTRICT
    async fetchDistrictAPI(){
      let responseJson = [];
      let response = await fetch(this.config.DistrictAPI+"/"+this.state.userRole+"/"+this.state.userName);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allDistrict: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL TALUKAS
    fetchTalukaAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.TalukaAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allTaluka: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN
    fetchSpanAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.SpanAPI+"/"+this.state.userRole+"/"+this.state.userName+"/"+value);
      try {
        responseJson = await response.json();
        //Successful response from the API Call
          this.setState({ allSpan: responseJson.data });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET ALL SPAN
    fetchSpanLengthAPI = async(value) => {
      let responseJson = [];

      let response = await fetch(this.config.SpanLength+"/"+value);
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        let spLength = responseJson.data[0].length;
        let milestoneSpan = parseInt(spLength, 10)+5;
        let milestoneSpanVal = [];
        for (var i=0 ; i <= milestoneSpan; i++) {
          milestoneSpanVal.push({ 'value': i });
        }

        this.setState({ spanLength: spLength, milestoneSpansLen: milestoneSpanVal });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }

    // CALLING API TO GET RING NO
    fetchRingNoAPI = async(value) => {
      let responseJson = [];
      let response = await fetch(this.config.GetRingNo+"/"+this.state.userRole+"/"+this.state.userName+"/"+value+"/2");
      try {
        //Successful response from the API Call
        responseJson = await response.json();
        this.state.selectedData['RingNo'] = responseJson.data[0].value;
        this.setState({ ringNoVal: responseJson.data, ringNoValue: responseJson.data[0].value });
      }catch (error) {
        console.log("Response error - ",error);
      }
    }


    // GETING DATA FROM FIELDS AND USING VALIDATION
    getData(data, fieldName){

      if(fieldName === "District"){
        this.state.selectedData.district = data;
        this.fetchTalukaAPI(data);
      }else if(fieldName === "Taluka"){
        this.state.selectedData.taluka = data;
        this.fetchSpanAPI(data);
      }else if(fieldName === "Span"){
        this.state.selectedData.span = data;
        this.fetchSpanLengthAPI(data);
        this.fetchRingNoAPI(data);
      }else if(fieldName === "ChainageStart"){
        this.setState({ chainageFrom: data })
        this.state.selectedData.chainageFrom = data;
      }else if(fieldName === "ChainageEnd"){
        this.setState({ chainageTo: data })
        this.state.selectedData.chainageTo = data;
      }else if(fieldName === "slctDate"){
        this.setState({ slctDateVal: data })
        this.state.selectedData.slctDate = data;
      }else if(fieldName === "milestoneFromTD"){
        this.setState({ milestoneToTD: (data + 1).toString(), milestoneFromTD: data })
        this.state.selectedData.milestoneFrom = data;
      }else{
        this.state.selectedData[fieldName] = data;
      }

    }


  // USING FOLLOWING COMPONENT TO VALIDATE FORM
    fnSubmitValidateForm = async() => {
      let self = this;
      let responseJson = [];
      const { selectedData } = this.state;

      this.setState({ isLoading: true });
      let chainageFrom = parseInt(selectedData.chainageFrom);
      let chainageTo = parseInt(selectedData.chainageTo);

      if(selectedData.district === '' || selectedData.taluka === ''){
        this.setState({ errorMsg: "Please select District and Taluka.", successMsg: "", isLoading: false });
      } else if(selectedData.span === ''){
        this.setState({ errorMsg: "Please select Span.", successMsg: "", isLoading: false });
      } else if(chainageTo < chainageFrom || chainageTo === chainageFrom){
        this.setState({ errorMsg: "Chainage End value must be higher than Chaninage From value.", successMsg: "", isLoading: false });
      } else if (( chainageTo - chainageFrom ) > 1000) {
        this.setState({ errorMsg: "You are only allowed to enter up to 1000 meters Chainage.", successMsg: "", isLoading: false });
      } else if ( selectedData.issues === '' || selectedData.issues.length === 0 ) {
        this.setState({ errorMsg: "Please select at least one Issue.", successMsg: "", isLoading: false });
      } else {
        let apiCall = this.config.CheckTransAPI+"/0";

       // LET'S CALL API
        let response = await fetch(apiCall, {
          method: 'POST',
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            data: selectedData
          })
        });
        try {
          // GOT SUCCESSFULLY RESPONSE FROM API
          responseJson = await response.json();

          console.log("Resp - ", responseJson)

          if(responseJson.data[0].value === ''){
            this.setState({ errorMsg: "" });
            // CALLING FOLLOWING COMPONENT TO SUBMIT FORM
            this.submitFormData();
          }else{
            let respVal1 = responseJson.data[0].value;
            respVal = respVal1.split('-');

            // RESPONSE BASED ERROR MESSAGES
            if(respVal[0] === 'range'){
              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal[1]+' to '+respVal[2]+', please select different range of Chainage Start & End Point.', successMsg: "", isLoading: false });
            }else{
              this.setState({ errorMsg: 'You have already entered Chainage Point '+respVal1+', You have not permission more than 1000 with these entries. \n\nPlease enter with another entry!', successMsg: "", isLoading: false });
            }
          }
        } catch (error) {
          alert(error);
          this.setState({ errorMsg:"", successMsg: "" });
        }
      }
    }

    // CUSTOMER ISSUES CHECKBOXES
    async issuesCheckbox(itemValue){
      let checkedIssues=[];
      let checkedIssuesList=[];
      checkedIssues = this.state.internalIssues;

      checkedIssues.map((item, i)=>{
        if(itemValue === item.value){
          item.isSelect = !item.isSelect;
        }
        if(item.hasOwnProperty('isSelect')){
          if(item.isSelect === true){
            checkedIssuesList[i] = item.value;
          }
        }
      })

      checkedIssuesList = checkedIssuesList.filter(function( element ) {
        return element !== undefined;
      });

      this.state.selectedData["issues"] = checkedIssuesList;
      this.setState({ internalIssues: checkedIssues })
    }


    // FOLLOWING COMPONENT USING TO SUBMIT FORM
    submitFormData = async() => {

      let responseJson = [];
      const { selectedData } = this.state;

      let response = await fetch(this.config.submitFormAPI, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          data: selectedData
        })
      });
      try {
        responseJson = await response.json();

        console.log("Response  - ", responseJson)

        if(responseJson.data[0].hasOwnProperty('error')){
          this.setState({ errorMsg: responseJson.data[0].error, successMsg: "", isLoading: false });
        }else{
          alert(responseJson.data[0].success);
          this.setState({ successMsg: responseJson.data[0].success, errorMsg: "", isLoading: false });
        }

      } catch (error) {
        alert(error);
        console.log(error);
        this.setState({ errorMsg: "", successMsg: "" });
      }

    }


    render() {

      let fromTo = [{
        "value": 'Taluka',
      }, {
        "value": 'GP',
      }];

      let ringType = [{
        "value": 'Taluka Ring',
      }, {
        "value": 'GP Ring',
      }];

      const { selectedData } = this.state;
      return (
          <Container style={styles.Container}>

            {/* START SECTION */}
              <ScrollView>

                <View style = {{ paddingVertical: 20, paddingLeft:15, paddingRight:15 }} >

                  <View style ={[ styles.mainEventsClass ]} >

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Date</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <DatePicker
                          style={{width: 200 }}
                          // date={this.state.date}
                          mode="date"
                          date= {this.state.slctDateVal}
                          format="YYYY-MM-DD"
                          minDate={fourDaysBack}
                          maxDate={new Date()}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          iconSource = {require("../../assets/images/date.png")}
                          customStyles={{
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 4,
                              marginLeft: 0,
                            },
                            dateInput: {
                              marginLeft: 36,
                              backgroundColor: "#f0f0f0",
                              borderWidth: 0,
                              color: "#888888",
                            }
                          }}
                          onDateChange={(value) => this.getData(value, "slctDate")}
                        />
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>District</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allDistrict}
                            // dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "District")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Taluka</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allTaluka}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "Taluka")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Span</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.allSpan}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            onChangeText={(value) => this.getData(value, "Span")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 0.35, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>From</Text>
                        </View>

                        <View style ={{ flex: 0.31, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>To</Text>
                        </View>

                        <View style ={{ flex: 0.33, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 16 }}>Span Length</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "33%" }}>
                          <Dropdown
                            label= ""
                            data={fromTo}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={selectedData.locFrom}
                            onChangeText={(value) => this.getData(value, "locFrom")}
                            error = ""
                          />
                        </View>
                        <View style ={{ width: "33%", paddingHorizontal: 10 }}>
                          <Dropdown
                            label= ""
                            data={fromTo}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={selectedData.locTo}
                            onChangeText={(value) => this.getData(value, "locTo")}
                            error = ""
                          />
                        </View>
                        <View style ={{ width: "33%" }}>
                          <TextInput
                            style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.spanLength}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Ring Type</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={ringType}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.rignTypeVal}
                            onChangeText={(value) => this.getData(value, "RingType")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Ring No</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            data={this.state.ringNoVal}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.ringNoValue}
                            onChangeText={(value) => this.getData(value, "RingNo")}
                            error = ""
                          />
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}>
                          <Text style={{ fontSize: 20, fontWeight: "bold" }}>Civil Work</Text>
                        </View>
                      </View>
                    </View>


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Activity</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= ""
                            dropdownPosition={1}
                            value="T&D"
                            dropdownOffset = {{ top: 8, left: 0 }}
                            disabled= {true}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', paddingBottom:10, }}>
                        <View style ={{ flex: 1, paddingBottom: 8 }}>
                          <Text style={{ fontSize: 16 }}>Milestone (KM)</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                        <View style ={{ width: "100%" }}>
                          <Dropdown
                            label= "Milestone From"
                            data={this.state.milestoneSpansLen}
                            dropdownPosition={1}
                            dropdownOffset = {{ top: 8, left: 0 }}
                            value={this.state.milestoneFromTD}
                            onChangeText={(value) => this.getData(value, "milestoneFromTD")}
                            error = ""
                          />
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', paddingBottom:10 }}>
                        <View style ={{ width: "100%" }}>
                          <Text style={{ color: "#999", fontSize: 10 }}>Milestone To</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', paddingBottom:6 }}>
                        <View style ={{ width: "100%" }}>
                          <TextInput
                            style={{ height: 40, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                            value={this.state.milestoneToTD}
                            editable = {false}
                          />
                        </View>
                      </View>
                    </View>

                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 10 }}>
                          <Text style={{ fontSize: 16 }}>Chainage Point (MTS)</Text>
                        </View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row' }}>
                        <View style={{ flex: 0.5, flexDirection:'row' }}>
                          <View style={{ flex: 0.25 }}>
                            <Text style={{ height: 40, textAlignVertical: "center" }} >Start</Text>
                          </View>
                          <View style={{ flex: 0.75 }}>
                            <TextInput
                              style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                              value={this.state.chainageFrom}
                              onChangeText={(value) => this.getData(value, "ChainageStart")}
                              keyboardType='phone-pad'
                            />
                          </View>
                        </View>

                        <View style={{ flex: 0.5, flexDirection:'row' }}>
                          <View style={{ flex: 0.25 }}>
                            <Text style={{ height: 40, textAlignVertical: "center", textAlign: "right" }} >End</Text>
                          </View>
                          <View style={{ flex: 0.75, alignItems: "flex-end" }}>
                            <TextInput
                              style={{ height: 40, width:120, textAlign: "center", backgroundColor: "#f0f0f0", color: "#878787", borderRadius: 2 }}
                              value={this.state.chainageTo}
                              onChangeText={(value) => this.getData(value, "ChainageEnd")}
                              keyboardType='phone-pad'
                            />
                          </View>
                        </View>
                      </View>
                    </View>

                    {/*  */}

                      <View style ={ styles.paddingBottomMore }>
                        <View style ={{ flex:1, flexDirection:'row', }}>
                          <View style ={{ flex: 1, paddingBottom: 5 }}>
                            <Text style={{ fontSize: 20, fontWeight: "bold" }}>Issues</Text>
                          </View>
                        </View>
                      </View>

                      <View style ={ styles.paddingBottomMore }>

                        <View style ={{ flex:1, flexDirection:'row', }}>
                          <View style ={{ flex: 1, paddingBottom: 10 }}>
                            <Text style={{ fontSize: 16 }}>Type of Issue</Text>
                          </View>
                        </View>

                        <View style ={{ flex:1, flexDirection:'row' }}>
                          <FlatList
                            data={ this.state.internalIssues }
                            extraData={this.state}
                            renderItem={({item}) =>
                              <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ paddingRight: 16, paddingTop: 5 }}>
                                  <CheckBox
                                    color= {(item.isSelect)? "#1ab679" : "#000" }
                                    checked= {(item.isSelect)? true : false }
                                    onPress={() => this.issuesCheckbox(item.value)}
                                  />
                                </View>
                                <View style={{ paddingLeft: 5 }}>
                                  <TouchableOpacity activeOpacity={1}>
                                    <Text style={{ textAlignVertical: "center", paddingTop: 7 }} >
                                      {item.value}
                                    </Text>
                                  </TouchableOpacity>
                                </View>

                              </View>

                            }
                            keyExtractor={item => item.value}
                            numColumns={2}
                          />
                        </View>
                      </View>

                    {/*  */}


                    <View style ={ styles.paddingBottomMore }>
                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ flex: 1, paddingBottom: 5 }}><Text style={{ fontSize: 16 }}>Remarks</Text></View>
                      </View>

                      <View style ={{ flex:1, flexDirection:'row', }}>
                        <View style ={{ width: "100%" }}>
                          <Textarea
                            containerStyle={{ height: 160, padding: 10, backgroundColor: '#f0f0f0' }}
                            style={{textAlignVertical: 'top', height: 150, fontSize: 14, color: '#8e8e8e',}}
                            onChangeText={(value) => this.getData(value, "Remarks")}
                            // defaultValue={this.state.text}
                            maxLength={200}
                            placeholder={'Comment About Work'}
                            placeholderTextColor={'#8e8e8e'}
                            underlineColorAndroid={'transparent'}
                          />
                        </View>
                      </View>
                    </View>


                  </View>

                  <KeyboardSpacer topSpacing ={5} />

                  {/* DISPLAY ERROR MESSAGE */}
                  <View style={{ paddingVertical: 5 }}>
                    {
                      (this.state.errorMsg != "")?(
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", }}>{this.state.errorMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                    {
                      (this.state.successMsg != "")?(
                        <Text style={{ color: "green", fontSize: 14, textAlign: "center" }}>{this.state.successMsg}</Text>
                      ):(
                        <Text></Text>
                      )
                    }

                  </View>

                  {
                    (this.state.submitBtnDisabled)?(
                      <View>
                        {
                          (this.state.isLoading)?
                          (
                            <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white">
                              <Text style={styles.loadMorebtnTxt}>Submiting...</Text>
                            </TouchableOpacity>
                          ):(
                            <TouchableOpacity activeOpacity={1} style={styles.loadMorebtn} underlayColor="white" onPress = {this.fnSubmitValidateForm}>
                              <Text style={styles.loadMorebtnTxt}>Submit</Text>
                            </TouchableOpacity>
                          )
                        }
                      </View>
                    ):(
                      <View>
                        <Text style={{ color: "red", fontSize: 14, textAlign: "center", paddingHorizontal: 10  }}>Stop Location Tracking to save your report.</Text>
                        <TouchableOpacity activeOpacity={1} style={[styles.loadMorebtn, { backgroundColor: "gray" }]} underlayColor="white">
                          <Text style={styles.loadMorebtnTxt}>Submit</Text>
                      </TouchableOpacity>
                      </View>
                    )
                  }

                </View>
              </ScrollView>
            {/* END SECTION */}

          </Container>
        )
    }


    static navigationOptions = ({navigation}) => {
      return {
        title: 'Issues Form',
        headerTintColor: '#FFFFFF',
        headerTitleStyle: {
          alignSelf: 'center',
          textAlign: "center",
          flex: 0.8,
          fontSize: 22,
          lineHeight: 25,
        },
        headerStyle: {
          height:84,
          borderBottomWidth: 0,
        },
        headerLeft:(
          <TouchableOpacity activeOpacity={1} underlayColor="white" onPress={navigation.getParam('openControlPanel')} >
            <MaterialIcons
              name="menu" color="#fff" size={26} style={{ marginLeft: 10  }} />
          </TouchableOpacity>
        ),
        headerBackground: (
          <View>
            <LinearGradient
              colors={['#2157c4', '#3b5998', '#1ab679']}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              style={{ paddingTop: (screenHeight-(screenHeight-(screenHeight/25))), elevation: 10 }}
            >
              <View style={{ width: '60%', marginLeft: "20%" }}>
                <Image  style={{ width: '100%', height: '100%', alignSelf: "center", zIndex:1, opacity:0.2 }} source = {require('../../assets/images/logo.png')} resizeMode= "contain" />
              </View>
            </LinearGradient>
          </View>
        ),
    }
  }
}

const styles = StyleSheet.create({
    Container: {
      backgroundColor: "#FFFFFF"
    },
    mainEventsClass:{
      marginVertical:4,
      //height: (screenHeight-(screenHeight-(screenHeight/12)))
    },
    paddingBottomMore: {
      paddingBottom: 15
    },

    loadMorebtn: {
      borderRadius: 50,
      flex: 1,
      backgroundColor: "#25AE88",
      paddingVertical: 15,
      marginTop: 15
    },
    loadMorebtnTxt: {
      fontWeight: "bold",
      color: "#fff",
      textAlign: "center",
      fontSize: 16
    },
    milestoneBtns: {
      borderRadius: 20,
      paddingVertical: 8
    },
    milestoneView: {
      flex:1,
      flexDirection:'row',
      backgroundColor: "#1e5aca",
      borderRadius: 20,
      padding: 2
    },
    milestoneBtnsTxt: {
      fontWeight: "bold",
      fontSize: 18,
      textAlign: "center"
    }

  });

export default IssuesForm;
